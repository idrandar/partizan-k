#ifndef SYSTEMCLASS_HH_
#define SYSTEMCLASS_HH_

/* The author: Alexander Ketner                                         */
/* Date of creation: 25 April 2007                                      */
/* Include prototype functions on the basic set of the systems software */
/* IDE Momentics & QNX Neutrino 6.3.0                                   */
// Modified for "Sarmat" project
// (C) I.Drandar, 2010, 2018 all rights reserved
// (C) I.Drandar, for Linux, 2018, all rights reserved

#include <sys/sysinfo.h>
#include <sys/utsname.h>
#include <sys/types.h>
#include <string>
#include <ctime>
#include <sys/resource.h>

// Contains a system info of operating system QNX Neutrino
class TSystemClass {
 struct sysinfo info;
 void get_host_info();
 utsname hostinfo;
 int latency_target_fd;
public:
 TSystemClass();
 ~TSystemClass();
 const char *GetOsName() const;
 const char *GetOsVersion() const;
 const char *GetOsBuild() const;
 const char *GetPlatform() const;
 const char *GetNodeName() const;
 off_t GetFreeMemory() const;
 size_t GetTotalMemorySize() const;
 size_t GetSharedMemorySize() const;
 std::time_t OSBootTime() const;
 const char* OSBootTimeStr() const;
 const char* ProgramStartTime() const;
 int GetCpuCount() const;
 std::string GetCPUType() const;
 std::string GetCPUFreq() const;
 rlimit GetMaximumPriority() const;
 void SetMaximumPriority(int);
 void SetDMALatency(int);
}; // end TSystemClass

#endif /*SYSTEM_HH_*/
