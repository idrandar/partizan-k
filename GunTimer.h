#ifndef GUNTIMER_H
#define GUNTIMER_H

// Processing data of "GUN" Timer channel
// (C) I.Drandar, 2019, all rights reserved

#include "Gun.h"

struct GunTimer { // types

 using InputPacket = _Gun::InputPacket;

 struct OutputPacket {
  char Mode : 2;
  bool Defence : 1;
  bool SpeedNegative : 1;
  bool AutoTimer : 1;
  bool ModeNext : 1;
  bool IsElevChannel : 1;
  Bits7<Upscale<int, 4096, 45>, 14> Time;
  Packed::SignedAngle Offset;
  Bits7<int, 7> AngleSpeed;
 };

 using InputData = _Gun::InputData;

 struct OutputData {
  _Gun::Workmode Mode;
  double Time;
 };

 struct CommandVector {};
 struct StatusVector {};

};

class ModuleGunTimer : public ExchangeModuleImpl<GunTimer> {
 void DecodePacket(bool) override;
 void EncodePacket(bool) override;
public:
 explicit ModuleGunTimer(const ModuleInit&);
 void Act();
};

#endif // GUNTIMER_H
