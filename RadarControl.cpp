// radar hardware control data
// (C) I.Drandar, 2019, all rights reserved
// Guidance and tracking controller - Radar

#include "RadarControl.h"
#include "DebugOut.h"

bool operator==(const RadarControl::OutputData& l, const RadarControl::InputData& r) {
 return
  l.TAGC == r.TAGC && l.RangeCalibrationInput == r.RangeCalibrationInput &&
  l.PIC == r.PIC && l.CFAR == r.CFAR &&
  l.ManualIF == r.ManualIF && l.ManualUHF == r.ManualUHF &&
  l.DetectionThreshold == r.DetectionThreshold &&
  l.ANI_Threshold == r.ANI_Threshold &&
  l.UHF_ControlThreshold == r.UHF_ControlThreshold &&
  l.IF_ControlThreshold == r.IF_ControlThreshold &&
  l.SignalCode == r.SignalCode &&
  l.CalibrationRangeCorrection == r.CalibrationRangeCorrection &&
  l.PassiveInterferenceCoherenceCoefficient == r.PassiveInterferenceCoherenceCoefficient &&
  l.MinimumPassiveInterferenceThreshold == r.MinimumPassiveInterferenceThreshold;
}

std::ostream& operator<<(std::ostream& _, const RadarControl::OutputData& d) {
 _ <<
  "temporary auto gain control: " << d.TAGC << "\t"
  "range calibration input: " << d.RangeCalibrationInput << "\n"
  "passive interference compensating: " << enum2str(d.PIC) << "\t"
  "CFAR: " << d.CFAR << "\n"
  "manual IF control: " << d.ManualIF << "\tmanual UHF control: " << d.ManualUHF << "\n"
  "detection threshold code: " << d.DetectionThreshold << " dB\t"
  "active noise interference threshold: " << d.ANI_Threshold << " dB\n"
  "UHF control threshold: " << d.UHF_ControlThreshold << " dB\t"
  "IF control threshold: " << d.IF_ControlThreshold << " dB\n"
  "signal code #" << d.SignalCode << "\t"
  "calibration range correction: " << d.CalibrationRangeCorrection << " m\n"
  "passive interference coherence coefficient: " << d.PassiveInterferenceCoherenceCoefficient << "\t"
  "minimum passive interference threshold: " << d.MinimumPassiveInterferenceThreshold << " dB";
 return _;
}

ModuleRadarControl::FakeData::FakeData(const ModuleInit& init, const QString& s) :
 use(init.GetConst(s, "use")) {
}

ModuleRadarControl::ModuleRadarControl(const ModuleInit& init) :
 BASE(init), Fake(init, "fake") {
}

void ModuleRadarControl::Act() {
 if (Command.Go) {
  Transmit();
 }
 CommitReception();
}

template <>
constexpr std::array enum_rep<RadarControl::PassiveInterferenceCompensating> = {
 "disabled", "automatic", "manual"
};

void ModuleRadarControl::EncodePacket(bool dump) {
 output.TAGC = Output.TAGC; // temporary auto gain control
 output.RangeCalibrationInput = Output.RangeCalibrationInput;
 output.PIC = static_cast<unsigned char>(Output.PIC);
 output.CFAR = Output.CFAR;
 output.ManualIF = Output.ManualIF;
 output.ManualUHF = Output.ManualUHF;
 output.DetectionThreshold = Output.DetectionThreshold; // dB
 output.ANI_Threshold = Output.ANI_Threshold; // dB
 output.UHF_ControlThreshold = Output.UHF_ControlThreshold; // dB
 output.IF_ControlThreshold = Output.IF_ControlThreshold; // dB
 output.SignalCode = Output.SignalCode;
 output.CalibrationRangeCorrection = Output.CalibrationRangeCorrection; // m
 output.PassiveInterferenceCoherenceCoefficient = Output.PassiveInterferenceCoherenceCoefficient; // γ
 output.MinimumPassiveInterferenceThreshold = Output.MinimumPassiveInterferenceThreshold; // β dB
 if (dump) {
  std::cout <<
   "Dump of transmitted " << GetName() << "::Output packet\n" <<
   Output << "\n";
 }
}

void ModuleRadarControl::DecodePacket(bool dump) {
 Input.TAGC = input.TAGC; // temporary auto gain control
 Input.RangeCalibrationInput = input.RangeCalibrationInput;
 Input.PIC = static_cast<Types::PassiveInterferenceCompensating>(input.PIC);
 Input.CFAR = input.CFAR;
 Input.ManualIF = input.ManualIF;
 Input.ManualUHF = input.ManualUHF;
 Input.DetectionThreshold = input.DetectionThreshold; // dB
 Input.ANI_Threshold = input.ANI_Threshold; // dB
 Input.UHF_ControlThreshold = input.UHF_ControlThreshold; // dB
 Input.IF_ControlThreshold = input.IF_ControlThreshold; // dB
 Input.SignalCode = input.SignalCode;
 Input.CalibrationRangeCorrection = input.CalibrationRangeCorrection; // m
 Input.PassiveInterferenceCoherenceCoefficient = input.PassiveInterferenceCoherenceCoefficient; // γ
 Input.MinimumPassiveInterferenceThreshold = input.MinimumPassiveInterferenceThreshold; // β dB
 if (dump) {
  std::cout <<
   "Dump of received " << GetName() << "::Input packet\n"
   "temporary auto gain control: " << Input.TAGC << "\t"
   "range calibration input: " << Input.RangeCalibrationInput << "\n"
   "passive interference compensating: " << enum2str(Input.PIC) << "\t"
   "CFAR: " << Input.CFAR << "\t"
   "manual IF control: " << Input.ManualIF << "\tmanual UHF control: " << Input.ManualUHF << "\n"
   "detection threshold code: " << Input.DetectionThreshold << " dB\t"
   "active noise interference threshold: " << Input.ANI_Threshold << " dB\n"
   "UHF control threshold: " << Input.UHF_ControlThreshold << " dB\t"
   "IF control threshold: " << Input.IF_ControlThreshold << " dB\n"
   "signal code #" << Input.SignalCode << "\t"
   "calibration range correction: " << Input.CalibrationRangeCorrection << " m\t"
   "passive interference coherence coefficient: " << Input.PassiveInterferenceCoherenceCoefficient << "\t"
   "minimum passive interference threshold" << Input.MinimumPassiveInterferenceThreshold << " dB\n";
 }
}

void ModuleRadarControl::PrepareStatusVector(bool) {
 if (Fake.use) {
  Status.NewInfo = true;
  Status.ExchangeFault = false;
  Status.data = Command.data;
 }
}
