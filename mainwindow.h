#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <memory>
#include <QTimer>
#include "Application.h"
#include "TrendsWindow.h"

namespace Ui {
class MainWindow;
}

class VI;

class Main : public QMainWindow {
    Q_OBJECT

public:
 explicit Main(QWidget *parent = 0);
 ~Main();

private slots:
 void Update();
 void on_btnTrends_clicked();
 void on_MainWindow_destroyed();

private:
 Ui::MainWindow* ui;
 QTimer tmr;
 std::unique_ptr<Application> app;
 std::unique_ptr<VI> itf;
 std::unique_ptr<TrendsWindow> trends;
};

#endif // MAINWINDOW_H

