/* The author: Vladislav Guyvoronskiy          */
/* Date of creation: 23 March 2007             */
/* Processing data from navigation complex     */
/* IDE Momentics & QNX Neutrino 6.3.0          */
// Modified for "Sarmat" project
// Modified for "Owl" project
// (C) I.Drandar, 2010-2018, all rights reserved
// Module name changed
// (C) I.Drandar, for Qt, 2018-2019, all rights reserved

#include "NS.h"
#include "DebugOut.h"
#include "common/CoordinatesConversion.h"

std::ostream& operator<<(std::ostream& _, const struct NS::InputData::Faults& F) {
 _ <<
  "header checksum: " << F.HeaderChecksum << "\tdata checksum: " << F.DataChecksum << "\t"
 "count: " << F.Count << "\tgeneral: " << F.General << "\toff: " << F.Off;
 return _;
}

ModuleNS::FakeData::FakeData(const ModuleInit& init, const QString& s, double TimeStep) :
 use(init.GetConst(s, "use")),
 Speed { init.GetConst(s, "speed-x"), init.GetConst(s, "speed-y"), init.GetConst(s, "speed-z") },
 Rot {
  init.GetConst(s, "rot-heading"), init.GetConst(s, "rot-rolling"), init.GetConst(s, "rot-pitch")
 },
 RotChange {
  Derivative{ Rot.Heading, TimeStep }, Derivative{ Rot.Rolling, TimeStep },
  Derivative{ Rot.Pitch, TimeStep }
 } {
}

ModuleNS::ModuleNS(const ModuleInit& init) :
 BASE(init), Fake(init, "fake", TimeStep()) {
}

void ModuleNS::DecodePacket(bool dump) {
 Input.Rot.Pitch = input.Pitch;
 Input.Rot.Rolling = input.Rolling;
 Input.Rot.Heading = input.Heading;
 Input.RotChange.Pitch = input.PitchChange;
 Input.RotChange.Rolling = input.RollingChange;
 Input.RotChange.Heading = input.HeadingChange;
 Input.SpeedPart.X = input.SpeedNorth;
 Input.SpeedPart.Y = input.SpeedEast;
 Input.SpeedPart.Z = input.SpeedDown;
 Input.Faults.HeaderChecksum = input.Faults.HeaderChecksum;
 Input.Faults.DataChecksum = input.Faults.DataChecksum;
 Input.Faults.Count = input.Faults.Count;
 Input.Faults.General = input.Faults.General;
 Input.Faults.Off = input.Faults.Off;
 Input.LTSTime = input.LTSTime;

 // testing stuff
 if (dump)
  std::cout <<
   "Dump of received " << GetName() << "::Input packet\n"
   "rotations (°):\t" << Input.Rot << "\n"
   "rotations change (°/s):\t" << Input.RotChange << "\n"
   "speed parts (m/s): " << Input.SpeedPart << "\n"
   "faults:\n" << Input.Faults << "\n"
   "LTS time: " << Input.LTSTime << "\n";
}

//=================== End of navMC.cc file====================================

void ModuleNS::Act() {
 CommitReception();
} // end Run

void ModuleNS::PrepareStatusVector(bool GotData) {
 if (Fake.use) {
  GotData = true;
  Status.NewInfo = true;
  Status.ExchangeFault = false;
  const double Time = counter.OverallTime();
  Status.data.SpeedPart.X = Fake.Speed.X(Time);
  Status.data.SpeedPart.Y = Fake.Speed.Y(Time);
  Status.data.SpeedPart.Z = Fake.Speed.Z(Time);
  Status.data.Rot.Heading = Fake.Rot.Heading(Time);
  Status.data.RotChange.Heading = Fake.RotChange.Heading(Time);
  Status.data.Rot.Rolling = Fake.Rot.Rolling(Time);
  Status.data.RotChange.Rolling = Fake.RotChange.Rolling(Time);
  Status.data.Rot.Pitch = Fake.Rot.Pitch(Time);
  Status.data.RotChange.Pitch = Fake.RotChange.Pitch(Time);
  Status.data.LTSTime = counter.TimeFromProgramStart();
 }
 if (GotData) {
  Status.Processed.NewInfo = true;
  Status.Processed.Time = Status.data.LTSTime;

  NormalizeRadians(Status.data.Rot.Heading);
  NormalizeRadiansSigned(Status.data.Rot.Pitch);
  NormalizeRadiansSigned(Status.data.Rot.Rolling);

  Status.Processed.SpeedPart = Status.data.SpeedPart;
//  Status.Processed.SpeedPart.Z = 0.0;

  Status.Processed.Rot = Status.data.Rot;
  Status.Processed.RotChange = Status.data.RotChange;

  const double TestHeading = atan2(Status.Processed.SpeedPart.Y, Status.Processed.SpeedPart.X);
  Status.AngleCheck = Status.Processed.Rot.Heading - TestHeading;
 } else {
  Status.Processed.NewInfo = false;
 }

} // end PrepareStatusVector
