#ifndef GUNELEV_H
#define GUNELEV_H

// Processing data of "GUN" elev channel
// (C) I.Drandar, 2015, 2017, all rights reserved
// (C) I.Drandar, for Qt, 2018-2020, all rights reserved

#include "Gun.h"

class ModuleGunElev : public ModuleGun {
 void ProcessCommandVector() override;
public:
 ModuleGunElev(const ModuleInit&);
 double GetDesiredAngle(bool, bool) const;
};

#endif // GUNELEV_H
