// Processing data of "GUN" (combat module) channel
// (C) I.Drandar, 2015, 2017, all rights reserved
// (C) I.Drandar, for Qt, 2018-2020, all rights reserved

#ifndef GUN_HH_
#define GUN_HH_

#include "data_types.h"
#include "common/ExchangeModule.h"
#include "collision/Extrapolation.h"
#include "Setup.h"

struct _Gun { // types
 enum Workmode : char {
  gmWork, gmTesting
 };

 struct InputData {
  struct Faults {
   bool ExchangeGun;
   bool ExchangeComp;
   bool PowerSupply;
   bool URef;
   bool Any() const { return ExchangeGun || ExchangeComp || PowerSupply || URef; }
  } Faults;
 };

 struct OutputData {
  Workmode Mode;
  double Position;
  bool IsElevChannel;
 };

 struct InputPacket {
  bool ExchangeCompFault : 1;
  bool ExchangeGunFault : 1;
  bool PowerSupplyFault : 1;
  bool URefFault : 1;
 };

 struct OutputPacket {
  char Mode : 2;
  bool Defence : 1;
  bool SpeedNegative : 1;
  bool AutoTimer : 1;
  bool ModeNext : 1;
  bool IsElevChannel : 1;
  Packed::SignedAngle Position;
  Packed::SignedAngle Offset;
  Bits7<int, 7> AngleSpeed;
 };

 struct StatusVector {
  double EvaluatedTime; // system lockal time of point angles
  bool HaveSolution;

  Spherical2DCoords Preliminary; // on without corrections
  Spherical2DCoords Final;       // on with correction
  bool UseFinal;
  struct Limits {
   bool RightHA;
   bool LeftHA;
   bool HighElev;
   bool LowElev;
  } Limits;
 };
 struct CommandVector {
  struct ExtrapolationPolynomials::Collision::InOut Extrapolation;
  bool HaveSolution;
  Spherical2DCoords PointingAngleMismatch;
  Spherical2DCoords Miss;
  Rotations Rot;
  int CollisionTaskPerformed;
  double EvaluatedTime;
  Spherical2DShipCoords Ship;
  double TimeOffset() const { return EvaluatedTime - Extrapolation.ShotTime; }
 };
};

std::ostream& operator<<(std::ostream&, const struct _Gun::InputData::Faults&);

class ModuleGun : public ExchangeModuleImpl<_Gun> {
 static constexpr double TimeOffsetMax = 0.1;
 static constexpr double _LIMIT = 10.0;
 const double TimeStep;
 const struct Algorithm {
  const bool UseDiff;
  const Spherical2DCoords DiffLimit;
  Algorithm(const ModuleInit&, const QString&);
 } Algorithm;

 Spherical2DShipCoords ShipPreliminary; // results of evaluation without corrections
 Spherical2DGroundCoords GroundPreliminary; // results of evaluation without corrections
 const struct FakeData {
  const bool use;
  FakeData(const ModuleInit&, const QString&);
 } Fake;

 void DecodePacket(bool) override;
 void EncodePacket(bool) override;
 void PrepareStatusVector(bool) override;
protected:
 const struct Constructive {
  const Spherical2DShipCoords Offset;
  const double HW_Offset;
  const double AngleSpeed;
  const double NeutralPart; // size of the neutral zone
  const Setup::Diagram Area;
  const bool AreaDisabled;
  // further items are evaluated from the area
  // and used inside of intermediate evaluations
  const double Leftmost;  // the left bound of the area
  const double Rightmost; // the right bound of the area
  const double Bisector;  // the center of the neutral zone
  const double Diff; // distance from the center of neutral zone to area bounds
  double NeutralLeft; // left bound of neutral area
  double NeutralRight; // right bound of neutral area
  Constructive(const ModuleInit&, const QString&);
  bool InArea(const Spherical2DShipCoords&) const;
  void EvaluateLimitsAndSetBounds(
   Spherical2DShipCoords&, Spherical2DShipCoords& Prev,
   struct Types::StatusVector::Limits&) const;
 } Constr;
 Spherical2DShipCoords ShipFinal; // results of evaluation with corrections
 Spherical2DGroundCoords GroundFinal; // results of evaluation with corrections
 Spherical2DShipCoords Prev; // used for restriction of movement
 const struct TestDump {
  const bool Position;
  TestDump(const ModuleInit&, const QString&);
 } TestDump;
 const struct Control {
  const bool UseShipCoords;
  const bool DesiredFinal;
  Control(const ModuleInit&, const QString&);
 } Control;

 void ClearStatus();
 void EvaluateUsingShipCoords();
 void EvaluateUsingGroundCoords();
 void ProcessCommandVector() override;
public:
 explicit ModuleGun(const ModuleInit&);
 void Act();
 double NextTime(double) const;
};

#endif /* GUN_HH_ */
