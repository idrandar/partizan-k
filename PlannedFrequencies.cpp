// plan of permmitted frequencies
// (C) I.Drandar, 2019, all rights reserved

// Guidance and tracking controller - Radar
#include "PlannedFrequencies.h"
#include "DebugOut.h"

bool operator==(const PlannedFrequencies::Item& l, const PlannedFrequencies::Item& r) {
 return l.Code == r.Code && l.Permitted == r.Permitted;
}

ModulePlannedFrequencies::ModulePlannedFrequencies(const ModuleInit& _) : BASE(_) {}

void ModulePlannedFrequencies::Act() {
 if (Command.Go) {
  Transmit();
 }
 CommitReception();
}

void ModulePlannedFrequencies::DecodePacket(bool dump) {
 Input.Count = input.Count;
 for (size_t i = 0; i < Input.Array.size(); ++i) {
  Input.Array[i].Permitted = input.Array[i].Permitted;
  Input.Array[i].Code = input.Array[i].Code;
 }
 if (dump) {
  std::cout <<
   "Dump of received " << GetName() << "::Input packet\n"
   "count:\n" << Input.Count << "\narray:";
  for (size_t i = 0; i < Input.Array.size(); ++i) {
   std::cout <<
    "\t[" << i << "]: permitted: "  << Input.Array[i].Permitted <<
    " code: " << Input.Array[i].Code;
  }
  std::cout << "\n";
 }
}

void ModulePlannedFrequencies::EncodePacket(bool dump) {
 output.Count = Output.Count;
 for (size_t i = 0; i < Output.Array.size(); ++i) {
  output.Array[i].Permitted = Output.Array[i].Permitted;
  output.Array[i].Code = Output.Array[i].Code;
 }
 if (dump) {
  std::cout <<
   "Dump of transmitted " << GetName() << "::Output packet\n"
   "count:\n" << Output.Count << "\narray:";
  for (size_t i = 0; i < Output.Array.size(); ++i) {
   std::cout <<
    "\t[" << i << "]: permitted: "  << Output.Array[i].Permitted <<
    " code: " << Output.Array[i].Code;
  }
  std::cout << "\n";
 }
}
