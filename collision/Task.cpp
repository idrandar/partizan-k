/*
 * CollisionTask.cc
 *
 *  Created on: 24.09.2015
 *      Author: Drandar
 */

// target-projectile collision task
// (C) I.Drandar, 2015, 2017 all rights  reserved
// (C) I.Drandar, for Qt, 2018, all rights reserved

#include "collision/Task.h"
#include <iostream>
#include "DebugOut.h" // weather

class CollisionTaskModule::CollisionCaller : public Caller {
 virtual status call() {
  return dynamic_cast<CollisionTaskModule&>(owner).Launch();
 }
public:
 CollisionCaller(CollisionTaskModule& _) : Caller(_) {}
 virtual ~CollisionCaller() {}
};

CollisionTaskModule::TestDump::TestDump(const ModuleInit& init, const QString& s) :
 PerformedFiltration(init.GetConst(s, "filtration-performed")), Performed(init.GetConst(s, "performed")),
 Weather(init.GetConst(s, "weather")) {
}

CollisionTaskModule::CollisionTaskModule(const ModuleInit& init) :
 Module(init), Launcher(init.Thread("launch"), new CollisionCaller(*this)),
 Solvers(), Performed(), TestDump(init, "test-dump") {
 const ModuleInitMap& SubModules = init.SubModules();
 for (ModuleInitMap::const_iterator i = SubModules.begin(); i != SubModules.end(); ++i) {
  Solvers.push_back(new SolverRecord(i.value()));
 }
}

CollisionTaskModule::~CollisionTaskModule() {
 for (SolversList::const_iterator i = Solvers.begin(); i != Solvers.end(); ++i) {
  delete *i;
 }
}

void CollisionTaskModule::Interact(const CommandVector& cmd, StatusVector& st) {
 Launcher.Lock();
 Command = cmd;
 Command.ShotTime += TimeStep();
 Launcher.Unlock();
 Launcher.WakeUp();
 Launcher.Lock();
 st = Status;
 Launcher.Unlock();
}

double CollisionTaskModule::NextTime(double t) const {
// std::cout << __func__ << "\t" << Performed % Refresh << std::endl;
 if (Command.ShotTime < time_step * 10 ||
  Command.Extra.Mode == SYSMODE_Initial ||
  Command.FiltrationPerformedCount < 10 ||
  !Command.NS_Extrapolation.OK ||
  Performed % Refresh == 0) {
// if ((Command.ShotTime > 0 && abs(t - Command.ShotTime) > time_step * 2.0)) {
  return t;
 } else {
  return Command.ShotTime;
 }
}

Module::status CollisionTaskModule::Launch() {
 Launcher.Wait();
 Launcher.Lock();
 const CommandVector cmd = Command;
 Launcher.Unlock();
 dump_if(TestDump.PerformedFiltration) << " performed filtration: " << cmd.FiltrationPerformedCount << "\n";
 if (cmd.FiltrationPerformedCount == 0) return THREAD_FAIL;
 ++Performed;

 dump_if(TestDump.Weather) << std::fixed << " weather:\n" << cmd.Extra.Weather << "\n";

 StatusVector st;
 for (SolversList::const_iterator i = Solvers.begin(); i != Solvers.end(); ++i) {
  // SetInDtCountAn(i);
  CollisionTaskSolverModule::CommandVector& cmd_s = (*i)->Command;
  cmd_s.ShotTime = cmd.ShotTime;
  cmd_s.Performed = Performed;
  cmd_s.NS_Extrapolation = cmd.NS_Extrapolation;
  cmd_s.Angles_Extrapolation = cmd.Angles_Extrapolation;
  cmd_s.PerformedFiltrationInCurrentMode = cmd.Extra.PerformedFiltrationInCurrentMode;
  cmd_s.FilterOut = cmd.FilterOut;
  cmd_s.Weather = cmd.Extra.Weather;
  cmd_s.GroundRaw = cmd.Extra.GroundRaw;
  cmd_s.ShipRaw = cmd.Extra.ShipRaw;
  cmd_s.AirTargetChanged = cmd.Extra.AirTargetChanged;
  //  Rendez(i);
  (*i)->Interact();
 }
 for (SolversList::const_iterator i = Solvers.begin(); i != Solvers.end(); ++i) {
  (*i)->GetStatus((*i)->Status);
 }
 st.Performed = Performed;
 Launcher.Lock();
 Status = st;
 Launcher.Unlock();
 return THREAD_OK;
}

void CollisionTaskModule::Start() {
 Module::Start();
 Launcher.Start();
 for (SolversList::const_iterator i = Solvers.begin(); i != Solvers.end(); ++i) {
  (*i)->Start();
 }
}

void CollisionTaskModule::Stop() {
 for (SolversList::const_iterator i = Solvers.begin(); i != Solvers.end(); ++i) {
  (*i)->Stop();
 }
 Launcher.Stop();
 Module::Stop();
}
