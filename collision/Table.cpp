 /*
 * CollisionTable.cc
 *
 *  Created on: 22.10.2015
 *      Author: Drandar
 */

// collision table
// (C) I.Drandar, 2015-2018, all rights reserved
// (C) I.Drandar, for Qt + C++1x, 2018, all rights reserved

#include "Table.h"
#include "control/exception.h"
#include <iomanip>
#include "common/CoordinatesConversion.h"

Compound WeaponInit::GetConst(const QString& Category, const QString& Name) const {
 try {
  return constants.Get(Category, Name);
 } catch (TException& e) {
  throw
   TException(
    __PRETTY_FUNCTION__, "\n[", Category, " ", Name, "]", e.what());
 }
}

CollisionTable::TestDump::TestDump(const WeaponInit& init, const QString& s) :
 ShotTime(init.GetConst(s, "shot-time")), NSTimeOffset(init.GetConst(s, "NS-time-offset")),
 ShipCoordinates(init.GetConst(s, "ship-coordinates")), ShipRotations(init.GetConst(s, "ship-rotations")),
 ProjectileStartCoordinates(init.GetConst(s, "projectile-start-coordinates")),
 FilterTimeOffset(init.GetConst(s, "filter-time-offset")), TargetCoordinates(init.GetConst(s, "target-coordinates")),
 DistanceCurrent(init.GetConst(s, "distance-current")), DistanceMeet(init.GetConst(s, "distance-meet")),
 NoSolution(init.GetConst(s, "no-solution-reason")), CollisionCoordinates(init.GetConst(s, "collision-coordinates")),
 AdvancedCoordinates(init.GetConst(s, "advanced-coordinates")),
 AdvancedDistanceXY(init.GetConst(s, "advanced-distance[x,y]")),
 ShootingBearing(init.GetConst(s, "shooting-bearing")), ShootingElevation(init.GetConst(s, "shooting-elevation")),
 FlightTime(init.GetConst(s, "flight-time")),
 AdvancedDistance(init.GetConst(s, "advanced-distance")) {
}

CollisionTable::CollisionTable(
 const QString& Owner, const WeaponInit& init, const Cartesian3DCoords& GunOnShip, bool DumpPermitted) try :
  Name(QString(init.Name()) + "@" + Owner), GunOnShip(GunOnShip), DumpPermitted(DumpPermitted), TestDump(init, "test-dump") {
} catch (TException& e) {
 throw
  TException(
   __PRETTY_FUNCTION__, "\n[", Owner, " ", init.Name(), " ", init.Type(), "]", e.what());
}

CollisionTable::OutData CollisionTable::Evaluate(
 const double TimeShot, const struct FilterModule::OutData& FltOut, const ExtrapolationPolynomials::NS& NS_Extrapolation) {
 DumpIf(TestDump.ShotTime, std::setprecision(5), "shot time: ", TimeShot, " s");

 // Topocentrical Ship   cordinates on time shot
 double dt_Shot_Nvg = TimeShot - NS_Extrapolation.Time;
 DumpIf(TestDump.NSTimeOffset, "NS time offset: ", dt_Shot_Nvg, " s");

 if (fabs(dt_Shot_Nvg) > dt_Shot_Nvg_max) {
  dt_Shot_Nvg = std::max(dt_Shot_Nvg, -dt_Shot_Nvg_max);
  dt_Shot_Nvg = std::min(dt_Shot_Nvg, dt_Shot_Nvg_max);
 }

 ShipCart = NS_Extrapolation.GetShipCart(dt_Shot_Nvg);
 DumpIf(
  TestDump.ShipCoordinates, "ship coordinates @ NS time, m: ", std::setprecision(2), ShipCart);

 // Ship angles of rotation and angle velocity on time shot
 ShRot = NS_Extrapolation.GetRotations(dt_Shot_Nvg);
 DumpIf(TestDump.ShipRotations, "ship rotations @ NS time, °: ", ShRot);

 // counting of transfer matrix
 // topocentrical cordinates of shell(snariad) on time shot
 ProjectileCoords = ShipCart + ConvertShipToGroundCartesian(GunOnShip, ShRot);
 DumpIf(
  TestDump.ProjectileStartCoordinates,
  "projectile coordinates @ start (NS time), m: ", ProjectileCoords);

 //Topocentrical Target cordinates on time shot
 dt_Shot_Flt = TimeShot - FltOut.Time;
 DumpIf(
  TestDump.FilterTimeOffset, std::setprecision(5), "filter time offset: ", dt_Shot_Flt, " s");

 TargetCoords = FltOut.Cart(dt_Shot_Flt);
 DumpIf(TestDump.TargetCoordinates, "target coordinates @ shot time, m: ", TargetCoords);

 if (std::isnan(TargetCoords.X) || std::isnan(TargetCoords.Y) || std::isnan(TargetCoords.Z)) {
  DumpIf(TestDump.NoSolution, srTargetCalulationError);
  return OutData(TimeShot, srTargetCalulationError, 0.0, {});
 }

 DistanceCurrent = AbsoluteDistance(TargetCoords, ShipCart);
 DumpIf(TestDump.DistanceCurrent, "current distance: ", DistanceCurrent, " m");

 return DoEvaluate(TimeShot, FltOut, NS_Extrapolation);
}

constexpr inline double CollisionTable::dt_Shot_Nvg_max = 0.5;
constexpr inline double CollisionTable::dt_Shot_Filter_max = 7.5;

std::ostream& operator<<(std::ostream& _, CollisionTable::SolutionResult r) {
 if (r != CollisionTable::srOK) {
  _ << "there is no solution because ";
 }
 if (r >= CollisionTable::srUnknown) {
  _ << "of unknown reason";
 } else {
  const char* reason[] = {
   "solution result is OK",
   "target is too low",
   "target is too high",
   "target is too near",
   "target is too far",
   "table is incorrect",
   "invalid data were passed to the function",
   "flight time is too big",
   "too many iterations were performed",
   "calculations needn't",
   "cannot calculate target location"
  };
  _ << reason[r];
 }
 return _;
}

std::ostream& operator<<(std::ostream& _, const CollisionTable::CorrectionPair& p) {
 _ << "to distance: " << p.ToDistance.value << " m,\tto height: " << p.ToHeight.value << " m";
 return _;
}

std::ostream& operator<<(std::ostream& _, CollisionTable::TablePos tp) {
 return _ << CollisionTable::ToSr(tp);
}

CollisionTable::SolutionResult CollisionTable::ToSr(TablePos tp) {
 const SolutionResult sr[] = {
  srOK, srTargetTooNear, srTargetTooFar, srTargetTooLow, srTargetTooHigh
 };
 return sr[tp];
}
