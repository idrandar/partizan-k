#ifndef AK630TABLE_H
#define AK630TABLE_H

// gun (AK100) collision table
// (C) I.Drandar, 2018, all rights reserved

#include "Table.h"

class GunAK630CollisionTable : public CollisionTable {
 static constexpr double MaximalFlightTime = 18.0; // Максимально допустимое время полета снаряда, c
 static constexpr double StartSpeed = 880.0;       // нач.номин.скорость снаряда относительно ствола, м/c
 static constexpr size_t HeightSize = 13;
 static constexpr size_t DistanceSize = 15;
 static constexpr size_t TimeSize = 37;
 static constexpr double HEIGHT_STEP = 400.0;
 static constexpr double DISTANCE_STEP = 400.0;

 struct GunBallisticData : public BallisticData {
  GunBallisticData() :
   BallisticData() {
  }
 } MetBalDt;
 struct Weather Weather;
 Cartesian3DCoords OEU_Location;

 struct Limits {
  double Low;
  double High;
  bool Out(double _) const { return _ < Low || _ > High; }
  bool Less(double _) const { return _ < Low; }
  bool More(double _) const { return _ > High; }
 };

 static const Limits LimDh[HeightSize];

 struct LimitsTable : public std::vector<Limits> {
  typedef std::vector<Limits> BASE;
  const double ArgumentMin;
  const double ArgumentStep;
  LimitsTable(size_t, const Limits[], double arg_min, double arg_step);
  bool OutBound(double) const;
  bool Less(double, double) const;
  bool Out(double, double) const;
 };

 const LimitsTable LimitsTab;

 typedef double H_D_Tab[HeightSize][DistanceSize];

 //-----------------------------------------------------------------
 struct DHTable : public std::vector<std::vector<double> > { // by h & d
  typedef std::vector<std::vector<double> > BASE;
  const double Coeff;
  const double LowThreshold;
  const double HeightMin;
  const double HeightStep;
  const double DistanceMin;
  const double DistanceStep;
  mutable SolutionResult Result;
  DHTable(
   const H_D_Tab, double Coeff = 1.0, double LowThreshold = 0.01,
   double height_min = 0.0, double height_step = HEIGHT_STEP, double distance_min = 0.0, double distance_step = DISTANCE_STEP);
  double GetVal(double, double) const;
  double GetVal(double, double, double) const;
  SolutionResult GetResult() const { return Result; }
 };

 struct Table : public std::vector<std::vector<double> > {
  using BASE = std::vector<std::vector<double>>;
  const double Coeff;
  const double RowStep;
  const double ColStep;
  static constexpr double eps = 0.01;
  static constexpr double LowThreshold = 0.01;
  Table(const BASE&, double Coeff, double RowStep = 0.5, double ColStep = 400.0);
  RetType GetVal(double R, double C) const;
  RetType GetVal(double R, double C, double) const;
 };
 static const Table FlightTimeFrom_D_Tab;
 static const Table Angle_Tab;
 static const Table Derivation_Tab;
 static const Table Crosswind_Tab;
 struct CorrTab {
  Table::BASE ToDistance;
  Table::BASE ToHeight;
 };
 struct TablePair {
  const Table ToDistance;
  const Table ToHeight;
  TablePair(const CorrTab&, double Coeff = 1.0);
  CorrectionPair GetVal(double, double, double) const;
 };
 static const TablePair Tailwind_Tab;
 static const TablePair Dencity_Tab;
 static const TablePair StartSpeed_Tab;
 static const TablePair Mass_Tab;
 const struct GunTestDump : public TestDump {
  const bool CollisionTime;
  const bool SpeedPart;
  const bool Cycle;
  const bool StartSpeed;
  const bool Tailwind;
  const bool Crosswind;
  const struct Corrections {
   const bool Tailwind;
   const bool Crosswind;
   const bool Pressure;
   const bool Temperature;
   const bool StartSpeed;
   const bool Mass;
   const bool Derivation;
   const bool Elevation;
   const bool ShipMotion;
   const bool Cumulative;
   Corrections(const WeaponInit&, const QString&, const QString&);
  } Corrections;
  GunTestDump(const WeaponInit&, const QString&);
 } TestDump;
 const unsigned int IterationsMax;
public:
 GunAK630CollisionTable(const QString&, const WeaponInit&, const Cartesian3DCoords&, bool);
 virtual ~GunAK630CollisionTable() {}
 virtual void Import(const ImportData&);
 virtual OutData DoEvaluate(double, const struct FilterModule::OutData&, const ExtrapolationPolynomials::NS&);

};

#endif // AK630TABLE_H
