/*
 * CollisionTaskSolver.cc
 *
 *  Created on: 29.09.2015
 *      Author: Drandar
 */

// target-projectile collision task solver for single combat module
// (C) I.Drandar, 2015-2017, all rights  reserved
// (C) I.Drandar, for Qt, 2018-2019, all rights reserved

#include "TaskSolver.h"
#include "AK100Table.h"
#include "AK630Table.h"
#include "common/CoordinatesConversion.h"
#include <iostream>
#include "DebugOut.h" // SysMode
#include "Setup.h"
#include <iomanip>

class CollisionTaskSolverModule::SolverCaller : public Caller {
 virtual status call() {
  return dynamic_cast<CollisionTaskSolverModule&>(owner).Solve();
 }
public:
 SolverCaller(CollisionTaskSolverModule& _) : Caller(_) {}
 virtual ~SolverCaller() {}
};

void CollisionTaskSolverModule::StatusVector::Reset(double) {
 data = {};
}

CollisionTaskSolverModule::TestDump::TestDump(const ModuleInit& init, const QString& s) :
 Performed(init.GetConst(s, "performed")), ShotTime(init.GetConst(s, "shot-time")),
 Mode(init.GetConst(s, "mode")), State(init.GetConst(s, "state")),
 ElapsedTime(init.GetConst(s, "elapsed-time")), Table(init.GetConst(s, "table")),
 Output(init.GetConst(s, "output")), OutputProcessed(init.GetConst(s, "output-processed")) {
}

CollisionTaskSolverModule::Algorithm::Algorithm(const ModuleInit& init, const QString& s) :
 WeaponType(init.GetConst(s, "weapon-type")), UseRaw(init.GetConst(s, "use-raw")),
 FixedDeltaTime(init.GetConst(s, "fixed-dt")), FullScheme(init.GetConst(s, "full-scheme")) {
}

CollisionTaskSolverModule::CollisionTaskSolverModule(const ModuleInit& init) :
 Module(init), Solver(init.Thread("solve"), new SolverCaller(*this)),
 Location(init.GetConst("constructive", "location")), OEU_Location(setup->GetConst("constructive", "OEU location")),
 Init(init.GetConst("constructive", "init")),
 State(tsInitial), HaveSolutionCount(), NoSolutionCount(),
 Table(), TestDump(init, "test-dump"), Algorithm(init, "algorithm"),
 Extrapolation(TimeStep(), Algorithm.FullScheme) {
 if (Algorithm.WeaponType == 0)
  Table =
   new GunAK630CollisionTable(Name, setup->GetWeapon("AK630"), Location, TestDump.Table);
 else
  Table =
   new GunAK100CollisionTable(Name, setup->GetWeapon("AK100"), Location, TestDump.Table);
 sem_init(&result_sem, 0, 0);
}

CollisionTaskSolverModule::~CollisionTaskSolverModule() {
 delete Table;
 sem_destroy(&result_sem);
}

void CollisionTaskSolverModule::ResetStatus(double ShotTime) {
// Clear(Out);
 Out.HaveSolution = false;
 Out.EvaluatedFlightTime = 0;
 Out.Result = CollisionTable::srCalculationsNotNeeded;
 Out.ShotTime = ShotTime;
}

std::ostream& operator<<(std::ostream& _, CollisionTaskSolverModule::TaskState s) {
 if (s > CollisionTaskSolverModule::tsHaveSolution) {
  _ << "off-limits: " << static_cast<int>(s);
 } else {
  const char* strstate[] = {
   "undefined", "initial", "shutdown", "no change", "no solution", "alignment with parallax", "have solution"
  };
  _ << strstate[s];
 }
 return _;
}

Module::status CollisionTaskSolverModule::Solve() {
 Solver.Wait();
 Solver.Lock();
 const CommandVector cmd = Command;
 Solver.Unlock();
 CTime calcTime; // start of elapsed time measuring
 calcTime.Start();
 const double TimeShot = cmd.ShotTime;
 dump_if(TestDump.ShotTime) << "shot time: " << std::fixed << TimeShot << " s\n";
 StatusVector st;
 st.Performed = cmd.Performed;
 dump_if(TestDump.Performed) << "performed: " << cmd.Performed << "\n";
 const SystemMode Mode = cmd.Mode;
 dump_if(TestDump.Mode) << "mode: " << cmd.Mode << "\n";

 if (st.Performed == 1) {
  st.Reset(cmd.ShotTime);
  Extrapolation.Init(TimeShot, Init, cmd.NS_Extrapolation);
 }

 State = tsInitial;
 Out.Result = CollisionTable::srCalculationsNotNeeded;
 Out.HaveSolution = false;
 Out.DistanceCurrent = Absolute(Command.FilterOut.Cart(TimeShot - Command.FilterOut.Time));
 Out.DistanceMeet = NAN;
 Out.EvaluatedFlightTime = 0.0;
 Out.ShotTime = TimeShot;
 Out.Ship = cmd.Angles_Extrapolation.ShipVal(0);
 Out.Ground = cmd.Angles_Extrapolation.GroundVal(0);
 switch (Mode) {
 case SYSMODE_Initial:
 case SYSMODE_SS:
  st.Reset(cmd.ShotTime);
  State = tsInitial;
  HaveSolutionCount = 0;
  NoSolutionCount = 0;
  break;
 case SYSMODE_MP:
 case SYSMODE_ETD:
  if (cmd.JointAlignment && !cmd.ParallaxJointAlignment) {
   State = tsNoSolution;
   HaveSolutionCount = 0;
   NoSolutionCount++;
   break;
  }

  if (cmd.JointAlignment && cmd.ParallaxJointAlignment) {
   State = tsNoSolution_WithParallax;
   HaveSolutionCount = 0;
   NoSolutionCount++;
   break;
  }
  [[fallthrough]];

 case SYSMODE_AT:

  if ((!cmd.PerformedFiltrationInCurrentMode) || (cmd.Angles_Extrapolation.Count == 0)) {
   //Скачка PUG нет, но HaveSolution обнуляется через  OutDt_r.HaveSolution = 0;
   State = tsNoChange;
   HaveSolutionCount = 0;
   NoSolutionCount = 0;
   break;
  }

  if (!cmd.FilterOut.Completed) {
   State = tsNoChange;
   HaveSolutionCount = 0;
   NoSolutionCount++;
   break;
  }
  [[fallthrough]];

 case SYSMODE_IT:

  {
   CollisionTable::ImportData _;
   _.Weather = Command.Weather;
   _.Ballistic = Command.Ballistic;
   _.ShipRaw = Command.ShipRaw;
   _.OEU_Location = OEU_Location;
   Table->Import(_);
   Out = Table->Evaluate(cmd.ShotTime, Command.FilterOut, Command.NS_Extrapolation);
  }

  if (Out.HaveSolution) {
   State = tsHaveSolution;
   HaveSolutionCount++;
   NoSolutionCount = 0;
  } else {
   State = tsNoSolution;
   HaveSolutionCount = 0;
   NoSolutionCount++;
  }
  break;
 case SYSMODE_Testing:

  ResetStatus(cmd.ShotTime);
  State = tsInitial;
  HaveSolutionCount = 0;
  NoSolutionCount = 0;
  break;
 case SYSMODE_Shutdown:

  ResetStatus(cmd.ShotTime);
  State = tsShutdown;
  HaveSolutionCount = 0;
  NoSolutionCount = 0;
  break;
 default:

  ResetStatus(cmd.ShotTime);
  State = tsInitial;
  HaveSolutionCount = 0;
  NoSolutionCount = 0;
  break;
 }
 dump_if(TestDump.State) <<
  "state: " << State << "\thave solution: " << HaveSolutionCount << "\tno solution: " << NoSolutionCount << "\n";
 dump_if(TestDump.Output) <<
  "output:\n\thave solution: " << Out.HaveSolution << "\n\t"
  "ground coordinates, °:\t" << std::setprecision(2) << Out.Ground << "\n\t"
  "shot time: " << Out.ShotTime << " s\t"
  "current distance: " << Out.DistanceCurrent << " m\tmeet distance: " << Out.DistanceMeet << " m\t"
  "flight time: " << Out.EvaluatedFlightTime << " s\n";
 ApplySolutionResults(cmd);

 Extrapolation.InOut = Out;
 if ((HaveSolutionCount > 0) && (NoSolutionCount == 0)) {
  if (HaveSolutionCount == 1)
   Extrapolation.Regenerate();
 }
 if ((HaveSolutionCount == 0) && (NoSolutionCount > 0)) {
  if (NoSolutionCount == 1)
   Extrapolation.Regenerate();
 }
 if (cmd.AirTargetChanged) {
  Extrapolation.Regenerate();
 }

 Extrapolation.Evaluate();

 dump_if(TestDump.OutputProcessed) <<
  "processed output:\n\t"
  "ground coordinates, °:\t" << std::setprecision(2) << Out.Ground << "\n\t"
  "ship coordinates, °:\t" << Out.Ship << "\n\tshot time: " << Out.ShotTime << " s\n";
 st.data.Extrapolation = Extrapolation.InOut;
 st.data.HaveSolution = Out.HaveSolution;
 st.Result = Out.Result;
 calcTime.Stop();
 dump_if(TestDump.ElapsedTime) << "calculation performed in " << std::scientific << calcTime.Span() << " s\n";
 st.ElapsedTime = calcTime.Span();
 Solver.Lock();
 Status = st;
 Solver.Unlock();
 sem_post(&result_sem);
 return THREAD_OK;
}

void CollisionTaskSolverModule::ApplySolutionResults(const CommandVector& cmd) {
 double dt_Shot_Ship;
 if (Algorithm.FixedDeltaTime) {
  dt_Shot_Ship = 0;
 } else {
  dt_Shot_Ship = cmd.ShotTime - cmd.NS_Extrapolation.Time;
 }
 Rotations Rot = cmd.NS_Extrapolation.GetRotations(dt_Shot_Ship);

 Spherical2DShipCoords Ship;
 Spherical2DGroundCoords Ground;
 switch (State) {
 case tsInitial:

  //Q = ParProc. Constr.MsParCM[nCM].AzInit;
  //F = ParProc. Constr.MsParCM[nCM].ElInit;
  Ship = Init;
  Ground = ConvertShipToGroundSpherical(Ship, Rot);
  Out.Ground = Ground;
  Out.Ship = Ship;
  break;

 case tsShutdown:

  //Q  = ParProc. Constr.MsParCM[nCM].AzInit;
  //F  = ParProc. Constr.MsParCM[nCM].ElInit;
  Ship = Init;
  Ground = ConvertShipToGroundSpherical(Ship, Rot);
  Out.Ground = Ground;
  Out.Ship = Ship;
  break;

 case tsNoChange:
  Out.Ground = Extrapolation.InOut.Ground;
  Out.Ship = Extrapolation.InOut.Ship;
  break;

 case tsNoSolution:
  if (Algorithm.UseRaw) {
   Out.Ground = cmd.GroundRaw;
   Out.Ship = cmd.ShipRaw;
  } else {
   const double dt_Shot_Tg = cmd.ShotTime - cmd.Angles_Extrapolation.Time;
//   std::cout << __func__ << "\t" << dt_Shot_Tg << "\t" << cmd.ShotTime << "\t" << cmd.Angles_Extrapolation.Time << std::endl;
   Out.Ship = cmd.Angles_Extrapolation.ShipVal(dt_Shot_Tg);
   Out.Ground = cmd.Angles_Extrapolation.GroundVal(dt_Shot_Tg);
//   Out.Ground = ConvertShipToGroundSpherical(Out.Ship, Rot);
  }
  break;

 case tsNoSolution_WithParallax:
  {
   const double dt_Shot_Tg = cmd.ShotTime - cmd.Angles_Extrapolation.Time;
   Ship = cmd.Angles_Extrapolation.ShipVal(dt_Shot_Tg);

   SphericalCoords TargetFromOEU { cmd.ReferenceDistance, Ship.Heading, Ship.Elevation };
   Cartesian3DCoords TargetFromOEUCart = SphericalToCartesian(TargetFromOEU);
   Cartesian3DCoords TargetFromZero = TargetFromOEUCart + OEU_Location;
   Cartesian3DCoords TargetFromCM = TargetFromZero - Location;
   SphericalCoords TargetFromCMShip = CartesianToSpherical(TargetFromCM);

   Spherical2DShipCoords Ship_r { TargetFromCMShip.Bearing, TargetFromCMShip.Elevation };
   Spherical2DGroundCoords Ground_r = ConvertShipToGroundSpherical(Ship_r, Rot);

//   std::cout << __func__ << Ground_r << std::endl;

   Out.Ground = Ground_r;
   Out.Ship = Ship_r;
  }
  break;

 case tsHaveSolution:

  Ground = Out.Ground;
  Ship = ConvertGroundToShipSpherical(Ground, Rot);
  Out.Ship = Ship;
  break;

 default:;

 }
// std::cout << __PRETTY_FUNCTION__ << State << Out.Ground << std::endl;
 Out.ShotTime = cmd.ShotTime;
}

void CollisionTaskSolverModule::Interact(const CommandVector& cmd, StatusVector&) {
 Solver.Lock();
 Command = cmd;
 Solver.Unlock();
 Solver.WakeUp();
}

void CollisionTaskSolverModule::GetStatus(StatusVector& st) {
 sem_wait(&result_sem);
 Solver.Lock();
 st = Status;
 Solver.Unlock();
}

void CollisionTaskSolverModule::Start() {
 Module::Start();
 Solver.Start();
}

void CollisionTaskSolverModule::Stop() {
 Solver.Stop();
 Module::Stop();
}
