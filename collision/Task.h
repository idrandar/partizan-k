/*
 * CollisionTask.hh
 *
 *  Created on: 24.09.2015
 *      Author: Drandar
 */

#ifndef COLLISIONTASK_HH_
#define COLLISIONTASK_HH_

// target-projectile collision task
// (C) I.Drandar, 2015, 2017 all rights  reserved
// (C) I.Drandar, for Qt + C++1x, 2018-2019, all rights reserved

#include "common/module.h"
#include "data_types.h"
#include "Filter.h"
#include "TaskSolver.h"
#include "common/Record.h"

class CollisionTaskModule : public Module {
public:
 struct OutData {};
 struct InData {};
 struct StatusVector {
  int Performed;
 };
 struct ExtraData { // external additional data from different sourses
  SystemMode Mode;
  bool PerformedFiltrationInCurrentMode;
  struct Weather Weather;
  Spherical2DGroundCoords GroundRaw;
  Spherical2DShipCoords ShipRaw;
  bool AirTargetChanged;
  ExtraData() :
   Mode(), PerformedFiltrationInCurrentMode(), Weather(), GroundRaw(), ShipRaw(), AirTargetChanged()  {
  }
 };
 struct CommandVector {
  int FiltrationPerformedCount;
  double ShotTime;
  ExtraData Extra;
  struct FilterModule::OutData FilterOut;
  ExtrapolationPolynomials::NS NS_Extrapolation;
  ExtrapolationPolynomials::Angles Angles_Extrapolation;
  CommandVector() :
   FiltrationPerformedCount(), ShotTime(), Extra(), FilterOut(),
   NS_Extrapolation(), Angles_Extrapolation() {}
 };
 typedef Record<CollisionTaskSolverModule> SolverRecord;
private:
 class CollisionCaller;
 Module::Thread Launcher;
 status Launch();
 CommandVector Command;
 StatusVector Status;
 typedef std::vector<SolverRecord*> SolversList;
 SolversList Solvers;
 int Performed;
 static constexpr int Refresh = 450;
 const struct TestDump {
  const bool PerformedFiltration;
  const bool Performed;
  const bool Weather;
  TestDump(const ModuleInit&, const QString&);
 } TestDump;
public:
 explicit CollisionTaskModule(const ModuleInit&);
 virtual ~CollisionTaskModule();
 virtual void Start();
 virtual void Stop();
 void Interact(const CommandVector&, StatusVector&);
 auto& SolverCommand(size_t Index = 0) { return Solvers[Index]->Command; }
 const auto& SolverStatus(size_t Index = 0) const { return Solvers[Index]->Status; }
 double NextTime(double t) const;
};
#endif /* COLLISIONTASK_HH_ */
