/*
 * CollisionTaskSolver.hh
 *
 *  Created on: 29.09.2015
 *      Author: Drandar
 */

#ifndef COLLISIONTASKSOLVER_HH_
#define COLLISIONTASKSOLVER_HH_

// target-projectile collision task solver for single combat module
// (C) I.Drandar, 2015-2017, all rights  reserved
// (C) I.Drandar, for Qt, 2018, all rights reserved

#include "common/module.h"
#include "Comp2.h"
#include "ExtrapolationPolynomials.h"
#include "Extrapolation.h"
#include "Filter.h"
#include "Table.h"

class CollisionTaskSolverModule : public Module {
public:
 struct InData {
 };
 struct CommandVector {
  SystemMode Mode;
  double ShotTime;
  int Performed;
  ExtrapolationPolynomials::Angles Angles_Extrapolation;
  ExtrapolationPolynomials::NS NS_Extrapolation;
  struct FilterModule::OutData FilterOut;
  bool PerformedFiltrationInCurrentMode;
  struct BallisticExt : public CollisionTable::Ballistic {
  };
  BallisticExt Ballistic;
  struct Weather Weather;
  bool JointAlignment;
  bool ParallaxJointAlignment;
  double ReferenceDistance;
  Spherical2DGroundCoords GroundRaw;
  Spherical2DShipCoords ShipRaw;
  bool AirTargetChanged;
  CommandVector() :
   Mode(), ShotTime(), Performed(),
   Angles_Extrapolation(), NS_Extrapolation(), FilterOut(), PerformedFiltrationInCurrentMode(), Ballistic(), Weather(),
   JointAlignment(), ParallaxJointAlignment(), ReferenceDistance(), GroundRaw(), ShipRaw(), AirTargetChanged() {}
 };
 struct OutData {
  bool HaveSolution;
  struct ExtrapolationPolynomials::Collision::InOut Extrapolation;
 };
 struct StatusVector {
  int Performed;
  CollisionTable::SolutionResult Result;
  OutData data;
  double ElapsedTime;
  void Reset(double);
 };
private:
 class SolverCaller;
 Module::Thread Solver;
 status Solve();
 const Cartesian3DCoords Location; // CM_Location
 const Cartesian3DCoords OEU_Location;
 const Spherical2DShipCoords Init;
 CommandVector Command;
 StatusVector Status;
 CollisionTable::OutData Out; // Out Data of function Evaluate  ZTM1, et
 enum TaskState {
  tsUndefined,
  tsInitial,
  tsShutdown,
  tsNoChange,
  tsNoSolution,
  tsNoSolution_WithParallax,
  tsHaveSolution
 };
 TaskState State;
 friend std::ostream& operator<<(std::ostream&, TaskState);
 sem_t result_sem;
 int HaveSolutionCount;
 int NoSolutionCount;
 CollisionTable* Table;
 const struct TestDump {
  const bool Performed;
  const bool ShotTime;
  const bool Mode;
  const bool State;
  const bool ElapsedTime;
  const bool Table;
  const bool Output;
  const bool OutputProcessed;
  TestDump(const ModuleInit&, const QString&);
 } TestDump;
 const struct Algorithm {
  const int WeaponType;
  const bool UseRaw;
  const bool FixedDeltaTime;
  const bool FullScheme;
  Algorithm(const ModuleInit&, const QString&);
 } Algorithm;
 ExtrapolationPolynomials::Collision Extrapolation;
// int SmootheCount;
 void ResetStatus(double);
 void ApplySolutionResults(const CommandVector&);
public:
 CollisionTaskSolverModule(const ModuleInit&);
 virtual ~CollisionTaskSolverModule();
 virtual void Start();
 virtual void Stop();
 void Interact(const CommandVector&, StatusVector&);
 void GetStatus(StatusVector&);
};
#endif /* COLLISIONTASKSOLVER_HH_ */
