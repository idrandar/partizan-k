/*
 * CollisionExtrapolationPolynomials.hh
 *
 *  Created on: 23.10.2015
 *      Author: Drandar
 */

// collision extrapolation polynomials
// (C) I.Drandar, 2015-2017, 2019, all rights reserved

#ifndef COLLISIONEXTRAPOLATIONPOLYNOMIALS_HH_
#define COLLISIONEXTRAPOLATIONPOLYNOMIALS_HH_

#include "common/Coordinates.h"
#include <vector>
#include "Table.h"

namespace ExtrapolationPolynomials {

//////////////////////////////////////////////////////////////////////////
//                Collision                                            //
//  - класс, содержащий данные и методы, позволяющие вычислять          //
//  по результатам решения задачи встречи в произвольые моменты времени //
//  экстраполированные значения оценок углов наведения CM.              //
//  Углы наведения вычисляются как в корабельной так и в                //
//  земной системах координат                                           //
//////////////////////////////////////////////////////////////////////////

class Collision {
 const Spherical2DCoords D_max;
 double const RP_dT;
 const Spherical2DGroundCoords ThGSGround;
 const Spherical2DShipCoords ThGSShip;
 static int const PolynomialDegree = 2;
 static size_t const NmSmSC = 20;
 struct TimeFiller;
 std::vector<double> SC_Time, SC_TimeRelative;
 std::vector<Spherical2DGroundCoords> SC_Ground;
 std::vector<Spherical2DShipCoords> SC_Ship;
 std::vector<Spherical2DGroundCoords> SC_ResultingGround;
 std::vector<Spherical2DShipCoords> SC_ResultingShip;
 template<typename T>
 void LimitCoordinate(
  double dt, typename T::CoordType T::* Coord, const T& Last, const T Ms_d[3], const T& ThGS, T& r) {
  typename T::CoordType min = typename T::CoordType();
  size_t ir = 0;
  for (size_t i = 0; i < 3; i++) {
   if (min > fabs(Ms_d[i].*Coord)) {
    min = fabs(Ms_d[i].*Coord);
    ir = i;
   }
  }
  if (min > ThGS.*Coord) {
   const typename T::CoordType cr = min / ThGS.*Coord;
   const typename T::CoordType cr1 = CountCf1(cr);
   typename T::CoordType d = cr1 * D_max.*(D_max.MappingCoord(Coord)) * dt;
   if (Ms_d[ir].*Coord < 0.0)
    d = -d;
   r.*Coord = Last.*Coord + d;
  } else {}
 }
public:
 struct InOut {
  double ShotTime;
  Spherical2DGroundCoords Ground;
  Spherical2DShipCoords Ship;
  double DistanceCurrent;
  double DistanceMeet;
  double FlightTime;
  CollisionTable::AllCorrections Corrections;
  InOut() = default;
  void operator=(const CollisionTable::OutData&);
  Spherical2DGroundCoords GroundVal(double dt) const;
  Spherical2DShipCoords ShipVal(double dt) const;
  const Spherical2DShipCoords& GetPnShip(size_t) const;
  const Spherical2DGroundCoords& GetPnGround(size_t) const;
 private:
  Spherical2DGroundCoords PnGround[5];
  Spherical2DShipCoords PnShip[5];
  friend struct Collision;
 } InOut;
 const enum EvaluationScheme { esTRIVIAL, esFULL } Scheme;
public:
 Collision(double dT, bool SchemeFull) :
  D_max { MATH_CONST::Deg2Rad(60.0), MATH_CONST::Deg2Rad(60.0) },
  RP_dT(dT),
  ThGSGround { RP_dT * D_max.HA, RP_dT * D_max.Elev },
  ThGSShip { ThGSGround.Bearing, ThGSGround.Elevation },
  SC_Time(NmSmSC), SC_TimeRelative(NmSmSC),
  SC_Ground(NmSmSC), SC_Ship(NmSmSC), SC_ResultingGround(NmSmSC), SC_ResultingShip(NmSmSC),
  InOut(), Scheme(SchemeFull ? esFULL : esTRIVIAL) {}
 void Init(double TimeShot, const Spherical2DShipCoords& Init, const ExtrapolationPolynomials::NS&);
 void Evaluate_Full();
 void Evaluate_Trivial();
 void Evaluate();
 void Regenerate();
 void Smoothe(size_t);

private:
 //double  CountCf  (int n, int N);
 double CountCf1(double cf);
};

}  // namespace ExtrapolationPolynomials


#endif /* COLLISIONEXTRAPOLATIONPOLYNOMIALS_HH_ */
