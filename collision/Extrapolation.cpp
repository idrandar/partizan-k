/*
 * CollisionExtrapolationPolynomials.cc
 *
 *  Created on: 23.10.2015
 *      Author: Drandar
 */

// collision extrapolation polynomials
// (C) I.Drandar, 2015-2017, all rights reserved
// (C) I.Drandar, for C++1x, 2018-2020, all rights reserved

#include "Extrapolation.h"
#include "common/CoordinatesConversion.h"
//#include <algorithm>
#include "common/Polynomial.h"
#include <iostream>

void ExtrapolationPolynomials::Collision::InOut::operator=(const CollisionTable::OutData& _) {
 ShotTime = _.ShotTime;
 Ground = _.Ground;
 Ship = _.Ship;
 DistanceCurrent = _.DistanceCurrent;
 DistanceMeet = _.DistanceMeet;
 FlightTime = _.EvaluatedFlightTime;
 Corrections = _.Corrections;
}

void ExtrapolationPolynomials::Collision::Init(double TimeShot, const Spherical2DShipCoords& ShipInit, const ExtrapolationPolynomials::NS &NS_ExtrapolationPolynomials) {
 InOut.ShotTime = TimeShot;

 const double dt = TimeShot - NS_ExtrapolationPolynomials.Time;
 const Rotations Rot = NS_ExtrapolationPolynomials.GetRotations(dt);
 const Spherical2DGroundCoords GroundInit = ConvertShipToGroundSpherical(ShipInit, Rot);

 std::fill_n(InOut.PnGround, 5, Spherical2DGroundCoords());
 std::fill_n(InOut.PnShip, 5, Spherical2DShipCoords());

 InOut.PnGround[0] = GroundInit;
 InOut.PnShip[0] = ShipInit;

 InOut.Ground = GroundInit;
 InOut.Ship = ShipInit;

 //DAz_max = ParProc.Constr.MsParCM[nCM].DAz_max;
 //DEl_max = ParProc.Constr.MsParCM[nCM].DEl_max;

 //CV.ThGS_A = 1.0 /double(RNPR_CN_FRQ) * DAz_max;
 //CV.ThGS_E = 1.0 /double(RNPR_CN_FRQ) * DEl_max;
 //printf("\n<Collision.Init>   nCM = %2d  Q = %8.6f\n",nGun,Q);
}

double ExtrapolationPolynomials::Collision::CountCf1(double cf) {
 double cf1;
 if (cf > 3.0)
  cf1 = 1.0;
 else if (cf > 2.0)
  cf1 = 0.66;
 else
  cf1 = 0.33;
 return cf1;
}

struct ExtrapolationPolynomials::Collision::TimeFiller {
 int i;
 const double Base;
 const double RP_dT;
 TimeFiller(double Base, double RP_dT) : i(), Base(Base), RP_dT(RP_dT) {}
 double operator()() {
  return Base - static_cast<double>(NmSmSC - i++) * RP_dT;
 }
};

void ExtrapolationPolynomials::Collision::Regenerate() {
 std::generate(SC_Time.begin(), SC_Time.end(), TimeFiller(InOut.ShotTime, RP_dT));
 std::fill(SC_Ground.begin(), SC_Ground.end(), InOut.Ground);
 std::fill(SC_Ship.begin(), SC_Ship.end(), InOut.Ship);
}

void ExtrapolationPolynomials::Collision::Smoothe(size_t wide) {
 if (wide > 1) {
  for (size_t i = 0; i < NmSmSC; ++i) {
   std::cout << SC_ResultingGround[i].Bearing << " ";
  }
  std::cout << std::endl;
  std::vector<Spherical2DGroundCoords> GroundDiff(NmSmSC - 1);
  for (size_t i = 0; i != NmSmSC - 1; ++i) {
   GroundDiff[i] = SC_ResultingGround[i + 1] - SC_ResultingGround[i];
   std::cout << GroundDiff[i].Bearing << " ";
  }
  std::cout << std::endl;
  std::vector<Spherical2DGroundCoords> GroundDiffSmoothed(NmSmSC - 1);
  for (size_t i = 1; i < wide; ++i) {
   Spherical2DGroundCoords sum = Spherical2DGroundCoords();
   for (size_t j = 0; j < i; ++j) {
    sum = sum + GroundDiff[j];
   }
   GroundDiffSmoothed[i - 1].Bearing = sum.Bearing / i;
   GroundDiffSmoothed[i - 1].Elevation = sum.Elevation / i;
   std::cout << GroundDiffSmoothed[i - 1].Bearing << " ";
  }
  for (size_t i = wide; i < NmSmSC; ++i) {
   Spherical2DGroundCoords sum = Spherical2DGroundCoords();
   for (size_t j = i - wide; j < i; ++j) {
    sum = sum + GroundDiff[j];
   }
   GroundDiffSmoothed[i - 1].Bearing = sum.Bearing / wide;
   GroundDiffSmoothed[i - 1].Elevation = sum.Elevation / wide;
   std::cout << GroundDiffSmoothed[i - 1].Bearing << " ";
  }
  std::cout << std::endl;
  std::vector<Spherical2DGroundCoords> ResultingGround_Copy(SC_ResultingGround.begin(), SC_ResultingGround.end());
  std::cout << SC_ResultingGround[0].Bearing << " ";
  for (size_t i = 1; i < NmSmSC; ++i) {
   SC_ResultingGround[i] = ResultingGround_Copy[i - 1] + GroundDiffSmoothed[i - 1];
   std::cout << SC_ResultingGround[i].Bearing << " ";
  }
  std::cout << std::endl;
  SC_Ground[NmSmSC - 1] = SC_ResultingGround[NmSmSC - 1];
 }

}

template <typename T, typename Array>
void NormalizeCoordinateArray(Array& array, typename T::CoordType T::* Coord) {
 typename T::CoordType min = (*array.begin()).*Coord;
 typename T::CoordType max = (*array.begin()).*Coord;
 for (typename Array::const_iterator i = array.begin(); i != array.end(); ++i) {
  min = std::min(min, (*i).*Coord);
  max = std::max(max, (*i).*Coord);
 }
 if (min < 0) {
  for (typename Array::iterator i = array.begin(); i != array.end(); ++i) {
   (*i).*Coord += MATH_CONST::M_2PI;
   (*i).*Coord = std::min((*i).*Coord, MATH_CONST::M_2PI);
  }
 }
 if (max > MATH_CONST::M_2PI) {
  for (typename Array::iterator i = array.begin(); i != array.end(); ++i) {
   (*i).*Coord -= MATH_CONST::M_2PI;
   (*i).*Coord = std::max((*i).*Coord, 0.0);
  }
 }
}

template<typename T, typename Array>
void NormalizeResultingCoordinateArray(Array& array, typename T::CoordType T::* Coord) {
 typename T::CoordType min = (*array.begin()).*Coord;
 typename T::CoordType max = (*array.begin()).*Coord;
 for (typename Array::const_iterator i = array.begin(); i != array.end(); ++i) {
  min = std::min(min, (*i).*Coord);
  max = std::max(max, (*i).*Coord);
 }
 if ((max - min) > M_PI)
  for (typename Array::iterator i = array.begin(); i != array.end(); ++i) {
   if ((*i).*Coord > M_PI)
    (*i).*Coord -= MATH_CONST::M_2PI;
  }
}

void ExtrapolationPolynomials::Collision::Evaluate_Full() { //as WCM
 const Spherical2DShipCoords LastShip = SC_Ship.back();
 Spherical2DGroundCoords rGround = InOut.Ground;
 Spherical2DShipCoords rShip = InOut.Ship;
 const double dt = InOut.ShotTime - SC_Time.back();
 if (dt < 0.00001) return;

 SC_Time.erase(SC_Time.begin());
 SC_Ground.erase(SC_Ground.begin());
 SC_Ship.erase(SC_Ship.begin());

 const Spherical2DGroundCoords LastGround = SC_Ground.back();
 Spherical2DGroundCoords Ms_dGround[3] = {
  InOut.Ground - LastGround,
  InOut.Ground - LastGround - MATH_CONST::M_2PI,
  InOut.Ground - LastGround + MATH_CONST::M_2PI
 };
 LimitCoordinate(dt, &Spherical2DGroundCoords::Bearing, LastGround, Ms_dGround, ThGSGround, rGround);
 NormalizeRadians(rGround.Bearing);

 LimitCoordinate(dt, &Spherical2DGroundCoords::Elevation, LastGround, Ms_dGround, ThGSGround, rGround);

 Spherical2DShipCoords Ms_dShip[3] = {
  InOut.Ship - LastShip,
  InOut.Ship - LastShip - MATH_CONST::M_2PI,
  InOut.Ship - LastShip + MATH_CONST::M_2PI
 };
 LimitCoordinate(dt, &Spherical2DShipCoords::Heading, LastShip, Ms_dShip, ThGSShip, rShip);
 NormalizeRadians(rShip.Heading);

 LimitCoordinate(dt, &Spherical2DShipCoords::Elevation, LastShip, Ms_dShip, ThGSShip, rShip);

 SC_Time.push_back(InOut.ShotTime);
 assert(SC_Time.size() == NmSmSC);
 SC_Ground.push_back(rGround);
 assert(SC_Ground.size() == NmSmSC);
 SC_Ship.push_back(rShip);
 assert(SC_Ship.size() == NmSmSC);

 NormalizeCoordinateArray(SC_Ground, &Spherical2DGroundCoords::Bearing);
 NormalizeCoordinateArray(SC_Ship, &Spherical2DShipCoords::Heading);

 // SC_TimeRelative[i] = SC_Time[i] - ShotTime;
 std::transform(SC_Time.begin(), SC_Time.end(), SC_TimeRelative.begin(), std::bind2nd(std::minus<double>(), InOut.ShotTime));

 //Resulting for A
 std::copy(SC_Ground.begin(), SC_Ground.end(), SC_ResultingGround.begin());
 NormalizeResultingCoordinateArray(SC_ResultingGround, &Spherical2DGroundCoords::Bearing);

 //Resulting for Q
 std::copy(SC_Ship.begin(), SC_Ship.end(), SC_ResultingShip.begin());
 NormalizeResultingCoordinateArray(SC_ResultingShip, &Spherical2DShipCoords::Heading);

 std::fill_n(InOut.PnGround, 5, Spherical2DGroundCoords());
 std::fill_n(InOut.PnShip, 5, Spherical2DShipCoords());

 // Count Inverce matrix
 InverseMatrix IM(PolynomialDegree + 1);
 IM.Evaluate(&SC_TimeRelative[0], NmSmSC);

 EvaluateOrdinalLeastSquaresPolynomial(&SC_TimeRelative[0], &Spherical2DGroundCoords::Bearing, &SC_ResultingGround[0], NmSmSC, IM, InOut.PnGround);
 EvaluateOrdinalLeastSquaresPolynomial(&SC_TimeRelative[0], &Spherical2DGroundCoords::Elevation, &SC_ResultingGround[0], NmSmSC, IM, InOut.PnGround);
 EvaluateOrdinalLeastSquaresPolynomial(&SC_TimeRelative[0], &Spherical2DShipCoords::Heading, &SC_ResultingShip[0], NmSmSC, IM, InOut.PnShip);
 EvaluateOrdinalLeastSquaresPolynomial(&SC_TimeRelative[0], &Spherical2DShipCoords::Elevation, &SC_ResultingShip[0], NmSmSC, IM, InOut.PnShip);
}

void ExtrapolationPolynomials::Collision::Evaluate_Trivial() {
 std::fill_n(InOut.PnGround, 5, Spherical2DGroundCoords());
 std::fill_n(InOut.PnShip, 5, Spherical2DShipCoords());
 InOut.PnGround[0] = InOut.Ground;
 InOut.PnShip[0] = InOut.Ship;
}

void ExtrapolationPolynomials::Collision::Evaluate() {
 if (Scheme == esFULL) {
  Evaluate_Full();
  return;
 }
 if (Scheme == esTRIVIAL) {
  Evaluate_Trivial();
  return;
 }
}

inline constexpr auto ElevMax = M_PI_2;
inline constexpr auto ElevMin = -M_PI_2;

Spherical2DGroundCoords ExtrapolationPolynomials::Collision::InOut::GroundVal(double dt) const {
 Spherical2DGroundCoords _ {
  EvaluatePolynomial(PnGround, &Spherical2DGroundCoords::Bearing, dt, PolynomialDegree),
  EvaluatePolynomial(PnGround, &Spherical2DGroundCoords::Elevation, dt, PolynomialDegree)
 };
 NormalizeRadians(_.Bearing);

 _.Elevation = std::max(ElevMin, _.Elevation);
 _.Elevation = std::min(ElevMax, _.Elevation);
 return _;
}

Spherical2DShipCoords ExtrapolationPolynomials::Collision::InOut::ShipVal(double dt) const {
 Spherical2DShipCoords _ {
  EvaluatePolynomial(PnShip, &Spherical2DShipCoords::Heading, dt, PolynomialDegree),
  EvaluatePolynomial(PnShip, &Spherical2DShipCoords::Elevation, dt, PolynomialDegree)
 };
 NormalizeRadians(_.Heading);

 _.Elevation = std::max(ElevMin, _.Elevation);
 _.Elevation = std::min(ElevMax, _.Elevation);
 return _;
}

const Spherical2DShipCoords& ExtrapolationPolynomials::Collision::InOut::GetPnShip(size_t n) const {
 return PnShip[std::min<decltype(n)>(n, 4)];
}

const Spherical2DGroundCoords& ExtrapolationPolynomials::Collision::InOut::GetPnGround(size_t n) const {
 return PnGround[std::min<decltype(n)>(n, 4)];
}

