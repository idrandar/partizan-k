/*
 * CollisionTable.hh
 *
 *  Created on: 04.08.2015
 *      Author: Drandar
 */

// collision table
// (C) I.Drandar, 2015-2017, all rights reserved
// (C) I.Drandar, for C++1x + Qt, 2018, all rights reserved

#ifndef CRENDT_HH_
#define CRENDT_HH_

#include "common/Coordinates.h"
#include "Filter.h"
#include "GunX.h"
#include <cmath>
#include "DebugOut.h"

// This class is used for initialization of class CollisionTable and its descendants
// by passing it as a constructor parameter.
// In its turn WeaponInit itself must be initialized by parameters read from the setup file
class WeaponInit {
 // Name of weapon. Used in diagnostic messages
 QString name;
 // Type of weapon. Used in diagnostic messages
 QString type;
 ConstTable constants; // list of user-tuned real constants
public:
 WeaponInit(
  QString Name, QString Type, const ConstTable& Constants = ConstTable()) :
  name(Name), type(Type), constants(Constants) {}
 QString Name() const { return name; }
 QString Type() const { return type; }
 Compound GetConst(const QString& Category, const QString& Name) const;
};
typedef StringKeyMap<WeaponInit> WeaponInitMap;

class CollisionTable {
 const QString Name;
 static const double dt_Shot_Nvg_max;
 static const double dt_Shot_Filter_max;
protected:
 const Cartesian3DCoords GunOnShip; // Coordinates gun on ship, m
 struct BallisticData {
  double dPressure; //  mm rtut stolb
  double dTemperature; //  grad C
  double StartSpeedMismatch; //  m/s
  double MassMismatch; // %
  BallisticData() : dPressure(), dTemperature(), StartSpeedMismatch() {
  }
 };
 enum TablePos {
  tpInside, tpRowLess, tpRowMore, tpColLess, tpColMore
 };
 struct RetType {
  TablePos pos;
  double value;
 };
 struct CorrectionPair {
  RetType ToDistance;
  RetType ToHeight;
 };
 friend CorrectionPair operator+(const CorrectionPair& l, const CorrectionPair& r) {
  const CorrectionPair _ = {
   { l.ToDistance.pos, l.ToDistance.value + r.ToDistance.value },
   { l.ToHeight.pos, l.ToHeight.value + r.ToHeight.value },
  };
  return _;
 }
 friend std::ostream& operator<<(std::ostream&, const CorrectionPair&);
 friend std::ostream& operator<<(std::ostream&, TablePos);

 const bool DumpPermitted;
 const struct TestDump {
  const bool ShotTime;
  const bool NSTimeOffset;
  const bool ShipCoordinates;
  const bool ShipRotations;
  const bool ProjectileStartCoordinates;
  const bool FilterTimeOffset;
  const bool TargetCoordinates;
  const bool DistanceCurrent;
  const bool DistanceMeet;
  const bool NoSolution;
  const bool CollisionCoordinates;
  const bool AdvancedCoordinates;
  const bool AdvancedDistanceXY;
  const bool ShootingBearing;
  const bool ShootingElevation;
  const bool FlightTime;
  const bool AdvancedDistance;
  TestDump(const WeaponInit&, const QString&);
 } TestDump;
 template <typename ...Args>
 void DumpIf(bool Condition, Args&&... args) {
  if (DumpPermitted && Condition) {
   std::cout << Name << ": " << std::fixed;
   (std::cout << ... << std::forward<Args>(args)) << "\n";
  }
 }

 // Evaluate && DoEvalute
 double dt_Shot_Nvg;
 Cartesian3DCoords ShipCart;
 Rotations ShRot;
 Cartesian3DCoords ProjectileCoords;
 double dt_Shot_Flt;
 Cartesian3DCoords TargetCoords;
 double DistanceCurrent;
 Spherical2DShipCoords ShipRaw;
public:

 enum SolutionResult {
  srOK,
  srTargetTooLow,
  srTargetTooHigh,
  srTargetTooNear,
  srTargetTooFar,
  srTableIsIncorrect,
  srInvalidInputData,
  srFlightTimeTooBig,
  srTooManyIterations,
  srCalculationsNotNeeded,
  srTargetCalulationError,
  srUnknown
 };
 static SolutionResult ToSr(TablePos);

 struct AllCorrections {
  CorrectionPair Tailwind;
  CorrectionPair Dencity; // air dencity
  CorrectionPair SpeedMismatch;
  CorrectionPair MassMismatch;
  Spherical2DCoords ShipMotion;
  double Derivation;
  double Crosswind;
  double AimingAngle;
 };

 struct OutData {
  double ShotTime;
  bool HaveSolution;
  SolutionResult Result;
  Spherical2DGroundCoords Ground;
  Spherical2DShipCoords Ship;
  double EvaluatedFlightTime; //  counted      time of flighing shell
  double DistanceCurrent;
  double DistanceMeet;
  AllCorrections Corrections;
  OutData() = default;
  OutData(double TimeShot, bool HaveSolution, const Spherical2DGroundCoords& pntGround,
   double TmFlt_Cnt, double TgD_tShot, double TgD_tRend, AllCorrections corr) :
   ShotTime(TimeShot), HaveSolution(HaveSolution), Result(srOK), Ground(pntGround), Ship(),
   EvaluatedFlightTime(TmFlt_Cnt), DistanceCurrent(TgD_tShot), DistanceMeet(TgD_tRend), Corrections(corr) {}
  OutData(double TimeShot, SolutionResult Result, double TgD_tShot, double TgD_tRend, AllCorrections corr) :
   ShotTime(TimeShot), HaveSolution(), Result(Result), Ground(), Ship(),
   EvaluatedFlightTime(), DistanceCurrent(TgD_tShot), DistanceMeet(TgD_tRend), Corrections(corr) {}
  OutData(double TimeShot, SolutionResult Result, double TgD_tShot, AllCorrections corr) :
   ShotTime(TimeShot), HaveSolution(), Result(Result), Ground(), Ship(),
   EvaluatedFlightTime(), DistanceCurrent(TgD_tShot), DistanceMeet(), Corrections(corr) {}
 };

 struct Ballistic {
  double StartSpeedMismatch; //  m/s
  double MassMismatch; //  kg
  Ballistic() :
   StartSpeedMismatch(0.0), MassMismatch(0.0) {
  }
 };
 CollisionTable(const QString&, const WeaponInit&, const Cartesian3DCoords&, bool);
 virtual ~CollisionTable() {}
 struct ImportData {
  struct Weather Weather;
  struct Ballistic Ballistic;
  Spherical2DShipCoords ShipRaw;
  Cartesian3DCoords OEU_Location;
  virtual ~ImportData() {}
 };
 virtual void Import(const ImportData&) = 0;
 OutData Evaluate(const double, const struct FilterModule::OutData&, const ExtrapolationPolynomials::NS&);
 virtual OutData DoEvaluate(const double, const struct FilterModule::OutData&, const ExtrapolationPolynomials::NS&) = 0;
};

std::ostream& operator<<(std::ostream&, CollisionTable::SolutionResult);
#endif /* CRENDT_HH_ */
