#ifndef NAVMC_HH_
#define NAVMC_HH_

/* The author: Vladislav Guyvoronskiy                */
/* Date of creation: 23 March 2007                   */
/* Processing data from navigation complex           */
/* IDE Momentics & QNX Neutrino 6.3.0                */
// Modified for "Sarmat" project
// Modified for "Owl" project
// (C) I.Drandar, 2010-2018, all rights reserved
// Module name changed
// (C) I.Drandar, for Qt, 2018-2019, all rights reserved

#include "common/ExchangeModule.h"
#include "data_types.h"

struct NS { // works as namespace
 typedef NOTHING OutputData;
 struct InputData {
  Rotations Rot;
  Rotations RotChange;
  Cartesian3DCoords SpeedPart;
  struct Faults {
   bool HeaderChecksum;
   bool DataChecksum;
   bool Count;
   bool General;
   bool Off;
   bool Any() const {
    return HeaderChecksum || DataChecksum || Count || General || Off;
   }
  } Faults;
  double LTSTime;
 };

 struct CommandVector {};

 struct StatusVector {
  NS_Data Processed;
  double AngleCheck;
 };

 struct InputPacket { // Packet description
  // types
  typedef Bits7<Upscale<int, 20861>, 14> RotChangeAngle;
  typedef Bits7<Upscale<int, 256>, 14> SpeedPartType;
  // fields
  Packed::SignedAngle20 Pitch;
  Packed::SignedAngle20 Rolling;
  Packed::UnsignedAngle20 Heading;
  SpeedPartType SpeedNorth;
  SpeedPartType SpeedEast;
  SpeedPartType SpeedDown;
  RotChangeAngle RollingChange;
  RotChangeAngle PitchChange;
  RotChangeAngle HeadingChange;
  std::byte Gap0[2];
  struct Faults {
   bool Gap : 2;
   bool HeaderChecksum : 1;
   bool DataChecksum : 1;
   bool Count : 1;
   bool General : 1;
   bool Off : 1;
  } Faults;
  Packed::LTS_Type LTSTime;
  InputPacket() = default;
 };
 typedef NOTHING OutputPacket;

};

std::ostream& operator<<(std::ostream&, const struct NS::InputData::Faults&);

class ModuleNS : public ExchangeModuleImpl<NS> {
 const struct FakeData {
  const bool use;
  const Cartesian3DCoordsType<Function> Speed;
  const RotationsType<Function> Rot;
  const RotationsType<PrimitiveFunction> RotChange;
  FakeData(const ModuleInit&, const QString&, double);
 } Fake;
 void DecodePacket(bool) override;
 void EncodePacket(bool) override {} // no output packet
 void PrepareStatusVector(bool) override;
public:
 explicit ModuleNS(const ModuleInit&);
 void Act() override;
}; // end

#endif /*NAVMC_HH_*/
