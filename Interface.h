/*
 * Interface.h
 *
 *  Created on: 25.01.2013
 *      Author: Drandar
 */
// define type describing visual interface on high abstraction level
// (C) I.Drandar, 2013-2020, all rights reserved

#ifndef INTERFACE_H_
#define INTERFACE_H_

#include "data_types.h"
#include "LRF.h"
#include "NS.h"
#include "Comp2.h"
#include "GTC.h"
#include "TV.h"
#include "LTS.h"
#include "GTC-2.h"
#include "GunHA.h"
#include "GunTimer.h"
// partizan stuff
#include "GTC-R-Mode.h"
#include "PlannedFrequencies.h"
#include "RadialSpeed.h"
#include "RadarETD.h"
#include "RadarSS.h"
#include "RadarControl.h"
#include "RadarTraining.h"
#include "ETD_DetectionData.h"
#include "SS_Data.h"
#include "RadarTestResult.h"
#include "Comp2Radar.h"
// end of partizan stuff
//#include "CombatModule.hh"
#include "Training.h"
#include "collision/Task.h"
#include "common/Record.h"

namespace Interface {

struct Status {
 bool Shutdown;       // выключение комплекса
 Status() : Shutdown() {}
};

template<typename T>
struct DisplayRecord {
 struct OutRecord : public T::OutData {
  unsigned int Received;
  unsigned int ReceptionFaults;
  void Assign(const typename T::OutData& _) {
   *static_cast<typename T::OutData*>(this) = _;
  }
 } Out;
 struct InRecord : public T::InData {
  unsigned int Transmitted;
  void Assign(const typename T::InData& _) {
   *static_cast<typename T::InData*>(this) = _;
  }
 } In;
 typename T::StatusVector Status;
 typename T::CommandVector Command;
 template <typename T0=T>
 void Assign(const Record<T0>& _) {
  Out.Assign(_.Out());
  Out.Received = _.Received();
  Out.ReceptionFaults = _.ReceptionFaults();
  In.Assign(_.Command.data);
  In.Transmitted = _.Transmitted();
  Status = _.Status;
 }
};

struct Command {
 struct _System {  
  struct _State { // панель Состояние
   SystemMode Mode; // режим системы
   bool ModeComplete; // установка режима окончена
   bool Fault; // ошибка системы
   bool TestingCompleted;
   SystemMode IntegralMode() const;
  } State;
  struct TD { // панель целеуказания
   bool Accept; // принять целеуказание
   double Distance; // дальность
   double Bearing; // курсовой угол целеуказания
   double Elevation; // угол места целеуказания
  } TD;
  enum Fault {
   ffNone,
   ffExchangeSerial,
   ffExchangeEthernet,
   ffLTS,
   ffNavData,
   ffTestingCompletedFault,
   ffTestingCompletedSuccess,
  };
  Fault FuncFault; // есть функциональная ошибка
 } System; // данные для панели "Система"
 struct Main {
  double Interval;
  struct External { // внешние абоненты
   DisplayRecord<ModuleNS> NS;
   DisplayRecord<ModuleGun> GunHA;
   DisplayRecord<ModuleGun> GunElev;
   DisplayRecord<ModuleGunTimer> GunTimer;
   DisplayRecord<ModuleGunX> GunX;
   struct Radar {
    DisplayRecord<ModuleGTCRMode> Mode;
    DisplayRecord<ModulePlannedFrequencies> PlannedFrequencies;
    DisplayRecord<ModuleRadialSpeed> RadialSpeed;
    DisplayRecord<ModuleRadarETD> ETD;
    DisplayRecord<ModuleRadarSS> SS;
    DisplayRecord<ModuleRadarControl> Control;
    DisplayRecord<ModuleRadarTraining> Training;
    DisplayRecord<ModuleETD_Detection> ETD_Detection;
    DisplayRecord<ModuleSS_Data> SS_Data;
    DisplayRecord<ModuleRadarTest> Test;
   } Radar;
  } External;
  struct Internal { // внутренние абоненты
   DisplayRecord<ModuleGTC> GTC;
   DisplayRecord<ModuleDAT> DAT[DAT_CHANNELS_COUNT];
   DisplayRecord<ModuleComp2> Comp2;
   DisplayRecord<ModuleLTS> LTS;
   DisplayRecord<ModuleLRF> LRF;
   DisplayRecord<ModuleTraining> Training;
   DisplayRecord<ModuleIR> IR;
   DisplayRecord<ModuleTV> TV;
   DisplayRecord<ModuleGTC2> GTC2;
   DisplayRecord<ModuleComp2Radar> Comp2Radar;
  } Internal;
  struct Evaluations {
   struct Filter {
    FilterModule::CommandVector In;
    FilterModule::StatusVector Out;
   };
   Filter FilterOptical;
   Filter FilterRadar;
   struct Collision {
    CollisionTaskModule::CommandVector In;
    CollisionTaskSolverModule::CommandVector Solver_In;
    CollisionTaskModule::StatusVector Out;
    CollisionTaskSolverModule::StatusVector Solver_Out;
   };
   Collision CollisionOptical;
   Collision CollisionRadar;
  } Evaluations;
  struct SysUnitStruct { // структура ошибок системы
   struct U41 { // ошибки прибора 4.1
    bool comExtTEWS; // ошибка расширителя COM-порта на УЭВМ
    bool comExtEthernet; // ошибка расширителя COM-порта на БЭВМ
   } u51;
  } Faults; // данные по ошибкам системы
 } Main;
 Comp2::SysMode Mode;
};

}

#endif /* INTERFACE_H_ */
