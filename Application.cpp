/* The author: Aleksander Ketner                  */
/* Date of creation: 02 August 2007               */
/* Principal function of the design the simulator */
/* IDE Momentics & QNX Neutrino 6.3.0             */
// Modified for "Sarmat" project
// (C) I.Drandar, 2010-2014, all rights reserved
// (C) I.Drandar for Linux + Qt, 2018, all rights reserved

#include "Application.h"
#include <iostream>
#include "systemClass.h"
#include "Setup.h"

const char Application::name[] = "Partizan-K";

Application::Application() {
 TSystemClass SysObj;
 std::cout << "Welcome to the " << name << "\n" << name << " system is starting\n";
 std::cout << "Time of system start:\t" << SysObj.OSBootTimeStr();
 std::cout << "Time of program start:\t" << SysObj.ProgramStartTime();
 std::cout << "System:\t" << SysObj.GetOsName() << '\n';
 std::cout << "Version:\t" << SysObj.GetOsVersion() << '\n';
 std::cout << "Build:\t" << SysObj.GetOsBuild() << '\n';
 std::cout << "Type of architecture: " << SysObj.GetPlatform() << '\n';
 std::cout << "Name of net node: " << SysObj.GetNodeName() << '\n';
 std::cout << "All memory size:\t" << SysObj.GetTotalMemorySize() << " bytes\n";
 std::cout << "Free memory size:\t" << SysObj.GetFreeMemory() << " bytes\n";
 std::cout << "Share memory size:\t" << SysObj.GetSharedMemorySize() << " bytes\n";
 std::cout << "Number of CPUs:\t" << SysObj.GetCpuCount() << '\n';
 std::cout << "Type of CPU:\t" << SysObj.GetCPUType() << '\n';
 std::cout << "CPU Frequency is:\t " << SysObj.GetCPUFreq() << " Hz\n";
 SysObj.SetMaximumPriority(99);
 auto rl = SysObj.GetMaximumPriority();
 std::cout <<
  "Max priority limit:\tmax: " << rl.rlim_max << "\tcurrent: " << rl.rlim_cur << std::endl;
 SysObj.SetDMALatency(0);
 setup = std::make_unique<Setup>((std::string(name) + ".xml").c_str());
} // End imitator_create

// This is close simulator system
Application::~Application() {
 std::cout << __PRETTY_FUNCTION__ << std::endl;
 cmxStrct->Stop();
 delete cmxStrct;
 setup.release();
 std::cout << name << " system is closing\nGood Bye!\n";
} // end imitator_destroy

void Application::Start() {
 std::cout << "Please wait while \"" << name << "\" system is starting\n";
 cmxStrct = new Worker(setup->GetModuleInit("Dispatcher"));
 cmxStrct->Start();
 std::cout  << name << " now is running...\n";
}
//========================= The end of imitator.cc file ==========================

