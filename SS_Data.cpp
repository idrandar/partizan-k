// radar SS mode data
// (C) I.Drandar, 2019, all rights reserved
// Guidance and tracking controller - Radar

#include "SS_Data.h"
#include "DebugOut.h"

ModuleSS_Data::FakeData::FakeData(const ModuleInit& init, const QString& s) :
 use(init.GetConst(s, "use")) {
}

ModuleSS_Data::ModuleSS_Data(const ModuleInit& init) :
 BASE(init), Fake(init, "fake") {
}

void ModuleSS_Data::Act() {
 CommitReception();
}

void ModuleSS_Data::DecodePacket(bool dump) {
 Input.OFT = static_cast<Radar::OperatingFrequencyTuning>(input.OFT);
 Input.FrequencyCode = input.FrequencyCode;
 Input.RepetitionPeriodCode = input.RepetitionPeriodCode;
 Input.RPA = static_cast<Radar::RepetitionPeriodAdjustment>(input.RPA);
 Input.TargetCount = input.TargetCount;
 Input.ANP = static_cast<Radar::ActiveNoisePresence>(input.ANP);
 Input.ActiveNoiseInterferenceProcessing = input.ActiveNoiseInterferenceProcessing;
 Input.LTS = input.LTS; // LTS time
 if (dump) {
  std::cout <<
   "Dump of received " << GetName() << "::Input packet\n"
   "operating frequency tuning: " << enum2str(Input.OFT) << "\t"
   "frequency code: " << Input.FrequencyCode << "\n"
   "repetition period code: " << Input.RepetitionPeriodCode << "\t"
   "repetition period adjustment: " << enum2str(Input.RPA) << "\n"
   "active noise interference processing: " << Input.ActiveNoiseInterferenceProcessing << "\t"
   "active noise presence: " << enum2str(Input.ANP) << "\t"
   "count of targets: " << Input.TargetCount << "\n"
   "LTS time: " << Input.LTS << "\n";
 }
}

void ModuleSS_Data::PrepareStatusVector(bool) {
 if (Fake.use) {
  Status.NewInfo = true;
  Status.ExchangeFault = false;
  auto& s = Status.data;
  s.OFT = Radar::OperatingFrequencyTuning{};
  if (s.OFT == Radar::OperatingFrequencyTuning::Random) {
   s.FrequencyCode = 0;
  }
  s.RPA = Radar::RepetitionPeriodAdjustment{};
  if (s.RPA == Radar::RepetitionPeriodAdjustment::Wobble) {
   s.RepetitionPeriodCode = 0;
  }
  s.TargetCount = 0;
  s.ANP = Radar::ActiveNoisePresence{};
  s.ActiveNoiseInterferenceProcessing = s.TargetCount == 1;
  s.LTS = counter.TimeFromProgramStart();
 }
}
