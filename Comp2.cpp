// (C) I.Drandar, 2015-2017, all rights reserved
// (C) I.Drandar, for Qt + C++1x, 2018-2020, all rights reserved

#include "Comp2.h"
#include "DebugOut.h"

std::ostream& operator<<(std::ostream& _, Comp2::SysMode M) {
 switch (M) {
 case Comp2::mWork:
  _ << "Work";
  break;
 case Comp2::mTesting:
  _ << "Testing";
  break;
 case Comp2::mTraining:
  _ << "Training";
  break;
 default:
  _ << "Unknown mode";
 }
 return _;
}

std::ostream& operator<<(std::ostream& _, const struct Comp2::InputData::Control::Training& T) {
 _ <<
  "Code: " << T.Code << "\tStationary Target: " << T.StationaryTarget << "\tOn: " << T.On << "\n"
  "Stationary Target Coordinates:\t" << T.StationaryTargetCoords;
 return _;
}

std::ostream& operator<<(std::ostream& _, const struct Comp2::InputData::Control& C) {
 _ <<
  "mode: " << C.Mode << "\tsubmode: " << C.SubMode << "\t"
  "IIT enable: " << C.IIT_Enable << "\tadditional search: " << C.AdditionalSearch << "\n"
  "heat standby: " << C.HeatStandby << "\tfire: " << C.Fire << "\t"
  "wiper on: " << C.WiperOn << "\tinternal heating on: " << C.InternalHeatingOn << "\t"
  "camera: " << C.Camera << "\n"
  "DAT channel: " << C.DATChannel << "\tradar: " << C.Radar << "\t"
  "low flying target: " << C.LowFlyingTarget << "\n"
  "training:\t" << C.Training << "\nExternal Target: " << C.ExternalTarget << "\n"
  "joystick:\t" << C.Joystick << "\n" <<
  "manual distance on: " << C.ManualDistanceOn << "\tmanual distance: " << C.ManualDistance << "\n";
 return _;
}

std::ostream& operator<<(std::ostream& _, const struct Comp2::InputData::IR& I) {
 _ <<
  "menu:\ton: " << I.Menu.On << "\t"
  "contrast\t+: " << I.ContrastPlus << "\t-:" << I.ContrastMinus << "\n"
  "brightness\t+: " << I.BrightnessPlus << "\t-: " << I.BrightnessMinus << "\n"
  "hot black: " << I.HotBlack << "\n"
  "optical zoom:\tplus: " << I.OpticalZoomPlus << "\tminus: " << I.OpticalZoomMinus << "\t"
  "digital zoom: " << I.DigitalZoom << "\n"
  "focus far: " << I.FocusFar << "\tfocus near: " << I.FocusNear << "\t"
  "calibration: " << I.Calibration << "\n"
  "brightness: " << I.Brightness << "\t"
  "conrast: " << I.Contrast << "\t"
  "center correction: " << I.CenterCorrection;
 return _;
}

std::ostream& operator<<(std::ostream& _, const struct Comp2::InputData::LRF::Signals& S) {
 _ <<
  "Work: " << S.Work << "\tFrequent: " << S.Frequent << "\tSingle: " << S.Single;
 return _;
}

std::ostream& operator<<(std::ostream& _, const struct Comp2::InputData::GunControl::BallisticData& B) {
 _ <<
  "Start Speed Mismatch: " << B.StartSpeedMismatch << " m/s\t"
  "Mass Mismatch: " << B.MassMismatch << " kg";
 return _;
}

std::ostream& operator<<(std::ostream& _, const struct Comp2::InputData::GunControl& C) {
 _ <<
  "drive control:\n" << "\tgun control: " << C.DriveControl.GunControl << "\t"
  "remote fuse control: " << C.DriveControl.RemoteFuseControl << "\n"
  "joint alignment: " << C.DriveControl.JointAlignment << "\t"
  "parallax joint alignment: " << C.DriveControl.ParallaxJointAlignment << "\n"
  "distance: " << C.DriveControl.Distance << " m\n"
  "drive corrections  (°):\npointing angle mismatch:\t" << C.DriveCorrections.PointingAngleMismatch << "\n"
  "situation angle mismatch:\t" << C.DriveCorrections.SituationAngleMismatch << "\n" <<
  "miss:\t" << C.DriveCorrections.Miss << "\n"
  "ballistic data:\n" << C.BalData << "\n";
 return _;
}

std::ostream& operator<<(std::ostream& _, const struct Comp2::OutputData::State& S) {
 _ <<
  "mode: " << S.Mode << "\tsubmode: " << S.SubMode << "\t"
  "additional search: " << S.AdditionalSearch << "\t"
  "low flying target: " << S.LowFlyingTarget << "\t"
  "mode is changing: " << S.ModeIsChanging << "\n"
  "ready:\tTV: " << S.Ready.TV << "\t:IR: " << S.Ready.IR << "\tnavigation data: " << S.Ready.Navig;
 return _;
}

std::ostream& operator<<(std::ostream& _, const struct Comp2::OutputData::GTC_State::DAT_State::Channel& C) {
 _ <<
  "processing method: " << C.Method << "\tbreak: " << C.Break << "\t"
  "capture: " << C.Capture << "\tdetection: " << C.Detection << "\t"
  "fault: " << C.Fault << "\tmode: " << C.Mode << "\ttesting completed: " << C.TestingCompleted << "\n"
  "credibility: " << C.Credibility << "\ttarget type: " << C.TT << "\n"
  "autotracking mismatch (°):\t" << C.AutoTrackingMismatch << "\n";
 return _;
}

std::ostream& operator<<(std::ostream& _, const Comp2::OutputData::GTC_State::DAT_State& D) {
 _ <<
  "Channel #1:\n" << D.Channel[DAT_CHANNEL_1] << "\nChannel #2:" << D.Channel[DAT_CHANNEL_2];
 return _;
}

std::ostream& operator<<(std::ostream& _, const struct Comp2::OutputData::BallisticModule& B) {
 _ <<
 "Mode " << B.Mode << "\tSubmode: " << B.SubMode << "\n"
 "State:\nTesting Completed: " << B.State.TestingCompleted << "\t"
 "pointing angle ready: " << B.State.PointingAngleReady  << "\n"
 "Exchange Faults:";
 return _;
}

std::ostream& operator<<(std::ostream& _, const Comp2::OutputData::GunState& C) {
 _ <<
  "tower position (°) " << C.TowerPosition << "\tdesired angle (°) " << C.DesiredAngle << "\n"
  "distance: " << C.CurrentDistance << " m\tmeet distance: " << C.MeetDistance << " m\t"
  "flight time: " << C.FlightTime << "s";
 return _;
}

std::ostream& operator<<(std::ostream& _, const Comp2::OutputData::GTC_State& G) {
 _ <<
  "general state: " << G.State << "\n"
  "power: " << G.Power << "\tunit 7.2 on: " << G._7_2 << "\t"
  "DAT channel: " << G.DATChannel << "inner inertial tracker: " << G.IIT << "\n"
  "synchronization:\trequest: " << G.SyncRequest << "\tdone: " << G.SyncDone << "\n"
  "faults:\n"
  "device controller: " << G.Faults.DeviceController << "\tLRF: " << G.Faults.LRF << "\t"
  "TV: " << G.Faults.TV << "\tIR: " << G.Faults.IR << "\tASS" << G.Faults.ASS << "\n"
  "encoder exchange:\t" << G.Faults.EncoderExchange << "\n"
  "encoder:\t" << G.Faults.Encoder << "\t"
  "detent:\t" << G.Faults.Detent << "\n"
  "GTC: " << G.Faults.GTC << "\n"
  "exchange:\tDAT: " << G.Faults.ExchangeDAT << "\tcomp: " << G.Faults.ExchangeComp << "\n"
  "DC: " << G.Faults.ExchangeDC << "\tLTS-DC: " << G.Faults.ExchangeLTS_DC << "\t"
  "navig: " << G.Faults.ExchangeNavig << "\n"
  "ПФ7.2:\toverheat: " << G.Faults.Overheat << "\tno 24 V: " << G.Faults.No24V << "\t"
  "no phase: " << G.Faults.NoPhase << "\tovercurrent: " << G.Faults.Overcurrent << "\t"
  "no 7.2: " << G.Faults.No7_2 << "\n"
  "ПФ1.1.2:\n"
  "amplifier HA engine: " << G.Faults.AmplifierHAEngine << "\t"
  "amplifier ЦП.06 HA: " << G.Faults.AmplifierHAA1 << "\n"
  "amplifier elev engine: " << G.Faults.AmplifierElevEngine << "\t"
  "amplifier ЦП.06 elev: " << G.Faults.AmplifierElevA3 << "\n"
  "exchange Q: " << G.Faults.ExchangeQ << "\t"
  "exchange E: " << G.Faults.ExchangeE << "\n"
  "training target: " << G.TrainingTarget << "\n"
  "current distance: " << G.Distance << " m\n"
  "OEUC location:\n" << G.OEUC << "\n"
  "TV field of view:\t" << G.TV << "\n"
  "IR field of view:\t" << G.IR << "\n"
  "DAT state:\n" << G.DAT;
 return _;
}

std::ostream& operator<<(std::ostream& _, const Comp2::OutputData::LRF_State& L) {
 _ <<
  "mode: " << L.Mode << "\treception fault: " << L.ReceptionFault << "\t"
  "control fault: " << L.ControlFault << "\t"
  "temperature ready: " << L.TemperatureReady << "\treadiness: " << L.Readiness << "\n"
  "faults:\n" << L.Faults << "\tstate:\n" << L.State << "\n"
  "measuring: " << L.Measuring << "\n"
  "distance #1: " << L.Distance1 << "\tdistance #2: " << L.Distance2 << "\tdistance #3: " << L.Distance3 << "\n"
  "resource count: " << L.Resource;
 return _;
}

std::ostream& operator<<(std::ostream& _, const struct Comp2::OutputData::Navig& N) {
 _ << "\trotations (°): " << N.Rot << "\n"
  "speed (m/s): " << N.SpeedPart << "\n"
  "rotations change (°/s): " << N.RotChange << "\n"
  "faults:\n" << N.Faults;
 return _;
}

std::ostream& operator<<(std::ostream& _, const Comp2::OutputData& O) {
 _ <<
  "State:\n" << O.State << "\nGTC State:\n" << O.GTC << "\n"
  "LRF State:\n" << O.LRF << "\n"
  "ballistic module state:\n" << O.BallisticModule << "\n"
  "gun state:\n" << O.Gun << "\n"
  "Navigation data:\n" << O.Navig << "\nSending time: " << O.Time << " s\n";
 return _;
}

void Comp2::InputData::Control::operator=(const struct InputPacket::Control& _) {
 Mode = static_cast<SysMode>(_.ModeAndSubmode.Mode);
 SubMode = static_cast<enum SubMode>(_.ModeAndSubmode.SubMode);
 IIT_Enable = _.ModeAndSubmode.IIT_Enable;
 AdditionalSearch = _.ModeAndSubmode.AdditionalSearch;
 WiperOn = _.Command1.WiperOn;
 InternalHeatingOn = _.Command1.InternalHeatingOn;
 Camera = static_cast<DAT::CameraType>(_.Command1.Camera);
 Fire = _.Command1.Fire;
 DATChannel = _.Command2.DATChannel;
 Radar = _.Command2.Radar;
 LowFlyingTarget = _.Command2.LowFlyingTarget;
 AirTarget = _.Command1.AirTarget;
 IROn = _.Command1.IROn;
 HeatStandby = _.Command1.HeatStandby;
 Training = _.Training;
 Training.StationaryTargetCoords = _.StationaryTargetCoords;
 ExternalTarget = _.ExternalTarget;
 Joystick = _.Joystick;
 ManualDistanceOn = _.ManualDistanceOn;
 ManualDistance = _.ManualDistance;
 OEUC = _.OEUC;
}

void Comp2::InputData::Control::Training::operator=(const struct InputPacket::Control::Training& _) {
 Code = _.Code;
 StationaryTarget = _.StationaryTarget;
 On = _.On;
}

void Comp2::InputData::GunControl::operator=(const InputPacket::GunControl& _) {
 DriveControl.GunControl = _.Control;
 DriveControl.RemoteFuseControl = _.RemoteFuseControl;
 DriveControl.JointAlignment = _.DriveControl.JointAlignment;
 DriveControl.ParallaxJointAlignment = _.DriveControl.ParallaxJointAlignment;
 DriveControl.Distance = _.DriveControl.Distance;
 DriveCorrections.Orientation = _.DriveCorrections.Orientation;
 DriveCorrections.PointingAngleMismatch = _.DriveCorrections.PointingAngleMismatch;
 DriveCorrections.SituationAngleMismatch = _.DriveCorrections.SituationAngleMismatch;
 DriveCorrections.Miss = _.DriveCorrections.Miss;
 BalData[0] =_.BallisticData[0];
 BalData[1] =_.BallisticData[1];
}

void Comp2::InputData::GunControl::BallisticData::operator=(
 const struct InputPacket::GunControl::BallisticData& _) {
 Type = _.Type;
 StartSpeedMismatch = _.StartSpeedMismatch;
 MassMismatch = _.MassMismatch;
}

void Comp2::OutputData::GTC_State::DAT_State::Channel::operator=(const DAT::InputData& _) {
 Method = _.Method;
 Break = _.Break;
 Capture = _.Capture;
 TestingCompleted = _.TestingCompleted;
 Detection = _.Detection;
 Fault = _.Fault;
 Mode = _.Mode;
 TT = _.TT;
 Credibility = _.Credibility;
 AutoTrackingMismatch = _.AutotrackingMismatch;
}

Comp2::OutputPacket::State::State(const struct Comp2::OutputData::State& _) :
 Mode(_.Mode), SubMode(_.SubMode),
 AdditionalSearch(_.AdditionalSearch), LowFlyingTarget(_.LowFlyingTarget) {
 Ready.TV = _.Ready.TV;
 Ready.IR = _.Ready.IR;
 Ready.Navig = _.Ready.Navig;
 Ready.ModeIsChanging = _.ModeIsChanging;
}

void Comp2::OutputPacket::GTC_State::operator=(const Comp2::OutputData::GTC_State& _) {
 Mode = _.State.Mode;
 ModeIsChanging = _.State.ModeIsChanging;
 DetentHA = _.State.Detent.HA;
 DetentElev = _.State.Detent.Elev;
 //
 Power = _.Power;
 SyncRequest = _.SyncRequest;
 SyncDone = _.SyncDone;
 _7_2 = _._7_2;
 DATChannel = _.DATChannel;
 IIT = _.IIT;
 HADriveOn = _.OEUC.DriveOn.HA;
 ElevDriveOn = _.OEUC.DriveOn.Elev;
 ElevLimitHigh = _.OEUC.ElevLimitHigh;
 ElevLimitLow = _.OEUC.ElevLimitLow;
 MismatchHA = _.Mismatch.HA;
 MismatchElev = _.Mismatch.Elev;
 // ПФ1.1
 Faults.DeviceController = _.Faults.DeviceController;
 Faults.LRF = _.Faults.LRF;
 Faults.TV = _.Faults.TV;
 Faults.IR = _.Faults.IR;
 Faults.ASS = _.Faults.ASS;
 // motors
 Faults.EncoderHAExchange = _.Faults.EncoderExchange.HA;
 Faults.EncoderElevExchange = _.Faults.EncoderExchange.Elev;
 //
 Faults.DetentHA = _.Faults.Detent.HA;
 Faults.DetentElev = _.Faults.Detent.Elev;
 Faults.EncoderHA = _.Faults.Encoder.HA;
 Faults.EncoderElev = _.Faults.Encoder.Elev;
 // ПФ4.1
 Faults.GTC = _.Faults.GTC; // ЦК.28
 Faults.ExchangeDAT = _.Faults.ExchangeDAT;
 Faults.ExchangeComp = _.Faults.ExchangeComp;
 Faults.ExchangeDC = _.Faults.ExchangeDC;
 Faults.ExchangeLTS_DC = _.Faults.ExchangeLTS_DC;
 Faults.ExchangeNavig = _.Faults.ExchangeNavig;
 // ПФ7.2
 Faults.NoPhase = _.Faults.NoPhase;
 Faults.Overcurrent = _.Faults.Overcurrent;
 Faults.Overheat = _.Faults.Overheat;
 Faults.No24V = _.Faults.No24V;
 Faults.No7_2 = _.Faults.No7_2;
 // ПФ1.1.2
 Faults.AmplifierHAEngine = _.Faults.AmplifierHAEngine;
 Faults.AmplifierHAA1 = _.Faults.AmplifierHAA1;
 Faults.AmplifierElevEngine = _.Faults.AmplifierElevEngine;
 Faults.AmplifierElevA3 = _.Faults.AmplifierElevA3;
 Faults.ExchangeQ = _.Faults.ExchangeQ;
 Faults.ExchangeE = _.Faults.ExchangeE;

 TrainingTarget = _.TrainingTarget;
 Distance = _.Distance;
 OEUC_Coords = _.OEUC;

 TV = _.TV;

 IR = _.IR;
 IR_Temperature = _.IR_Temperature;

 DAT = _.DAT;
}

Comp2::OutputPacket::GTC_State::OEUC_Coords::OEUC_Coords(const struct GTC::InputData::ORU& _) {
 Ground = _.Ground;
 Ship = _.Ship;
}

Comp2::OutputPacket::GTC_State::DAT_State::DAT_State(const Comp2::OutputData::GTC_State::DAT_State& _) :
 Channel1(_.Channel[DAT_CHANNEL_1]), Channel2(_.Channel[DAT_CHANNEL_2]) {}

Comp2::OutputPacket::GTC_State::DAT_State::Channel::Channel(
 const struct Comp2::OutputData::GTC_State::DAT_State::Channel& _) :
 Method(_.Method), Break(_.Break), Capture(_.Capture),
 TestingCompleted(_.TestingCompleted), Fault(_.Fault), Mode(_.Mode),
 Credibility(_.Credibility), Detection(_.Detection), TT(_.TT)  {
 AutoTrackingMismatch = _.AutoTrackingMismatch;
}

Comp2::OutputPacket::LRF_State::LRF_State(const OutputData::LRF_State& _) :
 Mode(_.Mode), Readiness(_.Readiness),
 ReceptionFault(_.ReceptionFault), ControlFault(_.ControlFault),
 TemperatureReady(_.TemperatureReady),
 Distance1(_.Distance1), Distance2(_.Distance2), Distance3(_.Distance3) {
 Faults.FRAM = _.Faults.FRAM;
 Faults.CRC = _.Faults.CRC;
 Faults.IntervalMeasurer = _.Faults.IntervalMeasurer;
 Faults.QSwitch = _.Faults.QSwitch;
 Faults.LPS = _.Faults.LPS;
 Faults.WTF = _.Faults.WTF;
 Faults.OpticalStart = _.Faults.OpticalStart;
 State.Targets = _.State.Targets;
 State.LowPeriod = _.State.LowPeriod;
 State.OnboardNetwork = _.State.OnboardNetwork;
 State.WTF = _.State.WTF;
 State.TestingCompleted = _.State.TestingCompleted;
 State.Failure = _.State.Failure;
 Measuring = _.Measuring;
}

Comp2::OutputPacket::BallisticModule::BallisticModule(const struct Comp2::OutputData::BallisticModule& _) :
 Mode(_.Mode), SubMode(_.SubMode),
 State(_.State), ExchangeFaults(_.ExchangeFaults),
 Limits(_.Limits), Faults(_.Faults) {
}

Comp2::OutputPacket::BallisticModule::State::State(const struct Comp2::OutputData::BallisticModule::State& _) :
 TestingCompleted(_.TestingCompleted), PointingAngleReady(_.PointingAngleReady),
 ITReady(_.ITReady) {
}

Comp2::OutputPacket::BallisticModule::ExchangeFaults::ExchangeFaults(
 const struct Comp2::OutputData::BallisticModule::ExchangeFaults& _) :
 Gun(_.Gun), Navig(_.Navig), GTC(_.GTC), Comp2(_.Comp2) {
}

Comp2::OutputPacket::BallisticModule::Limits::Limits(
 const struct OutputData::BallisticModule::Limits& _) :
 RightHA(_.RightHA), LeftHA(_.LeftHA), HighElev(_.HighElev), LowElev(_.LowElev) {
}

Comp2::OutputPacket::BallisticModule::Faults::Faults(
 const struct OutputData::BallisticModule::Faults& _) :
 GunXExchange(_.GunXExchange),
 GunHAExchange(_.GunHAExchange), GunHA(_.GunHA),
 GunElevExchange(_.GunElevExchange), GunElev(_.GunElev),
 GunTimerExchange(_.GunTimerExchange), GunTimer(_.GunTimer) {
}

Comp2::OutputPacket::GunState::GunState(const OutputData::GunState& _) :
 CurrentDistance(_.CurrentDistance), MeetDistance(_.MeetDistance), FlightTime(_.FlightTime) {
 DesiredAngle = _.DesiredAngle;
 TowerPosition = _.TowerPosition;
}

Comp2::OutputPacket::Navig::Navig(const struct OutputData::Navig& _) :
 Rot(_.Rot), RotChange(_.RotChange), Speed(_.SpeedPart) {
 Faults.HeaderChecksum = _.Faults.HeaderChecksum;
 Faults.DataChecksum = _.Faults.DataChecksum;
 Faults.General = _.Faults.General;
}

Comp2::OutputPacket::OutputPacket(const OutputData& _) :
 State(_.State), LRF(_.LRF), BallisticModule(_.BallisticModule),
 Gun(_.Gun), Navig(_.Navig), Time(_.Time) {
 GTC = _.GTC;
}

ModuleComp2::FakeData::Gun::BallisticData::operator Types::InputData::GunControl::BallisticData() const {
 Types::InputData::GunControl::BallisticData _;
 _.StartSpeedMismatch = StartSpeedMismatch;
 _.MassMismatch = MassMismatch;
 return _;
}

ModuleComp2::FakeData::Gun::BallisticData::BallisticData(
 const ModuleInit& init, const QString& s, const QString& p) :
 StartSpeedMismatch(init.GetConst(s, p + "start-speed-mismatch")),
 MassMismatch(init.GetConst(s, p + "mass-mismatch")) {
}

ModuleComp2::FakeData::Gun::DriveCorrections::DriveCorrections(
 const ModuleInit& init, const QString& s, const QString& p) :
 PointingAngle(init.GetConst(s, p + "pointing")), SituationAngle(init.GetConst(s, p + "situation")),
 Miss(init.GetConst(s, p + "miss")) {
}

ModuleComp2::FakeData::Gun::DriveCorrections::operator Types::DriveCorrections() const {
 Types::DriveCorrections _;
 _.PointingAngleMismatch = PointingAngle;
 _.SituationAngleMismatch = SituationAngle;
 _.Miss = Miss;
 return _;
}

ModuleComp2::FakeData::Gun::Gun(const ModuleInit& init, const QString& s, const QString& p) :
 DriveCorrections(init, s, p + "corr-"), BallisticData(init, s, p) {
}

ModuleComp2::FakeData::Weather::Weather(const ModuleInit& init, const QString& s) :
 WindSpeed(init.GetConst(s, "wind-speed")), WindBearing(init.GetConst(s, "wind-bearing")),
 Pressure(init.GetConst(s, "pressure")),
 Temperature(init.GetConst(s, "temperature")), ChargeTemperature(init.GetConst(s, "charge-temperature"))  {
}

ModuleComp2::FakeData::FakeData(const ModuleInit& init, const QString& s) :
 use(init.GetConst(s, "use")),
 Mode(static_cast<Types::SysMode>(static_cast<int>(init.GetConst(s, "mode")))),
 SubMode(init.GetConst(s, "submode")),
 Gun(init, s, ""), Weather(init, s),
 LRFSingle(init.GetConst(s, "lrf-single")), LRFFrequent(init.GetConst(s, "lrf-frequent")),
 LRFWork(init.GetConst(s, "lrf-work")) {
}

ModuleComp2::ModuleComp2(const ModuleInit& init) :
 BASE(init), Fake(init, "fake") {
// std::cout <<
//  __PRETTY_FUNCTION__ << ": size:\n"
//  "input: " << sizeof input << " = (" <<
//  sizeof input.Control << "=" << sizeof input.Control.ModeAndSubmode << "+" <<
//  sizeof input.Control.Command1 << "+" << sizeof input.Control.Command2 << "+" <<
//  sizeof input.Control.Training << "+" << sizeof input.Control.StationaryTargetCoords << "+" <<
//  sizeof input.Control.ExternalTarget << "+" << sizeof input.Control.Joystick << "+" <<
//  sizeof input.Control.ManualDistanceOn << "+" << sizeof input.Control.ManualDistance << "+" <<
//  sizeof input.Control.OEUC << ")+" <<
//  sizeof input.TV << "+" << sizeof input.IR << "+" <<
//  sizeof input.DAT_Control + sizeof input.Gap + sizeof input.DAT1_Control + sizeof input.DAT2_Control << "+" <<
//  sizeof input.DAT1_Coords + sizeof input.DAT2_Coords << "+" << sizeof input.LRF << "+" <<
//  sizeof input.Weather << "+" << sizeof input.GC << "+" << sizeof input.Time << "\n"
//  "output: " << sizeof output << " = " << sizeof output.State << "+" <<
//  sizeof output.GTC << "+" << sizeof output.LRF << "+" <<
//  sizeof output.BallisticModule << "+" << sizeof output.Gun << "+" <<
//  sizeof output.Navig << "+" << sizeof output.Time <<
//  std::endl;
// "offsetof Faults is " << offsetof(OutputPacket, GTC.Faults) << std::endl;
} // end

void ModuleComp2::DecodePacket(bool dump) { //decode info from comp #2
 Input.Control = input.Control;
 Input.DAT_Control.Rotation = input.DAT_Control.Rotation;
 Input.DAT_Control.Mark = input.DAT_Control.Mark;
 Input.TV.Focus.Manual = input.TV.Focus.Manual;
 Input.TV.Focus.SmoothlyPlus = input.TV.Focus.SmoothlyPlus;
 Input.TV.Focus.SmoothlyMinus = input.TV.Focus.SmoothlyMinus;
 Input.TV.Focus.Near = input.TV.Focus.Near;
 Input.TV.Focus.Smoothly = input.TV.Focus.Smoothly; // false - fixed, true - smoothly
 Input.TV.FOV.SmoothlyPlus = input.TV.FOV.SmoothlyPlus;
 Input.TV.FOV.SmoothlyMinus = input.TV.FOV.SmoothlyMinus;
 Input.TV.FOV.Narrow = input.TV.FOV.Narrow; // false - wide, true - narrow (zoom)
 Input.TV.FOV.Smoothly = input.TV.FOV.Smoothly; // false - fixed, true - smoothly
 Input.TV.Night = input.TV.FOV.Night; // false - day, true - night
 Input.TV.CenterCorrection = input.TV.CenterCorrection;
 Input.TV.Brightness = input.TV.Brightness;
 Input.TV.Contrast = input.TV.Contrast;
 Input.IR.FocusFar = input.IR.Command.FocusFar;
 Input.IR.FocusNear = input.IR.Command.FocusNear;
 Input.IR.HotBlack = input.IR.Command.HotBlack;
 Input.IR.Menu.On = input.IR.Command.MenuOn;
 Input.IR.ContrastPlus = input.IR.Command.ContrastPlus;
 Input.IR.ContrastMinus = input.IR.Command.ContrastMinus;
 Input.IR.BrightnessPlus = input.IR.Command.BrightnessPlus;
 Input.IR.BrightnessMinus = input.IR.Command.BrightnessMinus;
 Input.IR.Calibration = input.IR.Command.Calibration;
 Input.IR.DigitalZoom = input.IR.Command.DigitalZoom;
 Input.IR.OpticalZoomPlus = input.IR.Command.OpticalZoomPlus;
 Input.IR.OpticalZoomMinus = input.IR.Command.OpticalZoomMinus;
 Input.IR.CenterCorrection = input.IR.CenterCorrection;
 Input.IR.Brightness = input.IR.Brightness;
 Input.IR.Contrast = input.IR.Contrast;
// Input.GTC.IROn = input.Control.IROn;
 Input.DAT_Control.DAT1.Capture = input.DAT1.Capture;
 Input.DAT_Control.DAT1.On = input.DAT1.On;
 Input.DAT_Control.DAT1.Processing = input.DAT1.Processing;
 Input.DAT_Control.DAT1.Source = static_cast<DAT::CameraType>(input.DAT1.Source);
 Input.DAT_Control.DAT1.TT = static_cast<DAT::TargetType>(input.DAT1.TT);
 Input.DAT_Control.DAT1.Video1 = Input.DAT_Control.DAT1.Source;
 Input.DAT_Control.DAT1.Mark = input.DAT_Control.Mark;
 Input.DAT_Control.DAT1.ReserveMethodCoords = input.DAT1_Coords;
 Input.DAT_Control.DAT2.Capture = input.DAT2.Capture;
 Input.DAT_Control.DAT2.On = input.DAT2.On;
 Input.DAT_Control.DAT2.Processing = input.DAT2.Processing;
 Input.DAT_Control.DAT2.Source = static_cast<DAT::CameraType>(input.DAT2.Source);
 Input.DAT_Control.DAT2.TT = static_cast<DAT::TargetType>(input.DAT2.TT);
 Input.DAT_Control.DAT2.Video1 = Input.DAT_Control.DAT2.Source;
 Input.DAT_Control.DAT2.Mark = input.DAT_Control.Mark;
 Input.DAT_Control.DAT2.ReserveMethodCoords = input.DAT2_Coords;
 Input.LRF.Signals.Work = input.LRF.Work;
 Input.LRF.Signals.Single = input.LRF.Single;
 Input.LRF.Signals.Frequent = input.LRF.Frequent;
 Input.LRF.Signals.PilotLaser = input.LRF.PilotLaser;
 Input.LRF.Signals.Camera = input.LRF.Camera;
 Input.LRF.Frequency = input.LRF.Frequency;
 Input.LRF.TargetNumber = input.LRF.TargetNumber;
 Input.Weather.WindSpeed = input.Weather.WindSpeed;
 Input.Weather.WindBearing = input.Weather.WindBearing;
 Input.Weather.Pressure = input.Weather.Pressure;
 Input.Weather.Temperature = input.Weather.Temperature;
 Input.Weather.ChargeTemperature = input.Weather.ChargeTemperature;
 Input.GC = input.GC;
 Input.Time = input.Time;
 if (dump) {
  std::cout << "Dump of received " << Name << "::Input packet" <<
   "\nsystem control:\n" << Input.Control << "\n"
   "DAT generic control:\t"
   "rotation: " << Input.DAT_Control.Rotation << "\t"
   "mark: " << Input.DAT_Control.Mark << "\n"
   "DAT1 control:\n" << Input.DAT_Control.DAT1 << "\n"
   "DAT2 control:\n" << Input.DAT_Control.DAT2 << "\n"
   "TV control:\t" << Input.TV << "\n"
   "IR control:\t" << Input.IR << "\n"
   "LRF Control:\tsignals:\t" << Input.LRF.Signals << "\n"
   "frequency: " << Input.LRF.Frequency << "\ttarget number: " << Input.LRF.TargetNumber << "\n"
   "weather:\n" << Input.Weather << "\n"
   "gun control:\n" << Input.GC << "\n"
   "time of sending: " << Input.Time << " s\n";
 }
}

void ModuleComp2::EncodePacket(bool dump) { // code info to comp #2
 output.State = Output.State;
 output.GTC = Output.GTC;
 output.LRF = Output.LRF;
 output.BallisticModule = Output.BallisticModule;
 output.Gun = Output.Gun;
 output.Navig = Output.Navig;
 output.Time = Output.Time;
 // testing stuff
 if (dump) {
  std::cout << "Dump of " << GetName() << "::Output packet to be transmitted:\n" << Output << '\n';
 }
}

void ModuleComp2::Act() {
 CommitReception();
 Transmit();
}

void ModuleComp2::PrepareStatusVector(bool) {
 if (Fake.use) {
  Status.NewInfo = true;
  Status.ExchangeFault = false;
  auto Time = counter.OverallTime();
  auto& s = Status.data;
  s.Time = Time;
  s.Control.Mode = Fake.Mode;
  s.Control.SubMode = static_cast<Types::SubMode>(Fake.SubMode(Time));
  s.GC.BalData[0] = Fake.Gun.BallisticData;
  s.GC.DriveCorrections = Fake.Gun.DriveCorrections;
  s.GC.DriveControl.GunControl = true;
  s.Weather.WindSpeed = std::abs(Fake.Weather.WindSpeed(Time));
  s.Weather.WindBearing = Fake.Weather.WindBearing(Time);
  s.Weather.Pressure = std::abs(Fake.Weather.Pressure(Time));
  s.Weather.Temperature = Fake.Weather.Temperature(Time);
  s.Weather.ChargeTemperature = Fake.Weather.ChargeTemperature(Time);
  s.LRF.Signals.Single = Fake.LRFSingle;
  s.LRF.Signals.Frequent = Fake.LRFFrequent;
  s.LRF.Signals.Work = Fake.LRFWork;
 }
}
