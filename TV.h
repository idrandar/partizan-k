#ifndef TV_H
#define TV_H

// TV exchange
// (C) I.Drandar, 2018-2019, all rights reserved

#include "common/Coordinates.h"
#include "common/data_convert.h"
#include "common/ExchangeModule.h"

struct TV {

 using InputData = NOTHING;

 struct OutputData {
  struct Focus {
   bool SmoothlyPlus;
   bool SmoothlyMinus;
   bool Near; // false - far, true - near
   bool Smoothly; // false - fixed, true - smoothly
   bool Manual;
  } Focus;
  struct FieldOfView {
   bool SmoothlyPlus;
   bool SmoothlyMinus;
   bool Narrow; // false - wide, true - narrow (zoom)
   bool Smoothly; // false - fixed, true - smoothly
   bool Manual;
  } FOV;
  bool Night; // false - day, true - night
  unsigned int Brightness;
  unsigned int Contrast;
  Cartesian2DCoordsType<int> CenterCorrection;
 };

 using InputPacket = NOTHING;

 struct OutputPacket {
  struct Focus {
   bool SmoothlyPlus : 1;
   bool SmoothlyMinus : 1;
   bool Near : 1; // false - far, true - near
   bool Smoothly : 1; // false - fixed, true - smoothly
   bool Manual : 1;
   bool Gap : 1;
   bool Night : 1;
  } Focus;
  struct FieldOfView {
   bool SmoothlyPlus : 1;
   bool SmoothlyMinus : 1;
   bool Narrow : 1; // false - wide, true - narrow (zoom)
   bool Smoothly : 1; // false - fixed, true - smoothly
   bool Manual : 1; //
  } FOV;
  Bits7<unsigned int, 7> Brightness;
  Bits7<unsigned int, 7> Contrast;
  Cartesian2DCoordsType<Bits7<int, 7> > CenterCorrection;
 };

 struct CommandVector {
  bool Go;
 };

 struct StatusVector {
 };

};

std::ostream& operator<<(std::ostream&, const TV::OutputData&);
bool operator!=(const TV::OutputData&, const TV::OutputData&);

class ModuleTV : public ExchangeModuleImpl<TV> {
 void DecodePacket(bool) override;
 void EncodePacket(bool) override;
public:
 ModuleTV(const ModuleInit&);
 void Act() override;
};

#endif // TV_H
