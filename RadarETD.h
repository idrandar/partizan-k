#ifndef RADARETD_H
#define RADARETD_H

// radar ETD mode
// (C) I.Drandar, 2019, all rights reserved
// Guidance and tracking controller - Radar

#include "RadarCommon.h"
#include "common/ExchangeModule.h"

struct RadarETD {

 enum class TargetType {
  NotDetermined, Air, LowFlying, Helicopter, Surface, Coastal
 };

 struct InputData {
  bool EmissionPermitted;
  bool EmissionPermittedPhysical;
  bool Antenna; // 0 - equivalent, 1 - antenna
  TargetType TT;
  double Distance;
  bool LFM_SlopeNegative;
  Radar::DistanceScale DS;
  unsigned int SignalCode;
  Radar::OperatingFrequencyTuning OFT;
  unsigned int FrequencyCode;
  Radar::RepetitionPeriodAdjustment RPA;
  unsigned int RepetitionPeriodCode;
  Radar::MovingTargetSelection MTS1;
  Radar::MovingTargetSelection MTS2;
  Radar::FastFourierTransform FFT;
  bool ActiveNoiseAnalysis;
  bool OwnMovementCompensation;
  bool ActiveInterferenceAngularTracking;
  bool SumDeltaChannelsCalibration; // 0 - disabled, 1 - enabled
  bool RangeCalibration; // 0 - disabled, 1 - enabled
  unsigned int PulseCount;
 };

#pragma pack(push, 1)
struct InputPacket {
  bool : 1;
  bool EmissionPermittedPhysical : 1;
  bool EmissionPermitted : 1;
  bool Antenna : 1; // 0 - equivalent, 1 - antenna
  unsigned char TT : 3;
  Bits7<Scale<unsigned int, 4>, 14> Distance;
  unsigned int SignalCode : 3;
  unsigned char DS : 3;
  bool LFM_SlopeNegative : 1;
  union {
   struct {
    unsigned char : 5;
    unsigned char OFT : 2;
   };
   Bits7Shared<unsigned int, 9> FrequencyCode;
  } _;
  union {
   struct {
    unsigned char : 6;
    unsigned char RPA : 1;
   };
   Bits7Shared<unsigned int, 6> RepetitionPeriodCode;
  } __;
  bool OwnMovementCompensation : 1;
  bool ActiveNoiseAnalysis : 1;
  unsigned char FFT : 3;
  unsigned char MTS1 : 2;
  union {
   struct {
    bool : 2;
    bool ActiveInterferenceAngularTracking : 1;
    bool RangeCalibration : 1; // 0 - disabled, 1 - enabled
    bool SumDeltaChannelsCalibration : 1; // 0 - disabled, 1 - enabled
    unsigned char MTS2 : 2;
   };
   Bits7Shared<unsigned int, 9> PulseCount;
  } ___;
 };
#pragma pack(pop)

 struct OutputData {
  bool EmissionPermitted;
  bool Antenna; // 0 - equivalent, 1 - antenna
  TargetType TT;
  double Distance;
  bool LFM_SlopeNegative;
  Radar::DistanceScale DS;
  unsigned int SignalCode;
  Radar::OperatingFrequencyTuning OFT;
  unsigned int FrequencyCode;
  Radar::RepetitionPeriodAdjustment RPA;
  unsigned int RepetitionPeriodCode;
  Radar::MovingTargetSelection MTS1;
  Radar::MovingTargetSelection MTS2;
  Radar::FastFourierTransform FFT;
  bool ActiveNoiseAnalysis;
  bool OwnMovementCompensation;
  bool ActiveInterferenceAngularTracking;
  bool SumDeltaChannelsCalibration; // 0 - disabled, 1 - enabled
  bool RangeCalibration; // 0 - disabled, 1 - enabled
  unsigned int PulseCount;
 };

#pragma pack(push, 1)
 struct OutputPacket {
  bool Gap0 : 2;
  bool EmissionPermitted : 1;
  bool Antenna : 1; // 0 - equivalent, 1 - antenna
  unsigned char TT : 3;
  Bits7<Scale<unsigned int, 4>, 14> Distance;
  unsigned char SignalCode : 3;
  unsigned char DS : 3;
  bool LFM_SlopeNegative : 1;
  union {
   struct {
    unsigned char Gap : 5;
    unsigned char OFT : 2;
   };
   Bits7Shared<unsigned int, 9> FrequencyCode;
  } _;
  union {
   struct {
    unsigned char Gap : 6;
    unsigned char RPA : 1;
   };
   Bits7Shared<unsigned int, 6> RepetitionPeriodCode;
  } __;
  bool OwnMovementCompensation : 1;
  bool ActiveNoiseAnalysis : 1;
  unsigned char FFT : 3;
  unsigned char MTS1 : 2;
  union {
   struct {
    bool Gap : 2;
    bool ActiveInterferenceAngularTracking : 1;
    bool RangeCalibration : 1; // 0 - disabled, 1 - enabled
    bool SumDeltaChannelsCalibration : 1; // 0 - disabled, 1 - enabled
    unsigned char MTS2 : 2;
   };
   Bits7Shared<unsigned int, 9> PulseCount;
  } ___;
 };
#pragma pack(pop)

 struct CommandVector {
  bool Go;
 };
 struct StatusVector {};
};

bool operator==(const RadarETD::OutputData&, const RadarETD::InputData&);
std::ostream& operator<<(std::ostream&, const RadarETD::InputData&);
std::ostream& operator<<(std::ostream&, const RadarETD::OutputData&);

class ModuleRadarETD : public ExchangeModuleImpl<RadarETD> {
 const struct FakeData {
  const bool use;
  FakeData(const ModuleInit&, const QString&);
 } Fake;
 void DecodePacket(bool) override;
 void EncodePacket(bool) override;
 void PrepareStatusVector(bool) override;
public:
 ModuleRadarETD(const ModuleInit&);
 void Act() override;
};

#endif // RADARETD_H
