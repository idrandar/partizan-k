// radial speed control
// (C) I.Drandar, 2019, all rights reserved
// Guidance an tracking controller - Radar

#include "RadialSpeed.h"
#include "DebugOut.h"

ModuleRadialSpeed::ModuleRadialSpeed(const ModuleInit& _) : BASE(_) {}

void ModuleRadialSpeed::Act() {
 Transmit();
}

void ModuleRadialSpeed::EncodePacket(bool dump) {
 output.Target = Output.Target;
 output.PassiveInterference = Output.PassiveInterference;
 if (dump)
  std::cout <<
   "Dump of transmitted " << GetName() << "::Output packet\n"
   "radial speeds of:/ttarget: " << Output.Target << " m/s\t"
   "passive interference: " << Output.PassiveInterference << " m/s\n";
}
