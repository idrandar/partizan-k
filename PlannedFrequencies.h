#ifndef PLANNEDFREQUENCIES_H
#define PLANNEDFREQUENCIES_H

// plan of permmitted frequencies
// (C) I.Drandar, 2019, all rights reserved

// Guidance and tracking controller - Radar

#include "common/ExchangeModule.h"

struct PlannedFrequencies {

 struct Item {
  bool Permitted;
  unsigned int Code;
 };

 static constexpr size_t FrequenciesArraySize = 6;

 template <typename T>
 using FrequenciesArray = std::array<T, FrequenciesArraySize>;

 struct OutputData {
  size_t Count;
  FrequenciesArray<Item> Array;
 };

 union LowLevelItem {
  struct {
   bool Gap : 2;
   bool Permitted : 1;
  };
  Bits7Shared<unsigned int, 9> Code;
 };

 struct LowLevelPlan {
  unsigned char Count : 3;
  FrequenciesArray<LowLevelItem> Array;
 };

 using InputData = OutputData;
 using InputPacket = LowLevelPlan;
 using OutputPacket = LowLevelPlan;

 struct CommandVector {
  bool Go;
 };
 struct StatusVector {};
};

bool operator==(const PlannedFrequencies::Item&, const PlannedFrequencies::Item&);

class ModulePlannedFrequencies : public ExchangeModuleImpl<PlannedFrequencies> {
 void DecodePacket(bool) override;
 void EncodePacket(bool) override;
public:
 ModulePlannedFrequencies(const ModuleInit&);
 void Act() override;
};

#endif // PLANNEDFREQUENCIES_H
