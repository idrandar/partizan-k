/*
 * DAT.hh
 *
 *  Created on: 11.03.2015
 *      Author: Drandar
 */
// Processing data of "DAT" channels
// (C) I.Drandar, 2015-2018, all rights reserved
// (C) I.Drandar, for Qt, 2018-2019, all rights reserved

#ifndef DAT_HH_
#define DAT_HH_

#include "data_types.h"
#include "common/ExchangeModule.h"

struct DAT { // types

 enum PTProcessingMethod {
  CONTOUR, PINPOINT, BOUNDARY, POINT
 };
 enum ETProcessingMethod {
  HISTOGRAM_NO_BREAKDOWN, AVERAGE_STANDARD_CHANGE, HISTOGRAM_BREAKDOWN, AVERAGE_BREAKDOWN
 };

 enum Workmode {
  datWork, datTesting
 };

 // target type
 enum TargetType : char {
  ET, // extended target
  PT  // point target
 };

 enum CameraType : char {
  TV, IR
 };

 struct InputData {
  unsigned char Method;
  bool Break;
  bool Capture; // transition to autotracking mode
  bool Detection;
  bool TestingCompleted;
  TargetType TT;
  bool Fault; // channel is invalid
  Workmode Mode;
  double Credibility; // in percent
  Spherical2DCoords AutotrackingMismatch;
  double LTS;
 };

 struct OutputData {
  CameraType Video1;
  CameraType Video2;
  CameraType Video3;
  bool Rotation;
  bool Mark;
  unsigned char Processing; // PTProcessing or ETProcessing
  TargetType TT;
  CameraType Source;
  bool Capture;
  bool CaptureEnable;
  bool On;
  Cartesian2DCoordsType<int> ReserveMethodCoords;
 };

 struct InputPacket {
  // fields
  unsigned char Method : 2;
  bool Break : 1;
  bool Capture : 1; // transition to autotracking mode
  bool TestingCompleted : 1;
  char Mode : 1;
  char TT : 1;
  union {
   struct {
    bool Gap : 5;
    bool Detection : 1;
    bool Fault : 1; // channel is invalid
   };
   Bits7Shared<Scale<unsigned int, 4>, 5> Credibility;
  };
  Spherical2DCoordsType<Packed::SignedAngle20> AutotrackingMismatch;
  Packed::LTS_Type LTS;
 };

 struct OutputPacket {
  struct Control {
   char Video1 : 2;
   char Video2 : 2;
   char Video3 : 1;
   bool Rotation : 1;
   bool Mark : 1;
   bool Gap0 : 1;
   unsigned char Processing : 2;
   unsigned char TT : 1;
   unsigned char Source : 1;
   bool Capture : 1;
   bool CaptureEnable : 1;
   bool On : 1;
  } Control;
  Cartesian2DCoordsType<Bits7<int, 10> > ReserveMethodCoords;
  void operator=(const OutputData&);
  operator const OutputData() const;
 };

 struct StatusVector {};
 struct CommandVector {
  bool Capture;
 };
};

class ModuleDAT : public ExchangeModuleImpl<DAT> {
 virtual void DecodePacket(bool);
 virtual void EncodePacket(bool);
 const struct FakeData {
  const bool use;
  const Spherical2DCoordsType<Function> AutotrackingMismatch;
  FakeData(const ModuleInit&, const QString&);
 } Fake;
 virtual void PrepareStatusVector(bool);
public:
 explicit ModuleDAT(const ModuleInit&);
 void Act();
};

std::ostream& operator<<(std::ostream&, const DAT::OutputData&);

#endif /* DAT_HH_ */
