#ifndef TRAINMC_HH_
#define TRAINMC_HH_

/* The author: Vladislav Guyvoronskiy                */
/* Date of creation: 23 March 2007                   */
/* Processing data from navigation complex           */
/* IDE Momentics & QNX Neutrino 6.3.0                */
// Modified for "Sarmat" project
// (C) I.Drandar, 2010-2015, 2018-2020, all rights reserved

#include "control/time.h"
#include "common/module.h"
#include "data_types.h"
#include "common/ExchangeModule.h"

struct Training {

 typedef NOTHING InputData;
 typedef NOTHING InputPacket;

 enum class PictureType {
  Rectangular, Helicopter, Plane, Ship, Ellipse
 };

 struct OutputData {
  bool On;
  bool Square;
  bool Inversion;
  bool DirectionLeft;
  PictureType Type;
  double Size;
  SphericalCoords Sph;
 };

 struct OutputPacket {
  bool Direction : 1;
  bool Square : 1; // DAT::TargetType TT : 1;
  bool Inversion : 1;
  bool On : 1;
  unsigned char Type : 3;
  Bits7<unsigned int, 7> Size;
  Bits7<Scale<unsigned int, 2>, 14> Distance;
  Packed::UnsignedAngle20 Bearing;
  Packed::SignedAngle20 Elevation;
 };

 struct StatusVector {};

 struct CommandVector  {
  bool Start; // training is started
  bool Go;    // transmit package
  size_t TargetType;
  struct StationaryTarget {
   bool On;
   SphericalCoords Coords;
  } StationaryTarget;
  double LTS;
  CommandVector() : Start(), TargetType(), StationaryTarget() {}
 };

};

class ModuleTraining : public ExchangeModuleImpl<Training> {

 struct Variation {
  const Cartesian3DCoordsType<Function> Coord;
  const Function Size;
  const Function PT; // picture type
  const double Duration;
  Variation(const ConstCategoryTable&);
 };
 const std::vector<Variation> Variations;

 Cartesian3DCoords Origin; // zero point
 CTime LocalTime;          // counter of training seanse duration
 size_t var_index;         // index of current variation
 double bearing;           // last value of target bearing, used for direction evaluation
 void DecodePacket(bool) override;
 void EncodePacket(bool) override;
public:
 explicit ModuleTraining(const ModuleInit&);
 void PerformEvaluations(Types::OutputData&);
 void Act();
}; // end

#endif
