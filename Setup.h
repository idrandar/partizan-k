/*1
 * Setup.hh
 *
 *  Created on: 04.02.2011
 */
// load setup settings from external file
// (C) I.Drandar, 2011-2015, 2017, all rights reserved
// (C) I.Drandar, for Qt + C++1x, 2018, 2020, all rights reserved

#ifndef SETUP_HH_
#define SETUP_HH_

#include "common/module.h"
#include <vector>
#include <stdio.h>
#include "data_types.h"
#include "collision/Table.h"
#include "QtXml/QDomNode"
#include <memory>

class Setup {
 typedef StringKeyMap<DeviceInit> DeviceInitMap;
 DeviceInitMap devices;
 ModuleInitMap modulesetup;
 WeaponInitMap weaponsetup;
 struct Limits {
  double min;
  double max;
 };
 typedef Spherical2DCoordsType<Limits> Sector;
public:
 typedef std::vector<Sector> Diagram;
 struct Geometry {
  Cartesian3DCoords OEUC_Location;
 };
private:
 ConstTable constants;
 QString name;
 static double LoadNumericalAttribute(const QDomElement&, const QString&, double = 0.0);
 static Function LoadFunction(const QDomElement&, const QString&);
 static Spherical2DCoords LoadSpherical2DCoords(const QDomElement&);
 static Spherical2DShipCoords LoadSpherical2DShipCoords(const QDomElement&);
 static Cartesian2DCoords LoadCartesian2DCoords(const QDomElement&, const QString&);
 static Cartesian3DCoords LoadCartesian3DCoords(const QDomElement&);
 static Diagram LoadDiagram(const QDomElement&);
 static ConstTable LoadConstants(const QDomElement&);
 static PropertyBag LoadProperties(const QDomElement&, int level = 0);
 static double LoadAndDumpNumericalAttribute(const QDomElement&, const QString& Name, const QString& units = "");
 ModuleInitMap LoadModules(const QDomElement&);
 WeaponInitMap LoadWeapons(const QDomElement&);
 const DeviceInit& GetDevice(const QString&) const; // device by name
public:
 explicit Setup(const char*); // loads setup data from file with given name
 const ModuleInit& GetModuleInit(const QString&) const; // returns ModuleInit object by name
 const Compound GetConst(const QString& Category, const QString& Name) const;
 const WeaponInit& GetWeapon(const QString& Name) const;
 const char* Name() const { return name.toLocal8Bit(); }
};

extern std::unique_ptr<Setup> setup;
#endif /* SETUP_HH_ */
