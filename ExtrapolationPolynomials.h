/*
 * ExtrapolationPolynomials.hh
 *
 *  Created on: 22.09.2015
 *      Author: Drandar
 */

#ifndef EXTRAPOLATIONPOLYNOMIALS_HH_
#define EXTRAPOLATIONPOLYNOMIALS_HH_

// All types of extrapolation polynomials
// Three files merged
// (C) I.Drandar, 2015, all rights reserved
// (C) I.Drandar, for C++1x, 2018-2019, all rights reserved

#include "data_types.h"
#include <vector>
#include "common/MathConst.h"
#include <array>

namespace ExtrapolationPolynomials {

//////////////////////////////////////////////////////////////////////////
//                      ExtrapolationPolynomials::NS                    //
//   - класс, содержащий данные и методы, позволяющие                   //
//      1. Вычислять (пересчитывать) при приходе каждого пакета         //
//   навигационных данных корабля совокупность полиномов, экстраполяции //
//   значений навигационных параметров корабля:                         //
//   Vx,Vy,Vz,K,Teta,Psi.                                               //
//      2. С использованием данных полиномов выполнять локальную        //
//   экстраполяцию данных параметров на произвольный                    //
//   (до прихода следующего пакета нав. данных) момент времени          //
//      3. Вычислять (пересчитывать) при приходе каждого пакета         //
//   навигационных данных корабля полиномы, для локальной               //
//   экстраполяции координат X,Y,Z корабля в топоцетрической системе    //
//   координат.                                                         //
//      4. С использованием данных полиномов выполнять локальную        //
//   экстраполяцию координат корабля на произвольный                    //
//   (до прихода следующего пакета нав. данных) момент времени.         //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

class NS {
public:
 struct CurrentValues {
  static constexpr double dt_Crit = 0.15;

  Cartesian3DCoords SpeedPart;
  Cartesian3DCoords ShipCart0; // a0_Rx, a0_Ry, a0_Rz
  Cartesian3DCoords ShipCart1; // a1_Rx, a1_Ry, a1_Rz
  Rotations Rotations0;
  Rotations Rotations1;

  CurrentValues() : SpeedPart(), ShipCart0(), ShipCart1(), Rotations0(), Rotations1() {}
 };
private:
 CurrentValues CV;
public:

 double Time; // basic time of polinoms
 bool OK; // define readyness ExtrapolationPolynomials

 NS();

 void Prepare(const NS_Data&, SystemMode ModeC, bool&) ;

 Cartesian3DCoords GetVCart(double dt) const; // count value  Vx, Vy, Vz    for time t+dt
 Cartesian3DCoords GetShipCart(double dt) const; // count value  Rx, Ry, Rz    for time t+dt

 Rotations GetRotations(double dt) const; // count value  K, Psi, Teta    for time t+dt

 const CurrentValues& GetCV() const;
};

//////////////////////////////////////////////////////////////////////////
//                CTgAn_PkPnEx                                           //
//   - класс, содержащий данные и методы, позволяющие                    //
//      1. Вычислять (пересчитывать) при приходе каждого пакета          //
//   данных PkTgAnState совокупности полиномов экстраполяции             //
//   значений угловых координат цели в корабельной системе координат Q,F //
//   значений угловых координат цели в земной системе координат A,E      //
//      2. С использованием данных полиномов выполнять локальную         //
//   экстраполяцию углов Q,F,A,E  на произвольный                        //
//   (в допустимых пределах) момент времени.                             //
//                                                                       //
///////////////////////////////////////////////////////////////////////////

class Angles {
public:

 double Time; // time of measuament paramiters
 enum class ReadyState {
  Empty, Unsufficient, Sufficient
 };

 ReadyState Ready; // define readyness ExtrapolationPolynomials
 unsigned int Count;
 using CountType = std::common_type_t<decltype(Count), size_t>;

 static constexpr unsigned int DP_Nom = 2; // degree polinoms nominal
 static constexpr size_t Size = 20;
 static constexpr double dt_Crit = 0.15;
 int DP_Cur; // degree polinoms current

private:

 std::vector<double> Vc_T, Vc_T_Rsl;

 std::vector<Spherical2DShipCoords> Vc_RslShip;
 std::vector<Spherical2DGroundCoords> Vc_RslGround;
 std::vector<Spherical2DShipCoords> VcShip;
 std::vector<Spherical2DGroundCoords> VcGround;
 Spherical2DShipCoords PnShip[5]; // array of coefficients  extrapol. polinom for Q, F
 Spherical2DGroundCoords PnGround[5]; // array of coefficients fitting polinom for A, E
 static const bool dump = false;
public:

 Angles();

 void Evaluate(const Angles_Data&, double, const NS&, bool);

 Spherical2DShipCoords ShipVal(double dt) const; // count value  Q   for time t+dt // count value  F   for time t+dt

 Spherical2DGroundCoords GroundVal(double dt) const; // count value  A   for time t+dt // count value  E   for time t+dt
 const Spherical2DShipCoords& GetPnShip(size_t) const;
 const Spherical2DGroundCoords& GetPnGround(size_t) const;
 void Reset();
};

/////////////////////////////////////////////////////////////////////////////////
//                                                                             //
//  Класс, обеспечивающий экстраполяцию дальности цели, по результатам         //
//  обработки данных ЛД, поступивших от КНС                                    //
//                                                                             //
/////////////////////////////////////////////////////////////////////////////////

class Distance {
public:

 double Time;
 bool OK;

private:

 double Pn[3];

public:
 Distance();

 double Evaluate(double dt) const; // count value Ds for time t+dt
 void Assign(const DistanceData&);
};

struct All {
 struct NS NS;
 struct Angles Angles;
 struct Distance Distance;
 bool OK() const {
  return NS.OK && (Angles.Ready != Angles::ReadyState::Empty) && Distance.OK;
 }
 All() : NS(), Angles(), Distance() {}
};

}  // namespace ExtrapolationPolynomials

template <>
inline constexpr std::array enum_rep<ExtrapolationPolynomials::Angles::ReadyState> = {
 "empty", "unsufficient", "sufficient"
};

#endif /* EXTRAPOLATIONPOLYNOMIALS_HH_ */
