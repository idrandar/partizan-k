/* The author: Vladislav Guyvoronskiy          */
/* Date of creation: 17 April 2007             */
/* Processing dathrd_tmsta from angle tracking module  */
/* IDE Momentics & QNX Neutrino 6.3.0          */
// Modified for "Sarmat" project
// Converted for "Owl" project to "GTC"
// (C) I.Drandar, 2010-2017, all rights reserved
// (C) I.Drandar, for Qt + C++1x, 2018-2020, all rights reserved

#include "GTC.h"
#include "DebugOut.h"
#include "common/CoordinatesConversion.h"
#include <iomanip>

template <>
inline constexpr std::array enum_rep<GTC::Mode> = {
 "initial", "undefined:1", "ETD", "undefined:3", "MP", "undefined:5", "AT by DAT", "undefined:7",
 "autosearch by spiral", "undefined:9", "standalone search", "undefined:B", "low fying target",
 "IT", "AT by radar", "testing"
};

std::ostream& operator<<(std::ostream& _, const GTC::General_State& D) {
 _ <<
  "mode is changing: " << D.ModeIsChanging << "\tmode: " << D.Mode <<  "\n"
  "detent:\t" << D.Detent;
 return _;
}

std::ostream& operator<<(std::ostream& _, const struct GTC::InputData::ORU& O) {
 _ <<
  "Drive on:\t" << O.DriveOn <<
  "\nElev drive limit high:\t" << O.ElevLimitHigh << "\tElev drive limit low:\t" << O.ElevLimitLow <<
  "\nGround coordinate system (°):\t" << O.Ground << "\nShip coordinate system (°):\t" << O.Ship;
 return _;
}

GTC::InputPacket::General_State::operator GTC::General_State() const {
 return
  GTC::General_State {
   { DetentHA, DetentElev }, ModeIsChanging, static_cast<enum Mode>(Mode)
  };
}

GTC::InputPacket::General_State::General_State(const GTC::General_State& _) :
 DetentHA(_.Detent.HA), DetentElev(_.Detent.Elev),
 ModeIsChanging(_.ModeIsChanging), Mode(_.Mode) {
}

ModuleGTC::FakeData::FakeData(const ModuleInit& init, const QString& s) :
 use(init.GetConst(s, "use")), UseShipCoords(init.GetConst(s, "use-ship-coords")),
 Heading(init.GetConst(s, "heading")),
 Elevation(init.GetConst(s, "elevation")) {
}

ModuleGTC::TestDump::TestDump(const ModuleInit& init, const QString& s) :
 Ground(init.GetConst(s, "ground")) {
}

ModuleGTC::ModuleGTC(const ModuleInit& init) :
 BASE(init), Fake(init, "fake"), TestDump(init, "test-dump") {
// std::cout << sizeof output << std::endl;
}

void ModuleGTC::DecodePacket(bool dump) {
 Input.State = input.State;
 Input.Power = input.Power;
 Input.IIT_Permit = input.InnerInertialTrackingPermittion;
 Input.DATChannel = input.DATChannel;
 Input._7_2 = input._7_2;
 Input.IRReady = input.IRReady;
 Input.TVReady = input.TVReady;
 Input.LRFReady = input.LRFReady;
 Input.ORU.DriveOn.HA = input.HADriveOn;
 Input.ORU.DriveOn.Elev = input.ElevDriveOn;
 Input.ORU.ElevLimitHigh = input.ElevLimitHigh;
 Input.ORU.ElevLimitLow = input.ElevLimitLow;
 Input.ORU.Ground.Bearing = input.GroundBearing; // ground coordinate system
 Input.ORU.Ground.Elevation = input.GroundElevation; // ground coordinate system
 Input.ORU.Ship = input.Ship; // deck coordinate system OEUC position

 Input.Sine = input.Sine;
 Input.Cosine = input.Cosine;

 Input.SyncRequest = input.SyncRequest;
 Input.SyncDone = input.SyncDone;

 Input.Mismatch.HA = input.MismatchHA;
 Input.Mismatch.Elev = input.MismatchElev;

 Input.DetentError.HA = input.DetentErrorHA;
 Input.DetentError.Elev = input.DetentErrorElev;

 Input.LTS = input.LTS;
 // testing stuff
 if (dump) {
  std::cout << "dump of received " << GetName() << "::Input packet\n:"
   "DAT state:\n" << Input.State << "\n"
   "OEUC state:\n" << Input.ORU << "\n"
   "IR camera is ready: " << Input.IRReady << "\tTV camera is ready: " << Input.TVReady << "\n"
   "LRF is ready: " << Input.LRFReady << "\n"
   "DAT channel: " << Input.DATChannel << "\tpower: " << Input.Power << "\t"
   "inner inertial tracking permittion: " << Input.IIT_Permit << "\t7.2 on: " << Input._7_2 << "\n"
   "sine: " << Input.Sine << "\tcosine: " << Input.Cosine << "\n"
   "synchronization:\trequest: " << Input.SyncRequest << "\tdone: " << Input.SyncDone << "\n"
   "mismatch:\t" << Input.Mismatch << "\n"
   "detent error:\t" << Input.DetentError << "\n"
   "LTS time:\t" << Input.LTS << " s\n";
 }
}

void ModuleGTC::EncodePacket(bool dump) {
 // generic control
 output.Control.DATChannel = Output.DATChannel;
 output.Control.DAT1 = Output.DAT1;
 output.Control.IIT_Enable = Output.IIT_Enable;
 output.Control.WiperOn = Output.WiperOn;
 output.Control.HeatingOn = Output.HeatingOn;
 output.Control.Mode = Output.Mode;
 output.Control.IROn = Output.IROn;
 output.Control.Fire = Output.Fire;
 output.Control.HeatStandby = Output.HeatStandby;
 // OEUC guidance
 output.ORU.Bearing = Output.ORU.ETD.Bearing; // ETD;
 output.ORU.Elev = Output.ORU.ETD.Elevation;  // ETD;
 // handle control
 output.HandleDeviation = Output.Handle.Deviation;
 // testing stuff
 if (dump)
  std::cout << "Dump of " << Name << "::Output packet to be transmitted\n"
   "generic control:\n" <<
   "DAT channel: " << Output.DATChannel << "\tDAT1:" << Output.DAT1 << "\t"
   "IIT enable: " << Output.IIT_Enable << "\tmode: " << enum2str(Output.Mode) << "\n"
   "wiper on: " << Output.WiperOn << "\tHeating on: " << Output.HeatingOn << "\t"
   "IR on: " << Output.IROn << "\tfire: " << Output.Fire << "\t"
   "heat standby" << Output.HeatStandby << "\n"
   // OEUC guidance
   "ORU Guidance:\nETD:\t" << Output.ORU.ETD << "\n"  // ETD;
   // handle control
   "Handle Deviation: " << Output.Handle.Deviation << "\n";
}

void ModuleGTC::Act() {
 CommitReception();
 Transmit();
} // end ccRun

void ModuleGTC::PrepareStatusVector(bool OK) {
 if (Fake.use) {
  Status.NewInfo = true;
  Status.ExchangeFault = false;
  auto& s = Status.data;
  const auto& c = Command.data;
  if (!OK) {
   s.LTS = counter.TimeFromProgramStart();
  }
  const auto Time = Status.data.LTS;
  s.State.Mode = c.Mode;
  if (c.Mode == Types::mETD) {
   s.ORU.Ground = Command.data.ORU.ETD;
   s.ORU.Ship = ConvertGroundToShipSpherical(Status.data.ORU.Ground, Command.Rot);
  } else if (Fake.UseShipCoords) {
   s.ORU.Ship.Heading = Fake.Heading(Time);
   s.ORU.Ship.Elevation = Fake.Elevation(Time);
   s.ORU.Ground = ConvertShipToGroundSpherical(Status.data.ORU.Ship, Command.Rot);
  } else {
   s.ORU.Ground.Bearing = Fake.Heading(Time);
   s.ORU.Ground.Elevation = Fake.Elevation(Time);
   s.ORU.Ship = ConvertGroundToShipSpherical(Status.data.ORU.Ground, Command.Rot);
  }
  if (Status.data.ORU.Ship.Heading != 0.0) {
   s.ORU.DriveOn.HA = true;
  }
  if (s.ORU.Ship.Elevation > -M_PI_2) {
   s.ORU.DriveOn.Elev = true;
   s.ORU.ElevLimitLow = s.ORU.Ship.Elevation < (-M_PI_2 * 0.9);
   s.ORU.ElevLimitHigh = s.ORU.Ship.Elevation > (M_PI_2 * 0.9);
  }
 }
 dump_if(TestDump.Ground) <<
  "Time: " << std::setprecision(10) << Status.data.LTS << "\t"
  "Ground: " << Status.data.ORU.Ground << std::endl;
}

//=================== End of anglMC.cc file=================================
