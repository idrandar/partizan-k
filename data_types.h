#ifndef DATA_TYPES_H_
#define DATA_TYPES_H_

// project-specific data types
// (C) I.Drandar, 2010-2015 all rights reserved
// (C) I.Drandar, for C++1x, 2018-2019, all rights reserved

#include <stddef.h>
#include "common/data_convert.h"
#include "common/Coordinates.h"

// templates for frequently used in packages' definitions types
namespace Packed {

// LTS 40-bit time
// lsb weight is 125 ns
constexpr auto LTS_SCALE = 8000000;
using LTS_Type = Bits7Shared<Upscale<unsigned long long, LTS_SCALE>, 40>;

// π ≈ 103993/33102
// 14-bit signed angle. Range is -π..π
typedef Bits7<Upscale<int, 8192*113, 355>, 14> SignedAngle;

// used in CS, NS & Comp2
// 14-bit unsigned angle. Range is 0..2π
typedef Bits7<Upscale<unsigned int, 8192*113, 355>, 14> UnsignedAngle;

// used in NS, DAT, CombatModule
// 20-bit signed angle. Range is -π..π
typedef Bits7<Upscale<int, 524288*113, 355>, 20> SignedAngle20;
// used in NS, GTC
// 20-bit unsigned angle. Range is 0..2π
typedef Bits7<Upscale<unsigned int, 524288*113, 355>, 20> UnsignedAngle20;

typedef Bits7<Upscale<int, 1024*33102, 103993>, 11> SignedAngle11;
}

//enum GunIndex { // list of guns
// GUN_1,
// GUNS_COUNT // list-stop entry
//};

enum DATChannelIndex {
 DAT_CHANNEL_1, DAT_CHANNEL_2,
 DAT_CHANNELS_COUNT,
};

enum SystemMode { // orders of work
 SYSMODE_Initial,
 SYSMODE_ETD, // external tracking data
 SYSMODE_SS,
 SYSMODE_AT, // autotracking
 SYSMODE_IT, // inertial tracking
 SYSMODE_Testing,
 SYSMODE_ITD, // internal target data
 SYSMODE_MP, // manual pointing
 SYSMODE_Shutdown
};

struct NS_Data { // PkPar for Coordinate proccessing
 bool NewInfo;

 double Time;
 Cartesian3DCoords SpeedPart;
 Rotations Rot; // Teta -cren(BK,roll), Psi - different(KK,pitch)
 Rotations RotChange;
 NS_Data() = default;
};

struct Angles_Data { // User Format  processing part of packet PkTgAnState
 double Time;
 Spherical2DShipCoords Ship; // OEUC
 Spherical2DGroundCoords Ground; // OEUC
 Spherical2DCoords AutotrackingMismatch;
};

struct TargetCoordinates { // angles coordinates line of vision target with addings
 double Time;
 Spherical2DShipCoords Ship; //
 Spherical2DGroundCoords Ground;
};

//----------------Structures For Data Target Distance-------------------------------------------------------------
struct DistanceData { // User Format
 bool OK;
 double Time;
 double Distance;
 double Speed;
 double Acceleration;
};

struct Weather {
 double WindSpeed;
 double WindBearing;
 double Pressure;    // in bars
 double Temperature = 15.0; // air temperature in °C
 double ChargeTemperature = 15.0;
 static constexpr double NominalTemperature = 15.0;
 static constexpr double NominalPressure = 1.0;
// static constexpr double NominalDensity = 0.000012089598337;
 static constexpr double NominalDensity = 0.00010051847995835503;
 static constexpr double M_R = 0.0289644;
 double constexpr dDencity() const {
  return (M_R * Pressure / (Temperature + 273.15)) - NominalDensity;
 }
 double dTemperature() const {
  return Temperature - NominalTemperature;
 }
 double dChargeTemperature() const {
  return ChargeTemperature - NominalTemperature;
 }
 double dPressure() const {
  return (Pressure / NominalPressure - 1.0) * 750.06375542;
 }
};

#endif /*DATA_TYPES_H_*/
