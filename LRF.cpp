/* The author: Vladislav Guyvoronskiy          */
/* Date of creation: 19 April 2007             */
/* Processing data from range tracking module  */
/* IDE Momentics & QNX Neutrino 6.3.0          */
// Modified for "Sarmat" project
// (C) I.Drandar, 2010-2018, all rights reserved
// module name changed
// (C) I.Drandar, for Qt + C++1x, 2018-2020, all rights reserved

#include "LRF.h"
#include <iostream>
#include "Setup.h"
#include "DebugOut.h"
#include <iomanip>
#include "control/exception.h"

std::ostream& operator<<(std::ostream& _, const struct LRF::InputData& L) {
  _ <<
   "mode: " << L.Mode << "\nreadiness: " << L.Readiness << "\n"
   "reception fault: " << L.ReceptionFault << "\tcontrol fault: " << L.ControlFault <<
   "\ttemperature ready: " << L.TemperatureReady <<
   "\nDistance To Target 1:\t" << L.Distance1 << " m" << "\tDistance To Target 2:\t" << L.Distance2 << " m" <<
   "\tDistance To Target 3:\t" << L.Distance3 << " m" << "\n"
   "resource: " << L.Resource << "\n";
 return _;
}

std::ostream& operator<<(std::ostream& _, const struct LRF::InputData::Faults& F) {
 _ <<
  "FRAM: " << F.FRAM <<"\tCRC" << F.CRC << "\tinterval measurer: " << F.IntervalMeasurer << "\t"
  "Q-Switch: " << F.QSwitch << "\tБПЛ: " << F.LPS << "\tФПУ: " << F.WTF << "\t"
  "optical start: " << F.OpticalStart;
 return _;
}

std::ostream& operator<<(std::ostream& _, const struct LRF::InputData::State& S) {
 _ <<
  "number of targets: " << S.Targets << "\n"
  "low measuring period: " << S.LowPeriod << "\tonboard network fail: " << S.OnboardNetwork << "\t"
  "Ошибка драйв. ТЕС: " << S.WTF << "\ttesting completed: " << S.TestingCompleted << "\t"
  "failure: " << S.Failure;
 return _;
}

std::ostream& operator<<(std::ostream& _, const struct LRF::OutputData::Signals& S) {
 _ <<
  "\tWork: " << S.Work << "\tSingle: " << S.Single << "\tFrequent: " << S.Frequent << "\t"
  "Pilot: " << S.Pilot << "\tCamera: " << S.Camera << "\n";
 return _;
}

ModuleLRF::Time::Time(const ModuleInit& init, const QString& s) :
 IT_Trigger(init.GetConst(s, "IT-trigger")),
 RadiationInterval(init.GetConst(s, "radiation")) {
}

ModuleLRF::FakeData::FakeData(const ModuleInit& init, const QString& s) :
 use(init.GetConst(s, "use")),
 Count(init.GetConst(s, "count")),
 Distance1(init.GetConst(s, "distance1")), Distance2(init.GetConst(s, "distance2")),
 Distance3(init.GetConst(s, "distance3")) {
}

ModuleLRF::TestDump::TestDump(const ModuleInit& init, const QString& s) :
 Before(init.GetConst(s, "before")), After(init.GetConst(s, "after")),
 Tact(init.GetConst(s, "tact")), Distance(init.GetConst(s, "distance")),
 DeltaTime(init.GetConst(s, "delta-time")), Speed(init.GetConst(s, "speed")),
 NewDistance(init.GetConst(s, "new-distance")) {
}

ModuleLRF::ModuleLRF(const ModuleInit& init) try : BASE(init),
 D_INIT(setup->GetConst("initial", "distance")),
 UseMultipleTargets(init.GetConst("targets", "multiple")),
 MissCountMax(init.GetConst("targets", "miss-count-max")),
 Time(init, "time"), Fake(init, "fake"), TestDump(init, "test-dump"),
 RadiationMeasurer() {
} catch (TException& e) {
 std::cout << "exception in " << __PRETTY_FUNCTION__ << ":\t" << e.what() << std::endl;
 throw
  TException(GetName(), ":\t", e.what());
}

void ModuleLRF::DecodePacket(bool dump) { //decode info from UEVM
 Input.Mode = input.Mode;
 Input.Readiness = input.Readiness;
 Input.ReceptionFault = input.ReceptionFault;
 Input.ControlFault = input.ControlFault;
 Input.TemperatureReady = input.TemperatureReady;

 Input.Distance1 = input.D1;
 Input.Distance2 = input.D2;
 Input.Distance3 = input.D3;
 Input.Resource = input.Resource;
 Input.Faults.FRAM = input.Faults.FRAM;
 Input.Faults.CRC = input.Faults.CRC;
 Input.Faults.IntervalMeasurer = input.Faults.IntervalMeasurer;
 Input.Faults.QSwitch = input.Faults.QSwitch;
 Input.Faults.LPS = input.Faults.LPS; // БПЛ
 Input.Faults.WTF = input.Faults.WTF; // ФПУ
 Input.Faults.OpticalStart = input.Faults.OpticalStart;
 Input.State.Targets = input.State.Targets;
 Input.State.LowPeriod = input.State.LowPeriod;
 Input.State.OnboardNetwork = input.State.OnboardNetwork;
 Input.State.WTF = input.State.WTF; // Ошибка драйв. ТЕС
 Input.State.TestingCompleted = input.State.TestingCompleted;
 Input.State.Failure = input.State.Failure;
 Input.LTS = input.LTS;
 // testing stuff
 if (dump)
  std::cout << "Dump of received " << GetName() << "::Input packet\n"
   "mode: " << Input.Mode << "\treadiness: " << Input.Readiness << "\n"
   "reception fault: " << Input.ReceptionFault << "\tcontrol fault: " << Input.ControlFault << "\t"
   "temperature ready: " << Input.TemperatureReady << "\n"
   "distance #1: " << Input.Distance1 << "\tdistance #2: " << Input.Distance2 << "\tdistance #3: " << Input.Distance3 << "\n"
   "resource counter: " << Input.Resource << "\n"
   "faults:\n" << Input.Faults << "\n"
   "state:\n" << Input.State << "\n"
   "LTS time:\t" << Input.LTS << " s\n";
}

void ModuleLRF::EncodePacket(bool dump) {
 // LRF control
 output.Frequency = Output.Frequency;
 output.Frequent = Output.Signals.Frequent;
 output.Single = Output.Signals.Single;
 output.Work = Output.Signals.Work;
 output.Camera = Output.Signals.Camera;
 output.Pilot = Output.Signals.Pilot;
 output.StrobeNear = DISTANCE_LOW;
 output.StrobeFar = DISTANCE_HIGH;
 if (dump)
  std::cout <<
   "LRF control:\n" << Output.Signals << "\n"
   "frequency: " << Output.Frequency << "\n";
}

void ModuleLRF::Act() {
 if (Command.Go) {
  Transmit();
 }
 CommitReception();
} // end Run

//=================== End of LRF.cc file===================================

void ModuleLRF::ProcessCommandVector() {
 if (Command.Frequency >= 1.0) {
  time_step = 1.0 / Command.Frequency;
 }

 if (Command.data.Signals.Work && !Status.data.ReceptionFault) {
  if (Command.data.Signals.Measuring()) {
   PrepareMeasureMode();
  } else {
   PrepareWorkMode();
  }
 } else
  PrepareInitMode();
 if (!RadiationPermitted()) {
  Command.data.Signals.Frequent = false;
  Command.data.Signals.Single = false;
 }
} // end PrepareCmdData

void ModuleLRF::PrepareInitMode() {
 Status.distAT = false;
 tact = 0;
} // end

void ModuleLRF::PrepareWorkMode() {
 Status.distAT = false;
 tact = 0;
} // end

void ModuleLRF::PrepareMeasureMode() {
 if (!Command.data.Signals.Frequent) {
  Status.distAT = false;
  tact = 0;
 }
} // end


double ModuleLRF::FakeDistance(int no, double t) const {
 const std::array f = {
  &Fake.Distance1, &Fake.Distance2, &Fake.Distance3
 };
 return fabs((*f[no])(t));
}

void ModuleLRF::PostProcessStatusVector() {
 if (Fake.use) {
  Status.data.TemperatureReady = true;
  Status.ExchangeFault = false;
  if (Command.Go || Command.data.Signals.Frequent) {
   // simulate package reception as an answer to the last request
   pr->FakePull();
   Status.NewInfo = true;
   if (Command.data.Signals.Measuring()) {
    const double Time = counter.OverallTime();
    double buf[3] = { };
    buf[0] = FakeDistance(0, Time);
    buf[1] = FakeDistance(1, Time);
    buf[2] = FakeDistance(2, Time);
    const size_t count = Fake.Count(Time);
    std::sort(buf, buf + std::min(count, 3UL));
    if (count >= 1)
     Status.data.Distance1 = buf[0];
    if (count >= 2)
     Status.data.Distance2 = buf[1];
    if (count >= 3)
     Status.data.Distance3 = buf[2];
    Status.data.State.Targets = count;
    Status.data.LTS = Time;
    ++Status.data.Resource;
   }
   Status.data.Readiness = Command.data.Signals.Work;
  }
 }
 // check for intervals reset conditions
 if (!Command.data.Signals.Frequent) IT_IntervalMeasurer.Start();
 if (Command.data.Signals.Measuring()) RadiationMeasurer.Start(); // reset measuring interval
 // check for intervals expiration
 Status.Measuring = (RadiationMeasurer.Gone() < Time.RadiationInterval);
 Status.IT_Permitted =
  Command.data.Signals.Frequent && IT_IntervalMeasurer.Gone() > Time.IT_Trigger;
 if (Command.TrainingStart) {
  if (Command.data.Signals.Measuring()) {
   Status.processedData.new_D = true;
   Status.processedData.Distance = Command.TrainingDistance;
   Status.data.Distance1 = Command.TrainingDistance;
   Status.data.LTS = Command.TrainingTime;
  }
 }

 Status.processedData = {};
 Status.processedData.new_D = Status.data.State.Targets > 0;
 if (Command.data.Signals.Measuring()) {
  MeasuringProcess();
 }
 // save last successful measuring time
 dump_if(TestDump.NewDistance) << "new distance: " << Status.processedData.new_D << std::endl;
}

void ModuleLRF::MeasuringProcess() {
 if (Command.BlockingRadiation) {
  Status.data.Distance1 = 0;
 } else if (Command.TrainingButton) {
  if (Command.TrainingStart) {
   Status.data.Distance1 = Command.TrainingDistance;
   Status.data.TemperatureReady = true;
   Status.data.Readiness = true;
  }
  else Status.data.Distance1 = D_INIT;
 }

 if (Command.data.Signals.Single) SingleLRF_Results();
 else if (Command.data.Signals.Frequent) FreqLRF_Results();
 else Status.distAT = false;
} // end MeasuringModeInfoProcessing

void ModuleLRF::SingleLRF_Results() {
 Status.distAT = false;
 Status.processedData.DistanceChange = 0.;
 Status.processedData.DistanceChangeAcceleration = 0.;
 if (Command.Choice == 0) { // no target selected, using default
  Status.processedData.Distance = Status.data.Distance1;
 } else {
  // choice target enumeration goes from 1 to 3
  auto no = std::min(Command.Choice, Status.data.State.Targets);
  if (no == 1) Status.processedData.Distance = Status.data.Distance1;
  if (no == 2) Status.processedData.Distance = Status.data.Distance2;
  if (no == 3) Status.processedData.Distance = Status.data.Distance3;
 }
}

void ModuleLRF::FreqLRF_Results() {
 constexpr auto alpha = 0.5;
 constexpr auto beta = 0.2;
 constexpr auto gamma = 0.01;

 dump_if(TestDump.Before) << "before:\n";
 dump_if(TestDump.Tact && TestDump.Before) << "tact: " << tact << std::endl;
 dump_if(TestDump.Distance && TestDump.Before) <<
  "distance: " << D << " m\n";
 const double DSave = D;

 if (UseMultipleTargets && Status.data.Distance2 > 0) {
  tact = 0;
  Status.distAT = false;
 } else if (Status.data.Distance1 > 0 && ((Status.data.Distance2 <= 0 && UseMultipleTargets) || (!UseMultipleTargets))) {
  D = Status.data.Distance1;
  tact++;
  missCnt = 0; //miss counter

  switch (tact) {
  case 1:
   old_time = SampleTime();
   Vd = 0; //ocenka skorosti izmeneniya dal'nosti
   Ad = 0; //ocenka uskoreniya izmeneniya dal'nosti
   d1 = D;
   break;
  case 2:
   dT = SampleTime() - old_time;
   old_time = SampleTime();
   tau1 = dT;
   Vd = (D - d1) / dT;
   Ad = 0;
   d2 = D;
   break;
  case 3:
  default:
   dT = SampleTime() - old_time;
   old_time = SampleTime();
   tau2 = dT;
   d3 = D;
   Vd = (d3 - d2) * (tau1 + tau2) / tau1 / tau2 + (d1 - d3) * tau2 / tau1 / (tau1 + tau2);
   Ad = 2.0 * ((d1 - d2) / tau1 + (d3 - d2) / tau2) / (tau1 + tau2);
   tau1 = tau2;
   d1 = d2;
   d2 = d3;
   Status.distAT = true;
   break;
//  default:
   dT = SampleTime() - old_time;
//   old_time = SampleTime();
   const double De = d3 + Vd * dT + 0.5 * Ad * dT * dT; //  Dal'nost' extrapol. po ocenkam proshlogo takta
//   const double De = D + Vd * dT + 0.5 * Ad * dT * dT; //  Dal'nost' extrapol. po ocenkam proshlogo takta
   const double delD = D - De; //  Nevyazka
   const double Ve = Vd + Ad * dT; //  Skorost' extrapol. po ocenkam proshlogo takta
   const double Do = De + alpha * delD; //  Ocenka soprovozhdaemoy dal'nosti
   Vd = Ve + beta * delD / dT; //  Ocenka skorosti izmeneniya soprovozhdaemoy dal'nosti
   Ad = Ad + gamma * delD / (dT * dT); //  Ocenka uskoreniya izmeneniya soprovozhdaemoy dal'nosti
   D = Do;
   Status.distAT = true;
  }

//  Status.processedData.new_D = LastSampleTime < Status.data.LTS;
  Status.processedData.Distance = D;
  Status.processedData.DistanceChange = Vd;
  Status.processedData.DistanceChangeAcceleration = Ad;

 } else { // Number of targets == 0
  if (Status.distAT) {
   missCnt++;
   if (missCnt >= MissCountMax) Status.distAT = false;
   dT = SampleTime() - old_time;
   old_time = SampleTime();
  } else {
   tact = 0;
  }
 }
 dump_if(TestDump.After) << "after:\n";
 dump_if(TestDump.Tact && TestDump.After) << "tact: " << tact << "\n";
 dump_if(TestDump.DeltaTime && TestDump.After) << "delta time: " << dT << " s\n";
 dump_if(TestDump.Distance && TestDump.After) <<
  "distance: " << D << " m\tchanged by " << D - DSave << " m\n";
 dump_if(TestDump.Speed && TestDump.After) <<
  "evaluated speed: " << Vd << " m/s\tevaluated acceleration: "  << Ad << " m/s²\n";
}

double ModuleLRF::SampleTime() const {
 if (Command.TrainingButton)
  return Command.TrainingTime;
 else
  return Status.data.LTS;
}
