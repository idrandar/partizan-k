#ifndef LRF_HH_
#define LRF_HH_

/* The author: Vladislav Guyvoronskiy          */
/* Date of creation: 19 April 2007             */
/* Processing data from range tracking module (LRF) */
/* IDE Momentics & QNX Neutrino 6.3.0          */
// Modified for "Sarmat" project
// Modified for "Owl" project, 2005
// (C) I.Drandar, 2010-2017, all rights reserved
// module name changed
// (C) I.Drandar, for C++1x, 2018-2020, all rights reserved

#include "common/ExchangeModule.h"
#include "data_types.h"
#include "common/ctime.h"

struct LRF { // works as namespace
 struct InputData {
  double Distance1;
  double Distance2;
  double Distance3;
  unsigned char Mode;
  bool Readiness;
  bool ReceptionFault;
  bool ControlFault;
  bool TemperatureReady;
  unsigned int Resource;
  struct Faults {
   bool FRAM;
   bool CRC;
   bool IntervalMeasurer;
   bool QSwitch;
   bool LPS; // БПЛ
   bool WTF; // ФПУ
   bool OpticalStart;
  } Faults;
  struct State {
   unsigned int Targets;
   bool LowPeriod;
   bool OnboardNetwork;
   bool WTF; // Ошибка драйв. ТЕС
   bool TestingCompleted;
   bool Failure;
  } State;
  //
  double LTS; // LTS time
  InputData() = default;
  bool MultipleTargets() const { return Distance2 != 0 || Distance3 != 0; }
  bool SingleTarget() const {
   return Distance1 != 0 && Distance2 == 0 && Distance3 == 0;
  }
 };

 struct OutputData {
  struct Signals {
   bool Frequent;
   bool Single;
   bool Work;
   bool Pilot;
   bool Camera;
   bool Measuring() const { return Frequent || Single; }
  } Signals;
  double Frequency;
 };

 struct CommandVector {
  ::SystemMode Mode;
  // data
  //
  unsigned int TargetNumber; // number of target
  bool Single;
  bool ExchangeAllow;
  double TrainingDistance;
  double TrainingTime;
  bool TrainingStart;
  bool TrainingButton;
//  bool Enable;
  bool BlockingRadiation;  // corr.110818
  double Frequency;
  unsigned int Choice; // <= 3
  bool Go;
 };

 struct StatusVector {
  bool distAT; // autotracking by distance;
  struct MeasuringResults {
   double Distance;
   double DistanceChange;
   double DistanceChangeAcceleration;
   bool new_D;
  } processedData;
  bool IT_Permitted;
  bool Measuring;
  StatusVector() :
   distAT(), processedData(), IT_Permitted(), Measuring() {}
 };

 struct InputPacket {
  typedef Bits7<Scale<unsigned int, 2>, 14> DistanceType;
  char unsigned Mode : 3;
  bool Readiness : 1;
  bool ReceptionFault : 1;
  bool ControlFault : 1;
  bool TemperatureReady : 1;
  DistanceType D1;
  DistanceType D2;
  DistanceType D3;
  Bits7<Scale<unsigned int, 8>, 21> Resource;
  struct Faults {
   bool FRAM : 1;
   bool CRC : 1;
   bool IntervalMeasurer : 1;
   bool QSwitch : 1;
   bool LPS : 1; // БПЛ
   bool WTF : 1; // ФПУ
   bool OpticalStart : 1;
  } Faults;
  struct State {
   unsigned char Targets : 2;
   bool LowPeriod : 1;
   bool OnboardNetwork : 1;
   bool WTF : 1; // Ошибка драйв. ТЕС
   bool TestingCompleted : 1;
   bool Failure : 1;
  } State;
  Packed::LTS_Type LTS;
  InputPacket() :
   Mode(), Readiness(), ReceptionFault(), ControlFault(), TemperatureReady(),
   D1(), D2(), D3(), Resource() {}
 };

 struct OutputPacket {
  typedef Bits7<Scale<unsigned int, 2>, 14> StrobeType;
  bool Gap : 2;
  bool Pilot : 1;
  bool Camera : 1;
  bool Frequent : 1;
  bool Single : 1;
  bool Work : 1;
  Bits7<Upscale<unsigned int, 2>, 7> Frequency;
  StrobeType StrobeNear;
  StrobeType StrobeFar;
 };
};

std::ostream& operator<<(std::ostream&, const LRF::InputData&);
std::ostream& operator<<(std::ostream&, const struct LRF::InputData::Faults&);
std::ostream& operator<<(std::ostream&, const struct LRF::InputData::State&);
std::ostream& operator<<(std::ostream&, const struct LRF::OutputData::Signals&);

class ModuleLRF : public ExchangeModuleImpl<LRF> {
 // constants set via Setup
 const double D_INIT; // initial distance

 double old_time;
 enum {
  DISTANCE_LOW = 150,
  DISTANCE_HIGH = 30000
 };
 int tact;
 double D;
 double tau1; // Interval vremeni mezhdu pervym i vtorym izmereniyami
 double tau2; // Interval vremeni mezhdu vtorym i tret'im izmereniyami
 double d1, d2, d3; // Pervye tri znacheniya izmerennoy dal'nosti
 double dT; // Interval vremeni mezhdu izmereniyami
 int missCnt;
 double Vd; // Ocenka skorosti izmeneniya soprovozhdaemoy dal'nosti
 double Ad; // Ocenka uskoreniya izmeneniya soprovozhdaemoy dal'nosti
 // frequent mode controlling constants
 const bool UseMultipleTargets;
 const int MissCountMax;
 CTime IT_IntervalMeasurer;
 const struct Time {
  const double IT_Trigger;
  const double RadiationInterval;
  Time(const ModuleInit&, const QString&);
 } Time;
 const struct FakeData {
  const bool use;
  const Function Count;
  const Function Distance1;
  const Function Distance2;
  const Function Distance3;
  FakeData(const ModuleInit&, const QString&);
 } Fake;
 const struct TestDump {
  const bool Before;
  const bool After;
  const bool Tact;
  const bool Distance;
  const bool DeltaTime;
  const bool Speed;
  const bool NewDistance;
  TestDump(const ModuleInit&, const QString&);
 } TestDump;
 CTime RadiationMeasurer;
 void PrepareInitMode();
 void PrepareWorkMode();
 void PrepareMeasureMode();
 void MeasuringProcess();
 void SingleLRF_Results();
 void FreqLRF_Results();
 double FakeDistance(int, double) const;
 void DecodePacket(bool) override;
 void EncodePacket(bool) override;
 void ProcessCommandVector() override;
 void PostProcessStatusVector() override;
 bool Measuring() const {	return Command.data.Signals.Measuring(); }
public:
 explicit ModuleLRF(const ModuleInit&);
 void Act();
 double SampleTime() const;
 bool RadiationPermitted() const {
  return !Command.TrainingButton;
 }
}; // end rangeTrkModuleClass

#endif /*RANGETRKMC_HH_*/
