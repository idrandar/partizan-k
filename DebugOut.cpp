/*
 */
// Testing dump stuff
// (C) I.Drandar, 2011-2015, all rights reserved
// (C) I.Drandar, for Qt + C++1x, 2018-2019, all rights reserved

#include "DebugOut.h"

std::ostream& operator<<(std::ostream& _, DAT::TargetType T) {
 switch (T) {
 case DAT::ET:
  _ << "Extended target";
  break;
 case DAT::PT:
  _ << "Point target";
  break;
 default:
  _ << "Unknown target type: " << static_cast<int>(T);
 }
 return _;
}

std::ostream& operator<<(std::ostream& _, const Weather& w) {
 _ << "wind speed: " << w.WindSpeed << " m/s" << "\twind bearing: " << w.WindBearing << "°"
  "\nair temperature: " << w.Temperature << "°C\tcharge temperature: " << w.ChargeTemperature << "°C\n"
  "air pressure: " << w.Pressure << " bar";
 return _;
}

std::ostream& operator<<(std::ostream& _, const QString& s) {
 _ << s.toLocal8Bit().constData();
 return _;
}
