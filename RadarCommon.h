#ifndef RADARCOMMON_H
#define RADARCOMMON_H

// radar common data for all modes
// (C) I.Drandar, 2019, all rights reserved
// Guidance and tracking controller - Radar
#include <array>
#include "common/data_convert.h"

namespace Radar {

enum class DistanceScale {
 _6km = 1, _21km = 2, _18km = 3, _24km = 4, _36km = 5
};

enum class OperatingFrequencyTuning {
 Fixed = 1, Adaptive = 2, Random = 3
};

enum class RepetitionPeriodAdjustment {
 Fixed, Wobble
};

enum class MovingTargetSelection {
 Disabled, Filter1, Filter2, Filter3
};

enum class FastFourierTransform {
 Disabled, _16point, _32point, _64point, _128point, _256point
};

enum class ActiveNoisePresence {
 Absent, Sighting, Barrier
};

}

template <>
inline constexpr std::array enum_rep<Radar::DistanceScale> = {
 "undefined", "6 km", "12 km", "18 km", "24 km", "36 km"
};

template <>
inline constexpr std::array enum_rep<Radar::OperatingFrequencyTuning> = {
 "undefined", "fixed", "adaptive", "random"
};

template <>
inline constexpr std::array enum_rep<Radar::RepetitionPeriodAdjustment> = {
 "fixed", "wobble"
};

template <>
inline constexpr std::array enum_rep<Radar::MovingTargetSelection> = {
 "disabled", "filter 1", "filter 2", "filter 3"
};

template <>
inline constexpr std::array enum_rep<Radar::FastFourierTransform> = {
 "disabled", "16 point", "32 point", "64 point", "128 point", "256 point"
};

template <>
inline constexpr std::array enum_rep<Radar::ActiveNoisePresence> = {
 "absent", "sighting", "barrier"
};

#endif // RADARCOMMON_H
