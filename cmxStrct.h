#ifndef CMXSTRCT_HH_
#define CMXSTRCT_HH_

/* The author: Aleksander Ketner             */
/* Date of creation: 01 August 2007          */
/* Сlass cmxStruct                           */
/* The main class cmxStruct                  */
/* The main class of system SPIS             */
/* IDE Momentics & QNX Neutrino 6.3.0        */
// modified for "Sarmat" project
// modified for "Owl" project, 2014
// (C) I.Drandar, 2010-2018, all rights reserved
// (C) I.Drandar, for Qt + C++1x, 2018-2019, all rights reserved

#include "GTC.h"
#include "Comp2.h"
#include "NS.h"
#include "LRF.h"
#include "StatusVector.h"
#include "Training.h"
#include "DAT.h"
#include "common/Record.h"
#include "FRAM.h"
#include "control/time.h"
#include <memory>
#include "TV.h"
#include "IR.h"
#include "GTC-2.h"
#include "GunHA.h"
#include "GunElev.h"
#include "GunTimer.h"
#include "Setup.h"
// partizan stuff
#include "GTC-R-Mode.h"
#include "PlannedFrequencies.h"
#include "RadialSpeed.h"
#include "RadarETD.h"
#include "RadarSS.h"
#include "RadarControl.h"
#include "RadarTraining.h"
#include "ETD_DetectionData.h"
#include "SS_Data.h"
#include "RadarTestResult.h"
#include "Comp2Radar.h"
// end of partizan stuff

class Worker : public Module {

 const double D_INIT;

 const double TIMER1_FREQUENCY;
 const double TIMER2_FREQUENCY;
 std::unique_ptr<AbstractTimer> dsptTmr;

 SystemMode GetMode() const;

 // LRF
 Record<ModuleLRF> LRF;
 void LRFFormCommand();

 // NS stuff
 Record<ModuleNS> NS;
 void NSFormCommand();

 // angle
 Record<ModuleGTC> GTC;
 void GTCFormCommand();
// bool CheckDiagram(const Spherical2DCoords&) const;
 const double OEUC_Offset;

 Record<ModuleGTC2> GTC2;

 // TV stuff
 Record<ModuleTV> TV;
 void TVFormCommand();

 // IR stuff
 Record<ModuleIR> IR;
 void IRFormCommand();

 // Comp2 stuff
 Record<ModuleComp2> Comp2;
 void Comp2FormCommand();

 // training stuff
 Record<ModuleTraining> Training;
 void TrainingFormCommand();

 // interface
 Interface::Command InterfaceCommand;
 Interface::Status InterfaceStatus;
 void InterfaceFormCommand();
 bool CheckSysModeComplete();

 // LTS
 Record<ModuleLTS> LTS;
 void LTSFormCommand() {} // corr_LTS_etc_funcs.cc

 // DAT
 Record<ModuleDAT> DAT1;
 Record<ModuleDAT> DAT2;
 const DAT::InputData& CurrentDAT() const;
 void DATFormCommand(DATChannelIndex);

 // Filter
 void SetCurrDistance(DistanceData&);
 Record<FilterModule> Filter;
 void FilterFormCommand();

 Record<FilterModule> FilterRadar;
 void FilterRadarFormCommand();

 // Collision task
 Record<CollisionTaskModule> Collision;
 Record<CollisionTaskModule> CollisionRadar;
 void CollisionFormCommand(Record<CollisionTaskModule>&, Record<FilterModule>&);

 // Gun Modules
 Record<ModuleGunHA> GunHA;
 void GunHAFormCommand();
 Record<ModuleGunElev> GunElev;
 void GunElevFormCommand();
 Record<ModuleGunTimer> GunTimer;
 void GunTimerFormCommand();

 Record<ModuleGunX> GunX;
 void GunXFormCommand();

 // FRAM
 Record<ModuleFRAM> FRAM;
 void FRAMFormCommand();

 // partizan stuff
 Record<ModuleGTCRMode> GTCRMode;
 void GTCRModeFormCommand();

 Record<ModulePlannedFrequencies> PlannedFrequencies;
 void PlannedFrequenciesFormCommand();

 Record<ModuleRadialSpeed> RadialSpeed;
 void RadialSpeedFormCommand();

 Record<ModuleRadarETD> RadarETD;
 void RadarETDFormCommand();

 Record<ModuleRadarSS> RadarSS;
 void RadarSSFormCommand();

 Record<ModuleRadarControl> RadarControl;
 void RadarControlFormCommand();

 Record<ModuleRadarTraining> RadarTraining;
 void RadarTrainingFormCommand();

 Record<ModuleETD_Detection> ETD_Detection;
 void ETD_DetectionFormCommand();

 Record<ModuleSS_Data> SS_Data;
 void SS_DataFormCommand();

 Record<ModuleRadarTest> RadarTest;
 void RadarTestFormCommand();

 Record<ModuleComp2Radar> Comp2Radar;
 void Comp2RadarFormCommand();
 // end of partizan stuff

 status GetTick();

 SystemMode Mode;
 bool TestingCompleted;
 bool Fault;
 Interface::Command::Main::SysUnitStruct sysFaults;
 Interface::Command::_System::Fault funcFaults;

 class DispatcherCaller;
 Module::Thread dispatcher;

 // testing
 bool TestingStarted;
 CTime TestingTime;
 const double TESTING_TIME_MAX;
 const struct TEST_IGNORE_DEF {
  bool Exchange;
  TEST_IGNORE_DEF(const ModuleInit&, const char[]);
 } TEST_IGNORE;
 const struct TestDump {
  const bool Timer1;
  const bool ElapsedTime;
  TestDump(const ModuleInit&, const char[]);
 } TestDump;
 double Interval; // check for timer stability
 bool ProcessTestResults();
 bool ProcessRuntimeStatus();
 bool CompFaults();
public:
 explicit Worker(const ModuleInit& init);
 ~Worker() override;
 virtual void Act();
 virtual void Start() override;
 virtual void Stop() override;
};

#endif /*CMXSTRCT_HH_*/
