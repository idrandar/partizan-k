// Processing data of "GUN" HA channel
// (C) I.Drandar, 2015, 2017, all rights reserved
// (C) I.Drandar, for Qt, 2018-2020, all rights reserved

#include "GunHA.h"
#include "common/CoordinatesConversion.h"
#include "DebugOut.h"
#include <iomanip>

ModuleGunHA::ModuleGunHA(const ModuleInit& _) : ModuleGun(_) {}

void ModuleGunHA::ProcessCommandVector() {
 if (Command.CollisionTaskPerformed == 0) {
  ClearStatus();
 } else {
  if (Control.UseShipCoords) {
   EvaluateUsingShipCoords();
  } else {
   EvaluateUsingGroundCoords();
  }
  Status.EvaluatedTime = Command.EvaluatedTime;
  GroundFinal = ConvertShipToGroundSpherical(ShipFinal, Command.Rot);
  Constr.EvaluateLimitsAndSetBounds(ShipFinal, Prev, Status.Limits);
 }
 if (Control.UseShipCoords) {
  auto conclusive = ShipFinal.Heading + Constr.Offset.Heading;
  NormalizeRadians(conclusive);
  Command.data.Position = conclusive;
 } else {
  Command.data.Position = GroundFinal.Bearing;
 }
 ModuleGun::ProcessCommandVector();
}

double ModuleGunHA::GetDesiredAngle(bool AnglesOut, bool Offset) const {
 double desired = Constr.Offset.Heading;
 if (AnglesOut || Command.data.Mode == Types::gmTesting) {
  if (Status.UseFinal) {
   desired = Status.Final.HA;
  } else {
   desired = Status.Preliminary.HA;
  }
 }
 if (Offset) {
  desired -= Constr.Offset.Heading;
 }
 NormalizeRadians(desired);
 return desired;
}
