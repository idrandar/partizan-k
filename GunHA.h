#ifndef GUNHA_H
#define GUNHA_H

// Processing data of "GUN" HA channel
// (C) I.Drandar, 2015, 2017, all rights reserved
// (C) I.Drandar, for Qt, 2018-2020, all rights reserved

#include "Gun.h"

class ModuleGunHA : public ModuleGun {
 void ProcessCommandVector() override;
public:
 ModuleGunHA(const ModuleInit&);
 double GetDesiredAngle(bool, bool) const;
};

#endif // GUNHA_H
