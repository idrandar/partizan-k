/* The author: Victor Poputnikov         		*/
/* Date of creation: 13 September 2007   */
/* Virtual interface class				      		*/
/* IDE Momentics & QNX Neutrino 6.3.0    */
// Modified for "Sarmat" project
// Status vector class
// (C) I.Drandar, 2010-2014, all rights reserved

#include "StatusVector.h"

StatusVector StatusVector::vect;

//Constructor
StatusVector::StatusVector() {
 pthread_mutex_init(&mutex, NULL);
}

StatusVector::~StatusVector() {
 pthread_mutex_destroy(&mutex);
}

void StatusVector::SetStatus(const Interface::Status& _) {
 Lock l;
 vect.Status = _;
}

void StatusVector::GetStatus(Interface::Status& _) {
 Lock l;
 _ = vect.Status;
}

void StatusVector::GetCommand(Interface::Command& _) {
 Lock l;
 _ = vect.Command;
}

void StatusVector::SetCommand(const Interface::Command& _) {
 Lock l;
 vect.Command = _;
}
