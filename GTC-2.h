#ifndef GTC2_HH_
#define GTC2_HH_

// nothing to say...
// (C) I.Drandar, 2019, all rights reserved

#include "data_types.h"
#include "common/ExchangeModule.h"

// Guidance an tracking controller #2
struct GTC2 { // works as namespace

 using OutputData = NOTHING;

 struct Faults {
  // ПФ1.1
  bool DeviceController; // КП
  bool LRF;
  bool TV;
  bool IR;
  bool ASS;
  Spherical2DCoordsType<bool> EncoderExchange;
  // motors
  Spherical2DCoordsType<bool> Encoder;
  // ПФ4.1
  bool GTC; // ЦК.28
  bool ExchangeDAT;
  bool ExchangeComp;
  bool ExchangeDC;
  bool ExchangeLTS_DC;
  bool ExchangeNavig;
  // ПФ7.2
  bool Overheat;    // превышение критической температуры
  bool No24V;       // отсутствие 24 В
  bool NoPhase;     // отсутствие фаз
  bool Overcurrent; // превышение тока
  bool No7_2;       // не вкл. 7.2.
  // Доп. неисп. НК КС
  bool NS_NoH; // не заг.
  bool NS_NoD; // не дан.
  // ПФ1.1.2
  bool AmplifierHAEngine;
  bool AmplifierHAA1;
  bool AmplifierElevEngine;
  bool AmplifierElevA3;
  bool ExchangeQ;
  bool ExchangeE;
  Faults() = default;
 };

 struct InputPacket {
  // layout of state packet's fields
  // this structure is compiler-dependent
  // possible use of conditional compilation needed in case of
  // transferring to other platform

  using FOV_Type = Spherical2DCoordsType<Packed::UnsignedAngle20>;
  // fields
  FOV_Type TV;
  FOV_Type IR;
  struct Something {
   bool FRAM : 1;
   bool CRC : 1;
   bool IntervalMeasurer : 1;
   bool QSwitch : 1;
   bool LPS : 1; // БПЛ
   bool WTF0 : 1; // ФПУ
   bool OpticalStart : 1;
   bool : 1; // 8-th bit
   unsigned char Targets : 2;
   bool LowPeriod : 1;
   bool OnboardNetwork : 1;
   bool WTF1 : 1; // Ошибка драйв. ТЕС
   bool TestingCompleted : 1;
   bool Failure : 1;
  } Something;
  struct Faults {
   // ПФ1.1
   struct {
    bool DeviceController: 1;     // КП A6
    bool LRF : 1;                 // A9
    bool TV : 1;                  // A13
    bool IR : 1;                  // A12
    bool ASS : 1;                 // A11
    bool EncoderHAExchange : 1;   // КУ.B1
    bool EncoderElevExchange : 1; // УМ.B2
   };
   struct { // ПФ7.2
    bool Overheat : 1;    // превышение критической температуры
    bool No24V : 1;       // отсутствие 24 В
    bool NoPhase : 1;     // отсутствие фаз
    bool Overcurrent : 1; // превышение тока
    bool No7_2 : 1;       // не вкл. 7.2.
    bool NS_NoH : 1;      // не заг.
    bool NS_NoD : 1;      // не дан.
   };
   struct {
    // ПФ1.1.2
    bool AmplifierHAEngine : 1;
    bool AmplifierHAA1 : 1;
    bool AmplifierElevEngine : 1;
    bool AmplifierElevA3 : 1;
    // motor
    bool EncoderHA : 1;
    bool EncoderElev : 1;
     // ПФ4.1
    bool GTC : 1; // ЦК.28
   };
   struct {
    // ПФ1.1.2
    bool ExchangeQ : 1;
    bool ExchangeE : 1;
    // ПФ4.1
    bool ExchangeDAT : 1;
    bool ExchangeComp : 1;
    bool ExchangeDC : 1;
    bool ExchangeLTS_DC : 1;
    bool ExchangeNavig : 1;
   };
  } Faults;

  InputPacket() = default;
 };

 struct InputData {
  Spherical2DCoords TV;
  Spherical2DCoords IR;
  struct Something {
   bool FRAM;
   bool CRC;
   bool IntervalMeasurer;
   bool QSwitch;
   bool LPS; // БПЛ
   bool WTF0; // ФПУ
   bool OpticalStart;
   unsigned int Targets;
   bool LowPeriod;
   bool OnboardNetwork;
   bool WTF1; // Ошибка драйв. ТЕС
   bool TestingCompleted;
   bool Failure;
  } Something;
  struct Faults Faults;
  InputData() = default;
 };

 struct CommandVector {};

 struct StatusVector {};

 using OutputPacket = NOTHING;
};

std::ostream& operator<<(std::ostream&, const struct GTC2::Faults&);

class ModuleGTC2 : public ExchangeModuleImpl<GTC2> {
 void DecodePacket(bool) override;
 void EncodePacket(bool) override;
public:
 explicit ModuleGTC2(const ModuleInit&);
 void Act();
};

#endif /*ANGLEMC_HH_*/
