#ifndef COMP2RADAR_H
#define COMP2RADAR_H

// radar data transferred via Ethernet
// (C) I.Drandar, 2019-2020, all rights reserved
#include "common/ExchangeModule.h"
#include "GTC-R-Mode.h"
#include "RadarETD.h"
#include "RadarSS.h"
#include "RadarControl.h"
#include "RadarTraining.h"
#include "RadarTestResult.h"
#include "ETD_DetectionData.h"
#include "SS_Data.h"
#include "PlannedFrequencies.h"

struct Comp2Radar {

 struct InputData {
  // mode & submode
  GTCRMode::OutputData Mode;
  bool War;
  // ETD data
  RadarETD::OutputData ETD;
  // SS data
  RadarSS::OutputData SS;
  // radar control
  RadarControl::OutputData Control;
  // training
  RadarTraining::OutputData Training;
  PlannedFrequencies::FrequenciesArray<PlannedFrequencies::Item> PF;
  double Time;
 };

#pragma pack(push, 4)
 struct InputPacket {
  // mode & submode
  std::byte ModeToken; // ignored; equals 0xCA
  GTCRMode::OutputPacket Mode;
  std::byte ModeChecksum; // ignored
  bool War;
  // ETD packet
  std::byte ETDToken; // ignored; equals 0xCC
  RadarETD::OutputPacket ETD;
  std::byte ETDChecksum; // ignored
  // SS packet
  std::byte SSToken; // ignored; equals 0xCD
  RadarSS::OutputPacket SS;
  std::byte SSChecksum; // ignored
  std::byte Gap0[2];
  // radar control
  std::byte ControlToken; // ignored; equals 0xCE
  RadarControl::OutputPacket Control;
  std::byte ControlChecksum; // ignored
  std::byte Gap1;
  // training
  std::byte TrainingToken; // ignored; equals 0xCF
  RadarTraining::OutputPacket Training;
  std::byte TrainingChecksum; // ignored
  PlannedFrequencies::FrequenciesArray<PlannedFrequencies::LowLevelItem> PF;
  double Time;
 };
#pragma pack(pop)

 static constexpr size_t TargetsArraySize = 4;
 template <typename T>
 using TargetsArray = std::array<T, TargetsArraySize>;

 struct OutputData {
  // mode & submode
  GTCRMode::InputData Mode;
  bool War;
  // ETD data
  RadarETD::InputData ETD;
  // SS data
  RadarSS::InputData SS;
  // radar control
  RadarControl::InputData Control;
  // test result
  RadarTestResult::InputData TestResult;
  // ETD mode detection data
  struct ETD_Detection {
   bool ETD_Mode;
   unsigned int TargetCount;
   struct TargetData {
    ::ETD_Detection::RangeTrackingState RTS;
    double Distance;
    Spherical2DCoords Mismatch;
   };
   TargetsArray<TargetData> Targets;
   Spherical2DShipCoords ORU; // optical-radar unit
   double Time;
  } ETD_Detection;
  struct SS_Data {
   bool SS_Mode;
   Radar::OperatingFrequencyTuning OFT;
   unsigned int FrequencyCode;
   unsigned int RepetitionPeriodCode;
   Radar::RepetitionPeriodAdjustment RPA;
   Radar::ActiveNoisePresence ANP;
   bool ActiveNoiseInterferenceProcessing;
   double Time;
  } SS_Data;
  PlannedFrequencies::FrequenciesArray<PlannedFrequencies::Item> PF;
  double Time;
 };

#pragma pack(push, 4)
 struct OutputPacket {
  // mode & submode
  std::byte ModeToken; // ignored; equals 0xDA
  GTCRMode::InputPacket Mode;
  std::byte ModeChecksum; // ignored
  bool War;
  // ETD packet
  std::byte ETDToken; // ignored; equals 0xDC
  RadarETD::InputPacket ETD;
  std::byte ETDChecksum; // ignored
  // SS packet
  std::byte SSToken; // ignored; equals 0xDD
  RadarSS::InputPacket SS;
  std::byte SSChecksum; // ignored
  std::byte Gap0[2];
  // radar control
  std::byte ControlToken; // ignored; equals 0xDE
  RadarControl::InputPacket Control;
  std::byte ControlChecksum; // ignored
  std::byte Gap1;
  // test result
  std::byte TestToken; // ignored; equals 0xD5
  RadarTestResult::InputPacket TestResult;
  std::byte TestChecksum;
  std::byte Gap2;
  // ETD mode detection data
  struct ETD_Detection {
   bool ETD_Mode;
   unsigned char TargetCount;
   struct TargetData {
    float Distance;
    Spherical2DCoordsType<float> Mismatch;
   };
   TargetsArray<::ETD_Detection::RangeTrackingState> RTS;
   TargetsArray<TargetData> Targets;
   Spherical2DShipCoordsType<float> ORU; // optical-radar unit
   double Time;
  } ETD_Detection;
  // SS mode data
  struct SS_Data {
   bool SS_Mode;
   std::byte Gap[3];
   union {
    struct {
     unsigned char : 5;
     unsigned char OFT : 2;
    };
    Bits7Shared<unsigned int, 9> FrequencyCode;
   };
   union {
    Bits7Shared<unsigned int, 6> RepetitionPeriodCode;
    struct {
     unsigned char : 6;
     unsigned char RPA : 1;
    };
   };
   unsigned char : 4;
   unsigned char ANP : 2;
   bool ActiveNoiseInterferenceProcessing : 1;
   double Time;
  } SS_Data;
  PlannedFrequencies::FrequenciesArray<PlannedFrequencies::LowLevelItem> PF;
  double Time;
 };
#pragma pack(pop)

 struct CommandVector {};
 struct StatusVector {};
};

class ModuleComp2Radar : public ExchangeModuleImpl<Comp2Radar> {
 const struct FakeData {
  const bool use;
  const Function TrainingDistance;
  const Function TrainingRadialSpeed;
  const Function TrainingDopplerShift;
  FakeData(const ModuleInit&, const QString&);
 } Fake;
 void DecodePacket(bool) override;
 void EncodePacket(bool) override;
 void PrepareStatusVector(bool) override;
public:
 ModuleComp2Radar(const ModuleInit&);
 void Act() override;
};

#endif // COMP2RADAR_H
