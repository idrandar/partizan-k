/* The author: Aleksander Ketner                */
/* Date of creation: 01 August 2007             */
/* The main class cmxStruct                     */
/* Contains implementation of a class cmxStruct */
/* Abstract class of system SPIS                */
/* IDE Momentics & QNX Neutrino 6.3.0           */
// Modified for "Sarmat" project
// (C) I.Drandar, 2010-2018, all rights reserved
// (C) I.Drandar, for Qt + C++1x, 2018-2020, all rights reserved

#include "cmxStrct.h"
#include <iostream>
#include "common/CoordinatesConversion.h"
#include "DebugOut.h"
#include <future>
#include <sys/mman.h>

class Worker::DispatcherCaller : public Caller {
 virtual status call() {
  return dynamic_cast<Worker&>(owner).GetTick();
 }
public:
 DispatcherCaller(Worker& _) : Caller(_) {}
};

Worker::TEST_IGNORE_DEF::TEST_IGNORE_DEF(const ModuleInit& init, const char _[]) :
 Exchange(init.GetConst(_, "exchange")) {
}

Worker::TestDump::TestDump(const ModuleInit& init, const char s[]) :
 Timer1(init.GetConst(s, "timer1")), ElapsedTime(init.GetConst(s, "elapsed-time")) {
}

void Worker::Act() {
 const double TimeStep = 1 / TIMER1_FREQUENCY;

 Interval = counter.Gone();
 counter.Start();
 dump_if(TestDump.Timer1) << "time gone: " << std::fixed << Interval << " s" << std::endl;
 CTime calcTime; // start of elapsed time measuring
 if (GunHA.HaveTick(TimeStep)) {
  GunHAFormCommand();
  GunHA.Interact();
 }
 if (GunElev.HaveTick(TimeStep)) {
  GunElevFormCommand();
  GunElev.Interact();
 }
 if (GunTimer.HaveTick(TimeStep)) {
  GunTimerFormCommand();
  GunTimer.Interact();
 }

 if (GunX.HaveTick(TimeStep)) {
  GunXFormCommand();
  GunX.Interact();
 }

 if (HaveTick(TimeStep)) {
  InterfaceFormCommand();
  ::StatusVector::SetCommand(InterfaceCommand);
  ::StatusVector::GetStatus(InterfaceStatus);
 }

 if (InterfaceStatus.Shutdown) {
  Mode = SYSMODE_Shutdown;
 } else Mode = GetMode();


 if (NS.HaveTick(TimeStep)) {
  NSFormCommand();
  NS.Interact();
 }

 if (Training.HaveTick(TimeStep)) {
  TrainingFormCommand();
  Training.Interact();
 }

 if (GTC.HaveTick(TimeStep)) {
  GTCFormCommand();
  GTC.Interact();
 }

 if (GTC2.HaveTick(TimeStep)) {
  GTC2.Interact();
 }

 if (TV.HaveTick(TimeStep)) {
  TVFormCommand();
  TV.Interact();
 }

 if (IR.HaveTick(TimeStep)) {
  IRFormCommand();
  IR.Interact();
 }

 if (LRF.HaveTick(TimeStep)) {
  LRFFormCommand();
  LRF.Interact();
 }

 if (DAT1.HaveTick(TimeStep)) {
  DATFormCommand(DAT_CHANNEL_1);
  DAT1.Interact();
 }
 if (DAT2.HaveTick(TimeStep)) {
  DATFormCommand(DAT_CHANNEL_2);
  DAT2.Interact();
 }

 if (Comp2.HaveTick(TimeStep)) {
  Comp2FormCommand();
  Comp2.Interact();
 }

 if (Mode == SYSMODE_Testing) {
  if (!TestingStarted) {
   TestingTime.Start();
   TestingStarted = true;
  }
  ProcessTestResults();
 } else {
  TestingCompleted = false;
  ProcessRuntimeStatus();
  TestingStarted = false;
 }

 if (LTS.HaveTick(TimeStep)) {
  LTSFormCommand();
  LTS.Interact();
 }

 if (FRAM.HaveTick(TimeStep)) {
  FRAMFormCommand();
  FRAM.Interact();
 }

 // partizan stuff
 if (GTCRMode.HaveTick(TimeStep)) {
  GTCRModeFormCommand();
  GTCRMode.Interact();
 }

 if (PlannedFrequencies.HaveTick(TimeStep)) {
  PlannedFrequenciesFormCommand();
  PlannedFrequencies.Interact();
 }

 if (RadialSpeed.HaveTick(TimeStep)) {
  RadialSpeedFormCommand();
  RadialSpeed.Interact();
 }

 if (RadarETD.HaveTick(TimeStep)) {
  RadarETDFormCommand();
  RadarETD.Interact();
 }

 if (RadarSS.HaveTick(TimeStep)) {
  RadarSSFormCommand();
  RadarSS.Interact();
 }

 if (RadarControl.HaveTick(TimeStep)) {
  RadarControlFormCommand();
  RadarControl.Interact();
 }

 if (RadarTraining.HaveTick(TimeStep)) {
  RadarTrainingFormCommand();
  RadarTraining.Interact();
 }

 if (ETD_Detection.HaveTick(TimeStep)) {
  ETD_DetectionFormCommand();
  ETD_Detection.Interact();
 }

 if (SS_Data.HaveTick(TimeStep)) {
  SS_DataFormCommand();
  SS_Data.Interact();
 }

 if (RadarTest.HaveTick(TimeStep)) {
  RadarTestFormCommand();
  RadarTest.Interact();
 }

 if (Comp2Radar.HaveTick(TimeStep)) {
  Comp2RadarFormCommand();
  Comp2Radar.Interact();
 }
 // end of partizan stuff

 if (Filter.HaveTick(TimeStep)) {
  FilterFormCommand();
  Filter.Interact();
 }
 if (FilterRadar.HaveTick(TimeStep)) {
  FilterRadarFormCommand();
  FilterRadar.Interact();
 }
 if (Collision.HaveTick(TimeStep)) {
  CollisionFormCommand(Collision, Filter);
  Collision.Interact();
 }
 if (CollisionRadar.HaveTick(TimeStep)) {
  CollisionFormCommand(CollisionRadar, FilterRadar);
  CollisionRadar.Interact();
 }

 calcTime.Stop();
 dump_if(TestDump.ElapsedTime) << "task performed in " << std::scientific << calcTime.Span() << " s\n";
} // end Run

Worker::Worker(const ModuleInit& init) try :
 Module(init),
 D_INIT(setup->GetConst("initial", "distance")),
 TIMER1_FREQUENCY(init.GetConst("time", "frequency1")),
 TIMER2_FREQUENCY(init.GetConst("time", "frequency2")),
 dsptTmr(
  std::make_unique<ClockNanosleepTimer>(CLOCK_MONOTONIC, TIMER_ABSTIME, 1.0 / TIMER1_FREQUENCY)),
 LRF(setup->GetModuleInit("LRF")), NS(setup->GetModuleInit("NS")),
 GTC(setup->GetModuleInit("GTC")),
 OEUC_Offset(setup->GetConst("constructive", "bearings")),
 GTC2(setup->GetModuleInit("GTC-2")),
 TV(setup->GetModuleInit("TV")), IR(setup->GetModuleInit("IR")),
 Comp2(setup->GetModuleInit("Comp2")),
 Training(setup->GetModuleInit("Training")), LTS(setup->GetModuleInit("LTS")),
 DAT1(setup->GetModuleInit("DAT1")), DAT2(setup->GetModuleInit("DAT2")),
 Filter(setup->GetModuleInit("Filter")), FilterRadar(setup->GetModuleInit("Filter-Radar")),
 Collision(setup->GetModuleInit("Collision")), CollisionRadar(setup->GetModuleInit("Collision-Radar")),
 GunHA(setup->GetModuleInit("Gun-HA")), GunElev(setup->GetModuleInit("Gun-Elev")),
 GunTimer(setup->GetModuleInit("Gun-Timer")), GunX(setup->GetModuleInit("Gun-X")),
 FRAM(setup->GetModuleInit("FRAM")),
 // partizan stuff
 GTCRMode(setup->GetModuleInit("Radar Mode")),
 PlannedFrequencies(setup->GetModuleInit("Planned Frequencies")),
 RadialSpeed(setup->GetModuleInit("Radial Speed")),
 RadarETD(setup->GetModuleInit("Radar ETD")), RadarSS(setup->GetModuleInit("Radar SS")),
 RadarControl(setup->GetModuleInit("Radar Control")),
 RadarTraining(setup->GetModuleInit("Radar Training")),
 ETD_Detection(setup->GetModuleInit("ETD detection data")),
 SS_Data(setup->GetModuleInit("SS data")),
 RadarTest(setup->GetModuleInit("Test result")),
 Comp2Radar(setup->GetModuleInit("Comp2 Radar")),
 // end of partizan stuff
 Mode(), TestingCompleted(), Fault(), sysFaults(), funcFaults(),
 dispatcher(init.Thread("timing"), new DispatcherCaller(*this)),
 TestingStarted(), TestingTime(),
 TESTING_TIME_MAX(init.GetConst("time", "testing")),
 TEST_IGNORE(init, "testing-ignore"), TestDump(init, "test-dump") {
 std::cout << __PRETTY_FUNCTION__ << std::endl;
} catch (std::exception& e) {
 std::cout << "exception in " << __PRETTY_FUNCTION__ << ":\t" << e.what() << std::endl;
 throw;
}

Worker::~Worker() {}

Module::status Worker::GetTick() {
 dsptTmr->Wait();
// std::async(std::launch::async, [this](){
//  dispatcher.WakeUp(); // to do !!!!
  Act();
// });
 if (dispatcher.IsRunning()) {
  return THREAD_OK;
 } else {
  return THREAD_TERMINATE;
 }
}

void Worker::Start() {
 Module::Start();

 Comp2.Start();
 NS.Start();
 GTC.Start();
 GTC2.Start();
 LRF.Start();
 LTS.Start();
 DAT1.Start();
 DAT2.Start();
 GunHA.Start();
 GunElev.Start();
 GunTimer.Start();
 GunX.Start();
 FRAM.Start();
 TV.Start();
 IR.Start();
 Training.Start();
 // partizan stuff
 GTCRMode.Start();
 PlannedFrequencies.Start();
 RadialSpeed.Start();
 RadarETD.Start();
 RadarSS.Start();
 RadarControl.Start();
 RadarTraining.Start();
 ETD_Detection.Start();
 SS_Data.Start();
 RadarTest.Start();
 Comp2Radar.Start();
 // end of partizan stuff
 Filter.Start();
 FilterRadar.Start();
 Collision.Start();
 CollisionRadar.Start();

 std::cout << "Creating system timer:\n";
 // value of system frequency is assigned via setup file
// dsptTmr = std::make_unique<NanosleepTimer>(1.0 / TIMER1_FREQUENCY); // to do !!!
 dsptTmr->Start();
// t.setTimerType(Qt::PreciseTimer);
// connect(&t, &QTimer::timeout, this, &Worker::Act);
// t.start(1000.0 / TIMER1_FREQUENCY);
 dispatcher.Start();
 // now thread is started
// mlockall(0);
}

void Worker::Stop() {
 munlockall();
 dsptTmr->Stop();
 CollisionRadar.Stop();
 Collision.Stop();
 FilterRadar.Stop();
 Filter.Stop();
 // partizan stuff
 Comp2Radar.Stop();
 RadarTest.Stop();
 SS_Data.Stop();
 ETD_Detection.Stop();
 RadarTraining.Stop();
 RadarControl.Stop();
 RadarSS.Stop();
 RadarETD.Stop();
 RadialSpeed.Stop();
 PlannedFrequencies.Stop();
 GTCRMode.Stop();
 // end of partizan stuff
 Training.Stop();
 IR.Stop();
 TV.Stop();
 FRAM.Stop();
 GunX.Stop();
 GunTimer.Stop();
 GunElev.Stop();
 GunHA.Stop();
 DAT2.Stop();
 DAT1.Stop();
 LTS.Stop();
 GTC2.Stop();
 GTC.Stop();
 LRF.Stop();
 NS.Stop();
 Comp2.Stop();
 dispatcher.Stop();
 Module::Stop();
}
//=================== End of cmxStr.cc file===================================

SystemMode Worker::GetMode() const {
 if (InterfaceStatus.Shutdown) return SYSMODE_Shutdown;
 if (Comp2.Out().Control.Mode == Comp2::mTesting) return SYSMODE_Testing;
 if (!Comp2.Status.NewInfo || Comp2.Out().Control.SubMode == Comp2::smInitial) {
  return SYSMODE_Initial;
 }
 if (Comp2.Out().Control.SubMode == Comp2::smIT) return SYSMODE_IT;
 if (Comp2.Out().Control.SubMode == Comp2::smAT) {
// if (AT_3coord) {
  return SYSMODE_AT;
 }
 if (Comp2.Out().Control.SubMode == Comp2::smET) return SYSMODE_ETD;
 if (Comp2.Out().Control.SubMode == Comp2::smMP) return SYSMODE_MP;
 if (Comp2.Out().Control.SubMode == Comp2::smSS) return SYSMODE_SS;
// if (curModeKbd.submode == FKBD::ModeKbds::S_ITD) return SYSMODE_ITD;
 return SYSMODE_MP;
}

void Worker::TrainingFormCommand() {
 Training.Command.Start = Comp2.Out().Control.Training.On;
 Training.Command.TargetType = Comp2.Out().Control.Training.Code;
 Training.Command.StationaryTarget.On = Comp2.Out().Control.Training.StationaryTarget;
 Training.Command.StationaryTarget.Coords = Comp2.Out().Control.Training.StationaryTargetCoords;
 Training.Command.LTS = LTS.AdjustedTime();
 auto tr = Training.In();
 tr.On = Comp2.Out().Control.Mode == Comp2::mTraining && Training.Command.Start;
 tr.Square = Comp2.Out().DAT_Control.DAT1.TT == DAT::PT;
 if (Comp2.Out().Control.Camera == DAT::IR) {
  tr.Inversion = IR.In().HotBlack;
 } else {
  tr.Inversion = false;
 }
 Training.PerformEvaluations(tr);
 Training.Command.Go =
  tr.Sph != Training.In().Sph || tr.On != Training.In().On ||
  tr.Square != Training.In().Square || tr.Inversion != Training.In().Inversion ||
  tr.DirectionLeft != Training.In().DirectionLeft ||
  tr.Type != Training.In().Type || tr.Size != Training.In().Size;
 Training.In() = tr;
}

void Worker::NSFormCommand() {} // end

void Worker::LRFFormCommand() {
 LRF.Command.Mode = Mode;
 LRF.Command.TargetNumber = Comp2.Out().LRF.TargetNumber;
 if (LRF.Command.TargetNumber > 2) LRF.Command.TargetNumber = 0;
 LRF.Command.ExchangeAllow = true;
 LRF.Command.TrainingStart = Comp2.Out().Control.Training.On;
 LRF.Command.TrainingDistance = Training.In().Sph.Distance;
 LRF.Command.TrainingButton = Comp2.Out().Control.Mode == Comp2::mTraining;
 LRF.Command.TrainingTime = LTS.AdjustedTime();
 // LRFCommand.BlockingRadiation = !AngleStatus.CheckAngles() || !CheckDiagram(AngleInput().OEUC.Ship);
 LRF.Command.BlockingRadiation = false;
 LRF::OutputData _LRF;
 _LRF.Frequency = Comp2.Out().LRF.Frequency;
 LRF.Command.Choice = Comp2.Out().LRF.TargetNumber;
 _LRF.Signals.Frequent = Comp2.Out().LRF.Signals.Frequent;
 _LRF.Signals.Single = Comp2.Out().LRF.Signals.Single;
 _LRF.Signals.Work = Comp2.Out().LRF.Signals.Work;
 _LRF.Signals.Pilot = Comp2.Out().LRF.Signals.PilotLaser;
 _LRF.Signals.Camera = Comp2.Out().LRF.Signals.Camera;
 _LRF.Frequency = Comp2.Out().LRF.Frequency;
 if ((LRF.In().Frequency != _LRF.Frequency) ||
     (LRF.In().Signals.Frequent != _LRF.Signals.Frequent) ||
     (LRF.In().Signals.Single != _LRF.Signals.Single) ||
     (LRF.In().Signals.Work != _LRF.Signals.Work) ||
     (LRF.In().Signals.Camera != _LRF.Signals.Camera) ||
     (LRF.In().Signals.Pilot != _LRF.Signals.Pilot)) {
  LRF.In() = _LRF;
  LRF.Command.Go = true;
 } else {
  LRF.Command.Go = false;
 }
} // end FormRangeCmd()

void Worker::TVFormCommand() {
 auto _TV = Comp2.Out().TV;
 _TV.CenterCorrection.Y *= -1;
 if (TV.In() != _TV) {
  TV.In() = _TV;
  TV.Command.Go = true;
 } else {
  TV.Command.Go = false;
 }
}

void Worker::IRFormCommand() {
 auto ir = Comp2.Out().IR;
 ir.CenterCorrection.Y *= -1;
 auto& _ir = IR.In();
 if (_ir.MenuOn != ir.Menu.On ||
     _ir.ContrastPlus != ir.ContrastPlus || _ir.ContrastMinus != ir.ContrastMinus ||
     _ir.BrightnessPlus != ir.BrightnessPlus || _ir.BrightnessMinus != ir.BrightnessMinus ||
     _ir.HotBlack != ir.HotBlack || _ir.DigitalZoom != ir.DigitalZoom ||
     _ir.OpticalZoomPlus != ir.OpticalZoomPlus || _ir.OpticalZoomMinus != ir.OpticalZoomMinus ||
     _ir.FocusFar != ir.FocusFar || _ir.FocusNear != ir.FocusNear ||
     _ir.Brightness != ir.Brightness || _ir.Contrast != ir.Contrast ||
     _ir.Calibration != ir.Calibration|| _ir.CenterCorrection != ir.CenterCorrection) {
  _ir.MenuOn = ir.Menu.On;
  _ir.ContrastPlus = ir.ContrastPlus;
  _ir.ContrastMinus = ir.ContrastMinus;
  _ir.BrightnessPlus = ir.BrightnessPlus;
  _ir.BrightnessMinus = ir.BrightnessMinus;
  _ir.HotBlack = ir.HotBlack;
  _ir.DigitalZoom = ir.DigitalZoom;
  _ir.OpticalZoomPlus = ir.OpticalZoomPlus;
  _ir.OpticalZoomMinus = ir.OpticalZoomMinus;
  _ir.FocusFar = ir.FocusFar;
  _ir.FocusNear = ir.FocusNear;
  _ir.Brightness = ir.Brightness;
  _ir.Contrast = ir.Contrast;
  _ir.Calibration = ir.Calibration;
  _ir.CenterCorrection = ir.CenterCorrection;
  IR.Command.Go = true;
 } else {
  IR.Command.Go = false;
 }
}

inline constexpr double LowDistanceThreshold = 10;
void Worker::SetCurrDistance(DistanceData& d) {
 d.Speed = 0;
 d.Acceleration = 0.0;
 d.Time = LTS.Out().LTS;
 if (Mode == SYSMODE_IT) {
  d.OK = true;
  return;
 }
 if (Mode == SYSMODE_ETD) {
  if (Comp2.Out().Control.ExternalTarget.Distance > LowDistanceThreshold) {
   d.Distance = Comp2.Out().Control.ExternalTarget.Distance;
  }
  d.OK = true;
  return;
 }
 if (Mode == SYSMODE_ITD) {
  d.OK = true;
  return;
 }
 if (Comp2.Out().Control.ManualDistanceOn) {
  if (Comp2.Out().Control.ManualDistance > LowDistanceThreshold)
   d.Distance = Comp2.Out().Control.ManualDistance;
  d.OK = true;
  return;
 }
 if (Mode == SYSMODE_Initial && d.Distance <= LowDistanceThreshold) {
  d.Distance = D_INIT;
  return;
 }
 return;
}

void Worker::Comp2FormCommand() {
 Comp2.In().State.Mode = Comp2.Out().Control.Mode;
 Comp2.In().State.SubMode = Comp2.Out().Control.SubMode;
 Comp2.In().State.AdditionalSearch = GTC.Out().State.Mode == GTC::mAS_Spiral;
 Comp2.In().State.LowFlyingTarget = GTC.Out().State.Mode == GTC::mLFT;
 Comp2.In().State.ModeIsChanging = GTC.Out().State.ModeIsChanging;
 Comp2.In().State.Ready.TV = GTC.Out().TVReady;
 Comp2.In().State.Ready.IR = GTC.Out().IRReady;
 Comp2.In().BallisticModule.State.ITReady =
  (Mode == SYSMODE_AT) &&
  LRF.Status.IT_Permitted && Filter.Status.Extrapolation.OK() && Filter.Out().Completed;
 Comp2.In().State.Ready.Navig = !NS.ExchangeFault();
 Comp2.In().GTC.State = GTC.Out().State;
 Comp2.In().GTC.Power = GTC.Out().Power;
 Comp2.In().GTC._7_2 = GTC.Out()._7_2;
 Comp2.In().GTC.DATChannel = GTC.Out().DATChannel;
 Comp2.In().GTC.IIT = GTC.Out().IIT_Permit;
 Comp2.In().GTC.SyncRequest = GTC.Out().SyncRequest;
 Comp2.In().GTC.SyncDone = GTC.Out().SyncDone;
 // Faults
 auto GTC_Faults = GTC2.Out().Faults;
 Comp2.In().GTC.Faults.DeviceController = GTC_Faults.DeviceController;
 Comp2.In().GTC.Faults.LRF = GTC_Faults.LRF;
 Comp2.In().GTC.Faults.TV = GTC_Faults.TV;
 Comp2.In().GTC.Faults.IR = GTC_Faults.IR;
 Comp2.In().GTC.Faults.ASS = GTC_Faults.ASS;
 Comp2.In().GTC.Faults.EncoderExchange = GTC_Faults.EncoderExchange;
 Comp2.In().GTC.Faults.Encoder = GTC_Faults.Encoder;
 Comp2.In().GTC.Faults.Detent = GTC.Out().DetentError;
 Comp2.In().GTC.Faults.GTC = GTC_Faults.GTC;
 Comp2.In().GTC.Faults.ExchangeDAT = GTC_Faults.ExchangeDAT;
 Comp2.In().GTC.Faults.ExchangeComp = GTC_Faults.ExchangeComp;
 Comp2.In().GTC.Faults.ExchangeDC = GTC_Faults.ExchangeDC;
 Comp2.In().GTC.Faults.ExchangeLTS_DC = GTC_Faults.ExchangeLTS_DC;
 Comp2.In().GTC.Faults.ExchangeNavig = GTC_Faults.ExchangeNavig;
 Comp2.In().GTC.Faults.Overheat = GTC_Faults.Overheat;
 Comp2.In().GTC.Faults.No24V = GTC_Faults.No24V;
 Comp2.In().GTC.Faults.NoPhase = GTC_Faults.NoPhase;
 Comp2.In().GTC.Faults.Overcurrent = GTC_Faults.Overcurrent;
 Comp2.In().GTC.Faults.No7_2 = GTC_Faults.No7_2;
 // ПФ1.1.2
 Comp2.In().GTC.Faults.AmplifierHAEngine = GTC_Faults.AmplifierHAEngine;
 Comp2.In().GTC.Faults.AmplifierHAA1 = GTC_Faults.AmplifierHAA1;
 Comp2.In().GTC.Faults.AmplifierElevEngine = GTC_Faults.AmplifierElevEngine;
 Comp2.In().GTC.Faults.AmplifierElevA3 = GTC_Faults.AmplifierElevA3;
 Comp2.In().GTC.Faults.ExchangeQ = GTC_Faults.ExchangeQ;
 Comp2.In().GTC.Faults.ExchangeE = GTC_Faults.ExchangeE;

 if (Comp2.Out().Control.Training.On) {
  Comp2.In().GTC.TrainingTarget = Training.In().Sph;
 } else {
  Comp2.In().GTC.TrainingTarget = { 2000.0, 0.0, 0.0 };
 }
 Comp2.In().GTC.Distance = Filter.Command.DD.Distance;
 Comp2.In().GTC.OEUC = GTC.Out().ORU;
 Comp2.In().GTC.Mismatch = GTC.Out().Mismatch;
// Comp2.In().GTC.DAT.State.DATChannel = GTC.Out().State.DATChannel;
 Comp2.In().GTC.DAT.Channel[DAT_CHANNEL_1] = DAT1.Out();
 Comp2.In().GTC.DAT.Channel[DAT_CHANNEL_2].AutoTrackingMismatch.Elev = 1;
 Comp2.In().GTC.TV = GTC2.Out().TV;
 Comp2.In().GTC.IR = GTC2.Out().IR;
 Comp2.In().GTC.IR_Temperature = 0;
 auto& _LRF = Comp2.In().LRF;
 _LRF.Mode = LRF.Out().Mode;
 _LRF.Readiness = LRF.Out().Readiness;
 _LRF.ReceptionFault = LRF.Out().ReceptionFault;
 _LRF.ControlFault = LRF.Out().ControlFault;
 _LRF.TemperatureReady = LRF.Out().TemperatureReady;
 _LRF.Faults = LRF.Out().Faults;
 _LRF.State = LRF.Out().State;
 _LRF.Distance1 = LRF.Out().Distance1;
 _LRF.Distance2 = LRF.Out().Distance2;
 _LRF.Distance3 = LRF.Out().Distance3;
 _LRF.Measuring = LRF.Status.Measuring;
 _LRF.Resource = LRF.Out().Resource;
 Spherical2DCoords DesiredAngle;
 DesiredAngle.HA = GunHA.GetDesiredAngle(true, false);
 DesiredAngle.Elev = GunElev.GetDesiredAngle(true, false);
 auto& BM = Comp2.In().BallisticModule;
 BM.Mode = Comp2.In().State.Mode;
 BM.SubMode = Comp2.In().State.SubMode;
 BM.State.PointingAngleReady =
  GunHA.Status.HaveSolution && GunElev.Status.HaveSolution;
 BM.ExchangeFaults.Gun = GunHA.ExchangeFault() || GunElev.ExchangeFault();
 BM.ExchangeFaults.Comp2 = Comp2.ExchangeFault();
 BM.ExchangeFaults.GTC = GTC.ExchangeFault();
 BM.ExchangeFaults.Navig = NS.ExchangeFault() || NS.Out().Faults.Any();
 BM.Faults.GunXExchange = GunX.ExchangeFault();
 BM.Faults.GunHAExchange = GunHA.ExchangeFault();
 BM.Faults.GunHA = GunHA.Out().Faults.Any();
 BM.Faults.GunElevExchange = GunElev.ExchangeFault();
 BM.Faults.GunElev = GunElev.Out().Faults.Any();
 BM.Faults.GunTimerExchange = GunTimer.ExchangeFault();
 BM.Faults.GunTimer = GunTimer.Out().Faults.Any();
 BM.Limits.RightHA = GunHA.Status.Limits.RightHA;
 BM.Limits.LeftHA = GunHA.Status.Limits.LeftHA;
 BM.Limits.HighElev = GunElev.Status.Limits.HighElev;
 BM.Limits.LowElev = GunElev.Status.Limits.LowElev;
 Comp2.In().Gun.DesiredAngle = DesiredAngle;
 Comp2.In().Gun.TowerPosition.Heading = GunX.Out().Ship_HA[0];
 Comp2.In().Gun.TowerPosition.Heading +=
  Comp2.Out().GC.DriveCorrections.SituationAngleMismatch.HA;
 Comp2.In().Gun.TowerPosition.Elevation +=
  Comp2.Out().GC.DriveCorrections.SituationAngleMismatch.Elev;
 Comp2.In().Gun.MeetDistance = Collision.SolverStatus().data.Extrapolation.DistanceMeet;
 Comp2.In().Gun.CurrentDistance = Collision.SolverStatus().data.Extrapolation.DistanceCurrent;
 Comp2.In().Gun.FlightTime = Collision.SolverStatus().data.Extrapolation.FlightTime;
 Comp2.In().Navig.Rot = NS.Out().Rot;
 Comp2.In().Navig.RotChange = NS.Out().RotChange;
 Comp2.In().Navig.SpeedPart = NS.Out().SpeedPart;
 Comp2.In().Navig.Faults = NS.Out().Faults;
 Comp2.In().Time = GlobalTime();
}

void Worker::GTCFormCommand() {
 SphericalCoords Sph;
 auto& g = GTC.In();
 if (Mode == SYSMODE_ITD) {
  Sph = Comp2.Out().Control.ExternalTarget;// + CSOut().BearingChange * dT;
 } else if (Mode == SYSMODE_IT) {
  Spherical2DGroundCoords ETD_IT = Filter.Status.ETD_IT;
  Normalize(ETD_IT);
  GTC.In().ORU.ETD = ETD_IT;
 } else if (Mode == SYSMODE_MP && !Training.Command.Start) {
  GTC.In().ORU.ETD = GTC.Out().ORU.Ground;
 } else if (Mode == SYSMODE_ETD) {
  if (Training.Command.Start) {
   Sph = Training.In().Sph;
  } else {
   Sph = Comp2.Out().Control.ExternalTarget;
  }
 } else if (Mode == SYSMODE_SS) {
  GTC.In().ORU.ETD.Elevation = Comp2.Out().Control.ExternalTarget.Elevation;
 }

 const auto& ctrl = Comp2.Out().Control;
 // evaluate GTC mode by sygnals
 if (ctrl.AdditionalSearch) {
  g.Mode = GTC::mAS_Spiral;
 } else if (ctrl.LowFlyingTarget) {
  g.Mode = GTC::mLFT;
 } else if (Mode == SYSMODE_AT && ctrl.Radar) {
  g.Mode = GTC::mAT_Radar;
 } else if (Mode <= SYSMODE_Shutdown) {
  static constexpr std::array WorkModes = {
   GTC::mInitial, // SYSMODE_Initial,
   GTC::mETD,     // SYSMODE_ETD, // external tracking data
   GTC::mSS,      // SYSMODE_SS,  // standalone search
   GTC::mAT_DAT,  // SYSMODE_AT,  // autotracking
   GTC::mIT,      // SYSMODE_IT,  // inertial tracking
   GTC::mTesting, // SYSMODE_Testing,
   GTC::mETD,     // SYSMODE_ITD, // internal target data
   GTC::mMP,      // SYSMODE_MP,  // manual pointing
   GTC::mInitial, // SYSMODE_Shutdown
  };
  g.Mode = WorkModes[Mode];
 } else {
  g.Mode = GTC::mInitial;
 }
 g.WiperOn = ctrl.WiperOn;
 g.HeatingOn = ctrl.InternalHeatingOn;
 g.HeatStandby = ctrl.HeatStandby;
 g.Fire = ctrl.Fire;
 g.Handle.Deviation = ctrl.Joystick;
 g.DATChannel = ctrl.DATChannel;
 g.IIT_Enable = ctrl.IIT_Enable;
 g.HeatStandby = ctrl.HeatStandby;
 g.IROn = Comp2.Out().Control.IROn;
 GTC.Command.Rot = NS.Out().Rot;
}

inline constexpr double IT_Coeff = 1850.0;

void Worker::FilterFormCommand() {
 auto& F = Filter;
 auto FilterInput = F.In();
 auto& d = F.Command.DD;
 SetCurrDistance(d);
 if (Mode == SYSMODE_AT || Mode == SYSMODE_MP) {
  d.Time = LRF.SampleTime();
  if (LRF.Status.processedData.new_D) {
   if (LRF.Status.processedData.Distance > LowDistanceThreshold)
    d.Distance = LRF.Status.processedData.Distance;
   d.Speed = LRF.Status.processedData.DistanceChange;
   d.Acceleration = LRF.Status.processedData.DistanceChangeAcceleration;
   d.OK = true;
  }
 }
 F.Command.NS_Data = NS.Status.Processed;
 F.Command.Mode = Mode;
 FilterInput.Ship = GTC.Out().ORU.Ship;
 FilterInput.Ground = GTC.Out().ORU.Ground;
 FilterInput.Time = GTC.Out().LTS;
 FilterInput.AutotrackingMismatch = CurrentDAT().AutotrackingMismatch;
 F.Command.InertialtrackingMismatch.HA =
  MATH_CONST::Deg2Rad(Comp2.Out().Control.Joystick.X * GTC2.Out().TV.HA / IT_Coeff);
 F.Command.InertialtrackingMismatch.Elev =
  MATH_CONST::Deg2Rad(Comp2.Out().Control.Joystick.Y * GTC2.Out().TV.Elev / IT_Coeff);
 F.Command.AirTarget = Comp2.Out().Control.AirTarget;
 F.In() = FilterInput;
}

void Worker::FilterRadarFormCommand() {
 auto& F = FilterRadar;
 auto FilterInput = F.In();
 auto& d = F.Command.DD;
 SetCurrDistance(d);
 const auto& tgt = ETD_Detection.Out().First;
 if (Mode == SYSMODE_AT || Mode == SYSMODE_MP) {
  if (ETD_Detection.NewInfo() && tgt.RTS == ETD_Detection::RangeTrackingState::AT) {
   auto Distance = tgt.Distance;
   if (Distance > LowDistanceThreshold)
    d.Distance = Distance;
   d.Speed = 0;
   d.Acceleration = 0;
   d.OK = true;
  }
 }
 F.Command.NS_Data = NS.Status.Processed;
 auto m = Mode;
 if (tgt.RTS == ETD_Detection::RangeTrackingState::AT) {
  m = SYSMODE_AT;
 } else if (tgt.RTS == ETD_Detection::RangeTrackingState::IT) {
  m = SYSMODE_IT;
 }
 F.Command.Mode = m;
 FilterInput.Ship = GTC.Out().ORU.Ship;
 FilterInput.Ground = GTC.Out().ORU.Ground;
 FilterInput.Time = GTC.Out().LTS;
 if (ETD_Detection.NewInfo()) {
  FilterInput.AutotrackingMismatch = ETD_Detection.Out().First.CommonModeDeviation;
 } else {
  FilterInput.AutotrackingMismatch = {};
 }
 F.Command.InertialtrackingMismatch.HA =
  MATH_CONST::Deg2Rad(Comp2.Out().Control.Joystick.X * GTC2.Out().TV.HA / IT_Coeff);
 F.Command.InertialtrackingMismatch.Elev =
  MATH_CONST::Deg2Rad(Comp2.Out().Control.Joystick.Y * GTC2.Out().TV.Elev / IT_Coeff);
 F.Command.AirTarget = Comp2.Out().Control.AirTarget;
 F.In() = FilterInput;
}

void Worker::CollisionFormCommand(Record<CollisionTaskModule>& c, Record<FilterModule>& f) {
 c.Command.FiltrationPerformedCount = f.Status.Performed;
 c.Command.ShotTime = c.NextTime(LTS.AdjustedTime());
 c.Command.Extra.Mode = Mode;
 c.Command.Extra.PerformedFiltrationInCurrentMode = f.Status.PerformedFiltrationInCurrentMode;

 c.Command.Extra.Weather = Comp2.Out().Weather;
 c.Command.Extra.ShipRaw = GTC.Out().ORU.Ship;
 c.Command.Extra.GroundRaw = GTC.Out().ORU.Ground;
 c.Command.Extra.AirTargetChanged = f.Status.AirTargetChanged;
 auto bd = Comp2.Out().GC.BalData;
 auto& SolverCommand = c.SolverCommand();
 SolverCommand.Ballistic.StartSpeedMismatch = bd[0].StartSpeedMismatch; //  m/s
 SolverCommand.Ballistic.MassMismatch = bd[0].MassMismatch; //  kg
 SolverCommand.JointAlignment = Comp2.Out().GC.DriveControl.JointAlignment;
 SolverCommand.ParallaxJointAlignment = Comp2.Out().GC.DriveControl.ParallaxJointAlignment;
 SolverCommand.ReferenceDistance = Comp2.Out().GC.DriveControl.Distance;
 SolverCommand.Mode = Mode;
 c.Command.FilterOut = f.Status.data;
 c.Command.NS_Extrapolation = f.Status.Extrapolation.NS;
 c.Command.Angles_Extrapolation = f.Status.Extrapolation.Angles;
}

bool Worker::ProcessTestResults() {
 if (TestingCompleted) return true;
 return false;
}

bool Worker::ProcessRuntimeStatus() {
 sysFaults = {};
 funcFaults = {};
 return CompFaults();
}

bool Worker::CompFaults() {
 bool retval = false;
 if (!TEST_IGNORE.Exchange &&
  (!NS.IsOpen() || !GTC.IsOpen() || !LRF.IsOpen() || !LTS.IsOpen() || !Comp2.IsOpen())) { // corr110505.cc
  funcFaults = Interface::Command::_System::ffExchangeSerial;
  sysFaults.u51.comExtTEWS = true;
  retval = true;
 }
 if (NS.ExchangeFault() && GTC.ExchangeFault() && LRF.ExchangeFault() && LTS.ExchangeFault() && // corr110505.cc
  Comp2.ExchangeFault()) {
  funcFaults = Interface::Command::_System::ffExchangeSerial;
  sysFaults.u51.comExtTEWS = true;
  retval = true;
 }
 if (!TEST_IGNORE.Exchange && Comp2.ExchangeFault()) {
  funcFaults = Interface::Command::_System::ffExchangeEthernet;
  sysFaults.u51.comExtEthernet = true;
  retval = true;
 }
 return retval;
}

void Worker::InterfaceFormCommand() {
 InterfaceCommand.System.State.Mode = Mode;
 InterfaceCommand.System.State.ModeComplete = CheckSysModeComplete();
 InterfaceCommand.System.State.TestingCompleted = TestingCompleted;
 InterfaceCommand.System.State.Fault = Fault;

 InterfaceCommand.Main.Interval = Interval;
 InterfaceCommand.Main.External.GunHA.Assign(GunHA);
 InterfaceCommand.Main.External.GunElev.Assign(GunElev);
 InterfaceCommand.Main.External.GunTimer.Assign(GunTimer);
 InterfaceCommand.Main.External.GunX.Assign(GunX);

 InterfaceCommand.Main.Evaluations.CollisionOptical.Solver_In = Collision.SolverCommand();
 InterfaceCommand.Main.Evaluations.CollisionOptical.Solver_Out = Collision.SolverStatus();
 InterfaceCommand.Main.Evaluations.CollisionRadar.Solver_In = CollisionRadar.SolverCommand();
 InterfaceCommand.Main.Evaluations.CollisionRadar.Solver_Out = CollisionRadar.SolverStatus();

 InterfaceCommand.Main.External.NS.Assign(NS);

 InterfaceCommand.Main.External.Radar.Mode.Assign(GTCRMode);
 InterfaceCommand.Main.External.Radar.PlannedFrequencies.Assign(PlannedFrequencies);
 InterfaceCommand.Main.External.Radar.RadialSpeed.Assign(RadialSpeed);
 InterfaceCommand.Main.External.Radar.ETD.Assign(RadarETD);
 InterfaceCommand.Main.External.Radar.SS.Assign(RadarSS);
 InterfaceCommand.Main.External.Radar.Control.Assign(RadarControl);
 InterfaceCommand.Main.External.Radar.Training.Assign(RadarTraining);
 InterfaceCommand.Main.External.Radar.ETD_Detection.Assign(ETD_Detection);
 InterfaceCommand.Main.External.Radar.SS_Data.Assign(SS_Data);
 InterfaceCommand.Main.External.Radar.Test.Assign(RadarTest);

 if (Mode == SYSMODE_ITD) {
  InterfaceCommand.System.TD.Accept = true;
 } else if (Mode == SYSMODE_ETD) {
  InterfaceCommand.System.TD.Distance = Comp2.Out().Control.ExternalTarget.Distance;
  InterfaceCommand.System.TD.Bearing = Comp2.Out().Control.ExternalTarget.Bearing;
  InterfaceCommand.System.TD.Elevation = Comp2.Out().Control.ExternalTarget.Elevation;
  InterfaceCommand.System.TD.Accept = true;
 } else {
  InterfaceCommand.System.TD.Accept = false;
 }
 InterfaceCommand.System.FuncFault = funcFaults;

 InterfaceCommand.Main.Internal.GTC.Assign(GTC);
 InterfaceCommand.Main.Internal.DAT[DAT_CHANNEL_1].Assign(DAT1);
 InterfaceCommand.Main.Internal.DAT[DAT_CHANNEL_2].Assign(DAT2);
 InterfaceCommand.Main.Internal.TV.Assign(TV);
 InterfaceCommand.Main.Internal.IR.Assign(IR);
 InterfaceCommand.Main.Internal.LTS.Assign(LTS);
 InterfaceCommand.Main.Internal.LRF.Assign(LRF);
 InterfaceCommand.Main.Internal.GTC2.Assign(GTC2);
 InterfaceCommand.Main.Internal.Training.Assign(Training);
 InterfaceCommand.Main.Internal.Comp2Radar.Assign(Comp2Radar);

 InterfaceCommand.Main.Internal.Comp2.Assign(Comp2);

 InterfaceCommand.Main.Evaluations.FilterOptical.In = Filter.Command;
 InterfaceCommand.Main.Evaluations.FilterOptical.Out = Filter.Status;
 InterfaceCommand.Main.Evaluations.FilterRadar.In = FilterRadar.Command;
 InterfaceCommand.Main.Evaluations.FilterRadar.Out = FilterRadar.Status;
 InterfaceCommand.Main.Evaluations.CollisionOptical.In = Collision.Command;
 InterfaceCommand.Main.Evaluations.CollisionOptical.Out = Collision.Status;
 InterfaceCommand.Main.Evaluations.CollisionRadar.In = CollisionRadar.Command;
 InterfaceCommand.Main.Evaluations.CollisionRadar.Out = CollisionRadar.Status;

 InterfaceCommand.Main.Faults = sysFaults;
}

void Worker::DATFormCommand(DATChannelIndex _) {
 if (_ == DAT_CHANNEL_1) {
  DAT1.In() = Comp2.Out().DAT_Control.DAT1;
  DAT1.In().ReserveMethodCoords.X = Comp2.Out().DAT_Control.DAT1.ReserveMethodCoords.X * 350 / 2048;
  DAT1.In().ReserveMethodCoords.Y = Comp2.Out().DAT_Control.DAT1.ReserveMethodCoords.Y * 287 / 2048;
 } else if (_ == DAT_CHANNEL_2) {
  DAT2.In() = Comp2.Out().DAT_Control.DAT2;
  DAT2.In().ReserveMethodCoords.X = Comp2.Out().DAT_Control.DAT2.ReserveMethodCoords.X * 350 / 2048;
  DAT2.In().ReserveMethodCoords.Y = Comp2.Out().DAT_Control.DAT2.ReserveMethodCoords.Y * 287 / 2048;
 }
}

void Worker::GunHAFormCommand() {
 const auto& SolverStatus =
  ((Mode == SYSMODE_AT || Mode == SYSMODE_IT) && Comp2.Out().Control.Radar) ?
  CollisionRadar.SolverStatus() : Collision.SolverStatus();
 const auto& Control = Comp2.Out().GC;
 GunHA.Command.Extrapolation = SolverStatus.data.Extrapolation;
 GunHA.Command.HaveSolution = SolverStatus.data.HaveSolution;
 GunHA.Command.PointingAngleMismatch = Control.DriveCorrections.PointingAngleMismatch;
 GunHA.Command.Miss = Control.DriveCorrections.Miss;
 if (Mode != SYSMODE_Initial && Mode != SYSMODE_SS) {
  GunHA.Command.CollisionTaskPerformed = SolverStatus.Performed;
 } else {
  GunHA.Command.CollisionTaskPerformed = 0;
 }
// CM.Command.EvaluatedTime = LTS.AdjustedTime();
 GunHA.Command.EvaluatedTime = GunHA.NextTime(LTS.AdjustedTime());
 const ExtrapolationPolynomials::NS _NS = Filter.Status.Extrapolation.NS;
 if (_NS.OK)
  GunHA.Command.Rot =
   _NS.GetRotations(Filter.Status.NS_TimeOffset(GunHA.Command.EvaluatedTime));
 else
  GunHA.Command.Rot = NS.Out().Rot;
 GunHA.In().IsElevChannel = false;
 if (Mode == SYSMODE_Testing) {
  GunHA.In().Mode = _Gun::gmTesting;
 } else {
  GunHA.In().Mode = _Gun::gmWork;
 }
 GunHA.In().Position = GunHA.GetDesiredAngle(Control.DriveControl.GunControl, true);
 GunHA.Command.Ship.Heading = GunX.Out().Ship_HA[0];
}

void Worker::GunElevFormCommand() {
 const auto& SolverStatus =
  ((Mode == SYSMODE_AT || Mode == SYSMODE_IT) && Comp2.Out().Control.Radar) ?
  CollisionRadar.SolverStatus() : Collision.SolverStatus();
 const auto& Control = Comp2.Out().GC;
 GunElev.Command.Extrapolation = SolverStatus.data.Extrapolation;
 GunElev.Command.HaveSolution = SolverStatus.data.HaveSolution;
 GunElev.Command.PointingAngleMismatch = Control.DriveCorrections.PointingAngleMismatch;
 GunElev.Command.Miss = Control.DriveCorrections.Miss;
 if (Mode != SYSMODE_Initial && Mode != SYSMODE_SS) {
  GunElev.Command.CollisionTaskPerformed = SolverStatus.Performed;
 } else {
  GunElev.Command.CollisionTaskPerformed = 0;
 }
// CM.Command.EvaluatedTime = LTS.AdjustedTime();
 GunElev.Command.EvaluatedTime = GunElev.NextTime(LTS.AdjustedTime());
 const ExtrapolationPolynomials::NS _NS = Filter.Status.Extrapolation.NS;
 if (_NS.OK)
  GunElev.Command.Rot =
   _NS.GetRotations(Filter.Status.NS_TimeOffset(GunElev.Command.EvaluatedTime));
 else
  GunElev.Command.Rot = NS.Out().Rot;
 GunElev.In().IsElevChannel = true;
 if (Mode == SYSMODE_Testing) {
  GunElev.In().Mode = _Gun::gmTesting;
 } else {
  GunElev.In().Mode = _Gun::gmWork;
 }
 GunElev.In().Position = GunElev.GetDesiredAngle(Control.DriveControl.GunControl, true);
 GunElev.Command.Ship.Heading = GunX.Out().Ship_HA[0];
}

void Worker::GunTimerFormCommand() {
 const auto& SolverStatus =
  ((Mode == SYSMODE_AT || Mode == SYSMODE_IT) && Comp2.Out().Control.Radar) ?
  CollisionRadar.SolverStatus() : Collision.SolverStatus();
 if (Mode == SYSMODE_Testing) {
  GunTimer.In().Mode = _Gun::gmTesting;
 } else {
  GunTimer.In().Mode = _Gun::gmWork;
 }
 if (Comp2.Out().GC.DriveControl.RemoteFuseControl) {
  GunTimer.In().Time = SolverStatus.data.Extrapolation.FlightTime;
 } else {
  GunTimer.In().Time = 0;
 }
}

void Worker::GunXFormCommand() {
 GunX.In().Test = Mode == SYSMODE_Testing;
}

void Worker::FRAMFormCommand() {
 FRAM.Command.Location = Comp2.Out().Control.OEUC;
}

void Worker::GTCRModeFormCommand() {
 auto& c = GTCRMode.In();
 c = Comp2Radar.Out().Mode;
 const auto& s = GTCRMode.Out();
 GTCRMode.Command.Go = (s.Mode != c.Mode || s.Submode != c.Submode);
}

void Worker::PlannedFrequenciesFormCommand() {
 size_t count = 0;
 for (size_t i = 0; i < PlannedFrequencies.In().Array.size(); ++i) {
  PlannedFrequencies.In().Array[i].Permitted = Comp2Radar.Out().PF[i].Permitted;
  if (PlannedFrequencies.In().Array[i].Permitted) ++count;
  PlannedFrequencies.In().Array[i].Code = Comp2Radar.Out().PF[i].Code;
 }
 PlannedFrequencies.In().Count = count;
 PlannedFrequencies.Command.Go = false;
 if (PlannedFrequencies.In().Count != PlannedFrequencies.Out().Count ||
     PlannedFrequencies.In().Array != PlannedFrequencies.Out().Array) {
  PlannedFrequencies.Command.Go = true;
 }
}

void Worker::RadialSpeedFormCommand() {
 auto HorizontalSpeed = AbsoluteXY(NS.Out().SpeedPart);
 auto RayElev = GTC.Out().ORU.Ground.Elevation;
 auto RayBearing = GTC.Out().ORU.Ground.Bearing;
 auto cr = cos(RayElev);
 RadialSpeed.In().Target =
  -HorizontalSpeed * cr * cos(RayBearing - NS.Out().Rot.Heading) +
  NS.Out().SpeedPart.Z * sin(RayElev);
 RadialSpeed.In().PassiveInterference =
  Comp2.Out().Weather.WindSpeed * cr * cos(RayBearing - Comp2.Out().Weather.WindBearing);
}

void Worker::RadarETDFormCommand() {
 RadarETD.In() = Comp2Radar.Out().ETD;
 RadarETD.Command.Go =
  (GTCRMode.Out().Submode == GTCRMode::Submode::ETD) && !(RadarETD.In() == RadarETD.Out());
}

void Worker::RadarSSFormCommand() {
 RadarSS.In() = Comp2Radar.Out().SS;
 RadarSS.Command.Go =
  (GTCRMode.Out().Submode == GTCRMode::Submode::SS) && !(RadarSS.In() == RadarSS.Out());
}

void Worker::RadarControlFormCommand() {
 RadarControl.In() = Comp2Radar.Out().Control;
 RadarControl.Command.Go = !RadarControl.NewInfo() || !(RadarControl.In() == RadarControl.Out());
}

void Worker::RadarTrainingFormCommand() {
 RadarTraining.In() = Comp2Radar.Out().Training;
 RadarTraining.Command.Go = GTCRMode.Out().Mode == GTCRMode::Mode::Training;
}

void Worker::ETD_DetectionFormCommand() {
}

void Worker::SS_DataFormCommand() {
}

void Worker::RadarTestFormCommand() {
}

void Worker::Comp2RadarFormCommand() {
 Comp2Radar.In().Mode = GTCRMode.Out();
 Comp2Radar.In().War = Comp2Radar.Out().War;
 Comp2Radar.In().ETD = RadarETD.Out();
 Comp2Radar.In().SS = RadarSS.Out();
 Comp2Radar.In().Control = RadarControl.Out();
 Comp2Radar.In().TestResult = RadarTest.Out();
 Comp2Radar.In().ETD_Detection.ETD_Mode = false;
 Comp2Radar.In().ETD_Detection.TargetCount = ETD_Detection.Out().TargetCount;
 Comp2Radar.In().ETD_Detection.Targets[0].RTS = ETD_Detection.Out().First.RTS;
 Comp2Radar.In().ETD_Detection.Targets[0].Distance = ETD_Detection.Out().First.Distance;
 Comp2Radar.In().ETD_Detection.Targets[0].Mismatch = ETD_Detection.Out().First.CommonModeDeviation;
 for (size_t i = 0; i < ETD_Detection.Out().Others.size(); ++i) {
  Comp2Radar.In().ETD_Detection.Targets[i + 1].RTS = ETD_Detection.Out().Others[i].RTS;
  Comp2Radar.In().ETD_Detection.Targets[i + 1].Distance = ETD_Detection.Out().Others[i].Distance;
  Comp2Radar.In().ETD_Detection.Targets[i + 1].Mismatch = ETD_Detection.Out().Others[i].CommonModeDeviation;
 }
 Comp2Radar.In().ETD_Detection.ORU = GTC.Out().ORU.Ship;
 Comp2Radar.In().ETD_Detection.Time = ETD_Detection.Out().LTS;
 Comp2Radar.In().SS_Data.SS_Mode = false;
 Comp2Radar.In().SS_Data.OFT = SS_Data.Out().OFT;
 Comp2Radar.In().SS_Data.FrequencyCode = SS_Data.Out().FrequencyCode;
 Comp2Radar.In().SS_Data.RepetitionPeriodCode = SS_Data.Out().RepetitionPeriodCode;
 Comp2Radar.In().SS_Data.RPA = SS_Data.Out().RPA;
 Comp2Radar.In().SS_Data.ANP = SS_Data.Out().ANP;
 Comp2Radar.In().SS_Data.ActiveNoiseInterferenceProcessing = SS_Data.Out().ActiveNoiseInterferenceProcessing;
 Comp2Radar.In().SS_Data.Time = SS_Data.Out().LTS;
 for (size_t i = 0; i < PlannedFrequencies.In().Array.size(); ++i) {
  Comp2Radar.In().PF[i].Permitted = PlannedFrequencies.Out().Array[i].Permitted;
  Comp2Radar.In().PF[i].Code = PlannedFrequencies.Out().Array[i].Code;
 }
 Comp2Radar.In().Time = counter.OverallTime();
}

const DAT::InputData& Worker::CurrentDAT() const {
 if (DAT1.Status.NewInfo) return DAT1.Out();
 else return DAT2.Out();
}

bool Worker::CheckSysModeComplete() {
 return !Comp2.In().State.ModeIsChanging;
}
