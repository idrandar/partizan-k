#ifndef RADARTESTRESULT_H
#define RADARTESTRESULT_H

// radar testing results
// (C) I.Drandar, 2019, all rights reserved
// Guidance and tracking controller - Radar

#include "common/ExchangeModule.h"

struct RadarTestResult {

 struct InputData {
  // runtime data
  struct Faults {
   bool TransmitterPulsePower;
   bool SUMNoisePower;
   bool DF1NoisePower;
   bool DF2NoisePower;
   bool SUMCalibrationPower;
   bool DF1CalibrationPower;
   bool DF2CalibrationPower;
   bool SUMDirectWaveLevel;
   bool SUMBackwardWaveLevel;
   bool SUM_DF1AmplitudeDifference;
   bool SUM_DF2AmplitudeDifference;
   bool SynthesizerPLL_LoopCapture;
   bool A_E_Switch;
   bool TransmitterTemperature;
  } Faults;
  // end of runtime data
  double TransmitterPulsePower;      // W
  double SUMNoisePower;              // dB
  double DF1NoisePower;              // dB
  double DF2NoisePower;              // dB
  double SUMCalibrationPower;        // dB
  double DF1CalibrationPower;        // dB
  double DF2CalibrationPower;        // dB
  double SUMDirectWaveLevel;         // mV
  double SUMBackwardWaveLevel;       // mV
  double SUM_DF1AmplitudeDifference; // dB
  double SUM_DF2AmplitudeDifference; // dB
  double SUM_DF1PhaseDifference;     // °
  double SUM_DF2PhaseDifference;     // °
  bool Completed;
 };

 struct InputPacket {
  // runtime data
  struct Faults {
   bool : 8;
   bool TransmitterTemperature : 1;
   bool : 7;
   bool DF2CalibrationPower : 1;
   bool SUMDirectWaveLevel : 1;
   bool SUMBackwardWaveLevel : 1;
   bool SUM_DF1AmplitudeDifference : 1;
   bool SUM_DF2AmplitudeDifference : 1;
   bool SynthesizerPLL_LoopCapture : 1;
   bool A_E_Switch : 1;
   bool : 1;
   bool TransmitterPulsePower : 1;
   bool SUMNoisePower : 1;
   bool DF1NoisePower : 1;
   bool DF2NoisePower : 1;
   bool SUMCalibrationPower : 1;
   bool DF1CalibrationPower : 1;
  } Faults;
  // end of runtime data
  Bits7<Upscale<unsigned int, 2>, 14> TransmitterPulsePower;
  using PowerType = Bits7<Upscale<unsigned int, 8>, 14>;
  PowerType SUMNoisePower;
  PowerType DF1NoisePower;
  PowerType DF2NoisePower;
  PowerType SUMCalibrationPower;
  PowerType DF1CalibrationPower;
  PowerType DF2CalibrationPower;
  using WaveType = Bits7<Scale<unsigned int, 20>, 7>;
  WaveType SUMDirectWaveLevel;
  WaveType SUMBackwardWaveLevel;
  using AmplitudeType = Bits7<Upscale<int, 10>, 7>;
  AmplitudeType SUM_DF1AmplitudeDifference;
  AmplitudeType SUM_DF2AmplitudeDifference;
  using PhaseType = Bits7<Upscale<int, 16, 45>, 7>; // 180/64
  PhaseType SUM_DF1PhaseDifference;
  PhaseType SUM_DF2PhaseDifference;
  std::byte Gap[8];
  bool : 6;
  bool Completed : 1;
 };

 using OutputData = NOTHING;
 using OutputPacket = NOTHING;

 using CommandVector = NOTHING;
 struct StatusVector {};
};

std::ostream& operator<<(std::ostream&, const RadarTestResult::InputData&);

class ModuleRadarTest: public ExchangeModuleImpl<RadarTestResult> {
 const struct FakeData {
  const bool use;
  FakeData(const ModuleInit&, const QString&);
 } Fake;
 void DecodePacket(bool) override;
 void EncodePacket(bool) override {}
 void PrepareStatusVector(bool) override;
public:
 ModuleRadarTest(const ModuleInit&);
 void Act() override;
};

#endif // RADARTESTRESULT_H
