#ifndef GTCRMODE_H
#define GTCRMODE_H

// GTC R mode control
// (C) I.Drandar, 2019, all rights reserved

// Guidance an tracking controller - Radar

#include "common/ExchangeModule.h"

struct GTCRMode {

 enum class Mode {
  Work = 1, Training, Testing
 };
 enum class Submode {
  Standby,
  SS, // standalone search
  ETD // external target detected
 };

 struct OutputData {
  enum Mode Mode;
  enum Submode Submode;
 };

 using InputData = OutputData;

 struct CommandVector {
  bool Go;
 };

 struct StatusVector {
 };

 struct OutputPacket {
  unsigned char Mode : 2;
  unsigned char Submode : 2;
 };

 using InputPacket = OutputPacket;

};

template <>
inline constexpr std::array enum_rep<GTCRMode::Mode> = {
 "unknown mode", "work", "training", "testing"
};

template <>
inline constexpr std::array enum_rep<GTCRMode::Submode> = {
 "standby", "standalone search", "external target", "unknown"
};

class ModuleGTCRMode : public ExchangeModuleImpl<GTCRMode> {
 const struct FakeData {
  const bool use;
  FakeData(const ModuleInit&, const QString&);
 } Fake;
 void DecodePacket(bool) override;
 void EncodePacket(bool) override;
 void PrepareStatusVector(bool) override;
public:
 ModuleGTCRMode(const ModuleInit&);
 void Act();
};

#endif // GTCRMODE_H
