/*
 * DAT.cc
 *
 *  Created on: 11.03.2015
 *      Author: Drandar
 */
// Processing data of "DAT" channels
// (C) I.Drandar, 2015, 2017-2018, all rights reserved
// (C) I.Drandar, for Qt, 2018-2019, all rights reserved

#include "DAT.h"
#include "DebugOut.h"

std::ostream& operator<<(std::ostream& _, const DAT::OutputData& D) {
 _ <<
  "video output 1: " << D.Video1 << "\tvideo output 2: " << D.Video2 << "\t"
  "video output 3: " << D.Video3 << "\n"
  "rotation: " << D.Rotation << "\tmark: " << D.Mark << "\n"
  "processing method: " << D.Processing << "\t"
  "target type mark: " << D.TT << "\t"
  "stream source: " << D.Source << "\t"
  "capture command: " << D.Capture << "\tcapture enable: " << D.CaptureEnable << "\t"
  "on" << D.On << "\n"
  "reserve method coordinates:\t" << D.ReserveMethodCoords;
 return _;
}

ModuleDAT::FakeData::FakeData(const ModuleInit& init, const QString& s) :
 use(init.GetConst(s, "use")),
 AutotrackingMismatch {
  init.GetConst(s, "autotracking-mismatch-HA"), init.GetConst(s, "autotracking-mismatch-elev")
 } {
}

ModuleDAT::ModuleDAT(const ModuleInit& init) : BASE(init), Fake(init, "fake") {
// std::cout << __PRETTY_FUNCTION__ << " " << sizeof(InputPacket) << std::endl;
}

void ModuleDAT::Act() {
 Transmit();
 CommitReception();
}

void ModuleDAT::DecodePacket(bool dump) {
 Input.Method = input.Method;
 Input.Break = input.Break;
 Input.Capture = input.Capture; // transition to autotracking mode
 Input.Mode = static_cast<Types::Workmode>(input.Mode);
 Input.AutotrackingMismatch = input.AutotrackingMismatch;
 Input.Fault = input.Fault; // channel is invalid
 Input.TestingCompleted = input.TestingCompleted;
 Input.TT = static_cast<Types::TargetType>(input.TT);
 Input.Credibility = input.Credibility;
 Input.LTS = input.LTS;
 if (dump)
  std::cout << "Dump of received " << GetName() << "::Input packet\n:"
   "method: " << Input.Method << "\tbreak: " << Input.Break << "\tcapture: " << Input.Capture << "\n"
   "mode: " << Input.Mode << "\tfault: " << Input.Fault << "\n"
   "testing is completed: " << Input.TestingCompleted << "\ttarget type: " << Input.TT << "\n"
   "credibility: " << Input.Credibility << "\tdetection: " << Input.Detection << "\n"
   "autotracking mismatch (°): " << Input.AutotrackingMismatch << "\n"
   "LTS: " << Input.LTS << " s\n";
}

void ModuleDAT::EncodePacket(bool dump) {
 output.Control.Video1 = Output.Video1;
 output.Control.Video2 = Output.Video2;
 output.Control.Video3 = Output.Video3;
 output.Control.Rotation = Output.Rotation;
 output.Control.Mark = Output.Mark;
 output.Control.Processing = Output.Processing;
 output.Control.TT = Output.TT;
 output.Control.Source = Output.Source;
 output.Control.Capture = Output.Capture;
 output.Control.CaptureEnable = Output.CaptureEnable;
 output.Control.On = Output.On;
 output.ReserveMethodCoords = Output.ReserveMethodCoords;
 if (dump)
  std::cout << Output << "\n";
}

void ModuleDAT::PrepareStatusVector(bool) {
 if (Fake.use) {
  auto& d = Status.data;
  d.Capture = Command.Capture;
  d.Break = false;
  d.Method = Command.data.Processing;
  d.TT = Command.data.TT;
  d.LTS = counter.TimeFromProgramStart();
  d.AutotrackingMismatch.HA = Fake.AutotrackingMismatch.HA(d.LTS);
  d.AutotrackingMismatch.Elev = Fake.AutotrackingMismatch.Elev(d.LTS);
 }
}
