/*
 * PropertyBag.hh
 *
 *  Created on: 04.12.2014
 *      Author: Drandar
 */
// property bags definitions
// property bags are used for initialization of different objects
// by values got from initialization files, in which property names and values
// are stored as text strings
// (C) I.Drandar, 2013-2015, all rights reserved
// (C) I.Drandar, for Qt + C++1x, 2018-2020, all rights reserved

#ifndef PROPERTYBAG_HH_
#define PROPERTYBAG_HH_

#include "StringKeyMap.h"
#include "Coordinates.h"
#include "PrimitiveFunctions.h"
#include <any>
#include "data_convert.h"

enum class ComposeType {
 sum, prod
};

template <>
inline constexpr std::array enum_rep<ComposeType> = { "+", "∙" };

// generic function type
class Function {
 PrimitiveFunction prim; // primary
 ComposeType compose;
 std::vector<Function> sec; // secondary
public:
 Function(const PrimitiveFunction&, ComposeType, std::vector<Function>&);
 Function() = default;
 Function(double); // function that always returns invariant value
 Function(const PrimitiveFunction&);
 double operator()(double);
 double operator()(double) const;
 operator double() const;
};

struct Compound {
 std::any me;
 template <typename T>
 operator T() const {
  try {
   return std::any_cast<T>(me);
  } catch (std::exception&) {
   throw;
  }
 }
 operator bool() const { return std::any_cast<double>(me); }
 operator unsigned char() const { return std::any_cast<double>(me); }
 operator int() const { return std::any_cast<double>(me); }
 operator unsigned int() const { return std::any_cast<double>(me); }
 operator double() const;
 operator Function() const;
 Compound& operator=(const std::any& _) { me = _; return *this; }
};

struct ConstCategoryTable : public StringKeyMap<Compound> {
 Compound Get(const QString& Name) const;
 void Put(const QString& Name, const std::any&);
};

class ConstTable : public StringKeyMap<ConstCategoryTable> {
public:
 Compound Get(const QString& Category, const QString& Name) const;
 QList<mapped_type> GetAll(const QString& Category) const;
 void Put(const std::string& Category, const std::string& Name, double);
};

class PropertyBag : private StringKeyMap<QString> {
public:
 class ENUM_TAB : public StringKeyMap<int> {};
private:
 static struct BOOL_TAB : public ENUM_TAB {
  BOOL_TAB(); // static constructor
 } _bool_tab;
public:
 void Insert(const QString& key, const QString& value); // insert property value
 const QString& GetString(const QString& key) const; // get string property value
 double GetDouble(const QString& key) const; // get float property value
 double GetDouble(const QString& key, double def) const; // get float property value
                                                         // return default if property is absent
 int GetInt(const QString& key) const; // get integer property value
 int GetInt(const QString& key, int def) const; // get integer property value
                                                // return default if property is absent
 bool GetBool(const QString& key) const; // get boolean property value
 bool GetBool(const QString& key, bool def) const; // get boolean property value
                                                   // return default if property is absent
 int GetEnum(const QString& key, const ENUM_TAB& tab) const; // get enumeration value
 int GetEnum(const QString& key, const ENUM_TAB& tab, int def) const; // get enumeration value
                                                                      // return default if property is absent
};

#endif /* PROPERTYBAG_HH_ */
