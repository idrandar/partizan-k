/*
 * Arrays.hh
 *
 *  Created on: 05.02.2015
 *      Author: Drandar
 */

#ifndef POLYNOMIAL_HH_
#define POLYNOMIAL_HH_

#include <stddef.h>
#include <numeric>
#include <functional>
#include <vector>
#include <algorithm>

template <typename T>
class HornerByField : std::binary_function<double, T, double> { // helper class
 const double x;
 const double T::* field;
public:
 HornerByField(double _, double T::* field) : x(_), field(field) {}
 double operator()(double val, const T& a) {
  return val * x + a.*field; // Horner's scheme iteration
 }
};

double EvaluatePolynomial(const double[], double x, size_t);

template <typename T>
double EvaluatePolynomial(const T Coeffs[], double T::* f, double x, size_t Degree) {
 std::vector<T> _(&Coeffs[0], &Coeffs[Degree + 1]);
 return std::accumulate(_.rbegin(), _.rend(), 0.0, HornerByField<T>(x, f));
}

template <typename T>
class FormalDerivativeByField : std::unary_function<T, T> { // helper class
 size_t i;
 double T::* field;
public:
 FormalDerivativeByField(double T::* field) : i(), field(field) {}
 T operator()(const T& l, const T& r) {
  T result = r;
  result.*field = ++i * l.*field;
  return result;
 }
};

template <typename T>
void EvaluatePolynomialFormalDerivative(const T Input[], double T::* f, size_t Degree, T Output[]) {
 //  conting array coeffitients of derivative polinomial
 //  dg       -  degree of polynomial
 // pn       -  pointer to an array  coefficients of polynomial
 //  dpn      -  pointer to counted array  coefficients of derivative polynomial

 if (Degree > 0) {
  std::transform(&Input[1], &Input[Degree + 1], Output, Output, FormalDerivativeByField<T>(f));
 } else
  Output[0].*f = 0;
}

void EvaluatePolynomialFormalDerivative(const double Input[], size_t Degree, double Output[]);

typedef std::vector<std::vector<double> > GenericMatrix;
struct InverseMatrix : GenericMatrix {
 InverseMatrix(size_t size) : GenericMatrix(size, GenericMatrix::value_type(size)) {}
 InverseMatrix(size_t size, const double vt[], size_t n) : GenericMatrix(size, GenericMatrix::value_type(size)) {
  Evaluate(vt, n);
 }
 void Evaluate(const double vt[], size_t n);
};


template <typename T>
void EvaluateOrdinalLeastSquaresPolynomial(const double vt[],  double T::* field, const T vy[], size_t n, const InverseMatrix& im, T pn[]) {
 //  counting by least-squares method fitting polynom of degree 2
 // for measured values of parameter
 //  n          - number samples (or number measurements) of fitting parameter
 //  vt         - pointer to an array of time samples corresponding to measurements
 //  vy         - pointer to an array of measured values parameter
 //  invm[3][3] - inverse matrix of corresponding system linear equations
 //  pn[3]      - array of coefficient counted polinon -
 //  for calculation value of parameter in the form:
 //  p(t) = pn[0] + pn[1]*t +pn[2]*t*t
 // ( inverse sequence order coefficients accepted in MATLAB)

 const size_t Degree = im.size();
 double g[Degree];
 std::fill(g, g + Degree, 0);

 for (size_t i = 0; i < n; i++) {
  const double t = vt[i];
  double y[Degree];
  y[0] = vy[i].*field;
  for (size_t j = 1; j < Degree; ++j) {
   y[j] = y[j - 1] * t;
  }
  std::transform(y, y + Degree, g, g, std::plus<double>());
 }

 for (size_t i = 0; i < Degree; ++i) {
  pn[i].*field = std::inner_product(&im[i][0], &im[i][Degree], g, 0.0);
 }

 //printf("a0 = %12.10f  a1 = %12.10f a2 = %12.10f\n",pn[0],pn[1],pn[2]);
}

void EvaluateOrdinalLeastSquaresPolynomial(const double vt[], const double vy[], size_t n, const InverseMatrix& im, double pn[]);

#endif /* ARRAYS_HH_ */
