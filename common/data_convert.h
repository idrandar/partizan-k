#ifndef DATA_CONVERT_H_
#define DATA_CONVERT_H_

// this header contains templates and procedures used for encoding and decoding data
// used by serial exchange protocols
// (C) I.Drandar, 2010-2011, 2018-2020, all rights reserved

#include <assert.h>
#include <limits>
#include <initializer_list>
#include <cstring>

// enumeration that controls order of bytes inside "Bits7" types

enum Order {
 _LITTLE_ENDIAN,
 _BIG_ENDIAN
};

// base type for both Bits7 and Bits7Shared classes
// !!! This template must not be instatiated directly: instatiate derived templates only!
// derived templates provide conversion from integral type (mostly int or unsigned int) to
// so-called 7-bit code or vice versa
// 7-bit code means that data stored in buffer in such way: the MSB in every byte of storage is not used and always is equal to 0.
// This 0's are inserted after every 7th bit of original number, starting from LSB
// provides most generic constants & classes for derived classes
// unlike to its derived classes it have only 3 parameters:
//  T - type it reflects to
//  Bits - count of bits in inner representation
//  Shift - count of bits inner representation is shifted (default is 0 that means no shift)
//   if Shift > 0 then data are shifted left; released bit are filled with 0's,
//   if Shift < 0 then data are shifted right; extra bits are dropped
template <typename T, int Bits, int Shift = 0>
class Bits7Base {
public:
 // constants
 enum {
  BITLEN = 7, // maximal number of bits that can be stored in a single byte
  FULLBITS = Bits + Shift, // full count of bits in representation
  REP_SIZE = (FULLBITS / BITLEN) + ((FULLBITS % BITLEN) ? 1 : 0), // full count of bytes in inner representation
  MASK = 0x7F,
  SMALLPATTERN = (1 << Shift) - 1,
 };
protected:
 unsigned char _rep[REP_SIZE]; // inner representation
 static unsigned char Mask(unsigned int i) {
  static const unsigned char _mask[] = {0x00, 0x01, 0x03, 0x07, 0x0F, 0x1F, 0x3F, 0x7F};
  assert(i < sizeof _mask);
  return _mask[i];
 }
 static const T BIGPATTERN = (static_cast<T>(1) << FULLBITS) - 1;
 static const T PATTERN = ((static_cast<T>(1) << FULLBITS) - 1) ^ ((1 << Shift) - 1);
 static const T MAX = static_cast<T>(1) << Bits;
};

// traits class controlling bytes order-dependent behavior
template <class Base, Order Ord>
class OrderCaps {};

// partial specialization for little endian bytes order
template <typename Base>
class OrderCaps<Base, _LITTLE_ENDIAN> {
public:
 enum {
  LOW = 0,
  HIGH = Base::REP_SIZE - 1,
  INC = 1
 };
 static bool comp(int l, int r) { return l <= r; }
 static int MaskIndex(int i) { return i; }
};

// partial specialization for big endian bytes order
template <typename Base>
class OrderCaps<Base, _BIG_ENDIAN> {
public:
 enum {
  LOW = Base::REP_SIZE - 1,
  HIGH = 0,
  INC = -1
 };
 static bool comp(int l, int r) { return l >= r; }
 static int MaskIndex(int i) { return LOW - i; }
};

// template changing weight of less significant bit
// parameters:
//  T - type to which template maps
//  mult - weight of less significant bit (default 1)
//  zero - zero shift (default 0)
// if parameters mult and zero are default template does nothing
template <typename T, int mult = 1, int zero = 0>
class Scale {
public:
 typedef T InnerType;
 typedef T OuterType;
 static InnerType ToInner(const OuterType& outer) { return (outer - zero) / mult; }
 static OuterType ToOuter(const InnerType& inner) { return (inner * mult) + zero; }
};

// this template is used when LSB is less then 1
// maps type T to double
// parameters:
//  T - type to which template maps
//  div - divider; LSB weight is computed as 1/div
//  scale - this coefficient used to make results more accurate; if used LSB weight is scale/div
template <typename T, int div = 1, int scale = 1>
class Upscale {
public:
 typedef T InnerType;
 typedef double OuterType;
 static InnerType ToInner(const OuterType& outer) {
  return static_cast<InnerType>(outer * div / scale);
 }
 static OuterType ToOuter(const InnerType& inner) {
  return inner * static_cast<OuterType>(scale) / static_cast<OuterType>(div);
 }
};

// wrapper for "Scaler" template
template <typename T>
class ScaleCaps {
public:
 typedef typename T::InnerType InnerType;
 typedef typename T::OuterType OuterType;
 static InnerType ToInner(const OuterType& outer) { return T::ToInner(outer); }
 static OuterType ToOuter(const InnerType& inner) { return T::ToOuter(inner); }
};

// partial specializations
template <>
class ScaleCaps<int> {
public:
 typedef int InnerType;
 typedef int OuterType;
 static InnerType ToInner(const OuterType& outer) { return outer; }
 static OuterType ToOuter(const InnerType& inner) { return inner; }
};

template <>
class ScaleCaps<unsigned long long> {
public:
 typedef unsigned long long InnerType;
 typedef unsigned long long OuterType;
 static InnerType ToInner(const OuterType& outer) { return outer; }
 static OuterType ToOuter(const InnerType& inner) { return inner; }
};

template <>
class ScaleCaps<unsigned int> {
public:
 typedef unsigned int InnerType;
 typedef unsigned int OuterType;
 static InnerType ToInner(const OuterType& outer) { return outer; }
 static OuterType ToOuter(const InnerType& inner) { return inner; }
};

// class providing conversion to 7-bit code (see comment to class Bits7Base)
// parameters:
//  T - type to convert
//  Bits - number of bits in representation
//  Ord - order of bits
//  Shift - shift in bits
template <typename T, int Bits, Order Ord=_BIG_ENDIAN, int Shift=0>
class Bits7 : public Bits7Base<typename ScaleCaps<T>::InnerType, Bits, Shift> {
 typedef typename ScaleCaps<T>::OuterType OuterType; // short name for outer type
 typedef typename ScaleCaps<T>::InnerType InnerType; // short name for inner type
 typedef Bits7Base<InnerType, Bits, Shift> Base; // short name for base type
 typedef OrderCaps<Base, Ord> ord_caps; // short name for order traits type
 // constants
 enum {
  FULLBITS = Base::FULLBITS,
  BITLEN = Base::BITLEN,
  MASK = Base::MASK
 };
public:
 void operator=(OuterType rhs) { // conversion to inner representation
  InnerType val = ScaleCaps<T>::ToInner(rhs) << Shift; // scaling
  int rest = FULLBITS; // count of bits
  // cycle splitting value to chunks
  for (int i = ord_caps::LOW; ord_caps::comp(i, ord_caps::HIGH); i += ord_caps::INC) {
   if (rest < BITLEN) {
    // last chunk
    Base::_rep[i] = val & Base::Mask(rest); // store last chunk; it can have less than 7 bits
    break;
   } else {
    Base::_rep[i] = val & MASK; // store current chunk
     val >>= BITLEN; // move to next chunk
     rest -= BITLEN;
   }
  }
 }
 operator OuterType() const { // conversion from inner representation
  InnerType retval = InnerType(); // starting value; mostly is equal to 0
  // cycle gathering value from chunks
  for (int i = ord_caps::HIGH; ord_caps::comp(ord_caps::LOW, i); i -= ord_caps::INC) {
   // add next chunk
   retval <<= BITLEN;
   retval += Base::_rep[i] & MASK;
  }
  // testing for sign bit
  if (std::numeric_limits<InnerType>::is_signed) { // don't test for unsigned types
   const int REST = (FULLBITS % BITLEN ? FULLBITS % BITLEN : BITLEN); // count of significant bits in last chunk
   const unsigned char BITMASK = (1 << (REST - 1)); //
   if (Base::_rep[ord_caps::HIGH] & BITMASK) retval -= Base::MAX; // change sign if higher bit is set
  }
  return ScaleCaps<T>::ToOuter(retval >> Shift); // unscale
 }
};

// class providing conversion to 7-bit code (see comment to class Bits7Base)
// differs from template Bit7 in one feature: this template allows sharing memory with other data
// (e.g. by placing it inside "union"), that makes it slightly more complicated
// parameters: same as parameters of template Bits7 (see above)
template <typename T, int Bits, Order Ord=_BIG_ENDIAN, int Shift=0>
class Bits7Shared : public Bits7Base<typename ScaleCaps<T>::InnerType, Bits, Shift> {
 typedef typename ScaleCaps<T>::OuterType OuterType; // short name for outer type
 typedef typename ScaleCaps<T>::InnerType InnerType; // short name for inner type
 typedef Bits7Base<InnerType, Bits, Shift> Base; // short name for base type
 typedef OrderCaps<Base, Ord> ord_caps; // short name for order traits type
 enum {
   FULLBITS = Base::FULLBITS,
   MASK = Base::MASK,
   BITLEN = Base::BITLEN
 };
public:
 void operator=(const OuterType& rhs) { // conversion to inner representation
  int rest = FULLBITS; // count of bits
  InnerType pattern = Base::PATTERN;
  InnerType val = ScaleCaps<T>::ToInner(rhs) << Shift; // scaling
  // loop for splitting value to chunks
  for (int i = ord_caps::LOW; ord_caps::comp(i, ord_caps::HIGH); i += ord_caps::INC) {
   const unsigned char SETMASK = pattern & MASK; // pattern which determines bits can be set; other bits will be unchanged
   const unsigned char CLEARMASK = ~SETMASK & MASK;
   Base::_rep[i] &= CLEARMASK; // clear significant bits
   Base::_rep[i] |= val & SETMASK;
   if (rest < BITLEN) {
    // last chunk
    break;
   } else {
    // proceed with next chunk
    val >>= BITLEN;
    rest -= BITLEN;
    pattern >>= BITLEN;
   }
  }
 }
 operator OuterType() const { // conversion from inner representation
  InnerType retval = InnerType(); // starting value; mostly is equal to 0
  // cycle gathering value from chunks
  for (int i = ord_caps::HIGH; ord_caps::comp(ord_caps::LOW, i); i -= ord_caps::INC) {
   // add next chunk
   // CLEARMASK clears bits that don't belong to our type
   const unsigned char CLEARMASK = (Base::PATTERN >> (ord_caps::MaskIndex(i) * BITLEN)) & MASK;
   retval <<= BITLEN;
   retval += Base::_rep[i] & CLEARMASK;
  }
  // testing for sign bit
  if (std::numeric_limits<InnerType>::is_signed) { // don't test for unsigned types
   const int REST = (FULLBITS % BITLEN ? FULLBITS % BITLEN : BITLEN); // count of significant bits in last chunk
   const unsigned char BITMASK = (1 << (REST - 1)); //
   if (Base::_rep[ord_caps::HIGH] & BITMASK) retval -= Base::MAX; // change sign if higher bit is set
  }
  return ScaleCaps<T>::ToOuter(retval >> Shift); // unscale
 }
};

// this template forces a structure to pretend being an array
// type T - any structure
// optional type I - type of "array" elements (now only unsigned char is used)
template <typename T, typename I=unsigned char>
class RandomAccess : public T {
public:
 const I& operator[](size_t n) const {
  return *(reinterpret_cast<const I*>(this) + n);
 }
 I& operator[](size_t n) {
  return *(reinterpret_cast<I*>(this) + n);
 }
 size_t size() const { return sizeof(T) / sizeof(I); }
 I* begin() { return reinterpret_cast<I*>(this); }
 I* end() { return reinterpret_cast<I*>(this) + size(); }
 RandomAccess() : T() { std::memset(this, 0, sizeof(T));; }
 RandomAccess(const T& _) : T(_) {}
/* RandomAccess& operator=(const T& _) {
  if (&_ == this) return *this;
  *this = _;
  return *this;
 }*/
};

template <typename T>
RandomAccess<T> Wrap2RandomAccess(const T& _) {
 return RandomAccess<T>(_);
}

// following functions extract different parts from integer value
inline unsigned char HiByte(unsigned short _) { return (_  & 0xFF00) >> 0x08; }
inline unsigned char LoByte(unsigned short _) { return _ & 0xFF; }
inline unsigned char Byte0(unsigned int _) { return _ & 0xFF; }
inline unsigned char Byte1(unsigned int _) { return (_  & 0xFF00) >> 0x08; }
inline unsigned char Byte2(unsigned int _) { return (_  & 0xFF0000) >> 0x10; }

// make integer from bytes
inline unsigned int MakeInt(unsigned char _0, unsigned char _1, unsigned char _2 = 0) {
 return _0 | (_1 << 0x08) | (_2 << 0x10);
}
inline void SetLoByte(unsigned int& _, unsigned char l) {
 _ = MakeInt(l, HiByte(_));
}
inline void SetHiByte(unsigned int& _, unsigned char h) {
 _ = MakeInt(LoByte(_), h);
}

template <typename T>
constexpr auto enum_rep = {nullptr};

template <typename T>
const char* enum2str(T t) {
 auto index = static_cast<size_t>(t);
 if (index < enum_rep<T>.size()) {
  return enum_rep<T>[index];
 } else {
  return "undefined";
 }
}

#endif /*DATA_TYPES_H_*/
