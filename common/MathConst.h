// MathConst.hh
// Often used in numeric computations constants
// (C) I.Drandar, 2010, 2014-2015 all rights reserved
// (C) I.Drandar, for C++1x, 2018, all rights reserved

#ifndef MATHCONST_HH_
#include <math.h>

namespace MATH_CONST {
 constexpr double M_2PI = 2 * M_PI; // 2*pi
 constexpr double M_3PI_2 = 3 * M_PI/2; // 3*pi/2
 constexpr double DEG2RAD = M_PI / 180;
 constexpr double DEG2MIL = 6000.0 / 360.0;
 constexpr double Deg2Rad(double deg) { return deg * DEG2RAD; } // convert degrees to radians
 constexpr double RAD2DEG = 180 / M_PI;
 constexpr double MIL2DEG = 360.0 / 6000.0;
 constexpr double MIL2RAD = M_2PI / 6000.0;
 constexpr double Rad2Deg(double rad) { return rad * RAD2DEG; } // convert radians to degrees
};

#define MATHCONST_HH_


#endif /* MATHCONST_HH_ */
