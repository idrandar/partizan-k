/* The author: Vladislav Guyvoronskiy          */
/* Date of creation: 15 February 2007          */
/* Working with COM-ports                      */
/*             */
/* IDE Momentics & QNX Neutrino 6.3.0          */
// (C) I.Drandar, for Linux + C++1x, 2018-2019, all rights reserved

#include <unistd.h>
#include <fcntl.h>
#include <sys/time.h>
#include "cmp_ports.h"
#include <termios.h>
#include <iostream>
#include <string.h>
#include <sys/ioctl.h>

struct timeval timeout;
struct termios options;

/* function OpenComPort
 * opens port specified by "portname"
 * returns file descriptor for port */
int OpenComPort(const char *portname)
{
  int fd;  //file descriptor
  fd = open(portname, O_RDWR | O_NOCTTY | O_NONBLOCK);
  if (fd == -1) {
   std::cout << __PRETTY_FUNCTION__ << ": error " << strerror(errno) << std::endl;
  }
  fcntl(fd, F_SETFL, 0);
  return fd;
} // end OpenComPort
/*******************************************************/

/* function CloseComPort
 * closes port specified by descriptor (fd)
 * returns -1 if error, 0 - if success*/
int CloseComPort(int fd)
{
	 return close(fd);
} // end CloseComPort
/*******************************************************/

/* function SetVatTimeOut
 * set timeout for port
 * TimeOutValue - time in sec */
void SetVarTimeOut (int TimeOutValue)
{
	timeout.tv_sec = TimeOutValue;
	timeout.tv_usec = 0;
} // end SetVarTimeOut
/*******************************************************/

/* function SetComPort
 * set to the port, specified by descriptor (fd),
 * options, described in structure opt */
int SetComPort(int fd, int speed, ParityType Parity, int StopBits) {
 termios temp_opt;
 auto result = tcgetattr(fd, &temp_opt);
 if (result == -1) {
  std::cout << __PRETTY_FUNCTION__ << ": error " << strerror(errno) << std::endl;
  return result;
 }
 static const tcflag_t PARITY_CONST[] = {
  0, // NONE
  PARENB, // EVEN
  PARENB | PARODD, // ODD
  PARENB | PARODD | CMSPAR, // MARK
  PARENB | CMSPAR // SPACE
 };
 const int _STOPB = (StopBits == 2) ? CSTOPB : 0;
 temp_opt.c_cflag |= (CS8 | _STOPB | PARITY_CONST[Parity]);
 temp_opt.c_cc[VTIME] = 0;
// temp_opt.c_ispeed = speed;
// temp_opt.c_ospeed = speed;
// temp_opt.c_cflag &= ~(IHFLOW | OHFLOW);
 temp_opt.c_lflag &= ~(ECHO | ECHOE | ECHOK | ECHONL | ECHOKE | ISIG | ICANON);
// temp_opt.c_lflag = 0; // don't echo characters and don't generate signals
 temp_opt.c_iflag = IGNPAR;
 temp_opt.c_oflag = 0; // raw output
 typedef struct {
  unsigned int txFifoTrig; // transmit FIFO Trigger level (0 = default - 1..64 = change trigger level)
  unsigned int rxFifoTrig; // receive FIFO Trigger level (0 = default - 1..64 = change trigger level)
 } TDRV002_SET_FIFO_STRUCT;
 TDRV002_SET_FIFO_STRUCT setFifoBuf{64, 64};
#define TDRV002_IOC_MAGIC	0xB3
#define TDRV002_IOCT_SET_FIFOTRIG   _IOW(TDRV002_IOC_MAGIC, 6, TDRV002_SET_FIFO_STRUCT)	// FIFO Trigger Levels
 result = ioctl(fd, TDRV002_IOCT_SET_FIFOTRIG, &setFifoBuf);
 result = cfsetspeed(&temp_opt, speed);
 result = tcflush(fd, TCIFLUSH);
 result = tcsetattr(fd, TCSANOW, &temp_opt);
 if (result == -1) {
  std::cout << __PRETTY_FUNCTION__ << ": error " << strerror(errno) << std::endl;
 }
 return result;
} // end SetComPort
/*******************************************************/

/* function WriteComPort
 * writes "size" bytes from "buf" to port,
 * specified by descriptor (fd)
 * returns -1 if error, number of written bytes otherwise
 * If success, must return "size"  */
int WriteComPort (int fd, const unsigned char *buf, int size) {
	int num = write(fd, buf, size);
	if (num==0) num = -1;
	return num;
} // end WriteComPort

/*******************************************************/
/* function ReadComPort
 * reads "size" bytes to "buf" from port,
 * specified by descriptor (fd)
 * returns 0 if error, number of read bytes otherwise
 * If success, must return "size"  */
int ReadComPort (int fd, unsigned char *buf, int size) {
	return read(fd, buf, size);
} // end ReadComPort


/*******************************************************/
/* function ReadCondComPort
 * reads "size" bytes to "buf" from port,
 * specified by descriptor (fd)
 * returns 0 if error, number of read bytes otherwise
 * If success, must return "size"  */
/*int ReadCondComPort(int fd, unsigned char *buf, int size, int time) {
 int retval;
 if (time == 0)
  retval = readcond(fd, buf, size, size, 0, INT_MAX);
 else if (time < 0)
  retval = read(fd, buf, size);
 else
  retval = readcond(fd, buf, size, size, time, time);
 return retval;
} // end ReadCondComPort
*/

//int Query(int fd) {
// int data = 0;
// devctl(fd, DCMD_CHR_LINESTATUS, &data, sizeof data, NULL);
// return data;
//}
// end of this cmp_ports.cc file
