/*
 * UtilityFuntions.hh
 *
 *  Created on: 16.11.2015
 *      Author: Drandar
 */

// set of some primitive functions
// (C) I.Drandar, 2015-2016, all rights reserved
// (C) I.Drandar, for C++1x, 2018-2020, all rights reserved

#ifndef PRIMITIVEFUNCTIONS_HH_
#define PRIMITIVEFUNCTIONS_HH_

#include <functional>
#include "common/ctime.h"

// all-purpose simple function type
using PrimitiveFunction = std::function<double(double)>;

struct Sinusoid {
 const double Main;
 const double Amplitude;
 const double Period;
 const double TimeShift;
 double operator()(double) const;
};

// next 2 are range restricting functions
struct Range {
 const double Low;
 const double High;
 double operator()(double) const;
};

struct Modulo {
 const double Low;
 const double High;
 double operator()(double) const;
};

// random noise generator
struct Noise {
 const double Mean;
 const double Dev; // standard deviation
 double operator()(double) const;
};

// this function has a state
struct Integral {
 double Value; // seed value;
 CTime _t;
 double operator()(double); // not constant because it changes the state
};

struct Derivative {
 PrimitiveFunction f;
 double dt = 0.000001;
 double operator()(double) const;
};

struct Linear { // a∙x+b
 const double Slope; // a
 const double Init;  // b
 double operator()(double) const;
};

struct Power { // k∙x**a+b
 const double Exponent; // a
 const double Factor;   // k
 const double Init;     // b
 const double TimeShift;
 double operator()(double) const;
};

struct Exponent { // k∙a**x+b
 const double Base;   // a
 const double Factor; // k
 const double Shift;  // b
 const double TimeShift;
 double operator()(double) const;
};

struct Logarithm { // k∙ln(x)+b
 const double Factor; // k
 const double Shift;  // b
 const double TimeShift;
 double operator()(double) const;
};

struct Gauss { // a∙exp(-(4∙ln(2)(x-b)²)/w²)+h - gaussian (bell) curve
 const double Factor;    // a
 const double TimeShift; // b
 const double HalfWidth; // w
 const double Shift;     // h
 double operator()(double) const;
};

struct TanH { // k∙tanh((x-b)*c)+a - hyperbolic tangent
 const double Factor;    // k
 const double Shrink;    // c - fastens a functions
 const double Shift;     // a
 const double TimeShift; // b
 double operator()(double) const;
};
#endif /* PRIMITIVEFUNCTIONS_HH_ */
