/*
 * Sockets.hh
 *
 *  Created on: 16.05.2013
 *      Author: Drandar
 */

// Wrapper for sockets library
// based on code from "UNIX System Programming Using C++" by Terrence Chan
// an on "UNIX Network Programming: Networking APIs" by W. Richard Stevens
// (C) I.Drandar, 2013, all rights reserved
// (C) I.Drandar, for C++1x + Qt, 2018, all rights reserved

#ifndef SOCKETS_HH_
#define SOCKETS_HH_

#include <sys/socket.h>
#include <netinet/in.h>
#include <vector> // buftype
#include <QString>

enum AddressFamily { // address family
 INET = AF_INET, // IPv4
 INET6 = AF_INET6 // IPv6
};

// wrapper for socket address types
class SocketAddress {
 union {
  sockaddr addr; // generic
  sockaddr_in addr_in; // IPv4
 } _;
 void Init(AddressFamily);
public:
 SocketAddress(AddressFamily = INET); // default: sets all fields to 0
 SocketAddress(const QString&, int = 0, AddressFamily = INET); // sets IP-address via string and port as int
 const sockaddr& Addr() const { return _.addr; }
 sockaddr& Addr() { return _.addr; }
 const sockaddr_in& AddrIn() const { return _.addr_in; }
 sockaddr_in& AddrIn() { return _.addr_in; }
 void AssignIP(const std::string&); // set IP-address via string
 std::string GetIP() const; // get string-like ip address
 void AssignPort(int); // set port
 int GetPort() const; // get port
 socklen_t GetLength() const; //
};

class Socket {
public:
 enum Type {
  TCP = SOCK_STREAM,
  UDP = SOCK_DGRAM
 };
 enum HowToShutdown {
  SHUTDOWN_READ = SHUT_RD,
  SHUTDOWN_WRITE = SHUT_WR,
  SHUTDOWN_ALL = SHUT_RDWR
 };
 enum { FAIL = -1 };
 typedef std::vector<unsigned char> buftype;
private:
 int sid; // socket id
 const AddressFamily domain; // socket domain
 const Type type;   // socket type
 const int protocol;
 int fd() const; // returns socket descriptor
 static void WriteError(const char*);
 explicit Socket(int fd);
public:
 Socket(AddressFamily, Type, int = 0);
 ~Socket();
 bool Open();
 bool Close();
 bool good() const;
 bool bind(SocketAddress&);
 Socket accept(SocketAddress&);
 bool connect(const SocketAddress&);
 ssize_t write(const buftype&);
 ssize_t read(buftype&);
 ssize_t writeto(const buftype&, const SocketAddress&);
 ssize_t readfrom(buftype&, SocketAddress&);
 bool shutdown(HowToShutdown mode = SHUTDOWN_ALL);
};

#endif /* SOCKETS_HH_ */
