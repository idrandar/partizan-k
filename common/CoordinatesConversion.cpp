/*
 * CoordinatesConversion.cc
 *
 *  Created on: 03.02.2015
 *      Author: Drandar
 */

#include "CoordinatesConversion.h"
#include "CFreeFun.h"
#include "MathConst.h"

double Absolute(const Cartesian3DCoords& _) {
 return sqrt(_.X * _.X + _.Y * _.Y + _.Z * _.Z);
}

double AbsoluteDistance(const Cartesian3DCoords& l, const Cartesian3DCoords& r) {
 return sqrt((r.X - l.X) * (r.X - l.X) + (r.Y - l.Y) * (r.Y - l.Y) + (r.Z - l.Z) * (r.Z - l.Z));
}

double AbsoluteXY(const Cartesian3DCoords& _) {
 return sqrt(_.X * _.X + _.Y * _.Y);
}

const Cartesian3DCoords SphericalToCartesian(const SphericalCoords& Sph) {
 const double cosElev = cos(Sph.Elevation);
 return {
  Sph.Distance * cosElev * cos(Sph.Bearing),
  Sph.Distance * cosElev * sin(Sph.Bearing),
  Sph.Distance * sin(Sph.Elevation)
 };
}

const SphericalCoords CartesianToSpherical(const Cartesian3DCoords &Cart) {
 const double D = Absolute(Cart);
 return { D, CFreeFun::natan(Cart.Y, Cart.X), asin(Cart.Z / D) };
}

const Spherical2DGroundCoords CartesianToSpherical2DGround(const Cartesian3DCoords &Cart) {
 return { CFreeFun::natan(Cart.Y, Cart.X), atan(Cart.Z / AbsoluteXY(Cart)) };
}

void NormalizeRadians(double& _) {
 while (_ < 0.0)
  _ += MATH_CONST::M_2PI;
 while (_ >= MATH_CONST::M_2PI)
  _ -= MATH_CONST::M_2PI;
}

void NormalizeRadiansSigned(double& _) {
 while (_ < -M_PI)
  _ += MATH_CONST::M_2PI;
 while (_ > M_PI)
  _ -= MATH_CONST::M_2PI;
}

void Normalize(Spherical2DGroundCoords& _) {
 NormalizeRadians(_.Bearing);
 NormalizeRadiansSigned(_.Elevation);
}

void Normalize(Spherical2DShipCoords& _) {
 NormalizeRadiansSigned(_.Heading);
 NormalizeRadiansSigned(_.Elevation);
}

struct TransformMatrix {
 double _00, _01, _02;
 double _10, _11, _12;
 double _20, _21, _22;
 TransformMatrix(const Rotations&);
};


TransformMatrix::TransformMatrix(const Rotations& Rot) {
 // Counting matrix for transformation Cartesian coordinates from ship system
 // to topocentrical system
 // Paramiters
 //    AnEul    - structure containing Euler angles of ship relatively to
 //               topocentrical coordinate system
 const double sinK = sin(Rot.Heading);
 const double cosK = cos(Rot.Heading);
 const double sinPsi = sin(Rot.Pitch);
 const double cosPsi = cos(Rot.Pitch);
 const double sinTeta = sin(Rot.Rolling);
 const double cosTeta = cos(Rot.Rolling);

 _00 = cosK * cosPsi;
 _01 = sinK * cosPsi;
 _02 = -sinPsi;
 _10 = sinTeta * sinPsi * cosK - cosTeta * sinK;
 _11 = sinTeta * sinPsi * sinK + cosTeta * cosK;
 _12 = sinTeta * cosPsi;
 _20 = cosTeta * sinPsi * cosK + sinTeta * sinK;
 _21 = cosTeta * sinPsi * sinK - sinTeta * cosK;
 _22 = cosTeta * cosPsi;
}

const Cartesian3DCoords ConvertShipToGroundCartesianInvertedZ(const Cartesian3DCoords& Ship, const Rotations& r) {
 // Counting product of matrix 3x3 to 3-length vector
 // Paramiters
 //    VcIn[3]   - input  vector
 //    VcOut[3]  - computed (output) vector
 const TransformMatrix m(r);
 return {
  m._00 * Ship.X + m._10 * Ship.Y + m._20 * Ship.Z,
  m._01 * Ship.X + m._11 * Ship.Y + m._21 * Ship.Z,
  m._02 * Ship.X + m._12 * Ship.Y + m._22 * Ship.Z
 };
}

const Cartesian3DCoords ConvertShipToGroundCartesian(const Cartesian3DCoords& Ship, const Rotations& r) {
 // Counting product of matrix 3x3 to 3-length vector
 // Paramiters
 //    VcIn[3]   - input  vector
 //    VcOut[3]  - computed (output) vector
 const TransformMatrix m(r);
 return {
   m._00 * Ship.X + m._10 * Ship.Y - m._20 * Ship.Z,
   m._01 * Ship.X + m._11 * Ship.Y - m._21 * Ship.Z,
  -m._02 * Ship.X - m._12 * Ship.Y + m._22 * Ship.Z
 };
}

const Cartesian3DCoords ConvertGroundToShipCartesianInvertedZ(const Cartesian3DCoords& Ground, const Rotations& r) {
 // Counting product of transposed matrix 3x3 to 3-length vector
 // Paramiters
 //    VcIn[3]   - input  vector
 //    VcOut[3]  - computed (output) vector
 const TransformMatrix m(r);
 return {
  m._00 * Ground.X + m._01 * Ground.Y + m._02 * Ground.Z,
  m._10 * Ground.X + m._11 * Ground.Y + m._12 * Ground.Z,
  m._20 * Ground.X + m._21 * Ground.Y + m._22 * Ground.Z
 };
}

const Cartesian3DCoords ConvertGroundToShipCartesian(const Cartesian3DCoords& Ground, const Rotations& r) {
 // Counting product of transposed matrix 3x3 to 3-length vector
 // Paramiters
 //    VcIn[3]   - input  vector
 //    VcOut[3]  - computed (output) vector
 const TransformMatrix m(r);
 return {
   m._00 * Ground.X + m._01 * Ground.Y - m._02 * Ground.Z,
   m._10 * Ground.X + m._11 * Ground.Y - m._12 * Ground.Z,
  -m._20 * Ground.X - m._21 * Ground.Y + m._22 * Ground.Z
 };
}

const Spherical2DShipCoords ConvertGroundToShipSpherical(const Spherical2DGroundCoords& Ground, const Rotations& Rot) {
 // Parameters
 //    AnEul    - structure containing Euler angles of ship relatively to HF
 //    A        - input    azimuth   of unit vector in topocentrical coordinate system
 //    E        - input    elevation of unit vector in topocentrical coordinate system
 //    Q        - computed azimuth   of unit vector in ship          coordinate system
 //    F        - computed elevation of unit vector in ship          coordinate system


 SphericalCoords CrSph { 1.0, Ground.Bearing, Ground.Elevation };
 Cartesian3DCoords CrTpc = SphericalToCartesian(CrSph);

// CrTpc.InvertZ();
 Cartesian3DCoords CrDct_Ship = ConvertGroundToShipCartesian(CrTpc, Rot);
// CrDct_Ship.InvertZ();
 CrSph = CartesianToSpherical(CrDct_Ship);

 Spherical2DShipCoords Ship { CrSph.Bearing, CrSph.Elevation };

 if (Ship.Heading < 0)
  Ship.Heading += MATH_CONST::M_2PI;
 return Ship;
}

const Spherical2DGroundCoords ConvertShipToGroundSpherical(const Spherical2DShipCoords& Ship, const Rotations& Rot) {
 // Parameters
 //    AnEul    - structure containing Euler angles of ship relatively to HF
 //    A        - computed azimuth   of unit vector in topocentrical coordinate system
 //    E        - computed elevation of unit vector in topocentrical coordinate system
 //    Q        - input    azimuth   of unit vector in ship          coordinate system
 //    F        - input    elevation of unit vector in ship          coordinate system

 SphericalCoords CrSph { 1.0, Ship.Heading, Ship.Elevation };
 Cartesian3DCoords CrDct_Ship = SphericalToCartesian(CrSph);

// CrDct_Ship.InvertZ();
 Cartesian3DCoords CrTpc = ConvertShipToGroundCartesian(CrDct_Ship, Rot);
// CrTpc.InvertZ();

 CrSph = CartesianToSpherical(CrTpc);

 Spherical2DGroundCoords Ground { CrSph.Bearing, CrSph.Elevation };

 if (Ground.Bearing < 0)
  Ground.Bearing += MATH_CONST::M_2PI;
 return Ground;
}

