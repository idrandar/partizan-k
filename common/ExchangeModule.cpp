/*
 * ExchangeModule.cc
 *
 *  Created on: 22.01.2013
 *      Author: Drandar
 */
// exchange module class
// (C) I.Drandar, 2013-2014, 2016, all rights reserved
// (C) I.Drandar, for C++1x, 2018, all rights reserved

#include "ExchangeModule.h"
#include <iostream>
#include "control/exception.h"
#include "DebugOut.h"

class ExchangeModule::ReceiverCaller : public Caller {
 virtual status call() {
  return dynamic_cast<ExchangeModule&>(owner).DoRead();
 }
public:
 ReceiverCaller(ExchangeModule& _) : Caller(_) {}
};

class ExchangeModule::SenderCaller : public Caller {
 virtual status call() {
  return dynamic_cast<ExchangeModule&>(owner).DoWrite();
 }
public:
 SenderCaller(ExchangeModule& _) : Caller(_) {}
};

ExchangeModule::ExchangeModule(const ModuleInit& init) :
 Module(init), device(ExchangeDevice::Item(init.Port())), ReceptionFaultMax(init.ReceptionFaultMax()),
 GotPackage(), TransmissionErrors(), ExchangeFaultsCount(), ReceptionFaults(), RejectedCount(),
 NoReceptionCount(),
 receiver(init.Thread("input"), new ReceiverCaller(*this)),
 sender(init.Thread("output"), new SenderCaller(*this))  {
}

ExchangeModule::~ExchangeModule() {
 std::cout << __PRETTY_FUNCTION__ << std::endl;
 // dump statistics
 if (receiver.ShouldBeDumped())
  std::cout << "Module " << GetName() << " input statistics:\nCount of received packages:\t" << pr->Received() <<
   "\nCount of erroneous packages:\t" << ReceptionFaults << "\nIncluding rejected packages:\t" << RejectedCount << '\n';
 if (sender.ShouldBeDumped())
  std::cout << "Module " << GetName() << " input statistics:\nCount of transmitted packages:\t" << pr->Transmitted() <<
   "\nCount of errors during transmission:\t" << TransmissionErrors << '\n';
}

Module::status ExchangeModule::DoRead() {
 if (device == NULL)
  return THREAD_FAIL;
 if (device->IsOpen() && device->Registered(pr)) {
  bool result = GetPacket(); // get data to buffer
  pr->RequestAccepted();
  if (result) {
   if (pr->ChecksumTest()) { // test checksum
    // package is correct
    receiver.Lock();
    DecodePacket(receiver.ShouldBeDumped()); // parse packet
    GotPackage = true;
    ExchangeFaultsCount = 0;
    receiver.Unlock();
    if (receiver.ShouldBeDumped() || pr->ChecksumDump())
     std::cout << GetName() << "::GetPackage() reception OK. " << pr->Received() << " packages received\n";
    return THREAD_OK;
   } else { // checksum failed - return error
    ++ReceptionFaults; // increase count of reception faults
    ++ExchangeFaultsCount;
    ++RejectedCount;
    if (receiver.ShouldBeDumped()) {
     std::cout << GetName() << "::TestPackage() error: checksum failed" << std::endl;
    }
    result = false;
   }
  }
  if (result) {
   return THREAD_OK;
  } else {
   if (receiver.ShouldBeDumped() || pr->ChecksumDump())
    std::cout << GetName() << "::GetPackage() error: read from port " << device->Name() <<
     " failed\nCount of last fails:\t" << ExchangeFaultsCount << "\nFull count of fails:\t" <<
     ReceptionFaults << std::endl;
   return THREAD_FAIL;
  }
 } else {
  ExchangeFaultsCount = EXCHANGE_FAULTS_MAX + 1;
  return THREAD_TERMINATE;
 }
}

void ExchangeModule::CommitReception() {
 receiver.Lock();
 if (GotPackage) {
  NoReceptionCount = 0;
  DoCommitReception(true); // copy received data to inner structures
 } else {
  ++NoReceptionCount;
  if (NoReceptionCount > ReceptionFaultMax) DoCommitReception(false);
 }
 receiver.Unlock();
 GotPackage = false;
 PostProcessStatusVector();
}

void ExchangeModule::Transmit() { // generic transmit routine
 ProcessCommandVector();
 if (device != nullptr && device->IsOpen()) {
  sender.Lock();
  PrepareTransmission(); // copy data for transmission to outer buffers
  EncodePacket(sender.ShouldBeDumped()); // code data for transfer
  sender.Unlock();
  sender.WakeUp(); // resume the write thread
 }
}

void ExchangeModule::Start() {
 if (device == nullptr) {
  std::cout << GetName() << "::Start(): no exchange device exist\n";
  return;
 }
 if (!device->Register(pr)) {
  std::cout << GetName() << "::Start(): cannot register protocol " << pr->Type() << '\n';
  return;
 }
 try {
 if (!device->IsOpen()) {
  device->Open();
  if (!device->IsOpen()) {
   std::cout << GetName() << "::Start(): cannot open device " << device->Name() << std::endl;
   return;
  }
 }
 Module::Start(); // start read thread
 receiver.Start();
 sender.Start(); // start write thread
 } catch (const TException& e) {
  throw
   TException(__PRETTY_FUNCTION__, ":\t[", device->Name(), "@", GetName(), "]\n", e.what());
 }
}

void ExchangeModule::Stop() {
 if (device != nullptr && device->IsOpen()) {
  sender.Stop();
  receiver.Stop();
  Module::Stop();
  device->Close();
 }
 device = nullptr;
}

Module::status ExchangeModule::DoWrite() {
 if (device == nullptr)
  return THREAD_TERMINATE;
 if (device->IsOpen() && device->Registered(pr)) {
  sender.Wait();
  pr->WaitForRequestIfNeeded();
  Module::Thread::lock l(sender);
  SendedPackage = false;
  if (device == nullptr)
   return THREAD_TERMINATE;
  if (DoTransmit()) {
   // transmission OK
   SendedPackage = true;
   dump_if(sender.ShouldBeDumped()) << pr->Transmitted() << " packages transmitted\n";
   return THREAD_OK;
  } else {
   ++TransmissionErrors;
   if (sender.ShouldBeDumped())
    dump_if(sender.ShouldBeDumped()) << "transmission failed\n";
   return THREAD_FAIL;
  }
 } else
  return THREAD_TERMINATE;
}

