/*
 * PropertyBag.cc
 *
 *  Created on: 22.04.2013
 *      Author: Drandar
 */
// property bags definitions
// property bags are used for initialization of different objects
// by values got from initialization files, in which property names and values
// are stored as text strings
// (C) I.Drandar, 2013, 2015-2016, all rights reserved
// (C) I.Drandar, for Qt + C++1x, 2018-2020, all rights reserved

#include "PropertyBag.h"
#include "control/exception.h"
#include "stdlib.h"

double Function::operator()(double arg) {
 if (sec.empty()) { // end of recursion: no nested functions
  return prim(arg);
 } else { // accumulate result of nested functions
  double acc = 0;
  if (compose == ComposeType::sum) { // summarize
   acc = 0.0; // initial value;
   for (auto& f : sec) {
    acc += f(arg);
   }
  } else if (compose == ComposeType::prod) {
   acc = 1.0;
   for (auto& f : sec) {
    acc *= f(arg);
   }
  }
  return acc;
 }
}

double Function::operator()(double arg) const {
 if (sec.empty()) { // end of recursion: no nested functions
  return prim(arg);
 } else { // accumulate result of nested functions
  double acc = 0;
  if (compose == ComposeType::sum) { // summarize
   acc = 0.0; // initial value;
   for (const auto& f : sec) {
    acc += f(arg);
   }
  } else if (compose == ComposeType::prod) {
   acc = 1.0;
   for (const auto& f : sec) {
    acc *= f(arg);
   }
  }
  return prim(acc);
 }
}

Function::Function(double Const) :
 prim([Const](double) {
  return Const;
 }) {
}

Function::Function(const PrimitiveFunction& _) : prim(_), compose(), sec() {}

Function::Function(const PrimitiveFunction& p, ComposeType c, std::vector<Function>& s) :
 prim(p), compose(c), sec(s) {}

Function::operator double() const {
 return (*this)(0);
}

Compound::operator double() const {
 if (me.type() == typeid(Function)) {
  return std::any_cast<Function>(me);
 } else {
  return std::any_cast<double>(me);
 }
}

Compound::operator Function() const {
 if (me.type() == typeid(double)) {
  return std::any_cast<double>(me);
 } else {
  return std::any_cast<Function>(me);
 }
}

Compound ConstCategoryTable::Get(const QString& Name) const {
 const auto found = find(Name);
 if (found != end()) {
  return found.value(); // value found
 } else {
  // value not found
  throw TException("constant ", Name, " is undefined");
 }
}

void ConstCategoryTable::Put(const QString& Name, const std::any& Value) {
 (*this)[Name] = Value;
}

Compound ConstTable::Get(const QString& Category, const QString& Name) const {
 const const_iterator found = find(Category);
 if (found != end()) {
  return found.value().Get(Name);
 } else {
  // category not found
  throw
   TException(
    __PRETTY_FUNCTION__, ": Constant category ", Category, " is undefined: ");
 }
}

QList<ConstTable::mapped_type> ConstTable::GetAll(const QString& Category) const {
 return values(Category);
}

void PropertyBag::Insert(const QString& key, const QString& value) {
 (*this)[key] = value;
}

const QString& PropertyBag::GetString(const QString& key) const {
 const const_iterator found = find(key);
 if (found != end()) {
  return found.value();
 } else {
  throw TException(__PRETTY_FUNCTION__, ": Property with name ", key.constData(), " not found");
 }
}

double PropertyBag::GetDouble(const QString& key) const {
 const const_iterator found = find(key);
 if (found != end()) {
  const QString& rep = found.value();
  bool ok;
  double retval = rep.toDouble(&ok);
  if (ok) {
   // string represents correct double value
   return retval;
  } else {
   // string does not represent valid double
   throw
    TException(__PRETTY_FUNCTION__, ": Value of property ", key.constData(), " = ", rep.constData(), " is not double");
  }
 } else {
  throw TException(__PRETTY_FUNCTION__, ": Property with name ", key.constData(), " not found");
 }
}

double PropertyBag::GetDouble(const QString& key, double def) const {
 const const_iterator found = find(key);
 if (found != end()) {
  const QString& rep = found.value();
  bool ok;
  double retval = rep.toDouble(&ok);
  if (ok) {
   // string represents correct double value
   return retval;
  } else {
   // string does not represent valid double
   throw
    TException(__PRETTY_FUNCTION__, ": Value of property ", key.constData(), " = ", rep.constData(), " is not double");
  }
 } else {
  return def;
 }
}

int PropertyBag::GetInt(const QString& key) const {
 const const_iterator found = find(key);
 if (found != end()) {
  const QString& rep = found.value();
  bool ok;
  int retval = rep.toInt(&ok, 0);
  if (ok) {
   // string represents correct integer value
   return retval;
  } else {
   // string does not represent valid integer
   throw
    TException(__PRETTY_FUNCTION__, ": Value of property ", key, " = ", rep, " is not integer");
  }
 } else {
  throw TException(__PRETTY_FUNCTION__, ": Property with name ", key, " not found");
 }
}

int PropertyBag::GetInt(const QString& key, int def) const {
 const const_iterator found = find(key);
 if (found != end()) {
  const QString& rep = found.value();
  bool ok;
  int retval = rep.toInt(&ok, 0);
  if (ok) {
   // string represents correct integer value
   return retval;
  } else {
   // string does not represent valid integer
   throw
    TException(__PRETTY_FUNCTION__, ": Value of property ", key, " = ", rep, " is not integer");
  }
 } else {
  return def;
 }
}

int PropertyBag::GetEnum(const QString& key, const ENUM_TAB& tab) const {
 const const_iterator found = find(key);
 if (found != end()) {
  const QString& rep = found.value();
  bool ok;
  int retval = rep.toInt(&ok);
  if (ok) {
   // value is represented as integer constant
   // test it for correctness
   for (auto i = tab.begin(); i != tab.end(); ++i) {
    if (i.value() == retval) return retval; // value is correct
   }
   throw
    TException(__PRETTY_FUNCTION__, ": Value of property ", key.constData(), " = ", rep.constData(), " is incorrect");
  } else {
   // value is represented as string constant
   // convert it to integer
   const auto valfound = tab.find(rep);
   if (valfound != tab.end())
    return valfound.value(); // value is correct
   else
    throw TException(
     __PRETTY_FUNCTION__, ": Value of property ", key.constData(), " = ", rep.constData(), " is incorrect");
  }
 } else {
  throw TException(__PRETTY_FUNCTION__, ": Property with name ", key.constData(), " not found");
 }
}

int PropertyBag::GetEnum(const QString& key, const ENUM_TAB& tab, int def) const {
 const const_iterator found = find(key);
 if (found != end()) {
  const QString& rep = found.value();
  bool ok;
  int retval = rep.toInt(&ok);
  if (ok) {
   // value is represented as integer constant
   // test it for correctness
   for (auto i = tab.begin(); i != tab.end(); ++i) {
    if (i.value() == retval)
     return retval; // value is correct
   }
   throw
    TException(
     __PRETTY_FUNCTION__, ": Value of property ", key.constData(), " = ", rep.constData(), " is incorrect");
  } else {
   // value is represented as string constant
   // convert it to integer
   const auto valfound = tab.find(rep);
   if (valfound != tab.end()) return valfound.value(); // value is correct
   else
    throw
     TException(
      __PRETTY_FUNCTION__, ": Value of property ", key.constData(), " = ", rep.constData(), " is incorrect");
  }
 } else {
  return def;
 }
}

PropertyBag::BOOL_TAB::BOOL_TAB() {
 (*this)["true"] = true;
 (*this)["false"] = false;
}

PropertyBag::BOOL_TAB PropertyBag::_bool_tab;

bool PropertyBag::GetBool(const QString& key) const {
 try {
  return GetEnum(key, _bool_tab);
 } catch (TException&) {
  throw
   TException(__PRETTY_FUNCTION__, ": Boolean key ", key.constData(), " not found or incorrect");
 }
}

bool PropertyBag::GetBool(const QString& key, bool def) const {
 try {
  return GetEnum(key, _bool_tab, def);
 } catch (TException&) {
  throw
   TException(__PRETTY_FUNCTION__, ": Boolean key ", key.constData(), " is incorrect");
 }
}
