/*
 * CoordinatesConversion.hh
 *
 *  Created on: 03.02.2015
 *      Author: Drandar
 */

#ifndef COORDINATESCONVERSION_HH_
#define COORDINATESCONVERSION_HH_

#include "Coordinates.h"

double Absolute(const Cartesian3DCoords&);
double AbsoluteDistance(const Cartesian3DCoords&, const Cartesian3DCoords&);
double AbsoluteXY(const Cartesian3DCoords&);
const Cartesian3DCoords SphericalToCartesian(const SphericalCoords&);
const SphericalCoords CartesianToSpherical(const Cartesian3DCoords&);
const Spherical2DGroundCoords CartesianToSpherical2DGround(const Cartesian3DCoords&);

void NormalizeRadians(double&);
void NormalizeRadiansSigned(double&);
void Normalize(Spherical2DGroundCoords&);
void Normalize(Spherical2DShipCoords&);

const Cartesian3DCoords ConvertShipToGroundCartesianInvertedZ(const Cartesian3DCoords&, const Rotations&);
const Cartesian3DCoords ConvertShipToGroundCartesian(const Cartesian3DCoords&, const Rotations&);
const Cartesian3DCoords ConvertGroundToShipCartesianInvertedZ(const Cartesian3DCoords&, const Rotations&);
const Cartesian3DCoords ConvertGroundToShipCartesian(const Cartesian3DCoords&, const Rotations&);

const Spherical2DShipCoords ConvertGroundToShipSpherical(const Spherical2DGroundCoords&, const Rotations&);
const Spherical2DGroundCoords ConvertShipToGroundSpherical(const Spherical2DShipCoords&, const Rotations&);

#endif /* COORDINATESCONVERSION_HH_ */
