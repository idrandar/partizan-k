#include "CFreeFun.h"
#include "MathConst.h"
#include <stdlib.h>

double CFreeFun::natan(double u, double v) { // u = y,  v = x
 // counting azimuth for point (u,v ) in interval [0,2*pi]

 double a = 0.0;

 if (v == 0) {
  //if (u==0)  {  printf("<natan> y=0, x=0\n");  return(0); }
  if (u == 0.0)
   return 0.0;
  if (u < 0.0)
   a = MATH_CONST::M_3PI_2;
  if (u > 0.0)
   a = M_PI_2;
 } else {
  a = atan(u / v);
  if (v < 0.0)
   a += M_PI;
  if ((v > 0.0) && (u < 0.0))
   a += MATH_CONST::M_2PI;
 }
 return a;
}

