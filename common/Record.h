
/*
 * Record.hh
 *
 *  Created on: 14.09.2015
 *      Author: Drandar
 */

#ifndef RECORD_HH_
#define RECORD_HH_

// generic data record for specific module
template <typename T>
struct Record : public T {
 typename T::CommandVector Command; // cache for command vector
 typename T::StatusVector Status; // cache for status vector
 void Interact() {
  T::Interact(Command, Status);
 }
 bool ExchangeFault() const { return Status.ExchangeFault; }
 bool NewInfo() const { return Status.NewInfo; }
 const typename T::OutData& Out() const { return Status.data; }
 typename T::InData& In() { return Command.data; }
 Record(const ModuleInit& init) : T(init), Command(), Status() {}
};

#endif /* RECORD_HH_ */
