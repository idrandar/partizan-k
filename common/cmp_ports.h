/* The author: Vladislav Guyvoronskiy          */
/* Date of creation: 15 February 2007          */
/* Working with COM-ports                      */
/*                 */
/* IDE Momentics & QNX Neutrino 6.3.0          */
// (C) I.Drandar, for Linux, 2018, all rights reserved

#ifndef _CMP_PORTS_
#define _CMP_PORTS_

 int OpenComPort(const char *portname);
 int CloseComPort(int fd);
 void SetVarTimeOut (int TimeOutValue);

 enum ParityType {
  NONE, EVEN, ODD, MARK, SPACE
 };
 int SetComPort (int fd, int speed, ParityType=NONE, int StopBits = 1);
 int WriteComPort(int fd, const unsigned char *buf, int size);
 int ReadComPort (int fd, unsigned char *buf, int size);

#endif  // _CMP_PORTS_
// end of this cmp_ports.hh file
