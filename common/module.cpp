// generic module class
// (C) I.Drandar, 2010-2014, all rights reserved
// (C) I.Drandar, for Linux + C++1x + Qt, 2018, 2020 all rights reserved

#include "module.h"
#include <thread>
#include "control/exception.h"
#include <iostream>
#include "DebugOut.h"

Compound ModuleInit::GetConst(const QString& Category, const QString& Name) const {
 try {
  return constants.Get(Category, Name);
 } catch (TException& e) {
  throw TException(" [", Category, "/", Name, "] ", e.what());
 }
}

QList<ConstCategoryTable> ModuleInit::GetAll(const QString& Category) const {
 if (constants.contains(Category)) {
  return constants.values(Category);
 } else {
  throw TException("no category section <", Category, "> found");
 }
}

const ThreadInit& ModuleInit::Thread(const QString& Name) const {
 const auto found = threads.find(Name);
 if (found != threads.end()) {
  // thread found
  return found.value(); // value found
 } else {
  // value not found
  throw
   TException(
    __PRETTY_FUNCTION__, ": Module ", name, ": Thread ", Name, " is undefined");
 }
}

// ModuleThread thread function
// Constructor of owner Module class passes in parameter "arg" pointer to itself
void* Module::Thread::DoRun(void* arg) {
 if (disable) return nullptr;
 // get reference to owner class
 Module* mod = reinterpret_cast<Module*>(arg);
 std::cout << mod->GetName() << '.' << GetName() << " thread started" << std::endl;
 try {
  // start
  // main cycle
  while (RunningFlag) { // while Module class exists
   // do some work
   // if F returns THREAD_TERMINATE it means either all work is done or some circumstances
   // (e.g unrecoverable error) made further work meaningless
   if (caller->call() == THREAD_TERMINATE) break;
  }
// termination
 } catch (TException& except) {
  std::cout << mod->GetName() << '.' << GetName() << " thread catches exception: " << except.what() << '\n';
 } catch(...) {
  std::cout << mod->GetName() << '.' << GetName() << "thread catches an unrecognized exception\n";
 }
 std::cout << mod->GetName() << '.' << GetName() << " thread terminated\n";
 return nullptr;
}

void Module::Thread::Start() {
 try {
  if (!disable) {
   RunningFlag = true;
   Create();
  }
 } catch (TException& e) {
  throw
   TException(
    __PRETTY_FUNCTION__, "\t[", GetName(), "@", caller->Owner().GetName(), "]\n", e.what());
 }
}

void Module::Thread::Stop() {
 RunningFlag = false; // terminate threads
 WakeUp();
}

/*void Module::Thread::Send(const char* Message, unsigned int Delay) {
 sleep(Delay);
 SendDataPacket(coid, Message, strlen(Message) + 1);
}*/

void Module::Thread::Lock() {
 pthread_mutex_lock(&mutex);
}

void Module::Thread::Unlock() {
 pthread_mutex_unlock(&mutex);
}

void Module::Thread::WakeUp() {
 sem_post(&wait_sem);
}

void Module::Thread::Wait() {
 sem_wait(&wait_sem);
}

// ModuleThread constructor
// passes its parameters to Thread constructor
Module::Thread::Thread(const ThreadInit& init, Caller* c) :
 ::Thread(init.Priority(), &c->Owner()), _Name(init.Name()), caller(c),
 dump(init.Dump()), disable(init.Disable()) {
 pthread_mutexattr_t mutex_attr;
 pthread_mutexattr_init(&mutex_attr);
 pthread_mutexattr_setprotocol(&mutex_attr, PTHREAD_PRIO_PROTECT);
 pthread_mutexattr_setprioceiling(&mutex_attr, init.Priority() + 1);
 pthread_mutex_init(&mutex, &mutex_attr);
 sem_init(&wait_sem, 0, 0);
}

Module::Thread::~Thread() {
 while (IsRunning()) {
  std::this_thread::yield();
  Cancel();
 }
// Join(); // waiting for termination
 sem_destroy(&wait_sem);
 pthread_mutex_destroy(&mutex);
 delete caller;
}

Module::Module(const ModuleInit& init) :
 frequency(init.Frequency()), time_step(1.0 / frequency), dump(init.Dump()), Name(init.Name()),
 counter(), _counter(init.CounterShift()) {
 counter.Shift(init.CounterShift());
 std::cout << "Creating " << GetName() << " module class" << std::endl;
}

Module::~Module() {
 // cleanup
 std::cout << "Deleting " << GetName() << " module class" << std::endl;
}

void Module::Start() {
 std::cout << "Starting " << GetName() << " module's threads" << std::endl;
}

void Module::Stop() {
 std::cout << "Stopping " << GetName() << " module's threads\n";
}

// divide system frequency
bool Module::HaveTick() {
 const double eps = 1E-6;
 if ((counter.Gone() + eps) >= time_step) {
  if (dump) {
   std::cout << GetName() << "::HaveTick() at " << counter.TimeFromProgramStart() << " s" << std::endl;
  }
  counter.Shift(time_step);
  return true;
 } else return false;
}

// divide system frequency
bool Module::HaveTick(double Shift) {
 _counter += Shift;
 const double eps = 1E-5;
 if (_counter + eps >= time_step) {
  dump_if(dump) << "\t" << _counter << " s gone" << std::endl;
  _counter -= time_step;
  return true;
 } else return false;
}
