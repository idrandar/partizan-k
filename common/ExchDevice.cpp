/*
 * ExchDevice.cc
 *
 *  Created on: 23.04.2013
 *      Author: Drandar
 */
// exchange device description
// (C) I.Drandar, 2013-2017, all rights reserved
// (C) I.Drandar, for Linux + Qt + C++1x, 2018-2019, all rights reserved

#include "ExchDevice.h"
#include <iostream>
#include "common/lock.h" // Read/WriteLock;
#include <assert.h>
#include "ctime.h"
#include <iomanip>   // setprecision
#include "DebugOut.h"

ExchangeDevice::DevicePool::~DevicePool() {
 for (DevicePool::iterator i = pool.begin(); i != pool.end(); ++i) {
  i.value()->Close();
  delete i.value();
 }
 pool.clear();
 std::cout << __PRETTY_FUNCTION__ << std::endl;
}
ExchangeDevice::DevicePool ExchangeDevice::pool;

ExchangeDevice* ExchangeDevice::Item(const DeviceInit& _) {
 const QString name = _.Name();
 const DevicePool::const_iterator found = pool.find(name);
 if (found != pool.end()) {
  // device is present in the pool already
  return found.value();
 } else {
  ExchangeDevice* new_dev = nullptr;
  const QString type = _.Class();
  if (type.compare("RS-232") == 0 || type.compare("RS-422") == 0)
   new_dev = new RsDevice(_.Properties());
  if (type.compare("NET") == 0)
   new_dev = new NetDevice(_.Properties());
  if (new_dev != nullptr) pool[name] = new_dev; // insert just created device into pool
  return new_dev;
 }
}

ExchangeDevice::ExchangeDevice(const PropertyBag& _) :
 name(_.GetString("name")),
 debuginput(_.GetBool("debug-input", false)), debugoutput(_.GetBool("debug-output", false)),
 debugtime(_.GetBool("debug-time", false)),
 max_dump_bytes(_.GetInt("max-dump-bytes", std::numeric_limits<unsigned int>::max())),
 received(), transmitted(), readings(), writings(),
 max_out(), max_in(), stop() {
 pthread_mutex_init(&write_mutex, NULL);
}

ExchangeDevice::~ExchangeDevice() {
 std::cout << __PRETTY_FUNCTION__ << std::endl;
 pthread_mutex_destroy(&write_mutex);
}

void ExchangeDevice::DumpBuffer(const Protocol::RawArray& _) {
 std::cout << "(" << _.size() << " bytes)" << std::hex;
 constexpr size_t row_size = 0x20;
 for (size_t i = 0; i < row_size; ++i)
  std::cout << ((i % row_size) ? " " : "\n   ") << ((i < 0x10) ? "0" : "") << i;
 for (size_t i = 0; i < std::min(_.size(), max_dump_bytes); ++i) {
  if ((i % row_size) == 0) {
   std::cout << "\n" << i / row_size << ": ";
  } else {
   std::cout << " ";
  }
  const unsigned int d = _[i];
  std::cout << ((d < 0x10) ? "0" : "") << d;
 }
 if (_.size() > max_dump_bytes)
  std::cout << "\n. . .";
 std::cout << std::dec << std::endl;
}

void ExchangeDevice::DumpBuffer(const size_t size, const unsigned char _[]) {
 std::cout << std::hex;
 for (size_t i = 0; i < size; ++i) {
  std::cout << static_cast<unsigned int>(_[i]) << " ";
 }
 std::cout << std::dec << std::endl;
}

ExchangeDevice::MessageQueue::MessageQueue() {
 pthread_mutex_init(&mutex, NULL);
 pthread_cond_init(&cond, NULL);
}

ExchangeDevice::MessageQueue::~MessageQueue() {
 clear();
 pthread_cond_destroy(&cond);
 pthread_mutex_destroy(&mutex);
}

bool ExchangeDevice::MessageQueue::get(Protocol::RawArray& _) {
 Lock l(mutex);
 if (queue.empty()) {
  if (wait_max <= 0) {
   pthread_cond_wait(&cond, &mutex);
  } else {
   timespec abs_timeout;
   abs_timeout.tv_sec = static_cast<time_t>(wait_max);
   abs_timeout.tv_nsec = (wait_max - abs_timeout.tv_sec) * 1000000000L;
   pthread_cond_timedwait(&cond, &mutex, &abs_timeout);
  }
 }
 if (!queue.empty()) {
  _ = queue.front();
  queue.pop_front();
  return true;
 } else {
  return false;
 }
}

void ExchangeDevice::MessageQueue::clear() {
 Lock l(mutex);
 queue.clear();
}

void ExchangeDevice::MessageQueue::put(const Protocol::RawArray& _) {
 {
  Lock l(mutex);
  queue.push_back(_);
 }
 pthread_cond_signal(&cond);
}

ExchangeDevice::MessageQueueMap::MessageQueueMap() {
 pthread_rwlock_init(&guard, NULL);
}

ExchangeDevice::MessageQueueMap::~MessageQueueMap() {
 clear();
 pthread_rwlock_destroy(&guard);
}

void ExchangeDevice::MessageQueueMap::clear() {
 WriteLock l(guard);
 for (Impl::iterator i = _impl.begin(); i != _impl.end(); ++i) {
  delete i->second;
 }
 _impl.clear();
}

void ExchangeDevice::MessageQueueMap::insert(const Protocol* pr) {
 WriteLock l(guard);
 _impl[pr] = new MessageQueue();
}

const Protocol* ExchangeDevice::MessageQueueMap::accepted(const Socket::buftype& buf) {
 ReadLock l(guard);
 for (Impl::const_iterator it = _impl.begin(); it != _impl.end(); ++it) {
  if (it->first->Acceptable(buf)) return it->first;
 }
 return NULL;
}

ExchangeDevice::MessageQueue* ExchangeDevice::MessageQueueMap::get(const Protocol* pr) {
 ReadLock l(guard);
 Impl::iterator found = _impl.find(pr);
 assert(found != _impl.end());
 return found->second;
}

bool ExchangeDevice::MessageQueueMap::present(const Protocol* pr) const {
 ReadLock l(guard);
 return _impl.find(pr) != _impl.end();
}

RsDevice::PARITY_TAB::PARITY_TAB() {
 // init table for parsing ParityType values
 (*this)["NONE"] = NONE;
 (*this)["EVEN"] = EVEN;
 (*this)["ODD"] = ODD;
 (*this)["MARK"] = MARK;
 (*this)["SPACE"] = SPACE;
}

RsDevice::PARITY_TAB RsDevice::_parity_tab;

RsDevice::RsDevice(const PropertyBag& _) :
 ExchangeDevice(_), ::Thread(_.GetInt("priority", 20)), port(INVALID_PORT),
 serial(_.GetString("serial")), bps(_.GetInt("baudrate", 115200)),
 par(static_cast<ParityType>(_.GetEnum("parity", _parity_tab, NONE))), stopbits(_.GetInt("stopbits", 1)),
 time_out(_.GetInt("time-out", 0)),
 bufsize_factor(_.GetInt("buf-size-factor", 1)), dump_input_buffer(_.GetBool("dump-input-buffer", false)),
 in_buf(), out_buf(), min_in(std::numeric_limits<size_t>::max()) {
}

RsDevice::~RsDevice() {
 Close();
 std::cout << __PRETTY_FUNCTION__ << std::endl;
}

void RsDevice::Open() {
 assert(port == INVALID_PORT); // deny sequential openings
// std::cout << serial << "\t" << port << std::endl;
 port = OpenComPort(serial.toLocal8Bit());
// std::cout << serial << "\t" << port << std::endl;
 if (port == INVALID_PORT) {
  std::cout << Name() << ": Can't open serial: " << serial.toStdString() << '\n';
 } else {
  auto result = SetComPort(port, bps, par, stopbits);
  if (result == -1) {
   std::cout << Name() << ": Can't setup serial: " << serial.toStdString() << std::endl;
//   Close();
  } else {
   Create();
  }
 }
}

void RsDevice::Close() {
 if (port != INVALID_PORT) {
  stop = true;
  Cancel();
  if (debuginput)
   std::cout << Name() << ": full count of received bytes:\t" << received << '\n';
  if (debugoutput)
   std::cout << Name() << ": full count of transmitted bytes:\t" << transmitted << '\n';
  stop = true;
  Cancel();
  CloseComPort(port);
 }
 port = INVALID_PORT;
}

bool RsDevice::IsOpen() const {
 return port != INVALID_PORT;
}

bool RsDevice::Write(Protocol* pr) {
 if (IsOpen()) {
  Lock l(write_mutex);
  out_buf.resize(pr->OutSize()); // prepare temporary buffer for output data
  pr->Push(out_buf); // copy data to outer buffer
  if (debugoutput) {
   std::cout <<
    Name() << ": Writing to port " << serial.toLocal8Bit().constData() << " #" << ++writings;
   if (debugtime)
    std::cout << " @ " << std::setprecision(7) << CTime::TimeFromProgramStart() << " s";
   std::cout << "\nDump of transmitted package: ";
   DumpBuffer(out_buf);
  }
  CTime _;
  const int ret = WriteComPort(port, &out_buf[0], out_buf.size());
  if (ret > 0) transmitted += ret;
  if (ret == WRITE_ERROR) {
   // file error
   if (debugoutput) std::cout << Name() << ": Error during write to port " <<
    serial.toLocal8Bit().constData() << ": " << strerror(errno) << '\n';
   return false;
  }
  if (static_cast<size_t>(ret) != out_buf.size()) {
   // not all bytes were written
   if (debugoutput)
    std::cout << Name() << ": Error during write to port " << serial.toLocal8Bit().constData() << ": " << out_buf.size() <<
     " bytes needed to write but " << ret << " were written\n";
   return false;
  } else {
   if (debugoutput) std::cout << Name() << ": Writing to port " << serial.toLocal8Bit().constData() << " is OK\n";
   return true;
  }
 } else {
  return false;
 }
}

bool RsDevice::Read(Protocol* pr) {
 if (!IsOpen()) return false;
 if (!messages.present(pr)) {
  if (debuginput) std::cout << Name() << ": Protocol is unregistered\n";
  return false;
 } else {
  MessageQueue* mq = messages.get(pr);
  Protocol::RawArray buf;
  if (debuginput) {
   std::cout << Name() << " before getting data: buf size: " << buf.size() << std::endl;
  }
  const bool got_data = mq->get(buf); // in this point thread can be blocked
  if (got_data) {
   const size_t buf_size = buf.size();
   const size_t in_size = pr->InSize();
   if (buf_size != in_size) {
    std::cout << "read error:\t" << buf_size << " " << in_size << std::endl;
   }
   assert(buf.size() == pr->InSize());
   assert(pr->Acceptable(buf));
   pr->Pull(buf);
   return true;
  } else
   return false;
 }
}

bool RsDevice::Register(const Protocol* pr) {
 if (!Registered(pr)) {
  constexpr size_t min_packet_size = 0;
  auto _name = Name();
  auto _mod_name = pr->Owner();
  std::cout << _name << ": registering protocol for module " << _mod_name << std::endl;
  auto _in_size = pr->InSize();
  if (_in_size > min_packet_size) {
   // add new protocol instance to map
   messages.insert(pr); // new empty queue
   if (debuginput) {
    const size_t messages_count = messages.count();
    std::cout << _name << ": messages count = " << messages_count << std::endl;
   }
 //  max_out = std::max(max_out, pr->OutSize());
   max_in = std::max(max_in, _in_size);
   min_in = std::min(min_in, _in_size);
   if (debuginput) {
    std::cout << _name << ": max_in = " << max_in << std::endl;
   }
   return true;
  } else
   return false;
 } else {
  assert(!"Duplicate protocol registration");
  return false;
 }
}

bool RsDevice::Registered(const Protocol* _) const {
 const ComSimple* _test = dynamic_cast<const ComSimple*>(_); // testing that protocol is ComSimple
 if (_test == NULL) {
  assert(!"Invalid protocol type");
  return false;
 } else {
  return messages.present(_);
 }
}

struct IsProbablyHeader {
 bool operator()(unsigned char _) { return (_ & 0x80) != 0; }
};

void* RsDevice::DoRun(void*) {
 if (debuginput) {
  std::cout << Name() << ": Input thread started";
  if (debugtime)
   std::cout << " @ " << CTime::TimeFromProgramStart() << " s";
  std::cout << "\n";
 }
 Protocol::RawArray inp;
 auto inbufsize = std::max<size_t>(max_in * bufsize_factor, 256);
 unsigned char tmp[inbufsize];
 while (!stop) {
  if (!IsOpen()) break;
  if (debugtime)
   std::cout << "ready to read data from port " << Name() << " at " << CTime::TimeFromProgramStart() << " s\n";
  const ssize_t read = ReadComPort(port, tmp, inbufsize);
  if (debugtime)
   std::cout << "got " << read << " bytes from port " << Name() << " at " << CTime::TimeFromProgramStart() << " s\n";
  if (read > 0) {
   received += read;
   if (debuginput && dump_input_buffer) {
    std::cout <<
    Name() << ":\tRead chunk of " << read << " bytes.\n"
    "Dump of received data: ";
    DumpBuffer(read, tmp);
   }
   size_t prev_size = inp.size();
   inp.resize(inp.size() + read);
   std::copy(tmp, tmp + read, &inp[prev_size]);
// OR  std::copy(tmp, tmp + read, std::back_inserter(inp));
   if (debuginput) {
    std::cout <<
     Name() << ":\tNow temporary buffer contains " << inp.size() << " bytes.\n"
     "Total count of received bytes = " << received << "\n"
     "Dump of temporary buffer: ";
    DumpBuffer(inp);
   }
  } else if (read < 0) {
   // I/O error
   if (debuginput)
    std::cout << Name() << ":\tError during read: " << strerror(errno) << std::endl;
  } else { // read == 0
   if (debuginput)
    std::cout << Name() << ":\tNo data read after timeout\n";
  }
  do {
//   delay(0);
   // optimization
   Protocol::RawArray::iterator i = std::find_if(inp.begin(), inp.end(), IsProbablyHeader());
   unsigned int skipped_for_header = std::distance(inp.begin(), i);
   total_rejected += skipped_for_header;
   if (debuginput)
    std::cout <<
     Name() << ":\tskipping " << skipped_for_header << " bytes\n"
     "Total rejected bytes = " << total_rejected << std::endl;
   inp.erase(inp.begin(), i);

   auto l = inp.size();
   if (l < min_in) {
    if (debuginput) {
     std::cout <<
      Name() << ":\tunsufficient amount of data for recognizing of package: " << l <<
      " bytes" << std::endl;
    }
    break;
   }

   if (debuginput && skipped_for_header > 0) {
    std::cout <<
     Name() << ":\tNow temporary buffer contains " << inp.size() << " bytes.\nDump of temporary buffer: ";
    DumpBuffer(inp);
   }
   const Protocol* index = messages.accepted(inp);
   if (index != nullptr) {
    if (debuginput) {
     std::cout << Name() << ":\tData recognized by " << index->Owner() << std::endl;
    }
    MessageQueue* b = messages.get(index);
    Protocol::RawArray recognized_message(inp.begin(), inp.begin() + index->InSize());
    b->put(recognized_message);
    if (debuginput) {
     std::cout <<
      Name() << ": " << b->count() << " messages waiting for " << index->Owner() << std::endl;
    }
//    inp.erase(inp.begin(), inp.begin() + index->InSize());
    inp.erase(inp.begin(), std::find_if(inp.begin() + 1, inp.end(), IsProbablyHeader()));
    if (debuginput) {
     std::cout <<
      Name() << ":\tNow temporary buffer contains " << inp.size() << " bytes.\nDump of temporary buffer: ";
     DumpBuffer(inp);
    }
   } else {
    if (debuginput)
     std::cout << Name() << ":\tPackage is not recognized" << std::endl;
    if (inp.size() > inbufsize) {
     if (debuginput)
      std::cout << Name() << ":\t"
       "too many bytes in input buffer: " << inp.size() << " vs " << inbufsize << std::endl;
     inp.erase(inp.begin());
     total_rejected += 1;
     if (debuginput)
      std::cout <<
       Name() << ":\tskipping " << 1 << " bytes\n"
       "Total rejected bytes = " << total_rejected << std::endl;
    } else if (skipped_for_header == 0 && l >= max_in) {
     if (debuginput) {
      std::cout << Name() << ":\t"
       "unknown token: " << std::hex << "0x" << static_cast<int>(inp[0]) << std::dec << std::endl;
     }
     inp.erase(inp.begin());
     total_rejected += 1;
     if (debuginput)
      std::cout <<
       Name() << ":\tskipping " << 1 << " bytes\n"
       "Total rejected bytes = " << total_rejected << std::endl;
    }
//     else delay(0);
   }
  } while (inp.size() >= max_in);
 }
 if (debuginput) {
  std::cout << Name() << ":\tInput thread terminated\n";
 }
 return NULL;
}

NetDevice::NetDevice(const PropertyBag& _) :
 ExchangeDevice(_), ::Thread(_.GetInt("priority", 20)),
 s(INET, Socket::UDP),
 remote(_.GetString("remote-ip"), _.GetInt("remote-port")),
 host(_.GetString("host-ip"), _.GetInt("host-port")) {
}

NetDevice::~NetDevice() {
 Close();
 messages.clear();
 std::cout << __PRETTY_FUNCTION__ << std::endl;
}

void NetDevice::Open() {
 if (!IsOpen()) {
  s.Open();
//  SocketAddress _("192.168.108.140", 1212);
  if (!s.bind(host)) {
   s.Close();
  } else {
   Create();
  }
 }
}

void NetDevice::Close() {
 if (IsOpen()) {
  if (debuginput)
   std::cout << Name() << ": full count of received bytes:\t" << received << '\n';
  if (debugoutput)
   std::cout << Name() << ": full count of transmitted bytes:\t" << transmitted << '\n';
  stop = true;
  Cancel();
  s.Close();
 }
}

bool NetDevice::IsOpen() const {
 return s.good();
}

void* NetDevice::DoRun(void*) {
 if (debuginput) {
  std::cout << Name() << ": Input thread started\n";
 }
 while (!stop) {
  if (!IsOpen()) break;
  Socket::buftype buf(max_in);
  SocketAddress addr = host;
  const ssize_t read = s.readfrom(buf, addr);
  if (read > 0) {
   received += read;
   const Protocol* index = messages.accepted(buf);
   if (index != NULL) {
    if (debuginput) {
     std::cout << Name() << ": Read OK. Dump of received package: ";
     DumpBuffer(buf);
    }
    MessageQueue* q = messages.get(index);
    q->put(buf);
   } else {
    if (debuginput)
     std::cout << Name() << ": Drop an unrecognized package\n";
   }
  } else if (read < 0) {
   // socket error
   if (debuginput)
    std::cout << Name() << ": Error during read from "
     << remote.GetIP().c_str() << ": " << strerror(errno) << std::endl;
  } else { // read == 0
   if (debuginput)
    std::cout << Name() << ": Error during read from " << remote.GetIP().c_str() << ": empty message\n";
  }
 }
 if (debuginput) {
  std::cout << Name() << ": Input thread terminated\n";
 }
 return NULL;
}

bool NetDevice::Write(Protocol* pr) {
 if (IsOpen()) {
  Lock l(write_mutex);
  Socket::buftype buf(max_out);
  pr->Push(buf);
  if (debugoutput) {
   std::cout << Name() << ": Writing to " << remote.GetIP().c_str() << " #" << ++writings <<
    "\nDump of transmitted package: ";
   DumpBuffer(buf);
  }
  const ssize_t ret = s.writeto(buf, remote);
  if (ret == -1) {
   // socket error
   if (debugoutput)
    std::cout << Name() << ": Error during write to " << remote.GetIP().c_str() << ": " << strerror(errno) << '\n';
  }
  if (static_cast<size_t>(ret) != buf.size()) {
   // not all bytes were written
   if (debugoutput)
    std::cout << Name() << ": Error during write to " << remote.GetIP().c_str() << ": " << buf.size() <<
     " bytes needed to write but " << ret << " were written\n";
   return false;
  } else {
   if (debugoutput) std::cout << Name() << ": Writing to port " << remote.GetIP().c_str() << " is OK\n";
   return true;
  }
 } else return false;
}

bool NetDevice::Read(Protocol* pr) {
 if (!IsOpen()) return false;
 if (!messages.present(pr)) {
  if (debuginput) std::cout << Name() << ": Protocol is unregistered\n";
  return false;
 }
 else {
  MessageQueue* mq = messages.get(pr);
  Socket::buftype buf;
  const bool got_data = mq->get(buf); // in this point thread can be blocked
  if (got_data) {
   assert(buf.size() == pr->InSize());
   assert(pr->Acceptable(buf));
   pr->Pull(buf);
   return true;
  } else
   return false;
 }
}

bool NetDevice::Register(const Protocol* pr) {
 if (!Registered(pr)) {
  // add new protocol instance to map
  messages.insert(pr); // new empty queue
  max_out = std::max(max_out, pr->OutSize());
  max_in = std::max(max_in, pr->InSize());
  return true;
 } else {
  assert(!"Duplicate protocol registration");
  return false;
 }
}

bool NetDevice::Registered(const Protocol* pr) const {
 const Comp_Comp* _test = dynamic_cast<const Comp_Comp*>(pr); // test if protocol is applicable
 if (_test == NULL) {
  assert(!"Invalid protocol type");
  return false;
 } else {
  return messages.present(pr);
 }
}
