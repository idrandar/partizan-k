/*
 * Sockets.cc
 *
 *  Created on: 16.05.2013
 *      Author: Drandar
 */

// Wrapper for sockets library
// based on code from "UNIX System Programming Using C++" by Terrence Chan
// an on "UNIX Network Programming: Networking APIs" by W. Richard Stevens
// (C) I.Drandar, 2013, all rights reserved
// (C) I.Drandar, for Linux + C++1x + Qt, 2018, all rights reserved

#include "Sockets.h"
#include <iostream> // cerr
#include <arpa/inet.h> // inet_ntoa
#include <unistd.h> // close
#include "control/exception.h"

void SocketAddress::Init(AddressFamily af) {
 memset(&_.addr, 0, sizeof _.addr);
 _.addr.sa_family = af;
// switch (af) {
// case INET:
//  _.addr_in.sin_len = sizeof _.addr_in;
//  break;
// default:
//  _.addr.sa_len = sizeof _.addr;
//  break;
// }
}

SocketAddress::SocketAddress(AddressFamily af) {
 Init(af);
 switch (af) {
 case INET:
  _.addr_in.sin_addr.s_addr = htonl(INADDR_ANY);
  _.addr_in.sin_port = 0;
  break;
 default:
  break;
 }
}

SocketAddress::SocketAddress(const QString& ip, int port, AddressFamily af) {
 Init(af);
 int result;
 result = inet_pton(af, ip.toLocal8Bit(), &_.addr_in.sin_addr);
 if (result == 1) {
  switch (af) {
  case INET:
   _.addr_in.sin_port = htons(port);
   break;
  default:
   break;
  }
 }
}

void SocketAddress::AssignIP(const std::string& ip) {
 inet_pton(_.addr.sa_family, ip.c_str(), &_.addr);
}

std::string SocketAddress::GetIP() const {
 const size_t replen = (_.addr.sa_family == INET) ? INET_ADDRSTRLEN : INET6_ADDRSTRLEN;
 char rep[replen];
 std::string __ = inet_ntop(_.addr.sa_family, &_.addr_in.sin_addr, rep, replen);
 return __;
}

void SocketAddress::AssignPort(int port) {
 switch (_.addr.sa_family) {
 case INET:
  _.addr_in.sin_port = htons(port);
  break;
 default:
  throw TException(__PRETTY_FUNCTION__, "unknown address family of socket");
 }
}

int SocketAddress::GetPort() const {
 switch (_.addr.sa_family) {
 case INET:
  return ntohs(_.addr_in.sin_port);
 default:
  throw TException(__PRETTY_FUNCTION__, "unknown address family of socket");
 }
}

socklen_t SocketAddress::GetLength() const {
 return sizeof _.addr_in;
// return _.addr.sa_len;
}

int Socket::fd() const { return sid; }

Socket::Socket(AddressFamily domain, Type type, int protocol) : sid(FAIL), domain(domain), type(type), protocol(protocol) {}
Socket::Socket(int fd) : sid(fd), domain(INET), type(UDP), protocol() {}
Socket::~Socket() { Close(); }

void Socket::WriteError(const char* msg) {
 auto _err = errno;
 std::cout << msg << "() error: " << strerror(_err) << std::endl;
}

bool Socket::Open() {
 if (!good()) sid = socket(domain, type, protocol);
 if (!good()) WriteError("socket");
 return good();
}

bool Socket::Close() {
 if (!good()) return true;
 shutdown();
 const int ret = close(sid);
 sid = FAIL;
 if (ret == FAIL) WriteError("close");
 return ret != FAIL;
}

bool Socket::good() const { return sid >= 0; }

bool Socket::bind(SocketAddress& addr) {
 if (::bind(sid, &addr.Addr(), addr.GetLength()) == FAIL) {
  WriteError("bind");
  return false;
 } else {
  socklen_t len = addr.GetLength();
  if (getsockname(sid, &addr.Addr(), &len) == FAIL) {
   WriteError("getsockname");
   return false;
  } else {
   if (type != UDP) {
    const int BACKLOG_NUM = 5; // standard value
    if (listen(sid, BACKLOG_NUM) == FAIL) {
     WriteError("listen");
     return false;
    }
   }
  }
 }
 return true;
}

Socket Socket::accept(SocketAddress& _) {
 socklen_t size = _.GetLength();
 int id = ::accept(sid, &_.Addr(), &size);
 return Socket(id);
}

bool Socket::connect(const SocketAddress& _) {
 if (::connect(sid, &_.Addr(), _.GetLength()) == FAIL) {
  WriteError("connect");
  return false;
 } else return true;
}

ssize_t Socket::write(const buftype& buf) {
 return ::send(sid, &buf[0], buf.size(), 0);
}

ssize_t Socket::read(buftype& buf) {
 ssize_t ret = ::recv(sid, &buf[0], buf.size(), 0);
 if (ret >= 0) buf.resize(ret); // adjust buffer size
 return ret;
}

ssize_t Socket::writeto(const buftype& buf, const SocketAddress& to) {
 return ::sendto(sid, &buf[0], buf.size(), 0, &to.Addr(), to.GetLength());
}

ssize_t Socket::readfrom(buftype& buf, SocketAddress& from) {
 socklen_t size = from.GetLength();
// std::cout << "buf size: " << buf.size() << std::endl;
 ssize_t ret = ::recvfrom(sid, &buf[0], buf.size(), 0, &from.Addr(), &size);
// std::cout << "from: " << from.GetIP() << ":" << from.GetPort() << std::endl;
// ssize_t ret = ::recvfrom(sid, &buf[0], buf.size(), 0, NULL, 0);
 if (ret >= 0) buf.resize(ret); // adjust buffer size
 return ret;
}

bool Socket::shutdown(HowToShutdown mode) {
 if (!good()) return false;
 return ::shutdown(sid, mode) != FAIL;
}
