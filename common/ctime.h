#ifndef _CTIME_HH_
#define _CTIME_HH_

/* The author: Victor Poputnikov         */
/* Date of creation: 20 July 2007        */
/* A class for noting time               */
/* IDE Momentics & QNX Neutrino 6.3.0    */
// Modified for "Sarmat" project
// (C) I.Drandar, 2011, 2015 all rights reserved
// (C) I.Drandar, for Linux + C++1x, 2018, all rights
#include <chrono>

//Class for noting time based on ClockCycles function
class CTime {
 using _duration = std::chrono::duration<double>;
 using clock = std::chrono::high_resolution_clock;
 using time_point = std::chrono::time_point<clock>;
 time_point begin; // start mark
 time_point end; // finish mark
 time_point start; // marks creation time
 time_point finish;
 static const time_point ProgramStart;
public:
 CTime(); //Setting variable to 0 and getting cycles per
 //second value
 ~CTime();
 void Start(); // Sets start mark
 void Stop(); // Sets finish mark
// void Advance(double);
 double Span() const; // time between start and finish marks
 double Gone() const; // time between start and current point
 double OverallTime() const; // time spent from clock creation in seconds
 static double TimeFromProgramStart();
 void Shift(double); // change start mark;
// double Begin() const;
// double End() const;
}; // end

double GlobalTime();

#endif /*CTIME_HH_*/
