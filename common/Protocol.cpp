/*
 * Protocol.hh
 *
 *  Created on: 03.12.2014
 *      Author: Drandar
 */
// Exchange protocol description
// (C), I.Drandar, 2014-2016, all rights reserved
// (C) I.Drandar, for Qt, 2018, 2020 all rights reserved

#include "Protocol.h"
#include <assert.h>
#include <iostream>
#include "ExchDevice.h"

Protocol::Protocol(
 const std::string& type, const std::string& description, const QString& owner,
 const PropertyBag& _,
 size_t _inSize, size_t _outSize) :
 type(type), description(description), owner(owner),
 inSize(_inSize), outSize(_outSize),
 dump(_.GetBool("dump", false)),
 responseOnRequest(_.GetBool("response-on-request", false)),
 responseRequestMax(_.GetDouble("response-request-max-time", 0.0)),
 checkSumDump(_.GetBool("checksum-dump", false)),
 input_buffer(inSize), output_buffer(outSize), transmitted(), received() {
 sem_init(&responseOnRequest_sem, 0, 0);
 if (dump) {
  std::cout << type << ": input size = " << inSize << "\toutput size = " << outSize << std::endl;
 }
}

Protocol::~Protocol() {
 sem_destroy(&responseOnRequest_sem);
}

void Protocol::GetData(size_t size, unsigned char data[]) {
 assert(size == input_buffer.size());
 std::copy(input_buffer.begin(), input_buffer.end(), data);
}

void Protocol::PutData(size_t size, const unsigned char data[]) {
 assert(size == output_buffer.size());
 std::copy(data, data + size, output_buffer.begin());
}

void Protocol::Pull(const RawArray& outer) {
 assert(outer.size() >= InSize());
 std::copy(outer.begin(), outer.begin() + InSize(), input_buffer.begin());
 ++received;
}

void Protocol::FakePull() {
 ++received;
}

void Protocol::Push(RawArray& outer) {
 assert(outer.size() >= OutSize());
 std::copy(output_buffer.begin(), output_buffer.end(), outer.begin());
 ++transmitted;
}

unsigned char Protocol::ChecksumEvalSum(RawArray::const_iterator ptr, size_t size) {
 return std::accumulate(ptr, ptr + size, 0) & 0x7F;
}

unsigned char Protocol::ChecksumEvalXor(RawArray::const_iterator ptr, size_t size) {
 unsigned char sum = 0;
// std::cout <<  std::dec << size << "\n";
 for (unsigned int i = 0; i < size; ++i, ++ptr) {
  sum ^= *ptr;
//  std::cout << std::hex << static_cast<int>(*ptr) << "\t" << static_cast<int>(sum) << std::endl;
 }
 return sum & 0x7F;
}

void Protocol::WaitForRequestIfNeeded() {
 if (responseOnRequest) {
  if (responseRequestMax <= 0.0)
   sem_wait(&responseOnRequest_sem);
  else {
   timespec abs_timeout;
   abs_timeout.tv_sec = static_cast<time_t>(responseRequestMax);
   abs_timeout.tv_nsec = (responseRequestMax - abs_timeout.tv_sec) * 1000000000L;
   sem_timedwait(&responseOnRequest_sem, &abs_timeout);
  }
 }
}

void Protocol::RequestAccepted() {
 if (responseOnRequest) {
  sem_post(&responseOnRequest_sem);
 }
}

ComSimple::CHECKSUM_TAB::CHECKSUM_TAB() {
 (*this)["xor"] = CS_XOR;
 (*this)["sum"] = CS_SUM;
}

ComSimple::CHECKSUM_TAB ComSimple::_checksum_tab;

ComSimple::ComSimple(const QString& owner, const PropertyBag& _, size_t inSize, size_t outSize) :
 Protocol(
  "ComSimple", "Dumb byte-oriented protocol with 7-bit data bytes with checksum", owner, _,
  (_.GetInt("input-size", 0) ? _.GetInt("input-size", 0) : inSize) +
   sizeof(RawArray::value_type) + sizeof(RawArray::value_type),
  (_.GetInt("output-size", 0) ? _.GetInt("output-size", 0) : outSize) +
   sizeof(RawArray::value_type) + sizeof(RawArray::value_type)),
 __inputSize(_.GetInt("input-size", 0) ? _.GetInt("input-size", 0) : inSize),
 __outputSize(_.GetInt("output-size", 0) ? _.GetInt("output-size", 0) : outSize),
 inToken(_.GetInt("input-token", 0)),
 outToken(_.GetInt("output-token", 0)),
 inChecksumPos(__inputSize + sizeof(RawArray::value_type)),
 outChecksumPos(__outputSize + sizeof(RawArray::value_type)),
 inUsefulSize(__inputSize), outUsefulSize(__outputSize),
 UseCheckSum(_.GetBool("use-checksum", true)),
 checksum_type(static_cast<CheckSumType>(_.GetEnum("checksum-type", _checksum_tab, CS_XOR))) {
 input_buffer.resize(InSize());
 output_buffer.resize(OutSize());
 output_buffer[0] = outToken;
}

bool ComSimple::Acceptable(const RawArray& _) const {
 return
  _.size() >= InSize() && ((inToken != 0) ? (_[0] == inToken) : true);
}

bool ComSimple::ChecksumTest() const {
 unsigned int EvaluatedChecksum = 0;
 if (checksum_type == CS_XOR) {
  EvaluatedChecksum = ChecksumEvalXor(input_buffer.begin(), inChecksumPos);
 } else if (checksum_type == CS_SUM) {
  EvaluatedChecksum = ChecksumEvalSum(input_buffer.begin(), inChecksumPos);
 }
 if (checkSumDump) {
  std::cout <<
   Type() << ": checksum pos: " << std::dec << inChecksumPos << "\tinput buffer: ";
  ExchangeDevice::DumpBuffer(input_buffer.size(), &input_buffer[0]);
  std::cout << "evaluated checksum: " << std::hex << EvaluatedChecksum << "\n";
 }
 return
  ((UseCheckSum && inChecksumPos != 0) ?
   (EvaluatedChecksum == input_buffer[inChecksumPos]) : true);
}

void ComSimple::ChecksumApply() {
 if (outChecksumPos != 0) {
  if (checksum_type == CS_XOR) {
   output_buffer[outChecksumPos] = ChecksumEvalXor(output_buffer.begin(), outChecksumPos);
  } else if (checksum_type == CS_SUM) {
   output_buffer[outChecksumPos] = ChecksumEvalSum(output_buffer.begin(), outChecksumPos);
  }
 }
}

void ComSimple::GetData(size_t size, unsigned char data[]) {
 assert(size >= inUsefulSize);
 std::copy(&input_buffer[sizeof(RawArray::value_type)], &input_buffer[inChecksumPos], data);
}

void ComSimple::PutData(size_t size, const unsigned char data[]) {
 assert(size == outUsefulSize);
 // copy body
 std::copy(data, data + size, &output_buffer[sizeof(RawArray::value_type)]);
}

ComYetMoreSimple::ComYetMoreSimple(
 const QString& owner, const PropertyBag& _, size_t inSize, size_t outSize) :
 Protocol(
  "ComYetMoreSimple", "Dumb byte-oriented protocol with 7-bit data bytes without checksum", owner, _,
  inSize + sizeof(RawArray::value_type), outSize + sizeof(RawArray::value_type)),
  inToken(_.GetInt("input-token", 0)), outToken(_.GetInt("output-token", 0)),
 inUsefulSize(inSize), outUsefulSize(outSize) {
 input_buffer.resize(InSize());
 output_buffer.resize(OutSize());
 output_buffer[0] = outToken;
}

bool ComYetMoreSimple::Acceptable(const RawArray& _) const {
 return _.size() >= InSize() && ((inToken != 0) ? (_[0] == inToken) : true);
}

void ComYetMoreSimple::GetData(size_t size, unsigned char data[]) {
 assert(size == inUsefulSize);
 std::copy(&input_buffer[sizeof(RawArray::value_type)], &input_buffer[InSize()], data);
}

void ComYetMoreSimple::PutData(size_t size, const unsigned char data[]) {
 assert(size == outUsefulSize);
 // copy body
 std::copy(data, data + size, &output_buffer[sizeof(RawArray::value_type)]);
}

Comp_Comp::Comp_Comp(const QString& owner, const PropertyBag& _, size_t inSize, size_t outSize) :
 Protocol("COMP1-COMP2", "UDP-oriented protocol with header and body", owner, _, inSize, outSize) {}

bool Comp_Comp::Acceptable(const RawArray& outer) const {
 return outer.size() >= InSize();
}

bool Comp_Comp::ChecksumTest() const {
 return true;
}

void Comp_Comp::ChecksumApply() {}

void Comp_Comp::GetData(size_t size, unsigned char data[]) {
 assert(size == input_buffer.size());
 std::copy(input_buffer.begin(), input_buffer.end(), data);
}

void Comp_Comp::PutData(size_t size, const unsigned char data[]) {
 assert(size == output_buffer.size());
 // copy body
 std::copy(data, data + size, output_buffer.begin());
 // make header
}
