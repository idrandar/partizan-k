#ifndef MODULE_H_
#define MODULE_H_

// generic module class
// (C) I.Drandar, 2010-2014, all rights reserved
// (C) I.Drandar, for C++1x + Qt, 2018-2020, all rights reserved

#include "common/ExchDevice.h"
#include <semaphore.h>
#include "common/ctime.h"

// ModuleThread initialization bag
class ThreadInit {
 QString name; // name of the thread
 int priority; // priority of the thread
 bool dump; // thread-level dump
 bool disable; // can be disabled
public:
 ThreadInit(const char Name[] = "", int Priority = 1, bool Debug = false, bool disable = false) :
  name(Name), priority(Priority), dump(Debug), disable(disable) {}
 const QString& Name() const { return name; }
 int Priority() const { return priority; }
 bool Dump() const { return dump; }
 bool Disable() const { return disable; }
};

typedef StringKeyMap<ThreadInit> ThreadInitTable;

struct ModuleInit;
using ModuleInitMap = StringKeyMap<ModuleInit>;
// This class is used for initialization of class Module and its descendants
// by passing it as a constructor parameter.
// In its turn ModuleInit itself must be initialized by parameters read from the setup file
class ModuleInit {
 // Name of the module. Used in diagnostic messages
 QString name;
 // this field binds inner frequency of module to system frequency:
 double frequency;
 bool debug; // generic dump
 unsigned int receptionfaultmax; // count of packages needed for exchange fault condition raise
 double countershift;
 ThreadInitTable threads;
 DeviceInit device;  // for ExchangeModule only
 ProtocolInit protocol; // used with device
 ConstTable constants; // list of user-tuned real constants
 ModuleInitMap submodules;
public:
 ModuleInit(
  const char Name[], double frequency,
  bool Debug = false, unsigned int ReceptionFaultMax = 12, double CounterShift = 0.0,
  const ConstTable& Constants = ConstTable(),
  const ThreadInitTable& Threads = ThreadInitTable(),
  const DeviceInit& Device = DeviceInit(), const ProtocolInit& Protocol = ProtocolInit(),
  const ModuleInitMap submodules = ModuleInitMap()) :
  name(Name), frequency(frequency), debug(Debug), receptionfaultmax(ReceptionFaultMax),
  countershift(CounterShift), threads(Threads), device(Device), protocol(Protocol),
  constants(Constants), submodules(submodules) {}
 const QString& Name() const { return name; }
 double Frequency() const { return frequency; }
 bool Dump() const { return debug; }
 unsigned int ReceptionFaultMax() const { return receptionfaultmax; }
 double CounterShift() const { return countershift; }
 Compound GetConst(const QString& Category, const QString& Name) const;
 QList<ConstCategoryTable> GetAll(const QString& Category) const;
 const DeviceInit& Port() const { return device; }
 const ProtocolInit& Protocol() const { return protocol; }
 const ThreadInit& Thread(const QString& Name) const;
 const ModuleInitMap& SubModules() const { return submodules; }
 const ModuleInit& SubModule(const std::string&) const;
 ModuleInit& operator=(const ModuleInit&) = default;
};

class Module {
protected:
 // Caller::call exit status
 enum status {
  THREAD_OK, // DoRunInner exits normally
  THREAD_FAIL, // DoRunInner finds something wrong
  THREAD_TERMINATE // DoRun must immediately exit, terminating thread func
 };
private:
 // divisor binding "own" frequency to system frequency
 // this divisor in not associated with thread func
 const double frequency;
protected:
 double time_step; // 1 / frequency
 const bool dump; // for generic debug
 // symbolic name of module
 // const char* Name;
 const QString Name;

 // proxy caller class
 // redirects call to certain method in owner class
 // owner class must create a descendant class
 class Caller {
 protected:
  Module& owner; // pointer to owner
 public:
  explicit Caller(Module& m) : owner(m) {}
  virtual ~Caller() {}
  Module& Owner() { return owner; }
  virtual status call() = 0; // function to be overridden in descendant class
                             // redirects call to a thread's  work routine
 };

 // class of the thread object
 class Thread : public ::Thread {
 public:
  class lock {
   pthread_mutex_t& mutex;
  public:
   explicit lock(Thread& _) : mutex(_.mutex) { pthread_mutex_lock(&mutex); }
   ~lock() { pthread_mutex_unlock(&mutex); }
  };
  // exception-safe wrapper class for mutex
  Thread(const ThreadInit&, Caller*);
  virtual ~Thread();
  void Start();
  void Stop();
//  void Send(const char* Message, unsigned int Delay=0);
  void Lock();   // lock mutex
  void Unlock(); // unlock mutex
  void Wait();
  void WakeUp();
  bool ShouldBeDumped() const { return dump; }
  bool IsRunning() const { return RunningFlag; }
 private:
  const QString _Name; // for diagnostic messages
  pthread_mutex_t mutex;
  sem_t wait_sem;
  bool RunningFlag; // controls threads' living cycle
  Caller* caller;
  const bool dump; // controls diagnostic messages
  const bool disable; // disables thread
  // overridden thread func
  void* DoRun(void* arg);
  const QString& GetName() const { return _Name; }
 };
protected:
 // counter used in own frequency calculation
 CTime counter;
 double _counter;
public:
 explicit Module(const ModuleInit&);
 virtual ~Module();
 virtual void Start(); // starts threads
 virtual void Stop(); // terminates threads
 bool HaveTick(); // returns true if inner counter had reached to next tick
 bool HaveTick(double); // returns true if inner counter had reached to next tick
 double Frequency() const { return frequency; }
 double TimeStep() const { return time_step; }
 const QString& GetName() const { return Name; }
};

#define dump_if(Condition) if (Condition) std::cout << GetName() << "::" << __func__ << ": "

#endif /*MODULE_H_*/
