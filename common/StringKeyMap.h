/*
 * LessString.hh
 *
 *  Created on: 04.12.2014
 *      Author: Drandar
 */
// Declaration of map class, using std::string as a key
// (C) I.Drandar, 2014, all rights reserved
// (C) I.Drandar. for Qt, 2018 all rights reserved

#ifndef LESSSTRING_HH_
#define LESSSTRING_HH_

#include <QMap>

template <typename T>
using StringKeyMap = QMap<QString, T>;

#endif /* LESSSTRING_HH_ */
