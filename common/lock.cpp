/*
 * lock.cc
 *
 *  Created on: 23.05.2013
 *      Author: Drandar
 */
// wrappers for mutex locks
// (C) I.Drandar, 2013, all rights reserved

#include "lock.h"

Lock::Lock(pthread_mutex_t& _) : l(_) {
 pthread_mutex_lock(&l);
}

Lock::~Lock() {
 pthread_mutex_unlock(&l);
}

ReadLock::ReadLock(pthread_rwlock_t& _) : l(_) {
 pthread_rwlock_rdlock(&l);
}

ReadLock::~ReadLock() {
 pthread_rwlock_unlock(&l);
}

WriteLock::WriteLock(pthread_rwlock_t& _) : l(_) {
 pthread_rwlock_wrlock(&l);
}

WriteLock::~WriteLock() {
 pthread_rwlock_unlock(&l);
}
