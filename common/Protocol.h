/*
 * Protocol.hh
 *
 *  Created on: 03.12.2014
 *      Author: Drandar
 */
// Exchange protocol description
// (C) I.Drandar, 2014-2016, all rights reserved
// (C) I.Drandar, for Qt, 2018, 2020, all rights reserved

#ifndef PROTOCOL_HH_
#define PROTOCOL_HH_

#include <string>
#include <vector>
#include "common/PropertyBag.h"
#include <semaphore.h>

// Protocol initialization bag
class ProtocolInit {
 QString type; // class of serial port
 PropertyBag properties;
public:
 ProtocolInit(const QString& type = "", const PropertyBag& properties = PropertyBag()) :
  type(type), properties(properties) {}
 const char* Class() const { return type.toLocal8Bit(); }
 const PropertyBag& Properties() const { return properties; }
};

class Protocol { // generic protocol class
public:
 typedef std::vector<unsigned char> RawArray;
private:
 const std::string type; // readable name; used in diagnostics
 const std::string description; // readable description; used in diagnostics
 const QString owner;
 const size_t inSize; // size of input package
 const size_t outSize; // size of output package
 const bool dump;
 const bool responseOnRequest;
 const double responseRequestMax;
 sem_t responseOnRequest_sem;
protected:
 const bool checkSumDump;
 RawArray input_buffer;
 RawArray output_buffer;
 std::atomic<unsigned int> transmitted; // count of transmitted packages (host->channel)
 std::atomic<unsigned int> received; // count of received packages (channel->host)
 static unsigned char ChecksumEvalSum(RawArray::const_iterator, size_t);
 static unsigned char ChecksumEvalXor(RawArray::const_iterator, size_t);
public:
 Protocol(
  const std::string&, const std::string&, const QString&, const PropertyBag&, size_t, size_t);
 virtual ~Protocol();
 const char* Type() const { return type.c_str(); }
 const char* Description() const  { return description.c_str(); }
 const QString& Owner() const { return owner; }
 unsigned int Transmitted() const { return transmitted; }
 unsigned int Received() const { return received; }
 virtual void GetData(size_t size, unsigned char data[]); // fetch data from input buffer
 virtual void PutData(size_t size, const unsigned char data[]); // store data to output buffer
 virtual size_t InSize() const { return inSize; } // expected size of input raw package
 virtual size_t OutSize() const { return outSize; } // evaluated size of buffer for output raw package
 virtual bool Acceptable(const RawArray& outer) const = 0; // test input stream for valid package
 virtual bool ChecksumTest() const = 0; // tests checksum for correctness
 virtual void ChecksumApply() = 0; // applies checksum to output buffer
 virtual void Pull(const RawArray&); // moves raw input package to reception buffer of device
 void FakePull(); // does not pull anything but increments received count
 virtual void Push(RawArray&); // moves raw output package to transmission buffer of device
 virtual void SetTime(double) {}
 void WaitForRequestIfNeeded();
 void RequestAccepted();
 bool ChecksumDump() const { return checkSumDump; }
};

class ComSimple : public Protocol { // serial
 const unsigned char __inputSize;
 const unsigned char __outputSize;
 const unsigned char inToken;
 const unsigned char outToken;
 const size_t inChecksumPos;
 const size_t outChecksumPos;
 const size_t inUsefulSize;
 const size_t outUsefulSize;
 const bool UseCheckSum;
 static struct CHECKSUM_TAB : public PropertyBag::ENUM_TAB {
  CHECKSUM_TAB(); // static constructor
 } _checksum_tab;
 const enum CheckSumType {
  CS_XOR, CS_SUM
 } checksum_type;
public:
 ComSimple(const QString&, const PropertyBag&, size_t inSize, size_t outSize);
 virtual ~ComSimple() {}
 virtual bool Acceptable(const RawArray&) const;
 virtual bool ChecksumTest() const; // tests checksum for correctness
 virtual void ChecksumApply(); // applies checksum to output buffer
 virtual void GetData(size_t size, unsigned char data[]); // fetch data from input buffer
 virtual void PutData(size_t size, const unsigned char data[]); // store data to output buffer
};

class ComYetMoreSimple : public Protocol { // ComSimple without checksum
 const unsigned char inToken;
 const unsigned char outToken;
 const size_t inUsefulSize;
 const size_t outUsefulSize;
public:
 ComYetMoreSimple(const QString&, const PropertyBag&, size_t inSize, size_t outSize);
 virtual ~ComYetMoreSimple() {}
 virtual bool Acceptable(const RawArray&) const;
 virtual bool ChecksumTest() const { return true; } // no checksum!
 virtual void ChecksumApply() {} // no checksum
 virtual void GetData(size_t size, unsigned char data[]); // fetch data from input buffer
 virtual void PutData(size_t size, const unsigned char data[]); // store data to output buffer
};

class Comp_Comp : public Protocol {
public:
 Comp_Comp(const QString&, const PropertyBag&, size_t, size_t);
 virtual ~Comp_Comp() {}
 virtual bool Acceptable(const RawArray&) const;
 virtual bool ChecksumTest() const; // tests checksum for correctness
 virtual void ChecksumApply(); // applies checksum to output buffer
 virtual void GetData(size_t size, unsigned char data[]); // fetch data from input buffer
 virtual void PutData(size_t size, const unsigned char data[]); // store data to output buffer
 virtual void SetTime(double) {}
};

#endif /* PROTOCOL_HH_ */
