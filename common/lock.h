/*
 * lock.hh
 *
 *  Created on: 22.05.2013
 *      Author: Drandar
 */
// wrappers for mutex locks
// (C) I.Drandar, 2013, all rights reserved

#ifndef LOCK_HH_
#define LOCK_HH_

#include "pthread.h"

class Lock { // wrapper for standard mutex lock
 pthread_mutex_t& l;
public:
 Lock(pthread_mutex_t&);
 ~Lock();
};

class ReadLock { // wrapper for read lock
 pthread_rwlock_t& l;
public:
 ReadLock(pthread_rwlock_t&);
 ~ReadLock();
};

class WriteLock { // wrapper for read lock
 pthread_rwlock_t& l;
public:
 WriteLock(pthread_rwlock_t&);
 ~WriteLock();
};

#endif /* LOCK_HH_ */
