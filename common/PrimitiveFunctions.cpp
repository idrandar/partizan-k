/*
 * UtiltyFunctions.cc
 *
 *  Created on: 16.11.2015
 *      Author: Drandar
 */

// set of some primitive functions
// (C). I.Drandar, 2015-2016, all rights reserved
// (C) I.Drandar, for C++1x, 2018-2020, all rights reserved

#include "PrimitiveFunctions.h"
#include "MathConst.h"
#include <random>

double Sinusoid::operator()(double t) const {
 if (Period <= 0)
  return Main;
 else
  return Main + Amplitude * sin(MATH_CONST::M_2PI / Period * (t + TimeShift));
}

double Range::operator()(double t) const {
 if (t < Low) {
  return Low;
 } else if (t > High) {
  return High;
 } else {
  return t;
 }
}

double Modulo::operator()(double t) const {
 auto denom = High - Low;
 if (t > 0) {
  return fmod(t, denom) + Low;
 } else {
  return High - fmod(-t, denom);
 }
}

double Noise::operator()(double) const { // parameter is not used
 std::random_device e;
 std::normal_distribution r(Mean, Dev);
 return r(e);
}

double Integral::operator()(double t) {
 _t.Stop();
 auto dt = _t.Span();
 if (dt < 1) {
  Value += t * dt;
 }
 _t.Start();
 return Value;
}

double Derivative::operator()(double t) const {
 return (f(t) - f(t - dt)) / dt;
}

double Linear::operator()(double t) const {
 return Slope * t + Init;
}

double Power::operator()(double t) const {
 return Factor * pow((t - TimeShift), Exponent) + Init;
}

double Exponent::operator()(double t) const {
 if (Base >= 0) {
  return Factor * pow(Base, (t - TimeShift)) + Shift;
 } else {
  return Factor * pow(Base, round(t - TimeShift)) + Shift;
 }
}

double Logarithm::operator()(double t) const {
 return Factor * log(t - TimeShift) + Shift;
}

double Gauss::operator()(double t) const {
 auto constexpr w_c = 4 * log(2);
 auto T = (t - TimeShift) / HalfWidth;
 return Factor * exp(-(w_c * T * T)) + Shift;
}

double TanH::operator()(double t) const {
 return Factor * tanh((t - TimeShift) * Shrink) + Shift;
}
