/* The author: Victor Poputnikov         */
/* Date of creation: 20 July 2007        */
/* A class for noting time               */
/* IDE Momentics & QNX Neutrino 6.3.0    */
// Modified for "Sarmat" project
// (C) I.Drandar, 2011, 2015 all rights reserved
// (C) I.Drandar, for C++1x, 2018, all rights reserved
#include "ctime.h"
#include <math.h>
#include <iostream>

//Setting variable to 0 and getting cycles per second value
CTime::CTime() :
 start(std::chrono::high_resolution_clock::now()) {
// Start();
 finish = std::chrono::high_resolution_clock::now();
} // end

CTime::~CTime() {
 // to do
}

//Gets current clockcycles and writes it to 'begin'
void CTime::Start() {
 begin = std::chrono::high_resolution_clock::now();
} // end

//Gets current clockcycles and writes it to 'end'
void CTime::Stop() {
 end = std::chrono::high_resolution_clock::now();
} // end

double CTime::Span() const {
 return _duration(end - begin).count();
} // end

double CTime::Gone() const {
 return _duration(clock::now() - begin).count();
}

double CTime::OverallTime() const {
 return _duration(clock::now() - start).count();
}

void CTime::Shift(double Advance) {
// std::cout << __PRETTY_FUNCTION__ << "\t" << Advance << std::endl;
 begin += std::chrono::duration_cast<clock::duration>(std::chrono::duration<double>(Advance));
}

//double CTime::Begin() const {
// return begin / cps;
//}

//double CTime::End() const {
// return end / cps;
//}

const CTime::time_point CTime::ProgramStart = clock::now();

double CTime::TimeFromProgramStart() {
 return _duration(clock::now() - ProgramStart).count();
}

double GlobalTime() {
 timespec ts;
 if (clock_gettime(CLOCK_REALTIME, &ts) == 0) {
  return ts.tv_sec + ts.tv_nsec / 1000000000.0;
 } else {
  return NAN;
 }
}
