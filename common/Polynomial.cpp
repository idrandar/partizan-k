/*
 * Polynomial.cc
 *
 *  Created on: 24.02.2015
 *      Author: Drandar
 */

#include "Polynomial.h"

class Horner { // helper class
 const double x;
public:
 Horner(double _) : x(_) {}
 double operator()(double val, double a) {
  return val * x + a; // Horner's scheme iteration
 }
};

double EvaluatePolynomial(const double Coeffs[], double x, size_t Degree) {
 //  conting value of polinomial
 //  dg       -  degree of polynomial
 // pn       -  pointer to an array  coefficients of  polynomial
 //  x        -  value argument of polynomial
 //  retuns counted value of polynomial
 //  the value of  of polynomial counts in the form:
 //  y(x) = pn[0] + pn[1]*x + pn[2]*x*x +  ... + pn[dg] *x^dg
 //  counting is perfomed by Gorner's algorithm


 std::vector<double> _(&Coeffs[0], &Coeffs[Degree + 1]);
 return std::accumulate(_.rbegin(), _.rend(), 0.0, Horner(x));
 // std::vector<double> _(&Coeffs[0], &Coeffs[Degree]);
 // return std::accumulate(_.rbegin(), _.rend(), Coeffs[Degree + 1], Horner(x));
 /* int i;
  double y;

  y = pn[dg];
  for (i = dg - 1; i >= 0; i--)
  y = y * x + pn[i];

  return y;*/
}

class FormalDerivative: std::unary_function<double, double> { // helper class
 size_t i;
public:
 FormalDerivative() : i() {}
 double operator()(double _) {
  return ++i * _;
 }
};

void EvaluatePolynomialFormalDerivative(const double Input[], size_t Degree, double Output[]) {
 //  conting array coeffitients of derivative polinomial
 //  dg       -  degree of polynomial
 // pn       -  pointer to an array  coefficients of polynomial
 //  dpn      -  pointer to counted array  coefficients of derivative polynomial
 if (Degree > 0) {
  std::transform(&Input[1], &Input[Degree + 1], Output, FormalDerivative());
 } else
  Output[0] = 0;
}

void EvaluateOrdinalLeastSquaresPolynomial(const double vt[], const double vy[], size_t n, const InverseMatrix& im,
 double pn[4]) {
 //  counting by least-squares method fitting polynom of degree 3
 // for measured values of paramiter
 //  n          - number samples (or number measurements) of fitting paramiter
 //  vt         - pointer to an array of time samples corresponding to measurements
 //  vy         - pointer to an array of measured values paramiter
 //  invm[4][4] - inverse matrix of corresponding system linear equations
 //  pn[4]      - array of coeffitient counted polinon -
 //  for calculation value of paramiter in the form:
 //  p(t) = pn[0] + pn[1]*t + pn[2]*t*t + pn[3]*t*t*t
 // ( inverse sequence order coeffitients to accepted in MATLAB)
 const size_t Degree = im.size();
 double g[Degree];
 std::fill(g, g + Degree, 0);

 for (size_t i = 0; i < n; ++i) {
  const double t = vt[i];
  double y[Degree];
  y[0] = vy[i];
  for (size_t j = 1; j < Degree; ++j) {
   y[j] = y[j - 1] * t;
  }
  std::transform(y, y + Degree, g, g, std::plus<double>());
 }

 for (size_t i = 0; i < Degree; ++i) {
  pn[i] = std::inner_product(&im[i][0], &im[i][Degree], g, 0.0);
 }
}

void InverseMatrix::Evaluate(const double Input[], size_t n) {
 class Calculator {
 public:
  virtual void operator()(const double[], size_t, InverseMatrix&) const {}
 };
 class CalcIM1: public Calculator {
  virtual void operator()(const double[], size_t n, InverseMatrix& im) const {
   const double s0 = static_cast<double> (n);
   const double id = 1.0 / s0;
   im[0][0] = id;
  }
 };
 class CalcIM2: public Calculator {
  virtual void operator()(const double Input[], size_t n, InverseMatrix& im) const {
   // counting inverse matrix for special matrix
   //       | s0 s1 |
   //       | s1 s2 |
   // where  s0=sum(vt^0)=n, s1=sum(vt^1), s2=sum(vt^2), vt[n] - array time samples
   const double s0 = static_cast<double> (n);
   double s1 = 0;
   double s2 = 0;
   for (size_t i = 0; i < n; i++) {
    const double t1 = Input[i];
    const double t2 = t1 * t1;
    s1 += t1;
    s2 += t2;
   }

   const double id = 1.0 / (s0 * s2 - s1 * s1);
   im[0][0] = id * s2;
   im[0][1] = -id * s1;
   im[1][0] = im[0][1];
   im[1][1] = id * s0;
  }
 };
 class CalcIM3: public Calculator {
  virtual void operator()(const double Input[], size_t n, InverseMatrix& im) const {
   // counting inverse matrix for special matrix
   //       | s0 s1 s2 |
   //       | s1 s2 s3 |
   //       | s2 s3 s4 |,
   // where  s0=sum(vt^0)=n, s1=sum(vt^1), s2=sum(vt^2),s3=sum(vt^3),s4=sum(vt^4),
   // vt1[n] - array of time samples

   //double                   Ver_c11,Ver_c12,Ver_c13,Ver_c22,Ver_c23,Ver_c33,Ver_Norm;

   double s0 = static_cast<double> (n);
   double s1 = 0.0;
   double s2 = 0.0;
   double s3 = 0.0;
   double s4 = 0.0;
   for (size_t i = 0; i < n; i++) {
    const double t1 = Input[i];
    const double t2 = t1 * t1;
    s1 += t1;
    s2 += t2;
    s3 += t1 * t2;
    s4 += t2 * t2;
   }

   const double InverseDeterminant =
    1.0 / (s0 * s2 * s4 - s0 * s3 * s3 - s1 * s1 * s4 + 2.0 * s1 * s2 * s3 - s2 * s2 * s2);

   im[0][0] = InverseDeterminant * (s2 * s4 - s3 * s3);
   im[0][1] = InverseDeterminant * (s2 * s3 - s1 * s4);
   im[0][2] = InverseDeterminant * (s1 * s3 - s2 * s2);
   im[1][0] = im[0][1];
   im[1][1] = InverseDeterminant * (s0 * s4 - s2 * s2);
   im[1][2] = InverseDeterminant * (s1 * s2 - s0 * s3);
   im[2][0] = im[0][2];
   im[2][1] = im[1][2];
   im[2][2] = InverseDeterminant * (s0 * s2 - s1 * s1);
  }
  /*
   Ver_c11=s0*im[0][0]+s1*im[1][0]+s2*im[2][0];
   Ver_c12=s0*im[0][1]+s1*im[1][1]+s2*im[2][1];
   Ver_c13=s0*im[0][2]+s1*im[1][2]+s2*im[2][2];
   Ver_c22=s1*im[0][1]+s2*im[1][1]+s3*im[2][1];
   Ver_c23=s1*im[0][2]+s2*im[1][2]+s3*im[2][2];
   Ver_c33=s2*im[0][2]+s3*im[1][2]+s4*im[2][2];

   Ver_Norm=fabs(Ver_c11-1.0)+fabs(Ver_c12)+fabs(Ver_c13)+
   fabs(Ver_c22-1.0)+fabs(Ver_c23)+fabs(Ver_c33-1.0);

   if  (Ver_Norm>0.0001)
   {
   printf("<CountInv_SpMt3>  CallFun = %s n = %3d   Ver_Norm = %10.8f\n",&CallFun[0],n,Ver_Norm);
   for (i = 0; i < n; i++)
   {
   printf("<CountInv_SpMt3>  i = %3d  vt[i] = %8.6f\n", i, vt1[i]);
   }
   //abort();
   }
   */
 };
 class CalcIM4: public Calculator {
  virtual void operator()(const double Input[], size_t n, InverseMatrix& im) const {
   // counting inverse matrix for special matrix
   //       | s0 s1 s2 s3 |
   //       | s1 s2 s3 s4 |
   //       | s2 s3 s4 s5 |
   //       | s3 s4 s5 s6 |
   // where s0=sum(vt^0)=n,  s1=sum(vt^1), s2=sum(vt^2),s3=sum(vt^3),s4=sum(vt^4),s5=sum(vt^5),s6=sum(vt^6),
   // vt[n] -array of time samples
   long double s0 = static_cast<long double> (n);
   long double s1 = 0.0;
   long double s2 = 0.0;
   long double s3 = 0.0;
   long double s4 = 0.0;
   long double s5 = 0.0;
   long double s6 = 0.0;
   for (size_t i = 0; i < n; i++) {
    const long double t1 = Input[i];
    const long double t2 = t1 * t1;
    const long double t3 = t1 * t2;
    s1 += t1;
    s2 += t2;
    s3 += t3;
    s4 += t2 * t2;
    s5 += t2 * t3;
    s6 += t3 * t3;
   }

   const long double A11 = s2 * s4 * s6 - s2 * s5 * s5 - s3 * s3 * s6 + 2.0 * s3 * s4 * s5 - s4 * s4 * s4;
   const long double A22 = s0 * s4 * s6 - s0 * s5 * s5 - s2 * s2 * s6 + 2.0 * s2 * s3 * s5 - s3 * s3 * s4;
   const long double A33 = s0 * s2 * s6 - s0 * s4 * s4 - s1 * s1 * s6 + 2.0 * s1 * s3 * s4 - s2 * s3 * s3;
   const long double A44 = s0 * s2 * s4 - s0 * s3 * s3 - s1 * s1 * s4 + 2.0 * s1 * s2 * s3 - s2 * s2 * s2;

   const long double A12 = s1 * s4 * s6 - s1 * s5 * s5 - s2 * s3 * s6 + s3 * s3 * s5 + s2 * s4 * s5 - s3 * s4 * s4;
   const long double A13 = s1 * s3 * s6 - s1 * s4 * s5 - s2 * s2 * s6 + s2 * s3 * s5 + s2 * s4 * s4 - s3 * s3 * s4;
   const long double A14 = s1 * s3 * s5 - s1 * s4 * s4 - s2 * s2 * s5 + 2.0 * s2 * s3 * s4 - s3 * s3 * s3;

   const long double A23 = s0 * s3 * s6 - s0 * s4 * s5 - s1 * s2 * s6 + s1 * s3 * s5 + s2 * s3 * s4 - s3 * s3 * s3;
   const long double A24 = s0 * s3 * s5 - s0 * s4 * s4 - s1 * s2 * s5 + s1 * s3 * s4 + s2 * s2 * s4 - s2 * s3 * s3;
   const long double A34 = s0 * s2 * s5 - s0 * s3 * s4 - s1 * s1 * s5 + s1 * s3 * s3 + s1 * s2 * s4 - s2 * s2 * s3;

   const long double det = s0 * A11 - s1 * A12 + s2 * A13 - s3 * A14;
   const long double id = 1.0 / det;

   //printf("s1 = %12.10Lf  s2 = %12.10Lf s3 = %12.10Lf  s4 = %12.10Lf s5 = %12.10Lf s6 = %12.10Lf\n",s1,s2,s3,s4,s5,s6);
   //printf("A11 = %12.10Lf A12 = %12.10Lf A13 = %12.10Lf  A14 = %12.10Lf\n",id*A11,id*A12,id*A13,id*A14);
   //printf("determinant matrix 4x4 = %12.10Lf\n",det);

   im[0][0] = A11 * id;
   im[0][1] = -A12 * id;
   im[0][2] = A13 * id;
   im[0][3] = -A14 * id;
   im[1][0] = -A12 * id;
   im[1][1] = A22 * id;
   im[1][2] = -A23 * id;
   im[1][3] = A24 * id;
   im[2][0] = A13 * id;
   im[2][1] = -A23 * id;
   im[2][2] = A33 * id;
   im[2][3] = -A34 * id;
   im[3][0] = -A14 * id;
   im[3][1] = A24 * id;
   im[3][2] = -A34 * id;
   im[3][3] = A44 * id;

   //printf("b00 = %12.8f   b01 = %12.8f    b02=%12.8f\n",invm[0][0],invm[0][1],invm[0][2]);
  }
 };
 const size_t Degree = (size() > 0) ? (size() - 1) : 0;
 static CalcIM1 cm1;
 static CalcIM2 cm2;
 static CalcIM3 cm3;
 static CalcIM4 cm4;
 Calculator* Calc[] = { &cm1, &cm2, &cm3, &cm4 };
 (*Calc[Degree])(Input, n, *this);
}

