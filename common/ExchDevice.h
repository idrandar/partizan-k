/*
 * ExchDevice.hh
 *
 *  Created on: 23.04.2013
 *      Author: Drandar
 */
// exchange device description
// (C) I.Drandar, 2013-2016, all rights reserved
// (C) I.Drandar, for C++1x + Qt, 2018, all rights reserved

#ifndef EXCHDEVICE_HH_
#define EXCHDEVICE_HH_

#include "cmp_ports.h"
#include "Sockets.h"
#include <pthread.h> // pthread_rwlock_t
#include <list>
#include "control/thread.h" // Thread
#include "common/StringKeyMap.h"
#include "common/Protocol.h"

// Port initialization bag
class DeviceInit {
 QString name; // name of serial port
 QString type; // class of serial port
 PropertyBag properties;
public:
 DeviceInit(const QString& Name = "", const QString& type = "",
  const PropertyBag& properties = PropertyBag()) :
  name(Name), type(type), properties(properties) {}
 const QString& Name() const { return name; }
 const QString& Class() const { return type; }
 const PropertyBag& Properties() const { return properties; }
};

class ExchangeDevice { // generic exchange device class
 const QString name;
 static struct DevicePool : public StringKeyMap<ExchangeDevice*> {
  ~DevicePool();
 } pool;
protected:
 const bool debuginput; // issue diagnostic messages for input
 const bool debugoutput; // issue diagnostic messages for output
 const bool debugtime;
 const size_t max_dump_bytes;
 unsigned int received; // total count of received bytes
 unsigned int transmitted; // total count of transmitted bytes
 unsigned int readings; // total count of readings
 unsigned int writings; // total count of writings
 pthread_mutex_t write_mutex;
 struct MessageQueue {
  typedef std::list<Protocol::RawArray> Queue;
  pthread_mutex_t mutex;
  pthread_cond_t cond;
  Queue queue;
  static constexpr double wait_max = -0.1;
  void clear();
 public:
  MessageQueue();
  ~MessageQueue();
  bool get(Protocol::RawArray&);
  void put(const Protocol::RawArray&);
  size_t count() const { return queue.size(); }
 };
 class MessageQueueMap {
  typedef std::map<const Protocol*, MessageQueue*> Impl;
  Impl _impl;
  mutable pthread_rwlock_t guard;
 public:
  MessageQueueMap();
  ~MessageQueueMap();
  void clear();
  void insert(const Protocol*); // insert a new MessageQueue
  bool present(const Protocol*) const;
  const Protocol* accepted(const Socket::buftype&);
  MessageQueue* get(const Protocol*);
  size_t count() const { return _impl.size(); }
 };
 MessageQueueMap messages;
 size_t max_out;
 size_t max_in;
 std::atomic<bool> stop;
public:
 ExchangeDevice(const PropertyBag&);
 virtual ~ExchangeDevice();
 const QString& Name() const { return name; }
 virtual void Open() = 0;
 virtual void Close() = 0;
 virtual bool IsOpen() const = 0;
 virtual bool Write(Protocol*) = 0;
 virtual bool Read(Protocol*) = 0;
 virtual bool Register(const Protocol*) = 0;
 virtual bool Registered(const Protocol*) const = 0;
 static ExchangeDevice* Item(const DeviceInit&);
 void DumpBuffer(const Protocol::RawArray&);
 static void DumpBuffer(const size_t, const unsigned char[]);
};

class RsDevice : public ExchangeDevice, public Thread {
 int port; //  port handle
 // port settings
 const QString serial;
 const int bps; // bits per second
 const ParityType par; // type of Parity
 const int stopbits;
 const int time_out;
 const int bufsize_factor;
 const bool dump_input_buffer;
 static struct PARITY_TAB : public PropertyBag::ENUM_TAB { // static constructor
  PARITY_TAB();
 } _parity_tab;
 enum { //
  INVALID_PORT = -1,
  WRITE_ERROR = -1,
  READ_ERROR = -1
 };
 Protocol::RawArray in_buf;
 Protocol::RawArray out_buf;
 size_t min_in;
 unsigned int total_rejected;
public:
 RsDevice(const PropertyBag&);
 virtual ~RsDevice();
 virtual void Open();
 void Close() override;
 virtual bool IsOpen() const;
 virtual bool Write(Protocol*);
 virtual bool Read(Protocol*);
 virtual bool Register(const Protocol*);
 virtual bool Registered(const Protocol*) const;
 virtual void* DoRun(void*);
};

class NetDevice : public ExchangeDevice, public Thread {
 Socket s;
 // remote socket address
 SocketAddress remote;
 SocketAddress host;
public:
 NetDevice(const PropertyBag&);
 virtual ~NetDevice();
 virtual void Open();
 virtual void Close();
 virtual bool IsOpen() const;
 virtual bool Write(Protocol*);
 virtual bool Read(Protocol*);
 virtual bool Register(const Protocol*);
 virtual bool Registered(const Protocol*) const;
 virtual void* DoRun(void*);
};

#endif /* EXCHDEVICE_HH_ */
