/*
 * ExchangeModule.hh
 *
 *  Created on: 22.01.2013
 *      Author: Drandar
 */
// exchange module class
// (C) I.Drandar, 2013-2014, 2016, all rights reserved
// (C) I.Drandar, for C++1x, 2019, all rights reserved

#ifndef EXCHANGEMODULE_HH_
#define EXCHANGEMODULE_HH_

#include "common/module.h"
//#include "ExchDevice.hh"
//#include "control/system.hh"
#include "common/data_convert.h"
//#include "Protocol.hh"

class ExchangeModule : public Module {
protected:
 ExchangeDevice* device; // exchange device
 Protocol* pr;
 const unsigned int ReceptionFaultMax;
 std::atomic<bool> GotPackage; // have got fresh data from serial port
 std::atomic<bool> SendedPackage; // just sent data by serial port
 std::atomic<unsigned int> TransmissionErrors; // count of failures detected during transmission
 std::atomic<unsigned int> ExchangeFaultsCount; // count of consequent faults during reception
 std::atomic<unsigned int> ReceptionFaults; // total count of faults during reception
 std::atomic<unsigned int> RejectedCount; // count of rejected packets (checksum faults)
 std::atomic<unsigned int> NoReceptionCount; // count of consequent periods when no packet was admitted
 static constexpr unsigned int EXCHANGE_FAULTS_MAX = 12;
 Module::Thread receiver;
 Module::Thread sender;
 void CommitReception(); // copies received data into internal status vector
 void Transmit();  // transmits data from status vector

private:
 enum { READ_FAIL_MAX = 10000 };
 class ReceiverCaller;
 class SenderCaller;
 status DoRead(); // overridden; performs reading
 virtual bool GetPacket() = 0; // depends on packet's format
 virtual void DecodePacket(bool) = 0; // depends on packet's format
 virtual void EncodePacket(bool) = 0; // depends on packet's format
 virtual void DoCommitReception(bool) = 0; // performs implementation dependent part of CommitReception
 virtual void PrepareTransmission() = 0;
 virtual bool DoTransmit() = 0;
 virtual void PrepareStatusVector(bool) = 0;
 virtual void ProcessCommandVector() = 0;
 virtual void PostProcessStatusVector() = 0; // final processing of data not tied to actual reception
 status DoWrite();
public:
 explicit ExchangeModule(const ModuleInit&);
 virtual ~ExchangeModule();
 void Start() override; // starts thread, overridden because second thread is added
 void Stop() override;  // terminates thread, overridden because second thread is added
 bool IsOpen() const {
  if (device != NULL)
   return device->IsOpen();
  else
   return false;
 }
 virtual void Act() = 0; // perform some action
};

class NOTHING {}; // used for unimplemented structures

template <>
class RandomAccess<NOTHING> { // partial specialization
public:
 size_t size() const { return 0; }
 unsigned char operator[](size_t) const { return 0; } // stub
 unsigned char& operator[](size_t) { // stub
  static unsigned char _ = 0; // stub variable
  return _;
 }
 static size_t ChecksumPos() { return 0; } // stub
 size_t Checksum1Pos() const { return 0; } // stub
 static const size_t CHECKSUM1POS = 0;
 static const size_t TOKEN1POS = 0;
};

// proxy helper class
// provides standard implementation of some virtual
// (or even non-virtual) functions, defined in ExchangeModule
template <typename T>
class ExchangeModuleImpl : public ExchangeModule {
protected:
 // aliases
 typedef ExchangeModuleImpl<T> BASE;
 typedef T Types;

 // public types
public:
 using InputPacket = typename T::InputPacket;
 using InData = typename T::OutputData;
 using OutputPacket = typename T::OutputPacket;
 using OutData = typename T::InputData;
 struct CommandVectorBillet {
  typedef CommandVectorBillet BILLET;
  InData data;
  CommandVectorBillet() : data() {}
 };
 struct CommandVector : public CommandVectorBillet, public T::CommandVector {};
 struct StatusVectorBillet {
  typedef StatusVectorBillet BILLET;
  OutData data;
  bool ExchangeFault;
  bool NewInfo;
  void Reset() { data = OutData(); }
  StatusVectorBillet() : data(), ExchangeFault(), NewInfo() {}
 };
 struct StatusVector : public StatusVectorBillet, public T::StatusVector {};
protected:
 // mandatory members
 // data controlling behavior
 CommandVector Command;
 // data indicating state
 StatusVector Status;
 // raw data received from serial port
 RandomAccess<InputPacket> input;
 // raw data transmitted via serial port
 RandomAccess<OutputPacket> output;
 // high-level data received from serial port
 OutData Input;
 // high-level data transmitted via serial port
 InData Output;

 // constructors/destructor
 explicit ExchangeModuleImpl<T>(const ModuleInit& _) :
  ExchangeModule(_), Command(), Status(), input(), output(), Input(), Output() {
  pr = ProtocolCreate(_.Protocol());
 }

 virtual ~ExchangeModuleImpl<T>() {
  delete pr;
 }

 // receive raw data
 virtual bool GetPacket() {
  if (std::is_same<InputPacket, NOTHING>()) return false;
  else {
   if (device->Read(pr)) {
    pr->GetData(input.size(), &input[0]);
    return true;
   } else return false;
  }
 }
 // transmit raw data
 virtual bool DoTransmit() {
  if (std::is_same<OutputPacket, NOTHING>()) return false;
  else {
   pr->PutData(output.size(), &output[0]);
//   pr->SetTime(0); // TODO: set real time value
   pr->ChecksumApply();
   if (device != NULL)
    return device->Write(pr);
   else
    return false;
  }
 }

 // copy data from Command vector to output high-level packet
 virtual void PrepareTransmission() {
  if (!std::is_same<decltype(Output), NOTHING>())
   Output = Command.data;
 }

 void ProcessCommandVector() override {}
 void PrepareStatusVector(bool) override {}
 void PostProcessStatusVector() override {}

 // copy data from just received high-level packet to Status vector
 virtual void DoCommitReception(bool Success) {
  receiver.Lock();
  if (!std::is_same<decltype(Input), NOTHING>()) {
   if (Success) {
    Status.data = Input;
   } else Status.Reset();
   Status.ExchangeFault = !Success;
   Status.NewInfo = Success;
  }
  PrepareStatusVector(Success);
  receiver.Unlock();
 }

 Protocol* ProtocolCreate(const ProtocolInit& _) const {
  const std::string type = _.Class();
  if (type.compare("ComSimple") == 0)
   return new ComSimple(GetName(), _.Properties(), input.size(), output.size());
  if (type.compare("ComYetMoreSimple") == 0)
   return new ComYetMoreSimple(GetName(), _.Properties(), input.size(), output.size());
  if (type.compare("COMP-COMP") == 0)
   return new Comp_Comp(GetName(), _.Properties(), input.size(), output.size());
  return NULL; // default
 }

 // public members
public:
 // provides command vector assignment; non-virtual!
 void SetCommandVector(const CommandVector& _) { Command = _; }
 // extracts status data; non-virtual!
 const StatusVector& GetStatusVector() const { return Status; }

 void Interact(const CommandVector& c, StatusVector& s) {
  SetCommandVector(c);
  Act();
  s = GetStatusVector();
 }

 unsigned int Received() const { return pr->Received(); }
 unsigned int ReceptionFaults() const { return ExchangeModule::ReceptionFaults; }
 unsigned int Transmitted() const { return pr->Transmitted(); }
};

#endif /* EXCHANGEMODULE_HH_ */
