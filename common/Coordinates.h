/*
 * Coordinates.hh
 *
 *  Created on: 23.01.2015
 *      Author: Drandar
 */

#ifndef COORDINATES_HH_
#define COORDINATES_HH_

// types representing different coordinates
// (C) I.Drandar, for C++1x, 2018-2020, all rights reserved
#include <stddef.h>

template <typename T = double>
struct RotationsType { // SAnEul
 T Heading; // K
 T Rolling; // Teta
 T Pitch;   // Psi
 template <typename X>
 RotationsType<T>& operator=(const RotationsType<X>& _) { // cross-type assignment
  Heading = _.Heading;
  Rolling = _.Rolling;
  Pitch = _.Pitch;
  return *this;
 }
 template <typename X>
 constexpr operator RotationsType<X>() const { // type conversion
  return { Heading, Rolling, Pitch };
 }
 constexpr bool Empty() {
  return Heading == T() && Rolling == T() && Pitch == T();
 }
};
// binary operators
template <typename X0, typename X1>
constexpr RotationsType<> operator+(const RotationsType<X0>& l, const RotationsType<X1>& r) {
 return { l.Heading + r.Heading, l.Rolling + r.Rolling, l.Pitch + r.Pitch };
}
template <typename X, typename I>
constexpr RotationsType<X> operator*(const RotationsType<X>& l, I r) {
 return { l.Heading * r, l.Rolling * r, l.Pitch * r };
}
template <typename X, typename I>
constexpr RotationsType<X> operator*(I l, const RotationsType<X>& r) {
 return r * l;
}
typedef RotationsType<> Rotations;

template <typename TA=double, typename TD=double>
struct SphericalCoordsType { // SCrSph
 TD Distance;  // Ds
 TA Bearing;   // q
 TA Elevation; // eps
 template <typename XA, typename XD>
 SphericalCoordsType<TA, TD>& operator=(const SphericalCoordsType<XA, XD>& _) {
  // cross-type assignment
  Distance = _.Distance;
  Bearing = _.Bearing;
  Elevation = _.Elevation;
  return *this;
 }
 template <typename I>
 SphericalCoordsType<TA, TD>& operator*=(I _) {
  Distance *= _;
  Bearing *= _;
  Elevation *= _;
  return *this;
 }
 template <typename XA, typename XD>
 bool operator!=(const SphericalCoordsType<XA, XD>& _) {
  return Distance != _.Distance || Bearing != _.Bearing || Elevation != _.Elevation;
 }
};
typedef SphericalCoordsType<> SphericalCoords;

template <typename T=double>
struct Cartesian3DCoordsType { // SCrDct_3D
 typedef Cartesian3DCoordsType<T> Type;
 T X;
 T Y;
 T Z;
 typedef T Cartesian3DCoordsType::*Coordinate;
 static constexpr Coordinate CoordList[] = {
  &Cartesian3DCoordsType<T>::X,
  &Cartesian3DCoordsType<T>::Y,
  &Cartesian3DCoordsType<T>::Z
 };
 template <typename U>
 Cartesian3DCoordsType<T>& operator=(const Cartesian3DCoordsType<U>& _) { // cross-type assignment
  X = _.X;
  Y = _.Y;
  Z = _.Z;
  return *this;
 }
 template <typename U>
 constexpr operator Cartesian3DCoordsType<U>() const {
  return { X, Y, Z };
 }
 template <typename I>
 Cartesian3DCoordsType<T>& operator+=(I _) {
  X += _;
  Y += _;
  Z += _;
  return *this;
 }
 template <typename U>
 Cartesian3DCoordsType<T>& operator+=(Cartesian3DCoordsType<U> _) {
  X += _.X;
  Y += _.Y;
  Z += _.Z;
  return *this;
 }
 template <typename I>
 Cartesian3DCoordsType<T>& operator*=(I _) {
  X *= _;
  Y *= _;
  Z *= _;
  return *this;
 }
 template <typename I>
 Cartesian3DCoordsType<T>& operator/=(I _) {
  X /= _;
  Y /= _;
  Z /= _;
  return *this;
 }
 void InvertZ() { Z = -Z; }
};
// binary operators
template <typename T>
constexpr Cartesian3DCoordsType<T> operator+(
 const Cartesian3DCoordsType<T>& l, const Cartesian3DCoordsType<T>& r) {
 return { l.X + r.X, l.Y + r.Y, l.Z + r.Z };
}
template <typename T>
constexpr Cartesian3DCoordsType<T> operator-(
 const Cartesian3DCoordsType<T>& l, const Cartesian3DCoordsType<T>& r) {
 return { l.X - r.X, l.Y - r.Y, l.Z - r.Z };
}
template <typename T, typename I> //
constexpr Cartesian3DCoordsType<T> operator*(const Cartesian3DCoordsType<T>& l, I r) {
 return { l.X * r, l.Y * r, l.Z * r };
}
template <typename T, typename I> //
constexpr Cartesian3DCoordsType<T> operator*(I l, const Cartesian3DCoordsType<T>& r) {
 return r * l;
}
template <typename T, typename I> //
constexpr Cartesian3DCoordsType<T> operator/(const Cartesian3DCoordsType<T>& l, I r) {
 return { l.X / r, l.Y / r, l.Z / r };
}
template <typename T> //
constexpr Cartesian3DCoordsType<T> operator*(
 const Cartesian3DCoordsType<T>& l, const Cartesian3DCoordsType<T>& r) {
 return { l.X * r.X, l.Y * r.Y, l.Z * r.Z };
}
typedef Cartesian3DCoordsType<> Cartesian3DCoords;

template <typename T=double>
struct Cartesian2DCoordsType { // double_x_y
 T X;
 T Y;
 template <typename U>
 Cartesian2DCoordsType<T>& operator=(const Cartesian2DCoordsType<U>& _) { // cross-type assignment
  X = _.X;
  Y = _.Y;
  return *this;
 }
 template <typename Z>
 constexpr bool operator!=(const Cartesian2DCoordsType<Z>& r) const {
  return X != r.X || Y != r.Y;
 }
};
typedef Cartesian2DCoordsType<> Cartesian2DCoords;

template <typename T=double>
struct Spherical2DShipCoordsType {
 typedef T CoordType;
 T Heading;   // Q
 T Elevation; // F
 template <typename X>
 Spherical2DShipCoordsType<T>& operator=(const Spherical2DShipCoordsType<X>& _) {
  Heading = _.Heading;
  Elevation = _.Elevation;
  return *this;
 }
 template <typename X>
 Spherical2DShipCoordsType<T>& operator-=(const Spherical2DShipCoordsType<X>& _) {
  Heading -= _.Heading;
  Elevation -= _.Elevation;
  return *this;
 }
};
template <typename T, typename I>
constexpr Spherical2DShipCoordsType<T> operator*(const Spherical2DShipCoordsType<T>& l, I r) {
 return { l.Heading * r, l.Elevation * r };
}
template <typename T, typename I>
constexpr Spherical2DShipCoordsType<T> operator*(I l, const Spherical2DShipCoordsType<T>& r) {
 return r * l;
}
template <typename T>
constexpr Spherical2DShipCoordsType<T> operator-(
 const Spherical2DShipCoordsType<T>& l, const Spherical2DShipCoordsType<T>& r) {
 return { l.Heading - r.Heading, l.Elevation - r.Elevation };
}
template <typename T, typename I>
constexpr Spherical2DShipCoordsType<T> operator-(const Spherical2DShipCoordsType<T>& l, I r) {
 return { l.Heading - r, l.Elevation - r };
}
template <typename T>
constexpr Spherical2DShipCoordsType<T> operator+(
 const Spherical2DShipCoordsType<T>& l, const Spherical2DShipCoordsType<T>& r) {
 return { l.Heading + r.Heading, l.Elevation + r.Elevation };
}
template <typename T, typename I>
constexpr Spherical2DShipCoordsType<T> operator+(const Spherical2DShipCoordsType<T>& l, I r) {
 return { l.Heading + r, l.Elevation + r };
}
template <typename T>
constexpr bool operator!=(
 const Spherical2DShipCoordsType<T>& l, const Spherical2DShipCoordsType<T>& r) {
 return r.Heading != l.Heading || r.Elevation != l.Elevation;
}
typedef Spherical2DShipCoordsType<> Spherical2DShipCoords;

template <typename T=double>
struct Spherical2DGroundCoordsType {
 typedef T CoordType;
 T Bearing;   // A
 T Elevation; // E
 template <typename X>
 Spherical2DGroundCoordsType<T>& operator=(const Spherical2DGroundCoordsType<X>& _) {
  Bearing = _.Bearing;
  Elevation = _.Elevation;
  return *this;
 }
};
template <typename T, typename I>
constexpr Spherical2DGroundCoordsType<T> operator*(const Spherical2DGroundCoordsType<T>& l, I r) {
 return { l.Bearing * r, l.Elevation * r };
}
template <typename T, typename I>
constexpr Spherical2DGroundCoordsType<T> operator*(I l, const Spherical2DGroundCoordsType<T>& r) {
 return r * l;
}
template <typename T>
constexpr Spherical2DGroundCoordsType<T> operator-(
 const Spherical2DGroundCoordsType<T>& l, const Spherical2DGroundCoordsType<T>& r) {
 return { l.Bearing - r.Bearing, l.Elevation - r.Elevation };
}
template <typename T, typename I>
constexpr Spherical2DGroundCoordsType<T> operator-(const Spherical2DGroundCoordsType<T>& l, I r) {
 return { l.Bearing - r, l.Elevation - r };
}
template <typename T>
constexpr Spherical2DGroundCoordsType<T> operator+(
 const Spherical2DGroundCoordsType<T>& l, const Spherical2DGroundCoordsType<T>& r) {
 return { l.Bearing + r.Bearing, l.Elevation + r.Elevation };
}
template <typename T, typename I>
constexpr Spherical2DGroundCoordsType<T> operator+(const Spherical2DGroundCoordsType<T>& l, I r) {
 return  { l.Bearing + r, l.Elevation + r };
}
typedef Spherical2DGroundCoordsType<> Spherical2DGroundCoords;

template <typename T=double>
struct Spherical2DCoordsType {
 T HA;
 T Elev;
 template <typename X>
 Spherical2DCoordsType<T>& operator=(const Spherical2DCoordsType<X>& _) {
  HA = _.HA;
  Elev = _.Elev;
  return *this;
 }
 template <typename I>
 Spherical2DCoordsType<T>& operator*=(I _) {
  HA *= _;
  Elev *= _;
  return *this;
 }
 constexpr T Spherical2DCoordsType::* MappingCoord(
  T Spherical2DGroundCoordsType<T>::* Coord) const {
  if (Coord == &Spherical2DGroundCoordsType<T>::Bearing) return &Spherical2DCoordsType<T>::HA;
  else /* if (Coord == &Spherical2DGroundCoordsType<T>::Elevation) */
   return &Spherical2DCoordsType<T>::Elev;
 }
 constexpr T Spherical2DCoordsType::* MappingCoord(
  T Spherical2DShipCoordsType<T>::* Coord) const {
  if (Coord == &Spherical2DShipCoordsType<T>::Heading) return &Spherical2DCoordsType<T>::HA;
  else /* if (Coord == &Spherical2DShipCoordsType<T>::Elevation) */
   return &Spherical2DCoordsType<T>::Elev;
 }
};
template <typename T, typename I>
constexpr Spherical2DCoordsType<T> operator*(const Spherical2DCoordsType<T>& l, I r) {
 return { l.HA * r, l.Elev * r };
}
template <typename I>
constexpr Spherical2DCoordsType<bool> operator*(const Spherical2DCoordsType<bool>& l, I r) {
 return { l.HA && r, l.Elev && r };
}
template <typename T, typename I>
constexpr Spherical2DCoordsType<T> operator*(I l, const Spherical2DCoordsType<T>& r) {
 return r * l;
}
template <typename T>
constexpr Spherical2DCoordsType<T> operator+(
 const Spherical2DCoordsType<T>& l, const Spherical2DCoordsType<T>& r) {
 return { l.HA + r.HA, l.Elev + r.Elev };
}
template <typename T>
constexpr Spherical2DCoordsType<T> operator-(
 const Spherical2DCoordsType<T>& l, const Spherical2DCoordsType<T>& r) {
 return { l.HA - r.HA, l.Elev - r.Elev };
}
typedef Spherical2DCoordsType<> Spherical2DCoords;

#endif /* COORDINATES_HH_ */
