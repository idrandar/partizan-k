/*
 * Debug.hh
*/
// Testing dump stuff
// (C) I.Drandar, 2011-2015, all rights reserved
// (C) I.Drandar, for Qt + C++1x, 2018-2019, all rights reserved

#ifndef DEBUG_HH_
#define DEBUG_HH_

#include "LRF.h"
#include "GTC.h"
#include "DAT.h"
#include <iostream>
#include "common/Coordinates.h"
#include "common/MathConst.h"

std::ostream& operator<<(std::ostream&, DAT::TargetType);
std::ostream& operator<<(std::ostream&, const Weather&);

template <typename T>
std::ostream& operator<<(std::ostream& _, const Spherical2DCoordsType<T>& C) {
 const Spherical2DCoordsType<T> c = C * MATH_CONST::RAD2DEG;
 _ << "HA: " << c.HA << "\tElev: " << c.Elev;
 return _;
}

template <typename T>
std::ostream& operator<<(std::ostream& _, const Spherical2DGroundCoordsType<T>& C) {
 const Spherical2DGroundCoordsType<T> c = C * MATH_CONST::RAD2DEG;
 _ << "Bearing: " << c.Bearing << "\tElevation: " << c.Elevation;
 return _;
}

template <typename T>
std::ostream& operator<<(std::ostream& _, const Spherical2DShipCoordsType<T>& C) {
 const Spherical2DShipCoordsType<T> c = C * MATH_CONST::RAD2DEG;
 _ << "Heading: " << c.Heading << "\tElevation: " << c.Elevation;
 return _;
}

template <typename T>
std::ostream& operator<<(std::ostream& _, const Cartesian2DCoordsType<T>& C) {
 _ << "X: " << C.X << "\tY: " << C.Y;
 return _;
}

template <typename T>
std::ostream& operator<<(std::ostream& _, const Cartesian3DCoordsType<T>& C) {
 _ << "X: " << C.X << "\tY: " << C.Y << "\tZ: " << C.Z;
 return _;
}

template <typename T>
std::ostream& operator<<(std::ostream& _, const RotationsType<T>& R) {
 const RotationsType<T> r = R * MATH_CONST::RAD2DEG;
 _ << "Heading: " << r.Heading << "\tRolling: " << r.Rolling << "\tPitch: " << r.Pitch;
 return _;
}

template <typename T>
std::ostream& operator<<(std::ostream& _, const SphericalCoordsType<T>& C) {
 _ <<
  "Distance: " << C.Distance << "\t"
  "Bearing: " << C.Bearing * MATH_CONST::RAD2DEG << "\tElevation: " << C.Elevation * MATH_CONST::RAD2DEG;
 return _;
}

template <typename T>
std::ostream& operator<<(std::ostream& _, const RandomAccess<T>& H) {
 _ << std::hex;
 for (size_t i = 0; i < H.size(); ++i) {
  const unsigned int d = H[i];
  _ << ((d < 0x10) ? " 0" : " ") << d;
 }
 _ << std::dec;
 return _;
}

std::ostream& operator<<(std::ostream& _, const QString& s);

#endif /* DEBUG_HH_ */
