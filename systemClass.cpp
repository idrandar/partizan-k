/* The author: Alexander Ketner                       */
/* Date of creation: 25 April 2007                    */
/* The basic set of the systems software              */
/* IDE Momentics & QNX Neutrino 6.3.0                 */
// Modified for "Sarmat" project
// (C) I.Drandar, 2010-2011, 2018-2019, all rights reserved
// (C) I.Drandar, for Linux + C++1x, all rights reserved

#include "systemClass.h"
#include "control/exception.h"
#include <unistd.h>
#include <fstream>
#include <regex>
#include <sys/stat.h>
#include <fcntl.h>

constexpr int IS_FAILURE = -1;

TSystemClass::TSystemClass() {
 get_host_info();
 sysinfo(&info);
}

TSystemClass::~TSystemClass() {
 if (latency_target_fd >= 0) {
  close(latency_target_fd);
 }
}

// Returns the system information on operational system
void TSystemClass::get_host_info() {
 if (uname(&hostinfo) == IS_FAILURE) {
  throw TException(__PRETTY_FUNCTION__, ":\tError hostinfo: ", strerror(errno));
 }
} // end

// Returns a name of operational system
const char* TSystemClass::GetOsName() const {
 return hostinfo.sysname;
} // end

// Returns a version number of operational system
const char* TSystemClass::GetOsVersion() const {
 return hostinfo.release;
} // end

// Returns date of the assembly of operational system
const char* TSystemClass::GetOsBuild() const {
 return hostinfo.version;
} // end

// Returns type of architecture
const char* TSystemClass::GetPlatform() const {
 return hostinfo.machine;
} // end

// Returns a name of a network
const char* TSystemClass::GetNodeName() const {
 return hostinfo.nodename;
} // end

// Returns the information on size of the free memory in bytes
// To run in thread
off_t TSystemClass::GetFreeMemory() const {
 return info.freeram;
} // end

// Returns the information on total of accessible memory in system in bytes
// To run in thread
size_t TSystemClass::GetTotalMemorySize() const {
 return info.totalram;
} // end get_all_available_memory

// Returns the statistical information on shared memory in bytes
// To run in thread
size_t TSystemClass::GetSharedMemorySize() const {
 return info.sharedram;
} // end get_info_share_mem

// Returns loadtime of system
const char* TSystemClass::OSBootTimeStr() const {
 auto t = std::time(nullptr) - info.uptime;
 return ctime(&t);
} // end TimeQNXBoot

// Returns start time of program
const char* TSystemClass::ProgramStartTime() const {
 auto t = std::time(nullptr);
 return ctime(&t);
} //

// Returns number of processors in system
int TSystemClass::GetCpuCount() const {
 return sysconf(_SC_NPROCESSORS_CONF);
} // end GetNumCpu

// Returns the type of processsor
std::string TSystemClass::GetCPUType() const {
 constexpr char cpuinfo_filename[] = "/proc/cpuinfo";
 std::ifstream cpu(cpuinfo_filename);
 char buf[1024];
 std::regex r("model name[[:space:]]*:[[:space:]]*");
 while (!cpu.getline(buf, sizeof buf).eof()) {
  std::cmatch m;
  std::regex_search(buf, m, r);
  if (m.size() > 0) {
   return m.suffix();
  }
 }
 return std::string();
} // end GetTypeCPU

// Returns the frequency of processsor
std::string TSystemClass::GetCPUFreq() const {
 constexpr char cpuinfo_filename[] = "/proc/cpuinfo";
 std::ifstream cpu(cpuinfo_filename);
 char buf[1024];
 std::regex r("cpu MHz[[:space:]]*:[[:space:]]*");
 while (!cpu.getline(buf, sizeof buf).eof()) {
  std::cmatch m;
  std::regex_search(buf, m, r);
  if (m.size() > 0) {
   return m.suffix();
  }
 }
 return std::string();
} // end GetTypeCPU


rlimit TSystemClass::GetMaximumPriority() const {
 rlimit r;
 auto _ = getrlimit(RLIMIT_RTPRIO, &r);
 if (_ == IS_FAILURE) {
  throw
   TException(__PRETTY_FUNCTION__, ": getrlimit error: ", strerror(errno));
 }
 return r;
}

void TSystemClass::SetMaximumPriority(int value) {
 rlimit r;
 r.rlim_max = 99;
 r.rlim_cur = value;
 auto _ = setrlimit(RLIMIT_RTPRIO, &r);
 if (_ == IS_FAILURE) {
  throw
   TException(__PRETTY_FUNCTION__, ": setrlimit error: ", strerror(errno));
 }
}

void TSystemClass::SetDMALatency(int value) {
 struct stat s;
 if (stat("/dev/cpu_dma_latency", &s) == 0) {
  int latency_target_fd = open("/dev/cpu_dma_latency", O_RDWR);
  if (latency_target_fd == IS_FAILURE) {
   std::cout << "cannot open /dev/cpu_dma_latency" << std::endl;
   return;
  }
  int ret = write(latency_target_fd, &value, 4);
  if (ret == 0) {
   std::cout << "# error setting cpu_dma_latency to " << value << "!: " << strerror(errno) << std::endl;
   close(latency_target_fd);
   return;
  }
  std::cout << "/dev/cpu_dma_latency set to " << value << std::endl;
 }
}

//=================== End of system.cc file===================================
