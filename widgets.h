#ifndef _WIDGETS_H_
#define _WIDGETS_H_

// "Sarmat" project
// Widgets list
// (C) I.Drandar, 2010-2017, all rights reserved
// (C) I.Drandar, for Qt, 2018-2020, all rights reserved

#include "widgets/CoordCluster.h"
#include "widgets/TrendLine.h"
#include "data_types.h"
#include "Interface.h"

// Declarations

namespace Ui {
class MainWindow;
class TrendsWindow;
}

class VI { // visual interface
 static const ColorStringPair SubModeStr[];
 static const ColorStringPair SystemFuncFaultStr[];
 using ui = const Ui::MainWindow&;

public:
 struct System { // System pane
  // buttons
//  RadioClass ShowAuxiliaryTrends;
  ColorModeSelectValue<SystemMode> SubMode;
  ColorSelectValue<Interface::Command::_System::Fault> FuncFault;
  class TD {
   bool Accept;
   WidgClass Pane;
   FloatValue Distance;
   AngleValue Bearing;
   AngleValue Elevation;
  public:
   TD(ui);
   void Assign(const struct Interface::Command::_System::TD&);
  } TD;
  System(ui);
 } System;

 template <typename T>
 struct ProcessingModeStr {
  const char** operator()() { return NULL; }
 };

 struct Main { // main data
  static const ColorStringPair SolutionResultStr[];
  // TV tab
  struct TV_Control {
   struct FocusControl {
    Highlight SmoothlyPlus;
    Highlight SmoothlyMinus;
    Highlight Near;
    Highlight Smoothly;
    Highlight Manual;
    void Assign(const struct ::TV::OutputData::Focus&);
   } Focus;
   struct FieldOfView {
    Highlight SmoothlyPlus;
    Highlight SmoothlyMinus;
    Highlight Narrow;
    Highlight Smoothly;
    Highlight Manual;
    void Assign(const ::TV::OutputData::FieldOfView&);
   } FOV;
   Highlight Night; // false - day, true - night
   IntValue Brightness;
   IntValue Contrast;
   CoordinateCluster::Cartesian2DCoordsInt CenterCorrection;
   void Assign(const TV::OutputData&);
  };
  struct GTC_Faults {
   // ПФ1.1
   Highlight DeviceController; // КП
   Highlight LRF;
   Highlight TV;
   Highlight IR;
   Highlight ASS;
   CoordinateCluster::Spherical2DBool EncoderExchange;
   // motors
   CoordinateCluster::Spherical2DBool Encoder;
   // ПФ4.1
   Highlight GTC; // ЦК.27
   Highlight ExchangeDAT;
   Highlight ExchangeComp;
   Highlight ExchangeDC;
   Highlight ExchangeLTS_DC;
   Highlight ExchangeNavig;
   // ПФ7.2
   Highlight Overheat;    // превышение критической температуры
   Highlight No24V;       // отсутствие 24 В
   Highlight NoPhase;     // отсутствие фаз
   Highlight Overcurrent; // превышение тока
   Highlight No7_2;       // не вкл. 7.2.
   // Доп. неисп. НК КС
   Highlight NS_NoH; // не заг.
   Highlight NS_NoD; // не дан.
   // ПФ1.1.2
   Highlight AmplifierHAEngine;
   Highlight AmplifierHAA1;
   Highlight AmplifierElevEngine;
   Highlight AmplifierElevA3;
   Highlight ExchangeQ;
   Highlight ExchangeE;
   void Assign(const GTC2::Faults&);
  };
  // OEUC tab
  struct GTC_ORU {
   CoordinateCluster::Spherical2DBool DriveOn;
   Highlight ElevLimitHigh;
   Highlight ElevLimitLow;
   CoordinateCluster::Spherical2DGroundCoords Ground;
   CoordinateCluster::Spherical2DShipCoords Ship; // deck coordinate system OEUC position
   void Assign(const struct GTC::InputData::ORU&);
  };
  struct Gun_Faults {
   Highlight ExchangeComp;
   Highlight ExchangeGun;
   Highlight PowerSupply;
   Highlight URef;
   void Assign(const struct _Gun::InputData::Faults&);
  };
  struct LRF_Faults {
   Highlight FRAM;
   Highlight CRC;
   Highlight IntervalMeasurer;
   Highlight QSwitch;
   Highlight LPS; // БПЛ
   Highlight WTF; // ФПУ
   Highlight OpticalStart;
   void Assign(const struct LRF::InputData::Faults&);
  };
  struct LRF_State {
   IntValue Targets;
   Highlight LowPeriod;
   Highlight OnboardNetwork;
   Highlight WTF; // Ошибка драйв. ТЕС
   Highlight TestingCompleted;
   Highlight Failure;
   void Assign(const struct LRF::InputData::State&);
  };
  struct WeatherType {
   FloatValue WindSpeed;
   AngleValue WindBearing;
   FloatValue Pressure;
   FloatValue Temperature; // air temperature in °C
   FloatValue ChargeTemperature;
   void Assign(const struct Weather&);
  };
  struct NS_Faults {
   Highlight HeaderChecksum;
   Highlight DataChecksum;
   Highlight Count;
   Highlight General;
   Highlight Off;
   void Assign(const struct NS::InputData::Faults&);
  };
  struct ETD_In {
   Highlight EmissionPermitted;
   Highlight Antenna; // 0 - equivalent, 1 - antenna
   SelectValue<RadarETD::TargetType> TT;
   FloatValue Distance;
   Highlight LFM_SlopeNegative;
   SelectValue<Radar::DistanceScale> DS;
   IntValue SignalCode;
   SelectValue<Radar::OperatingFrequencyTuning> OFT;
   IntValue FrequencyCode;
   SelectValue<Radar::RepetitionPeriodAdjustment> RPA;
   IntValue RepetitionPeriodCode;
   SelectValue<Radar::MovingTargetSelection> MTS1;
   SelectValue<Radar::MovingTargetSelection> MTS2;
   SelectValue<Radar::FastFourierTransform> FFT;
   Highlight ActiveNoiseAnalysis;
   Highlight OwnMovementCompensation;
   Highlight ActiveInterferenceAngularTracking;
   Highlight SumDeltaChannelsCalibration; // 0 - disabled, 1 - enabled
   Highlight RangeCalibration; // 0 - disabled, 1 - enabled
   IntValue PulseCount;
   void Assign(const RadarETD::OutputData&);
  };
  struct ETD_Out {
   Highlight EmissionPermitted;
   Highlight EmissionPermittedPhysical;
   Highlight Antenna; // 0 - equivalent, 1 - antenna
   SelectValue<RadarETD::TargetType> TT;
   FloatValue Distance;
   Highlight LFM_SlopeNegative;
   SelectValue<Radar::DistanceScale> DS;
   IntValue SignalCode;
   SelectValue<Radar::OperatingFrequencyTuning> OFT;
   IntValue FrequencyCode;
   SelectValue<Radar::RepetitionPeriodAdjustment> RPA;
   IntValue RepetitionPeriodCode;
   SelectValue<Radar::MovingTargetSelection> MTS1;
   SelectValue<Radar::MovingTargetSelection> MTS2;
   SelectValue<Radar::FastFourierTransform> FFT;
   Highlight ActiveNoiseAnalysis;
   Highlight OwnMovementCompensation;
   Highlight ActiveInterferenceAngularTracking;
   Highlight SumDeltaChannelsCalibration; // 0 - disabled, 1 - enabled
   Highlight RangeCalibration; // 0 - disabled, 1 - enabled
   IntValue PulseCount;
   void Assign(const RadarETD::InputData&);
  };
  struct SS_In {
   Highlight EmissionPermitted;
   Highlight Antenna; // 0 - equivalent, 1 - antenna
   FloatValue DistanceBegin; // of passive noise strobe
   FloatValue DistanceEnd;   // of passive noise strobe
   Highlight LFM_SlopeNegative;
   SelectValue<Radar::DistanceScale> DS;
   IntValue SignalCode;
   SelectValue<Radar::OperatingFrequencyTuning> OFT;
   IntValue FrequencyCode;
   SelectValue<Radar::RepetitionPeriodAdjustment> RPA;
   IntValue RepetitionPeriodCode;
   SelectValue<Radar::MovingTargetSelection> MTS1;
   SelectValue<Radar::MovingTargetSelection> MTS2;
   SelectValue<Radar::FastFourierTransform> FFT;
   Highlight ActiveNoiseAnalysis;
   Highlight OwnMovementCompensation;
   Highlight RangeCalibration; // 0 - disabled, 1 - enabled
   IntValue PulseCount;
   void Assign(const RadarSS::OutputData&);
  };
  struct SS_Out {
   Highlight EmissionPermitted;
   Highlight EmissionPermittedPhysical;
   Highlight Antenna; // 0 - equivalent, 1 - antenna
   FloatValue DistanceBegin; // of passive noise strobe
   FloatValue DistanceEnd;   // of passive noise strobe
   Highlight LFM_SlopeNegative;
   SelectValue<Radar::DistanceScale> DS;
   IntValue SignalCode;
   SelectValue<Radar::OperatingFrequencyTuning> OFT;
   IntValue FrequencyCode;
   SelectValue<Radar::RepetitionPeriodAdjustment> RPA;
   IntValue RepetitionPeriodCode;
   SelectValue<Radar::MovingTargetSelection> MTS1;
   SelectValue<Radar::MovingTargetSelection> MTS2;
   SelectValue<Radar::FastFourierTransform> FFT;
   Highlight ActiveNoiseAnalysis;
   Highlight OwnMovementCompensation;
   Highlight RangeCalibration; // 0 - disabled, 1 - enabled
   IntValue PulseCount;
   void Assign(const RadarSS::InputData&);
  };
  struct Control_Data {
   Highlight TAGC; // temporary auto gain control
   Highlight RangeCalibrationInput;
   SelectValue<RadarControl::PassiveInterferenceCompensating> PIC;
   Highlight CFAR;
   Highlight ManualIF;
   Highlight ManualUHF;
   FloatValue DetectionThreshold; // dB
   FloatValue ANI_Threshold; // dB
   FloatValue UHF_ControlThreshold; // dB
   FloatValue IF_ControlThreshold; // dB
   IntValue SignalCode;
   FloatValue CalibrationRangeCorrection; // m
   FloatValue PassiveInterferenceCoherenceCoefficient; // γ
   FloatValue MinimumPassiveInterferenceThreshold; // β dB
   void Assign(const RadarControl::OutputData&);
  };
  struct RadarTest_Data {
   // runtime data
   struct Faults_Data {
    Highlight TransmitterPulsePower;
    Highlight SUMNoisePower;
    Highlight DF1NoisePower;
    Highlight DF2NoisePower;
    Highlight SUMCalibrationPower;
    Highlight DF1CalibrationPower;
    Highlight DF2CalibrationPower;
    Highlight SUMDirectWaveLevel;
    Highlight SUMBackwardWaveLevel;
    Highlight SUM_DF1AmplitudeDifference;
    Highlight SUM_DF2AmplitudeDifference;
    Highlight SynthesizerPLL_LoopCapture;
    Highlight A_E_Switch;
    Highlight TransmitterTemperature;
    void Assign(const struct RadarTestResult::InputData::Faults&);
   } Faults;
   // end of runtime data
   FloatValue TransmitterPulsePower;      // W
   FloatValue SUMNoisePower;              // dB
   FloatValue DF1NoisePower;              // dB
   FloatValue DF2NoisePower;              // dB
   FloatValue SUMCalibrationPower;        // dB
   FloatValue DF1CalibrationPower;        // dB
   FloatValue DF2CalibrationPower;        // dB
   FloatValue SUMDirectWaveLevel;         // mV
   FloatValue SUMBackwardWaveLevel;       // mV
   FloatValue SUM_DF1AmplitudeDifference; // dB
   FloatValue SUM_DF2AmplitudeDifference; // dB
   FloatValue SUM_DF1PhaseDifference;     // °
   FloatValue SUM_DF2PhaseDifference;     // °
   Highlight Completed;
   void Assign(const RadarTestResult::InputData&);
  };
  struct PlannedFrequenciesItem {
   Highlight Permitted;
   IntValue Code;
   void Assign(const ::PlannedFrequencies::Item&);
  };
  using PlanArray = ::PlannedFrequencies::FrequenciesArray<PlannedFrequenciesItem>;
 public:
  class External {
   class NS { // navigation system tab
    CoordinateCluster::Rotations Rot;
    CoordinateCluster::Rotations RotChange;
    CoordinateCluster::Cartesian3DCoords SpeedPart;
    FloatValue SpeedAbsolute;
    NS_Faults Faults;
    FloatValue LTS;
    IntValue Received;
    IntValue ReceptionFaults;
   public:
    NS(ui);
    void Assign(const Interface::DisplayRecord<ModuleNS>::OutRecord&);
   } NS;
   class GunX {
    class Out {
     _GunX::Channels<AngleValue> Ship_HA;
     _GunX::Channels<FloatValue> ADC;
     Highlight FaultCS;
     Highlight FaultPV;
     Highlight FaultADC;
     IntValue Received;
     IntValue ReceptionFaults;
    public:
     Out(ui);
     void Assign(const Interface::DisplayRecord<ModuleGunX>::OutRecord&);
    } Out;
    class In {
     Highlight Test;
     IntValue Transmitted;
    public:
     In(ui);
     void Assign(const Interface::DisplayRecord<ModuleGunX>::InRecord&);
    } In;
   public:
    GunX(ui);
    void Assign(const Interface::DisplayRecord<ModuleGunX>&);
   } GunX;
   struct Gun {
    struct Out {
     Gun_Faults Faults;
     IntValue Received;
     IntValue ReceptionFaults;
     void Assign(const Interface::DisplayRecord<ModuleGun>::OutRecord&);
    } Out;
    struct In {
     SelectValue<_Gun::Workmode> Mode;
     AngleValue Position;
     Highlight IsElevChannel;
     IntValue Transmitted;
     void Assign(const Interface::DisplayRecord<ModuleGun>::InRecord&);
    } In;
   public:
    void Assign(const Interface::DisplayRecord<ModuleGun>&);
   };
   Gun GunHA;
   Gun GunElev;
   struct GunTimer {
    struct Out {
     Gun_Faults Faults;
     IntValue Received;
     IntValue ReceptionFaults;
     void Assign(const Interface::DisplayRecord<ModuleGunTimer>::OutRecord&);
    } Out;
    struct In {
     SelectValue<_Gun::Workmode> Mode;
     FloatValue Time;
     IntValue Transmitted;
     void Assign(const Interface::DisplayRecord<ModuleGunTimer>::InRecord&);
    } In;
   public:
    void Assign(const Interface::DisplayRecord<ModuleGunTimer>&);
   } GunTimer;
   struct GTC_Radar {
    struct Mode {
     struct Out {
      SelectValue<GTCRMode::Mode> Mode;
      SelectValue<GTCRMode::Submode> Submode;
      IntValue Received;
      IntValue ReceptionFaults;
      Out(ui);
      void Assign(const Interface::DisplayRecord<ModuleGTCRMode>::OutRecord&);
     } Out;
     struct In {
      SelectValue<GTCRMode::Mode> Mode;
      SelectValue<GTCRMode::Submode> Submode;
      IntValue Transmitted;
      In(ui);
      void Assign(const Interface::DisplayRecord<ModuleGTCRMode>::InRecord&);
     } In;
     Mode(ui);
     void Assign(const Interface::DisplayRecord<ModuleGTCRMode>&);
    } Mode;
    struct PlannedFrequencies {
     struct Plan {
      IntValue Count;
      PlanArray Array;
      void Assign(const ::PlannedFrequencies::OutputData&);
     };
     struct Out {
      struct Plan Plan;
      IntValue Received;
      IntValue ReceptionFaults;
      Out(ui);
      void Assign(const Interface::DisplayRecord<ModulePlannedFrequencies>::OutRecord&);
     } Out;
     struct In {
      struct Plan Plan;
      IntValue Transmitted;
      In(ui);
      void Assign(const Interface::DisplayRecord<ModulePlannedFrequencies>::InRecord&);
     } In;
     PlannedFrequencies(ui);
     void Assign(const Interface::DisplayRecord<ModulePlannedFrequencies>&);
    } PlannedFrequencies;
    struct RadialSpeed {
     FloatValue Target;
     FloatValue PassiveInterference;
     IntValue Transmitted;
     RadialSpeed(ui);
     void Assign(const Interface::DisplayRecord<ModuleRadialSpeed>::InRecord&);
    } RadialSpeed;
    struct ETD {
     struct Out {
      ETD_Out Data;
      IntValue Received;
      IntValue ReceptionFaults;
      Out(ui);
      void Assign(const Interface::DisplayRecord<ModuleRadarETD>::OutRecord&);
     } Out;
     struct In {
      ETD_In Data;
      IntValue Transmitted;
      In(ui);
      void Assign(const Interface::DisplayRecord<ModuleRadarETD>::InRecord&);
     } In;
     ETD(ui);
     void Assign(const Interface::DisplayRecord<ModuleRadarETD>&);
    } ETD;
    struct SS {
     struct Out {
      SS_Out Data;
      IntValue Received;
      IntValue ReceptionFaults;
      Out(ui);
      void Assign(const Interface::DisplayRecord<ModuleRadarSS>::OutRecord&);
     } Out;
     struct In {
      SS_In Data;
      IntValue Transmitted;
      In(ui);
      void Assign(const Interface::DisplayRecord<ModuleRadarSS>::InRecord&);
     } In;
     SS(ui);
     void Assign(const Interface::DisplayRecord<ModuleRadarSS>&);
    } SS;
    struct Control {
     struct Out {
      struct Control_Data Data;
      IntValue Received;
      IntValue ReceptionFaults;
      Out(ui);
      void Assign(const Interface::DisplayRecord<ModuleRadarControl>::OutRecord&);
     } Out;
     struct In {
      struct Control_Data Data;
      IntValue Transmitted;
      In(ui);
      void Assign(const Interface::DisplayRecord<ModuleRadarControl>::InRecord&);
     } In;
     Control(ui);
     void Assign(const Interface::DisplayRecord<ModuleRadarControl>&);
    } Control;
    struct Training {
     FloatValue Distance;     // m
     FloatValue RadialSpeed;  // m/s
     FloatValue DopplerShift; // Hz
     IntValue Transmitted;
     Training(ui);
     void Assign(const Interface::DisplayRecord<ModuleRadarTraining>::InRecord&);
    } Training;
    struct ETD_Detection {
     SelectValue<Radar::OperatingFrequencyTuning> OFT;
     IntValue FrequencyCode;
     IntValue RepetitionPeriodCode;
     SelectValue<Radar::RepetitionPeriodAdjustment> RPA;
     IntValue TargetCount;
     SelectValue<Radar::ActiveNoisePresence> ANP;
     Highlight ActiveNoiseInterferenceProcessing;
     struct FirstTarget {
      SelectValue<::ETD_Detection::RangeTrackingState> RTS;
      FloatValue Distance;
      CoordinateCluster::Spherical2DCoords CommonModeDeviation;
      CoordinateCluster::Spherical2DCoords QuadratureDeviation;
      FloatValue SignalToNoiseRatio;
      FloatValue RadialSpeed;
      FirstTarget(ui);
      void Assign(const ::ETD_Detection::FirstTarget&);
     } First;
     struct Target {
      SelectValue<::ETD_Detection::RangeTrackingState> RTS;
      FloatValue Distance;
      CoordinateCluster::Spherical2DCoords CommonModeDeviation;
      FloatValue SignalToNoiseRatio;
      FloatValue RadialSpeed;
      void Assign(const ::ETD_Detection::Target&);
     };
     ::ETD_Detection::TargetsArray<Target> Others;
     FloatValue LTS; // LTS time
     IntValue Received;
     IntValue ReceptionFaults;
     ETD_Detection(ui);
     void Assign(const Interface::DisplayRecord<ModuleETD_Detection>::OutRecord&);
    } ETD_Detection;
    struct SS_Data {
     SelectValue<Radar::OperatingFrequencyTuning> OFT;
     IntValue FrequencyCode;
     IntValue RepetitionPeriodCode;
     SelectValue<Radar::RepetitionPeriodAdjustment> RPA;
     IntValue TargetCount;
     SelectValue<Radar::ActiveNoisePresence> ANP;
     Highlight ActiveNoiseInterferenceProcessing;
     FloatValue LTS; // LTS time
     IntValue Received;
     IntValue ReceptionFaults;
     SS_Data(ui);
     void Assign(const Interface::DisplayRecord<ModuleSS_Data>::OutRecord&);
    } SS_Data;
    struct Test {
     RadarTest_Data Data;
     IntValue Received;
     IntValue ReceptionFaults;
     Test(ui);
     void Assign(const Interface::DisplayRecord<ModuleRadarTest>::OutRecord&);
    } Test;
    GTC_Radar(ui);
    void Assign(const struct Interface::Command::Main::External::Radar&);
   } GTC_Radar;
  public:
   External(ui);
   void Assign(const struct Interface::Command::Main::External&);
   void Lock();
  } External;
  class Internal {
   class GTC {
    class Out { // GTC_Out tab
     SelectValue<::GTC::Mode> Mode;
     Highlight ModeIsChanging;
     Highlight TVReady;
     Highlight IRReady;
     Highlight LRFReady;
     Highlight Power;
     Highlight IIT_Permit;
     IntValue DATChannel;
     Highlight _7_2;
     // OEUC tab
     GTC_ORU ORU;
     FloatValue Sine;
     FloatValue Cosine;
     CoordinateCluster::Spherical2DBool Mismatch;
     CoordinateCluster::Spherical2DBool Detent;
     CoordinateCluster::Spherical2DBool DetentError;
     Highlight SyncRequest;
     Highlight SyncDone;
     FloatValue LTS;
     IntValue Received;
     IntValue ReceptionFaults;
    public:
     Out(ui);
     void Assign(const Interface::DisplayRecord<ModuleGTC>::OutRecord&);
    } Out;
    class In { // OEU controller tab
     SelectValue< ::GTC::Mode> Mode;
     Highlight WiperOn;
     Highlight HeatingOn;
     Highlight Fire;
     Highlight HeatStandby;
     Highlight IIT_Enable;
     Highlight IROn;
     IntValue DATChannel;
     Highlight DAT1;
     // OEUC tab
     struct ORU_Control {
      CoordinateCluster::Spherical2DGroundCoords ETD;
      ORU_Control(ui);
      void Assign(const ::GTC::ORU_Control&);
     } ORU;
     struct Handle_Control {
      CoordinateCluster::Cartesian2DCoordsInt Offset;
      Handle_Control(ui);
      void Assign(const ::GTC::Handle_Control&);
     } Handle;
     // TV tab
     IntValue Transmitted;
    public:
     In(ui);
     void Assign(const Interface::DisplayRecord<ModuleGTC>::InRecord&);
    } In;
   public:
    GTC(ui);
    void Assign(const Interface::DisplayRecord<ModuleGTC>&);
   } GTC;
   class DAT {
    struct Out { // dat tab(s)
     Highlight Fault; // channel is invalid
     SelectValue<::DAT::Workmode> Mode;
     SelectValue<::DAT::TargetType> TT;
     IntValue Method;
     Highlight Break;
     Highlight Capture; // transition to autotracking mode
     IntValue Credibility; // in percent
     Highlight Detection;
     Highlight TestingCompleted;
     CoordinateCluster::Spherical2DCoords AutotrackingMismatch;
     FloatValue LTS;
     List ChannelNo;
     IntValue Received;
     IntValue ReceptionFaults;
     Out(ui _);
     void Assign(const Interface::DisplayRecord<ModuleDAT>[]);
    } Out;
    struct In {
     SelectValue<::DAT::CameraType> Video1;
     SelectValue<::DAT::CameraType> Video2;
     SelectValue<::DAT::CameraType> Video3;
     Highlight Rotation;
     Highlight Mark;
     IntValue Processing;
     SelectValue<::DAT::TargetType> TT;
     SelectValue<::DAT::CameraType> Source;
     Highlight Capture;
     Highlight CaptureEnable;
     Highlight On;
     CoordinateCluster::Cartesian2DCoordsInt ReserveMethodCoords;
     List ChannelNo;
     IntValue Transmitted;
     In(ui _);
     void Assign(const Interface::DisplayRecord<ModuleDAT>[]);
    } In;
   public:
    DAT(ui _);
    void Assign(const Interface::DisplayRecord<ModuleDAT>[]);
    void Lock();
    void Unlock();
   } DAT;
   class Comp2 {
    struct Out { // computer 2 tab
     struct Control {
      SelectValue<::Comp2::SysMode> Mode;
      SelectValue<::Comp2::SubMode> Submode;
      Highlight IIT_Enable;
      Highlight AdditionalSearch;
      Highlight IROn;
      Highlight WiperOn;
      Highlight InternalHeatingOn;
      SelectValue<::DAT::CameraType> Camera;
      Highlight Fire;
      Highlight AirTarget;
      Highlight HeatStandby;
      IntValue DATChannel;
      Highlight Radar;
      Highlight LowFlyingTarget;
      struct Training {
       IntValue Code;
       Highlight StationaryTarget;
       Highlight On;
       CoordinateCluster::SphericalCoords StationaryTargetCoords;
       Training(ui);
       void Assign(const struct ::Comp2::InputData::Control::Training&);
      } Training;
      CoordinateCluster::SphericalCoords ExternalTarget;
      IntValue HandleX;
      IntValue HandleY;
      Highlight ManualDistanceOn;
      IntValue ManualDistance;
      CoordinateCluster::Spherical2DShipCoords OEUC;
      Control(ui);
      void Assign(const struct ::Comp2::InputData::Control&);
     } Control;
     WeatherType Weather;
     struct DAT_Control {
      Highlight Rotation;
      Highlight Mark;
      struct Channel {
       IntValue Processing;
       SelectValue<::DAT::TargetType> TT;
       SelectValue<::DAT::CameraType> Source;
       Highlight Capture;
       Highlight On;
       void Assign(const ::DAT::OutputData&);
      };
      Channel DAT1;
      Channel DAT2;
      DAT_Control(ui);
      void Assign(const struct ::Comp2::InputData::DAT_Control&);
     } DAT_Control;
     TV_Control TV;
     struct IR_Control {
      Highlight HotBlack;
      Highlight DigitalZoom;
      Highlight OpticalZoomPlus;
      Highlight OpticalZoomMinus;
      struct _Menu {
       Highlight On;
       void Assign(const struct ::Comp2::InputData::IR::Menu&);
      } Menu;
      Highlight ContrastPlus;
      Highlight ContrastMinus;
      Highlight BrightnessPlus;
      Highlight BrightnessMinus;
      Highlight FocusFar;
      Highlight FocusNear;
      Highlight Calibration;
      IntValue Brightness;
      IntValue Contrast;
      CoordinateCluster::Cartesian2DCoordsInt CenterCorrection;
      void Assign(const struct ::Comp2::InputData::IR&);
     } IR;
     struct LRF_Control {
      IntValue TargetNumber;
      Highlight Work;
      Highlight Single;
      Highlight Frequent;
      Highlight PilotLaser;
      IntValue Frequency;
      LRF_Control(ui);
      void Assign(const struct ::Comp2::InputData::LRF&);
     } LRF;
     struct GunControl {
      struct DriveControl {
       Highlight GunControl;
       Highlight RemoteFuseControl;
       Highlight JointAlignment;
       Highlight ParallaxJointAlignment;
       FloatValue Distance;
       DriveControl(ui);
       void Assign(const struct ::Comp2::InputData::GunControl::DriveControl&);
      } DriveControl;
      struct DriveCorrections {
       CoordinateCluster::Spherical2DShipCoords Orientation;
       CoordinateCluster::Spherical2DCoords PointingAngleMismatch;
       CoordinateCluster::Spherical2DCoords SituationAngleMismatch;
       CoordinateCluster::Spherical2DCoords Miss;
       DriveCorrections(ui);
       void Assign(const struct ::Comp2::DriveCorrections&);
      } DriveCorrections;
      struct BallisticData {
       FloatValue StartSpeedMismatch;
       FloatValue MassMismatch;
       void Assign(const ::Comp2::InputData::GunControl::BallisticData&);
      } BallisticData;
      GunControl(ui);
      void Assign(const ::Comp2::InputData::GunControl&);
     };
     GunControl GC;
     FloatValue Time;
     IntValue Received;
     IntValue ReceptionFaults;
     Out(ui);
     void Assign(const Interface::DisplayRecord<ModuleComp2>::OutRecord&);
    } Out;
    struct In {
     struct State {
      SelectValue<::Comp2::SysMode> Mode;
      SelectValue<::Comp2::SubMode> SubMode;
      Highlight AdditionalSearch;
      Highlight LowFlyingTarget;
      Highlight ModeIsChanging;
      struct Ready {
       Highlight TV;
       Highlight IR;
       Highlight Navig;
       Ready(ui);
       void Assign(const struct ::Comp2::OutputData::State::Ready&);
      } Ready;
      void Assign(const struct ::Comp2::OutputData::State&);
      State(ui);
     } State;
     struct GTC_State {
      SelectValue< ::GTC::Mode> Mode;
      Highlight ModeIsChanging;
      CoordinateCluster::Spherical2DBool Detent;
      Highlight Power;
      Highlight _7_2;
      Highlight IIT;
      Highlight SyncRequest;
      Highlight SyncDone;
      IntValue DATChannel;
      struct Faults {
       // 1.1
       Highlight DeviceController;
       Highlight LRF;
       Highlight TV;
       Highlight IR;
       Highlight ASS;
       CoordinateCluster::Spherical2DBool EncoderExchange;
       // motor
       CoordinateCluster::Spherical2DBool Encoder;
       CoordinateCluster::Spherical2DBool Detent;
       // 4.1
       Highlight GTC; // ЦК.28
       Highlight ExchangeDAT;
       Highlight ExchangeComp;
       Highlight ExchangeDC;
       Highlight ExchangeLTS_DC;
       Highlight ExchangeNavig;
       // 7.2
       Highlight Overheat;    // превышение критической температуры
       Highlight No24V;       // отсутствие 24 В
       Highlight NoPhase;     // отсутствие фаз
       Highlight Overcurrent; // превышение тока
       Highlight No7_2;       // не вкл. 7.2.
       // ПФ1.1.2
       Highlight AmplifierHAEngine;
       Highlight AmplifierHAA1;
       Highlight AmplifierElevEngine;
       Highlight AmplifierElevA3;
       Highlight ExchangeQ;
       Highlight ExchangeE;
       Faults(ui);
       void Assign(const struct ::Comp2::OutputData::GTC_State::Faults&);
      } Faults;
      FloatValue Distance;
      GTC_ORU OEUC;
      CoordinateCluster::Spherical2DBool Mismatch;
      CoordinateCluster::SphericalCoords Training;
      struct DAT_State {
       struct Channel {
        List ChannelNo;
        IntValue Method;
        Highlight Break;
        Highlight Capture;
        IntValue Credibility;
        Highlight Detection;
        Highlight Fault;
        Highlight TestingCompleted;
        SelectValue<::DAT::Workmode> Mode;
        SelectValue<::DAT::TargetType> TT;
        CoordinateCluster::Spherical2DCoords AutoTrackingMismatch;
        Channel(ui);
        void Assign(const struct ::Comp2::OutputData::GTC_State::DAT_State::Channel[]);
       } Channel;
       DAT_State(ui _);
       void Assign(const ::Comp2::OutputData::GTC_State::DAT_State&);
      } DAT;
      GTC_State(ui);
      void Assign(const ::Comp2::OutputData::GTC_State&);
     } GTC;
     class TV_State {
      CoordinateCluster::Spherical2DCoords FOV;
     public:
      TV_State(ui);
      void Assign(const Spherical2DCoords&);
     } TV;
     class IR_State {
      CoordinateCluster::Spherical2DCoords FOV;
      FloatValue Temperature;
     public:
      IR_State(ui);
      void Assign(const Spherical2DCoords&, double);
     } IR;
     struct LRF_State {
      SelectValue<int> Mode;
      Highlight Readiness;
      Highlight ReceptionFault;
      Highlight ControlFault;
      Highlight TemperatureReady;
      LRF_Faults Faults;
      Main::LRF_State State;
      IntValue Distance1;
      IntValue Distance2;
      IntValue Distance3;
      Highlight Measuring;
      IntValue Resource;
      LRF_State(ui);
      void Assign(const ::Comp2::OutputData::LRF_State&);
     } LRF;
     struct BallisticModule {
      SelectValue<::Comp2::SysMode> Mode;
      SelectValue<enum ::Comp2::SubMode> SubMode;
      struct State {
       Highlight TestingCompleted;
       Highlight PointingAngleReady;
       Highlight ITReady;
       State(ui);
       void Assign(const struct ::Comp2::OutputData::BallisticModule::State&);
      } State;
      struct ExchangeFaults {
       Highlight Gun;
       Highlight Navig;
       Highlight GTC;
       Highlight Comp2;
       ExchangeFaults(ui);
       void Assign(const struct ::Comp2::OutputData::BallisticModule::ExchangeFaults&);
      } ExchangeFaults;
      struct Limits {
       Highlight RightHA;
       Highlight LeftHA;
       Highlight HighElev;
       Highlight LowElev;
       Limits(ui);
       void Assign(const struct ::Comp2::OutputData::BallisticModule::Limits&);
      } Limits;
      struct Faults {
       Highlight GunXExchange;
       Highlight GunHAExchange;
       Highlight GunHA;
       Highlight GunElevExchange;
       Highlight GunElev;
       Highlight GunTimerExchange;
       Highlight GunTimer;
       Faults(ui);
       void Assign(const struct ::Comp2::OutputData::BallisticModule::Faults&);
      } Faults;
      BallisticModule(ui);
      void Assign(const struct ::Comp2::OutputData::BallisticModule&);
     } BallisticModule;
     struct GunState {
      CoordinateCluster::Spherical2DShipCoords TowerPosition;
      CoordinateCluster::Spherical2DCoords DesiredAngle;
      FloatValue CurrentDistance;
      FloatValue MeetDistance;
      FloatValue FlightTime;
      GunState(ui);
      void Assign(const ::Comp2::OutputData::GunState&);
     } Gun;
     struct Navig {
      CoordinateCluster::Rotations Rot;
      CoordinateCluster::Rotations RotChange;
      CoordinateCluster::Cartesian3DCoords Speed;
      NS_Faults Faults;
      void Assign(const struct ::Comp2::OutputData::Navig&);
     } Navig;
     FloatValue Time;
     IntValue Transmitted;
     In(ui);
     void Assign(const Interface::DisplayRecord<ModuleComp2>::InRecord&);
    } In;
   public:
    Comp2(ui);
    void Assign(const Interface::DisplayRecord<ModuleComp2>&);
   } Comp2;
   struct LTS {
    FloatValue Time;
    IntValue Received;
    IntValue ReceptionFaults;
    IntValue Transmitted;
    LTS(ui);
    void Assign(const Interface::DisplayRecord<ModuleLTS>&);
   } LTS;
   class TV {
    class In {
     TV_Control Data;
     IntValue Transmitted;
    public:
     In(ui);
     void Assign(const Interface::DisplayRecord<ModuleTV>::InRecord&);
    } In;
   public:
    TV(ui);
    void Assign(const Interface::DisplayRecord<ModuleTV>&);
   } TV;
   class IR {
    struct In {
     struct Control {
      Highlight HotBlack;
      Highlight DigitalZoom;
      Highlight OpticalZoomPlus;
      Highlight OpticalZoomMinus;
      Highlight MenuOn;
      Highlight ContrastPlus;
      Highlight ContrastMinus;
      Highlight BrightnessPlus;
      Highlight BrightnessMinus;
      Highlight FocusFar;
      Highlight FocusNear;
      Highlight Calibration;
      IntValue Brightness;
      IntValue Contrast;
      CoordinateCluster::Cartesian2DCoordsInt CenterCorrection;
      Control(ui);
      void Assign(const ::IR::OutputData&);
     } Data;
     IntValue Transmitted;
     In(ui);
     void Assign(const Interface::DisplayRecord<ModuleIR>::InRecord&);
    } In;
   public:
    IR(ui);
    void Assign(const Interface::DisplayRecord<ModuleIR>&);
   } IR;
   class GTC2 {
    class Out {
     CoordinateCluster::Spherical2DCoords TV;
     CoordinateCluster::Spherical2DCoords IR;
     Highlight FRAM;
     Highlight CRC;
     Highlight IntervalMeasurer;
     Highlight QSwitch;
     Highlight LPS;
     Highlight WTF0;
     Highlight OpticalStart;
     IntValue Targets;
     Highlight LowPeriod;
     Highlight OnboardNetwork;
     Highlight WTF1; // Ошибка драйв. ТЕС
     Highlight TestingCompleted;
     Highlight Failure;
     GTC_Faults Faults;
     IntValue Received;
     IntValue ReceptionFaults;
    public:
     Out(ui);
     void Assign(const Interface::DisplayRecord<ModuleGTC2>::OutRecord&);
    } Out;
   public:
    GTC2(ui);
    void Assign(const Interface::DisplayRecord<ModuleGTC2>&);
   } GTC2;
   class LRF {
    class Out {
     IntValue Mode;
     Highlight Readiness;
     Highlight ReceptionFault;
     Highlight ControlFault;
     Highlight TemperatureReady;
     IntValue D1;  // замечания от 24.03
     IntValue D2;  // замечания от 24.03
     IntValue D3;  // замечания от 24.03
     IntValue Resource;
     LRF_Faults Faults;
     LRF_State State;
     FloatValue LTS;
     IntValue Received;
     IntValue ReceptionFaults;
    public:
     Out(ui);
     void Assign(const Interface::DisplayRecord<ModuleLRF>::OutRecord&);
    } Out;
    struct In {
     IntValue Frequency;
     Highlight Work;
     Highlight Single;
     Highlight Frequent;
     Highlight Camera;
     Highlight Pilot;
     IntValue Transmitted;
     In(ui);
     void Assign(const Interface::DisplayRecord<ModuleLRF>::InRecord&);
    } In;
   public:
    LRF(ui);
    void Assign(const Interface::DisplayRecord<ModuleLRF>&);
   } LRF;
   // fake target tab
   struct Training {
    Highlight On;
    Highlight Square;
    Highlight Inversion;
    Highlight DirectionLeft;
    SelectValue<::Training::PictureType> Type;
    IntValue Size;
    CoordinateCluster::SphericalCoords Sph;
    IntValue Transmitted;
    Training(ui);
    void Assign(const Interface::DisplayRecord<ModuleTraining>::InRecord&);
   } Training;
   class Comp2Radar { // computer 2 for radar tab
    struct Out {
     SelectValue<GTCRMode::Mode> Mode;
     SelectValue<GTCRMode::Submode> Submode;
     Highlight War;
     ETD_In ETD;
     SS_In SS;
     Control_Data Control;
     FloatValue Distance;     // m
     FloatValue RadialSpeed;  // m/s
     FloatValue DopplerShift; // Hz
     PlanArray PF;
     FloatValue Time;
     IntValue Received;
     IntValue ReceptionFaults;
     Out(ui);
     void Assign(const Interface::DisplayRecord<ModuleComp2Radar>::OutRecord&);
    } Out;
    struct In {
     SelectValue<GTCRMode::Mode> Mode;
     SelectValue<GTCRMode::Submode> Submode;
     Highlight War;
     ETD_Out ETD;
     SS_Out SS;
     Control_Data Control;
     RadarTest_Data Test;
     struct ETD_Detection {
      Highlight ETD_Mode;
      IntValue TargetCount;
      struct TargetData {
       SelectValue<::ETD_Detection::RangeTrackingState> RTS;
       FloatValue Distance;
       CoordinateCluster::Spherical2DCoords Mismatch;
       void Assign(const ::Comp2Radar::OutputData::ETD_Detection::TargetData&);
      };
      ::Comp2Radar::TargetsArray<TargetData> Targets;
      CoordinateCluster::Spherical2DShipCoords ORU; // optical-radar unit
      FloatValue Time;
      ETD_Detection(ui);
      void Assign(const struct ::Comp2Radar::OutputData::ETD_Detection&);
     } ETD_Detection;
     struct SS_Data {
      Highlight SS_Mode;
      SelectValue<Radar::OperatingFrequencyTuning> OFT;
      IntValue FrequencyCode;
      IntValue RepetitionPeriodCode;
      SelectValue<Radar::RepetitionPeriodAdjustment> RPA;
      SelectValue<Radar::ActiveNoisePresence> ANP;
      Highlight ActiveNoiseInterferenceProcessing;
      FloatValue Time; // LTS time
      SS_Data(ui);
      void Assign(const struct ::Comp2Radar::OutputData::SS_Data&);
     } SS_Data;
     PlanArray PF;
     FloatValue Time;
     IntValue Transmitted;
     In(ui);
     void Assign(const Interface::DisplayRecord<ModuleComp2Radar>::InRecord&);
    } In;
   public:
    Comp2Radar(ui);
    void Assign(const Interface::DisplayRecord<ModuleComp2Radar>&);
    void Lock();
    void Unlock();
   } RadarComp2;
  public:
   Internal(ui);
   void Assign(const struct Interface::Command::Main::Internal&);
   void Lock();
   void Unlock();
  } Internal;
  struct Evaluations {
   static constexpr size_t PolyDegree = 5;
   struct Filter {
    struct In {
     struct AnglesData {
      FloatValue Time;
      CoordinateCluster::Spherical2DShipCoords OEUC; // in ship coordinates
      CoordinateCluster::Spherical2DCoords AutotrackingMismatch;
      void Assign(const Angles_Data&);
     } data;
     SelectValue<SystemMode> Mode;
     struct DistanceData { // User Format
      Highlight OK;
      FloatValue Time;
      FloatValue Distance;
      FloatValue Speed;
      FloatValue Acceleration;
      void Assign(const ::DistanceData&);
     } DD;
     struct NS_Data { // PkPar for Coordinate proccessing
      Highlight NewInfo;
      FloatValue Time;
      CoordinateCluster::Cartesian3DCoords SpeedPart;
      CoordinateCluster::Rotations Rot; // Teta -cren(BK,roll), Psi - different(KK,pitch)
      CoordinateCluster::Rotations RotChange;
      void Assign(const ::NS_Data&);
     } NS_Data;
     void Assign(const FilterModule::CommandVector&);
    } In;
    struct Out {
     struct OutData {
      FloatValue Time;
      Highlight Completed; // Dn: Done
      IntValue CoordinatesCount; // For current filter. If Filter Simpl: CoordinatesCount=1;
      struct Cartesian3DCoordsExtended {
       CoordinateCluster::Cartesian3DCoords Coords;
       FloatValue Time;
       void Assign(const FilterModule::OutData::Cartesian3DCoordsExtended&);
      } TargetGroundCart; // Counted Tg coordinates on current Tackt filter
      std::array<CoordinateCluster::Cartesian3DCoords, PolyDegree> PolynomialCoeff;
      std::array<CoordinateCluster::Cartesian3DCoords, 2> PolynomialTest;
      void Assign(const FilterModule::OutData&);
     } data;
     Highlight PerformedFiltrationInCurrentMode;
     IntValue Performed; // counter
     struct Extrapolation {
      struct NS {
       struct CurrentValues {
        CoordinateCluster::Cartesian3DCoords SpeedPart;
        CoordinateCluster::Cartesian3DCoords ShipCart0; // a0_Rx, a0_Ry, a0_Rz
        CoordinateCluster::Cartesian3DCoords ShipCart1; // a1_Rx, a1_Ry, a1_Rz
        CoordinateCluster::Rotations Rotations0;
        CoordinateCluster::Rotations Rotations1;
        void Assign(const ExtrapolationPolynomials::NS::CurrentValues&);
       } CV;
       FloatValue Time; // basic time of polinoms
       Highlight OK; // define readyness ExtrapolationPolynomials
       CoordinateCluster::Cartesian3DCoords ShipTest1;
       CoordinateCluster::Rotations RotationsTest1;
       void Assign(const ExtrapolationPolynomials::NS&);
      } NS;
      struct Angles {
       FloatValue Time; // time of measuament paramiters
       SelectValue<ExtrapolationPolynomials::Angles::ReadyState> Ready; // define readyness ExtrapolationPolynomials
       IntValue Count;
       IntValue DP_Cur; // degree polinoms current
       std::array<
        CoordinateCluster::Spherical2DShipCoords, PolyDegree
       > PnShip; // array of coefficients  extrapol. polinom for Q, F
       std::array<
        CoordinateCluster::Spherical2DGroundCoords, PolyDegree
       > PnGround; // array of coefficients fitting polinom for A, E
       CoordinateCluster::Spherical2DShipCoords ShipTest;
       CoordinateCluster::Spherical2DGroundCoords GroundTest;
       void Assign(const ExtrapolationPolynomials::Angles&);
      } Angles;
      void Assign(const ExtrapolationPolynomials::All&);
     } Extrapolation;
     FloatValue ElapsedTime;
     void Assign(const FilterModule::StatusVector&);
    } Out;
    void Assign(const struct Interface::Command::Main::Evaluations::Filter&);
   };
   Filter FilterOptical;
   Filter FilterRadar;
   struct Collision {
    struct In {
     IntValue FiltrationPerformedCount;
     FloatValue ShotTime;
     struct ExtraData { // external additional data from different sources
      SelectValue<SystemMode> Mode;
      Highlight PerformedFiltrationInCurrentMode;
      void Assign(const CollisionTaskModule::ExtraData&);
     } Extra;
     void Assign(const CollisionTaskModule::CommandVector&);
    } In;
    struct Solver_In {
     FloatValue StartSpeedMismatch; //  m/s
     FloatValue MassMismatch; //  kg
     void Assign(const CollisionTaskSolverModule::CommandVector&);
    } Solver_In;
    struct Out {
     IntValue Performed;
     void Assign(const CollisionTaskModule::StatusVector&);
    } Out;
    struct Solver_Out {
     IntValue Performed;
     ColorSelectValue<CollisionTable::SolutionResult> Result;
     struct OutData {
      Highlight HaveSolution;
      struct Extrapolation {
       FloatValue ShotTime;
       FloatValue FlightTime;
       CoordinateCluster::Spherical2DGroundCoords Ground;
       CoordinateCluster::Spherical2DShipCoords Ship;
       FloatValue DistanceCurrent;
       FloatValue DistanceMeet;
       std::array<
        CoordinateCluster::Spherical2DShipCoords, PolyDegree
       > PnShip; // array of coefficients  extrapol. polinom for Q, F
       std::array<
        CoordinateCluster::Spherical2DGroundCoords, PolyDegree
       > PnGround; // array of coefficients fitting polinom for A, E
       static constexpr size_t Tests = 2;
       std::array<CoordinateCluster::Spherical2DShipCoords, Tests> ShipTest;
       std::array<CoordinateCluster::Spherical2DGroundCoords, Tests> GroundTest;
       void Assign(const struct ExtrapolationPolynomials::Collision::InOut&);
      } Extrapolation;
      struct Corrections {
       FloatValue TailwindDistance;
       FloatValue TailwindHeight;
       FloatValue DencityDistance;
       FloatValue DencityHeight;
       FloatValue SpeedStartDistance;
       FloatValue SpeedStartHeight;
       CoordinateCluster::Spherical2DCoords ShipMotion;
       AngleValue Derivation;
       AngleValue Crosswind;
       AngleValue AimingAngle;
       void Assign(const CollisionTable::AllCorrections&);
      } Corrections;
      void Assign(const CollisionTaskSolverModule::OutData&);
     } data;
     FloatValue ElapsedTime;
     void Assign(const CollisionTaskSolverModule::StatusVector&);
    } Solver_Out;
    void Assign(const struct Interface::Command::Main::Evaluations::Collision&);
   };
   Collision CollisionOptical;
   Collision CollisionRadar;
   Evaluations(ui);
   void Assign(const struct Interface::Command::Main::Evaluations&);
  } Evaluations;
//  RadioClass Stop;
  Main(ui);
  void Assign(const struct Interface::Command::Main&);
 } Main;

 VI(ui);
};

class TW { // trends window
 using td = Ui::TrendsWindow&;
 struct External {
  struct NS { // navigation system tab
   Trend Speed;
   TrendLine::Cartesian3D SpeedPart;
   TrendLine::Simple SpeedAbs;
   Trend Rot;
   TrendLine::Rotations Rotations;
   NS(td);
   void Assign(const Interface::DisplayRecord<ModuleNS>::OutRecord&);
   void Suspend();
   void Resume();
  } NS;
  struct Weather {
   Trend WindSpeed;
   TrendLine::Simple WindSpeedLine;
   Trend WindBearing;
   TrendLine::Degrees WindBearingLine;
   Trend Pressure;
   TrendLine::Simple PressureLine;
   Trend Temperature;
   TrendLine::Simple AirTemperature;
   TrendLine::Simple ChargeTemperature;
   Weather(td);
   void Assign(const ::Comp2::InputData&);
   void Suspend();
   void Resume();
  } Weather;
  struct Radar {
   struct ETD_Detection {
    Trend Distance;
    TrendLine::Simple D;
    Trend Angles;
    TrendLine::Spherical2D Common;
    TrendLine::Spherical2D Quadrature;
    ETD_Detection(td);
    void Assign(const ::ETD_Detection::InputData&);
    void Suspend();
    void Resume();
   } ETD_Detection;
   Radar(td);
   void Assign(const struct Interface::Command::Main::External::Radar&);
   void Suspend();
   void Resume();
  } Radar;
  struct Gun {
   Trend Pos;
   TrendLine::Spherical2DShip Position;
   Gun(td);
   void Assign(const struct Interface::Command::Main::External&);
   void Suspend();
   void Resume();
  } Gun;
  External(td);
  void Assign(const struct Interface::Command::Main::External&);
  void Suspend();
  void Resume();
 } External;
 struct Internal {
  struct GTC {
   Trend ORU;
   TrendLine::Spherical2DGround Ground;
   TrendLine::Spherical2DShip Ship;
   Trend ETD;
   TrendLine::Spherical2DGround ETD_Ground;
   GTC(td);
   void Assign(const Interface::DisplayRecord<ModuleGTC>&);
   void Suspend();
   void Resume();
  } GTC;
  struct DAT {
   Trend AM;
   TrendLine::Spherical2D AutotrackingMismatch;
   DAT(td);
   void Assign(const Interface::DisplayRecord<ModuleDAT>&);
   void Suspend();
   void Resume();
  } DAT;
  struct LRF {
   Trend Distances;
   TrendLine::Simple D1;
   TrendLine::Simple D2;
   TrendLine::Simple D3;
   LRF(td);
   void Assign(const ::LRF::InputData&);
   void Suspend();
   void Resume();
  } LRF;
  struct Training {
   Trend Distance;
   TrendLine::Simple D;
   Trend Angles;
   TrendLine::Spherical2DGround A;
   Training(td);
   void Assign(const Interface::DisplayRecord<ModuleTraining>&);
   void Suspend();
   void Resume();
  } Training;
  Internal(td);
  void Assign(const struct Interface::Command::Main::Internal&);
  void Suspend();
  void Resume();
 } Internal;
 struct Evaluations {
  struct Filter {
   struct In {
    Trend Angles;
    TrendLine::Spherical2DGround Ground;
    TrendLine::Spherical2DShip Ship;
    TrendLine::Spherical2D AutotrackingMismatch;
    Trend Distance;
    TrendLine::Simple D;
    void Assign(const FilterModule::CommandVector&);
    void Suspend();
    void Resume();
   } In;
   struct Out {
    Trend Angles;
    TrendLine::Spherical2DGround Ground;
    TrendLine::Spherical2DShip Ship;
    Trend Target;
    TrendLine::Cartesian3D Coords;
    Trend ShipMotion;
    TrendLine::Cartesian3D ShipCoords;
    void Assign(const FilterModule::StatusVector&);
    void Suspend();
    void Resume();
   } Out;
   void Assign(const struct Interface::Command::Main::Evaluations::Filter&);
   void Suspend();
   void Resume();
  };
  Filter FilterOptical;
  Filter FilterRadar;
  struct Collision {
   struct Out {
    Trend Angles;
    TrendLine::Spherical2DGround Ground;
    TrendLine::Spherical2DShip Ship;
    Trend Distances;
    TrendLine::Simple Target;
    TrendLine::Simple Meet;
    Trend Time;
    TrendLine::Simple Flight;
    struct Corrections {
     Trend ToDistance;
     TrendLine::Simple ToDistanceTailwind;
     TrendLine::Simple ToDistanceDencity;
     TrendLine::Simple ToDistanceStartSpeed;
     Trend ToHeight;
     TrendLine::Simple ToHeightTailwind;
     TrendLine::Simple ToHeightDencity;
     TrendLine::Simple ToHeightStartSpeed;
     Trend ToBearing;
     TrendLine::Degrees Derivation;
     TrendLine::Degrees Crosswind;
     TrendLine::Degrees ShipMotionHA;
     Trend ToElevation;
     TrendLine::Degrees AimingAngle;
     TrendLine::Degrees ShipMotionElev;
     void Assign(double, const CollisionTable::AllCorrections&);
     void Suspend();
     void Resume();
    } Corrections;
    void Assign(const CollisionTaskSolverModule::StatusVector&);
    void Suspend();
    void Resume();
   } Out;
   void Assign(const struct Interface::Command::Main::Evaluations::Collision&);
   void Suspend();
   void Resume();
  };
  Collision CollisionOptical;
  Collision CollisionRadar;
  struct Service {
   Trend ElapsedTime;
   TrendLine::Simple FilterOptical;
   TrendLine::Simple FilterRadar;
   TrendLine::Simple CollisionOptical;
   TrendLine::Simple CollisionRadar;
   TrendLine::Simple Cumulative;
   Service(td);
   void Assign(const struct Interface::Command::Main::Evaluations&);
   void Suspend();
   void Resume();
  } Service;
  Evaluations(td);
  void Assign(const struct Interface::Command::Main::Evaluations&);
  void Suspend();
  void Resume();
 } Evaluations;
 struct Trajectories {
  Trend XY;
  TrendLine::Curve XY_Optical;
  TrendLine::Curve XY_Radar;
  TrendLine::Curve XY_Ship;
  Trend XZ;
  TrendLine::Curve XZ_Optical;
  TrendLine::Curve XZ_Radar;
  TrendLine::Curve XZ_Ship;
  Trend YZ;
  TrendLine::Curve YZ_Optical;
  TrendLine::Curve YZ_Radar;
  TrendLine::Curve YZ_Ship;
  Trajectories(td);
  void Assign(const struct Interface::Command::Main::Evaluations& _);
  void Suspend();
  void Resume();
 } Trajectories;
 Trend SysTimer;
 TrendLine::Simple Tick;
public:
 TW(td);
 void Assign(const struct Interface::Command::Main&);
 void Suspend();
 void Resume();
};

#endif
