// radar ETD mode detection data
// (C) I.Drandar, 2019, all rights reserved
// Guidance and tracking controller - Radar

#include "ETD_DetectionData.h"
#include "DebugOut.h"

ModuleETD_Detection::FakeData::FakeData(const ModuleInit& init, const QString& s) :
 use(init.GetConst(s, "use")),
 TargetCount(static_cast<double>(init.GetConst(s, "target-count"))),
 Distance(init.GetConst(s, "distance")),
 CommonModeDeviation {
  init.GetConst(s, "common-HA"), init.GetConst(s, "common-elev")
 },
 QuadratureDeviation {
  init.GetConst(s, "quadrature-HA"), init.GetConst(s, "quadrature-elev")
 } {
}

ModuleETD_Detection::ModuleETD_Detection(const ModuleInit& init) :
 BASE(init), Fake(init, "fake") {
}

void ModuleETD_Detection::Act() {
 CommitReception();
}

void ModuleETD_Detection::DecodePacket(bool dump) {
 Input.OFT = static_cast<Radar::OperatingFrequencyTuning>(input.OFT);
 Input.FrequencyCode = input.FrequencyCode;
 Input.RepetitionPeriodCode = input.RepetitionPeriodCode;
 Input.RPA = static_cast<Radar::RepetitionPeriodAdjustment>(input.RPA);
 Input.TargetCount = input.TargetCount;
 Input.ANP = static_cast<Radar::ActiveNoisePresence>(input.ANP);
 Input.ActiveNoiseInterferenceProcessing = input.ActiveNoiseInterferenceProcessing;
 Input.First.RTS = static_cast<Types::RangeTrackingState>(input.First.RTS);
 Input.First.Distance = input.First.Distance;
 Input.First.CommonModeDeviation = input.First.CommonModeDeviation;
 Input.First.QuadratureDeviation = input.First.QuadratureDeviation;
 Input.First.SignalToNoiseRatio = input.First.SignalToNoiseRatio;
 Input.First.RadialSpeed = input.First.RadialSpeed;
 for (size_t i = 0; i < Input.Others.size(); ++i) {
  Input.Others[i].RTS = static_cast<Types::RangeTrackingState>(input.Others[i].RTS);
  Input.Others[i].Distance = input.Others[i].Distance;
  Input.Others[i].CommonModeDeviation = input.Others[i].CommonModeDeviation;
  Input.Others[i].SignalToNoiseRatio = input.Others[i].SignalToNoiseRatio;
  Input.Others[i].RadialSpeed = input.Others[i].RadialSpeed;
 }
 Input.LTS = input.LTS; // LTS time
 if (dump) {
  std::cout <<
   "Dump of received " << GetName() << "::Input packet\n"
   "operating frequency tuning: " << enum2str(Input.OFT) << "\t"
   "frequency code: " << Input.FrequencyCode << "\n"
   "repetition period code: " << Input.RepetitionPeriodCode << "\t"
   "repetition period adjustment: " << enum2str(Input.RPA) << "\n"
   "active noise interference processing: " << Input.ActiveNoiseInterferenceProcessing << "\t"
   "active noise presence: " << enum2str(Input.ANP) << "\n"
   "count of targets: " << Input.TargetCount << "\ntarget #0:\n"
   "range tracking state: " << enum2str(Input.First.RTS) << "\t"
   "distance: " << Input.First.Distance << " m\n"
   "common mode deviation: (°):\t" << Input.First.CommonModeDeviation << "\n"
   "quadrature deviation: (°):\t" << Input.First.QuadratureDeviation << "\n"
   "signal to noise ratio: " << Input.First.SignalToNoiseRatio << " dB\t"
   "radial speed: " << Input.First.RadialSpeed << "\n";
  for (size_t i = 0; i < Input.Others.size(); ++i) {
   std::cout <<
    "target #" << i + 1 << ":\nrange tracking state: " << enum2str(Input.Others[i].RTS) << "\t"
    "distance: " << Input.Others[i].Distance << " m\n"
    "common mode deviation: (°):\t" << Input.Others[i].CommonModeDeviation << "\n"
    "signal to noise ratio: " << Input.Others[i].SignalToNoiseRatio << " dB\t"
    "radial speed: " << Input.Others[i].RadialSpeed << "\n";
  }
  std::cout << "LTS time: " << Input.LTS << "\n";
 }
}

void ModuleETD_Detection::PrepareStatusVector(bool) {
 if (Fake.use) {
  Status.NewInfo = true;
  Status.ExchangeFault = false;
  auto& s = Status.data;
  s.OFT = Radar::OperatingFrequencyTuning{};
  if (s.OFT == Radar::OperatingFrequencyTuning::Random) {
   s.FrequencyCode = 0;
  }
  s.RPA = Radar::RepetitionPeriodAdjustment{};
  if (s.RPA == Radar::RepetitionPeriodAdjustment::Wobble) {
   s.RepetitionPeriodCode = 0;
  }
  s.TargetCount = Fake.TargetCount;
  auto t = counter.TimeFromProgramStart();
  s.ANP = Radar::ActiveNoisePresence{};
  s.ActiveNoiseInterferenceProcessing = s.TargetCount == 1;
  s.First = {};
  s.Others = {};
  if (s.TargetCount > 0) {
   s.First.RTS = Types::RangeTrackingState::AT;
   s.First.Distance = std::abs(Fake.Distance(t));
   s.First.CommonModeDeviation = {
    Fake.CommonModeDeviation.HA(t),
    Fake.CommonModeDeviation.Elev(t)
   };
   s.First.QuadratureDeviation = {
    Fake.QuadratureDeviation.HA(t),
    Fake.QuadratureDeviation.Elev(t)
   };
   s.First.SignalToNoiseRatio = {};
   s.First.RadialSpeed = {};
  }
  for (size_t i = 1; i < s.TargetCount; ++i) {
   auto& t = s.Others[i - 1];
   t.RTS = Types::RangeTrackingState{};
   t.Distance = {};
   t.CommonModeDeviation = {};
   t.SignalToNoiseRatio = {};
   t.RadialSpeed = {};
  }
  s.LTS = t;
 }
}
