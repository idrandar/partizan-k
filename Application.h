#ifndef IMITATOR_HH_
#define IMITATOR_HH_

/* The author: Alexander Ketner       */
/* Date of creation: 14 May 2007      */
/* IDE Momentics & QNX Neutrino 6.3.0 */
// Modified for "Sarmat" project
// System starting class
// (C) I.Drandar, 2010, all rights reserved
// (C) I.Drandar for Linux+Qt, 2018, all rights reserved

#include "cmxStrct.h"

class Application {
 static const char name[];
 Worker* cmxStrct;
public:
 Application();
 ~Application();
 void Start();
};
// #define     DEBUG_PROCESS   1

#endif /*IMITATOR_HH_*/
