#ifndef GUNMC_HH_
#define GUNMC_HH_

// Processing data of "Comp2" channels
// (C) I.Drandar, 2015-2017, all rights reserved
// (C) I.Drandar, for Qt + C++1x, 2018-2020, all rights reserved

#include "common/ExchangeModule.h"
#include "data_types.h"
#include "common/data_convert.h"
#include "GTC.h"
#include "LRF.h"
#include "IR.h"
#include "TV.h"
#include "DAT.h"
#include "GTC-2.h"
#include "NS.h"
#include "common/MathConst.h"

struct Comp2 { // works as namespace
 enum SysMode {
  mWork, mTesting, mTraining, mInitial
 };

 enum SubMode  {
  smInitial, smMP, smET, smAT, smIT,
  smSS, // standalone search
  stNONE
 };

 struct Degrees {
  Degrees() : _rep() {}
  Degrees(const double& _) : _rep(_ * MATH_CONST::RAD2DEG) {}
  void operator=(double _) { _rep = _ * MATH_CONST::RAD2DEG; }
  operator double() const { return _rep * MATH_CONST::DEG2RAD; }
 private:
  double _rep;
 };

 struct OutputData {
  struct State {
   SysMode Mode;
   enum SubMode SubMode;
   bool AdditionalSearch;
   bool LowFlyingTarget;
   bool ModeIsChanging;
   struct Ready {
    bool TV;
    bool IR;
    bool Navig;
   } Ready;
  } State;
  struct GTC_State {
   GTC::General_State State;
   bool Power;
   bool _7_2;
   unsigned int DATChannel; // 0 - own, 1 - SmTracker
   bool IIT;
   bool SyncRequest;
   bool SyncDone;
   SphericalCoords TrainingTarget;
   double Distance;
   struct GTC::InputData::ORU OEUC;
   Spherical2DCoordsType<bool> Mismatch;
   struct Faults { // maps Fault struct from GTC-2
    bool DeviceController;
    bool LRF;
    bool TV;
    bool IR;
    bool ASS;
    Spherical2DCoordsType<bool> EncoderExchange;
    Spherical2DCoordsType<bool> Detent;
    Spherical2DCoordsType<bool> Encoder;
    bool GTC; // ЦК.28
    bool ExchangeDAT;
    bool ExchangeComp;
    bool ExchangeDC;
    bool ExchangeLTS_DC;
    bool ExchangeNavig;
    bool Overheat;    // превышение критической температуры
    bool No24V;       // отсутствие 24 В
    bool NoPhase;     // отсутствие фаз
    bool Overcurrent; // превышение тока
    bool No7_2;       // не вкл. 7.2.
    // ПФ1.1.2
    bool AmplifierHAEngine;
    bool AmplifierHAA1;
    bool AmplifierElevEngine;
    bool AmplifierElevA3;
    bool ExchangeQ;
    bool ExchangeE;
   } Faults;
   Spherical2DCoords TV;
   Spherical2DCoords IR;
   double IR_Temperature;
   struct DAT_State {
    struct Channel {
     unsigned char Method;
     bool Break;
     bool Capture;
     bool TestingCompleted;
     unsigned int Credibility;
     bool Detection;
     bool Fault;
     DAT::Workmode Mode;
     DAT::TargetType TT;
     Spherical2DCoords AutoTrackingMismatch;
     void operator=(const DAT::InputData&);
    };
    Channel Channel[DAT_CHANNELS_COUNT];
   } DAT;
  } GTC;
  struct LRF_State {
   unsigned char Mode;
   bool ReceptionFault;
   bool ControlFault;
   bool TemperatureReady;
   bool Readiness;
   struct ::LRF::InputData::Faults Faults;
   struct ::LRF::InputData::State State;
   bool Measuring;
   unsigned int Distance1;
   unsigned int Distance2;
   unsigned int Distance3;
   unsigned int Resource;
  } LRF;
  struct BallisticModule {
   SysMode Mode;
   enum SubMode SubMode;
   struct State {
    bool TestingCompleted;
    bool PointingAngleReady;
    bool ITReady;
   } State;
   struct ExchangeFaults {
    bool Gun;
    bool Navig;
    bool GTC;
    bool Comp2;
   } ExchangeFaults;
   struct Limits {
    bool RightHA;
    bool LeftHA;
    bool HighElev;
    bool LowElev;
   } Limits;
   struct Faults {
    bool GunXExchange;
    bool GunHAExchange;
    bool GunHA;
    bool GunElevExchange;
    bool GunElev;
    bool GunTimerExchange;
    bool GunTimer;
   } Faults;
  } BallisticModule;
  struct GunState {
   Spherical2DShipCoords TowerPosition;
   Spherical2DCoords DesiredAngle;
   double CurrentDistance;
   double MeetDistance;
   double FlightTime;
  };
  GunState Gun;
  struct Navig {
   Rotations Rot;
   Rotations RotChange;
   Cartesian3DCoords SpeedPart;
   struct NS::InputData::Faults Faults;
  } Navig;
  double Time;
 };

 struct DriveCorrections {
  Spherical2DShipCoords Orientation;
  Spherical2DCoords PointingAngleMismatch;
  Spherical2DCoords SituationAngleMismatch;
  Spherical2DCoords Miss;
  DriveCorrections() = default;
 };

#pragma pack(push, 4)
 // layout of state packet's fields
 // this structure is compiler-dependent
 // possible use of conditional compilation needed in case of
 // transferring to other platform
 struct InputPacket {
  // constants
  // types
  // fields
  struct Control {
   struct  {
    unsigned char Mode : 2;
    unsigned char SubMode : 3;
    bool AdditionalSearch : 1;
    bool IIT_Enable : 1;
   } ModeAndSubmode;
   struct {
    bool WiperOn : 1;
    bool InternalHeatingOn : 1;
    unsigned char IROn : 1;
    bool Fire : 1;
    bool HeatStandby : 1;
    unsigned char Camera : 1;
    bool AirTarget : 1;
   } Command1;
   struct {
    unsigned char DATChannel : 1;
    bool Radar : 1; // 0 - DAT, 1 - radar
    bool LowFlyingTarget : 1;
   } Command2;
   struct Training {
    unsigned char Code : 5;
    bool StationaryTarget : 1;
    bool On : 1;
   } Training;
   SphericalCoordsType<Degrees> StationaryTargetCoords;
   SphericalCoordsType<Degrees> ExternalTarget;
   Cartesian2DCoordsType<short> Joystick;
   bool ManualDistanceOn;
   short ManualDistance;
   Spherical2DShipCoordsType<Degrees> OEUC;
  } Control;
  struct TV_Control {
   struct Focus {
    bool SmoothlyPlus : 1;
    bool SmoothlyMinus : 1;
    bool Near : 1; // false - far, true - near
    bool Smoothly : 1; // false - fixed, true - smoothly
    bool Manual : 1;   // false - autofocus
   } Focus;
   struct FieldOfView {
    bool SmoothlyPlus : 1;
    bool SmoothlyMinus : 1;
    bool Narrow : 1; // false - wide, true - narrow (zoom)
    bool Smoothly : 1; // false - fixed, true - smoothly
    bool : 2;
    bool Night : 1; // false - day, true - night
   } FOV;
   Cartesian2DCoordsType<char> CenterCorrection;
   unsigned char Brightness;
   unsigned char Contrast;
   std::byte Gap[2];
  } TV;
  struct IR_Control {
   struct Command {
    bool FocusFar : 1;
    bool FocusNear : 1;
    bool MenuOn : 1;
    bool ContrastPlus : 1;
    bool ContrastMinus : 1;
    bool BrightnessMinus : 1;
    bool Calibration : 1;
    bool : 1; // 7-th bit
    bool OpticalZoomPlus : 1;
    bool OpticalZoomMinus : 1;
    bool DigitalZoom : 1;
    bool : 2; // empty
    bool BrightnessPlus : 1; // a.k.a. right
    bool HotBlack : 1;
   } Command;
   Cartesian2DCoordsType<char> CenterCorrection;
   unsigned char Brightness;
   unsigned char Contrast;
   std::byte Gap[2];
  } IR;
  struct DAT_Control {
   bool Gap : 5;
   bool Rotation : 1;
   bool Mark : 1;
  } DAT_Control;
  std::byte Gap;
  struct ChannelControl {
   unsigned char Processing : 2;
   unsigned char TT : 1;
   unsigned char Source : 1;
   bool Capture : 1;
   bool Gap : 1;
   bool On : 1;
  } DAT1, DAT2;
  Cartesian2DCoordsType<short> DAT1_Coords;
  Cartesian2DCoordsType<short> DAT2_Coords;
  struct LRF_Control {
   unsigned char TargetNumber : 2;
   bool PilotLaser : 1;
   bool Camera : 1;
   bool Frequent : 1;
   bool Single : 1;
   bool Work : 1;
   unsigned char Frequency;
  } LRF;
  struct Weather {
   double WindSpeed;
   Degrees WindBearing;
   double Pressure;
   double Temperature; // air temperature in °C
   double ChargeTemperature;
  } Weather;
  struct GunControl {
   struct DriveControl {
    bool JointAlignment;
    bool ParallaxJointAlignment;
    double Distance;
   } DriveControl;
   struct DriveCorrections {
    Spherical2DShipCoordsType<Degrees> Orientation;
    Spherical2DCoordsType<Degrees> PointingAngleMismatch;
    Spherical2DCoordsType<Degrees> SituationAngleMismatch;
    Spherical2DCoordsType<Degrees> Miss;
    DriveCorrections() : PointingAngleMismatch(), SituationAngleMismatch(), Miss() {}
   } DriveCorrections;
   struct BallisticData {
    char Type;
    std::byte Gap;
    short StartSpeedMismatch;
    double MassMismatch;
   } BallisticData[2];
   bool Control;
   bool RemoteFuseControl;
  } GC;
  double Time;
  InputPacket() = default;
 };
#pragma pack(pop)

 struct InputData { // state data; transferred from Comp2 to CComp
  struct Control {
   SysMode Mode;
   enum SubMode SubMode;
   bool IIT_Enable;
   bool AdditionalSearch;
   bool WiperOn;
   bool InternalHeatingOn;
   DAT::CameraType Camera;
   bool Fire;
   bool AirTarget;
   bool IROn;
   bool HeatStandby;
   unsigned int DATChannel;
   bool Radar;
   bool LowFlyingTarget;
   struct Training {
    int Code;
    bool StationaryTarget;
    bool On;
    SphericalCoords StationaryTargetCoords;
    void operator=(const struct InputPacket::Control::Training&);
   } Training;
   SphericalCoords ExternalTarget;
   Cartesian2DCoordsType<int> Joystick;
   bool ManualDistanceOn;
   double ManualDistance;
   Spherical2DShipCoords OEUC;
   void operator=(const struct InputPacket::Control&);
  } Control;
  struct DAT_Control {
   bool Rotation;
   bool Mark;
   DAT::OutputData DAT1;
   DAT::OutputData DAT2;
  } DAT_Control;
  ::TV::OutputData TV;
  struct IR {
   bool FocusFar;
   bool FocusNear;
   struct Menu {
    bool On;
   } Menu;
   bool ContrastPlus;
   bool ContrastMinus;
   bool BrightnessPlus;
   bool BrightnessMinus;
   bool HotBlack;
   bool Calibration;
   bool OpticalZoomPlus;
   bool OpticalZoomMinus;
   bool DigitalZoom;
   unsigned int Brightness;
   unsigned int Contrast;
   Cartesian2DCoordsType<int> CenterCorrection;
  } IR;
  struct LRF {
   struct Signals {
    bool Work;
    bool Single;
    bool Frequent;
    bool PilotLaser;
    bool Camera;
   } Signals;
   double Frequency;
   unsigned int TargetNumber;
  } LRF;
  struct Weather Weather;
  struct GunControl {
   struct DriveControl {
    bool JointAlignment;
    bool ParallaxJointAlignment;
    bool GunControl;
    bool RemoteFuseControl;
    double Distance;
    DriveControl() = default;
   } DriveControl;
   struct DriveCorrections DriveCorrections;
   struct BallisticData {
    bool Type;
    double StartSpeedMismatch;
    double MassMismatch;
    void operator=(const struct InputPacket::GunControl::BallisticData&);
   } BalData[2];
   void operator=(const InputPacket::GunControl&);
  };
  GunControl GC;
  double Time;
 };

 struct CommandVector {};
 struct StatusVector {};

 // layout of control packet's fields
 // this structure is compiler-dependent
 // possible use of conditional compilation needed in case of
 // transferring to other platform
#pragma pack(push, 4)
 struct OutputPacket {
  // constants
  // types
  // fields
  struct State {
   SysMode Mode : 2;
   enum SubMode SubMode : 3;
   bool AdditionalSearch : 1;
   bool LowFlyingTarget : 1;
   struct Ready {
    bool TV : 1;
    bool IR : 1;
    bool : 1;
    bool Navig : 1;
    bool : 2;
    bool ModeIsChanging :1;
   } Ready;
   std::byte Gap[2];
   State() = default;
   State(const struct OutputData::State&);
  } State;
  struct GTC_State {
   struct {
    bool DetentHA : 1;
    bool DetentElev : 1;
    bool ModeIsChanging : 1;
    unsigned char Mode : 4;
   };
   //
   struct {
    bool Power : 1;
    bool _7_2: 1;
    bool : 1;
    unsigned char DATChannel : 1;
    bool IIT : 1;
    bool SyncDone : 1;
    bool SyncRequest : 1;
   };
   //
   bool HADriveOn : 1;
   bool ElevDriveOn : 1;
   bool ElevLimitHigh : 1;
   bool ElevLimitLow : 1;
   bool : 1;
   bool MismatchHA : 1;
   bool MismatchElev : 1;
   struct Faults {
    struct { // faults-0
     // ПФ7.2
     bool Overheat : 1;    // превышение критической температуры
     bool No24V : 1;       // отсутствие 24 В
     bool NoPhase : 1;     // отсутствие фаз
     bool Overcurrent : 1; // превышение тока
     bool No7_2 : 1;       // не вкл. 7.2.
    };
    struct { // faults-1
     //  ПФ1.1
     bool DeviceController : 1;
     bool LRF : 1;
     bool TV : 1;
     bool IR : 1;
     bool ASS : 1;
     bool EncoderHAExchange : 1;
     bool EncoderElevExchange : 1;
    };
    struct { // faults-2
     bool : 3; // D0-D2
     bool DetentHA : 1;
     bool DetentElev : 1;
     bool EncoderHA : 1;   // M2
     bool EncoderElev : 1; // M1
    };
    struct { // faults-3
     bool GTC : 1;   // ЦК.28
     bool : 1;       // deleted
     bool ExchangeDAT : 1;
     bool ExchangeComp : 1;
     bool ExchangeDC : 1;
     bool ExchangeLTS_DC : 1;
     bool ExchangeNavig : 1;
    };
    struct { // faults-4
     // ПФ1.1.2
     bool AmplifierHAEngine : 1;
     bool AmplifierHAA1 : 1;
     bool AmplifierElevEngine : 1;
     bool AmplifierElevA3 : 1;
     bool ExchangeQ : 1;
     bool ExchangeE : 1;
    };
   } Faults;
   SphericalCoordsType<Degrees> TrainingTarget;
   double Distance;
   struct OEUC_Coords {
    Spherical2DGroundCoordsType<Degrees> Ground; // ground coordinate system
    Spherical2DShipCoordsType<Degrees> Ship; // deck coordinate system OEUC position
    OEUC_Coords() : Ground(), Ship() {}
    OEUC_Coords(const struct GTC::InputData::ORU&);
   } OEUC_Coords;
   Spherical2DCoordsType<Degrees> TV;
   double IR_Temperature;
   Spherical2DCoordsType<Degrees> IR;
   struct DAT_State {
    struct Channel {
     unsigned char Method : 2;
     bool Break : 1;
     bool Capture : 1;
     bool TestingCompleted : 1;
     bool Fault : 1;
     DAT::Workmode Mode : 1;
     bool : 1;
     unsigned char Credibility : 5; // not used
     bool Detection : 1;
     char TT : 1;
     char : 1;
     // std::byte[2] Gap;
     Spherical2DCoordsType<Degrees> AutoTrackingMismatch;
     Channel() = default;
     Channel(const struct OutputData::GTC_State::DAT_State::Channel&);
    };
    Channel Channel1;
    Channel Channel2;
    DAT_State() = default;
    DAT_State(const OutputData::GTC_State::DAT_State&);
   } DAT;
   void operator=(const OutputData::GTC_State&);
  } GTC;
  struct LRF_State {
   char unsigned Mode : 3;
   bool Readiness : 1;
   bool ReceptionFault : 1;
   bool ControlFault : 1;
   bool TemperatureReady : 1;
   struct ::LRF::InputPacket::Faults Faults;
   struct ::LRF::InputPacket::State State;
   bool Measuring : 1;
   unsigned short Distance1;
   unsigned short Distance2;
   unsigned short Distance3;
   unsigned int Resource;
   LRF_State() = default;
   LRF_State(const OutputData::LRF_State&);
  } LRF;
  struct BallisticModule {
   SysMode Mode : 8;
   enum SubMode SubMode : 8;
   struct State {
    bool TestingCompleted : 1;
    bool PointingAngleReady : 1;
    bool : 1;
    bool ITReady : 1;
    State() = default;
    State(const struct OutputData::BallisticModule::State&);
   } State;
   struct ExchangeFaults {
    bool : 1;
    bool Gun : 1;
    bool Navig : 1;
    bool GTC : 1;
    bool Comp2 : 1;
    ExchangeFaults() = default;
    ExchangeFaults(const struct OutputData::BallisticModule::ExchangeFaults&);
   } ExchangeFaults;
   struct Limits {
    bool : 3;
    bool RightHA : 1;
    bool LeftHA : 1;
    bool HighElev : 1;
    bool LowElev : 1;
    Limits() = default;
    Limits(const struct OutputData::BallisticModule::Limits&);
   } Limits;
   struct Faults {
    bool GunXExchange : 1;     // нет обмена с модулем ПВ.19
    bool GunHAExchange : 1;    // нет обмена с модулем ПА.10-1
    bool GunHA : 1;       // неисправность модуля ПА.10-1
    bool GunElevExchange : 1;  // нет обмена с модулем ПА.10-2
    bool GunElev : 1;     // неисправность модуля ПА.10-2
    bool GunTimerExchange : 1; // нет обмена с модулем ПА.10-3
    bool GunTimer : 1;    // неисправность модуля ПА.10-3
    Faults() = default;
    Faults(const struct OutputData::BallisticModule::Faults&);
   } Faults;
   std::byte StatusFaults;
   BallisticModule() = default;
   BallisticModule(const struct OutputData::BallisticModule&);
  } BallisticModule;
  struct GunState {
   Spherical2DShipCoordsType<Degrees> TowerPosition;
   Spherical2DCoordsType<Degrees> DesiredAngle;
   double CurrentDistance;
   double MeetDistance;
   double FlightTime;
   GunState() = default;
   GunState(const OutputData::GunState&);
  } Gun;
  struct Navig {
   struct Faults {
    bool Gap : 2;
    bool HeaderChecksum : 1;
    bool DataChecksum : 1;
    bool Count : 1;
    bool General : 1;
    bool Off : 1;
   } Faults;
   RotationsType<Degrees> Rot;
   RotationsType<Degrees> RotChange;
   Cartesian3DCoords Speed;
   Navig() = default;
   Navig(const struct OutputData::Navig&);
  } Navig;
  double Time;
  void operator=(const OutputData&);
  OutputPacket(const OutputData&);
  OutputPacket() = default;
 };
};
#pragma pack(pop)

class ModuleComp2 : public ExchangeModuleImpl<Comp2> {
 const struct FakeData {
  const bool use;
  const Types::SysMode Mode;
  const Function SubMode;
  struct Gun {
   struct DriveCorrections {
    const Spherical2DCoords PointingAngle;
    Spherical2DCoords SituationAngle;
    Spherical2DCoords Miss;
    operator Types::DriveCorrections() const;
    DriveCorrections(const ModuleInit&, const QString& section, const QString& prefix);
   } DriveCorrections;
   struct BallisticData {
    const double StartSpeedMismatch;
    const double MassMismatch;
    operator struct Types::InputData::GunControl::BallisticData() const;
    BallisticData(const ModuleInit&, const QString& section, const QString& prefix);
   } BallisticData;
   Gun(const ModuleInit&, const QString& section, const QString& prefix);
  } Gun;
  struct Weather {
   const Function WindSpeed;
   const Function WindBearing;
   const Function Pressure;
   const Function Temperature; // air temperature in °C
   const Function ChargeTemperature;
   Weather(const ModuleInit&, const QString& section);
  };
  const struct Weather Weather;
  const bool LRFSingle;
  const bool LRFFrequent;
  const bool LRFWork;
  FakeData(const ModuleInit&, const QString&);
 } Fake;
 void DecodePacket(bool) override;
 void EncodePacket(bool) override;
 void PrepareStatusVector(bool) override;
public:
 explicit ModuleComp2(const ModuleInit&);
 void Act() override;
}; // end

#endif /*GUNMC_HH_*/
