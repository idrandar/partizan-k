#ifndef ETD_DETECTIONDATA_H
#define ETD_DETECTIONDATA_H

// radar ETD mode detection data
// (C) I.Drandar, 2019, all rights reserved
// Guidance and tracking controller - Radar

#include "common/ExchangeModule.h"
#include "RadarCommon.h"
#include "data_types.h"

struct ETD_Detection {

 static constexpr size_t TargetsArraySize = 3;
 template <typename T>
 using TargetsArray = std::array<T, TargetsArraySize>;

 enum class RangeTrackingState : unsigned char {
  NoCapture, AT, IT, Break
 };

 struct FirstTarget {
  RangeTrackingState RTS;
  double Distance;
  Spherical2DCoords CommonModeDeviation;
  Spherical2DCoords QuadratureDeviation;
  double SignalToNoiseRatio;
  double RadialSpeed;
 };
 struct Target {
  RangeTrackingState RTS;
  double Distance;
  Spherical2DCoords CommonModeDeviation;
  double SignalToNoiseRatio;
  double RadialSpeed;
 };
 struct InputData {
  Radar::OperatingFrequencyTuning OFT;
  unsigned int FrequencyCode;
  unsigned int RepetitionPeriodCode;
  Radar::RepetitionPeriodAdjustment RPA;
  unsigned int TargetCount;
  Radar::ActiveNoisePresence ANP;
  bool ActiveNoiseInterferenceProcessing;
  FirstTarget First;
  TargetsArray<Target> Others;
  double LTS; // LTS time
 };

 struct InputPacket {
  union {
   struct {
    unsigned char : 5;
    unsigned char OFT : 2;
   };
   Bits7Shared<unsigned int, 9> FrequencyCode;
  };
  union {
   Bits7Shared<unsigned int, 6> RepetitionPeriodCode;
   struct {
    unsigned char : 6;
    unsigned char RPA : 1;
   };
  };
  unsigned char TargetCount : 3;
  unsigned char : 1;
  unsigned char ANP : 2;
  bool ActiveNoiseInterferenceProcessing : 1;
  using DistanceType = Bits7Shared<unsigned int, 16>;
  using DeviationType = Spherical2DCoordsType<Packed::SignedAngle>;
  using STNRType = Bits7<Upscale<unsigned int, 4>, 7>;
  using RSType = Bits7<Upscale<int, 8>, 14>;
  struct {
   union {
    struct {
     unsigned char : 2;
     unsigned char RTS : 3;
    };
    DistanceType Distance;
   };
   DeviationType CommonModeDeviation;
   DeviationType QuadratureDeviation;
   STNRType SignalToNoiseRatio;
   RSType RadialSpeed;
  } First;
  struct LowLevelTarget {
   union {
    struct {
     unsigned char : 2;
     unsigned char RTS : 3;
    };
    DistanceType Distance;
   };
   DeviationType CommonModeDeviation;
   STNRType SignalToNoiseRatio;
   RSType RadialSpeed;
  };
  TargetsArray<LowLevelTarget> Others;
  Packed::LTS_Type LTS; // LTS time
 };

 using OutputData = NOTHING;
 using OutputPacket = NOTHING;

 using CommandVector = NOTHING;
 struct StatusVector {};
};

template <>
inline constexpr std::array enum_rep<ETD_Detection::RangeTrackingState> = {
 "no capture",  "AT", "IT", "break"
};

class ModuleETD_Detection : public ExchangeModuleImpl<ETD_Detection> {
 const struct FakeData {
  const bool use;
  const size_t TargetCount;
  const Function Distance;
  Spherical2DCoordsType<Function> CommonModeDeviation;
  Spherical2DCoordsType<Function> QuadratureDeviation;
  FakeData(const ModuleInit&, const QString&);
 } Fake;
 void DecodePacket(bool) override;
 void EncodePacket(bool) override {}
 void PrepareStatusVector(bool) override;
public:
 ModuleETD_Detection(const ModuleInit&);
 void Act() override;
};

#endif // ETD_DETECTIONDATA_H
