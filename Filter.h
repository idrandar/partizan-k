/*
 * Filter.hh
 *
 *  Created on: 14.09.2015
 *      Author: Drandar
 */

#ifndef FILTER_HH_
#define FILTER_HH_

// Filter class
// (C) I.Drandar, 2015-2016, all rights reserved
// (C) I.Drandar, for Qt, 2018, all rights reserved

#include "common/module.h"
#include "common/Coordinates.h"
#include "data_types.h"
#include "ExtrapolationPolynomials.h"

class FilterModule : public Module {
public:
 struct OutData {
  struct Cartesian3DCoordsExtended {
   Cartesian3DCoords Coords;
   double Time;
   Cartesian3DCoordsExtended() : Coords(), Time() {}
   Cartesian3DCoordsExtended(const Cartesian3DCoords& Coords, double Time) : Coords(Coords), Time(Time) {}
  };

  double Time;
  bool Completed; // Dn: Done
  size_t CoordinatesCount; // For current filter. If Filter Simpl: CoordinatesCount=1;
  Cartesian3DCoordsExtended TargetGroundCart; // Counted Tg coordinates on current Tackt filter
  Cartesian3DCoords PolynomialCoeffs[5];

  void Set_FilterNotNeeded(double t); // if No Need to do Tact flt
  void Set_FilterNeededNotDone(double t); // if Need    to do Tact flt, but not Done
  void Set_FilterSimple(double t, const Cartesian3DCoordsExtended&);
  void SetTargetCoord(double t) {
   TargetGroundCart.Time = t;
   const double TimeOffset = t - Time;
   TargetGroundCart.Coords = Cart(TimeOffset);
  }

  Cartesian3DCoords Cart(double dt, size_t range = 4) const;
  OutData() : Time(), Completed(), CoordinatesCount(), TargetGroundCart(), PolynomialCoeffs() {}
 };
 typedef Angles_Data InData;
 struct StatusVector {
  OutData data;
  bool PerformedFiltrationInCurrentMode;
  int Performed; // counter
  ExtrapolationPolynomials::All Extrapolation;
  double ElapsedTime;
  Spherical2DGroundCoords ETD_IT;
  bool AirTargetChanged;
  double Angles_TimeOffset(double Time) const { return Time - Extrapolation.Angles.Time; }
  double Distance_TimeOffset(double Time) const { return Time - Extrapolation.Distance.Time; }
  double NS_TimeOffset(double Time) const { return Time - Extrapolation.NS.Time; }
  StatusVector() :
   data(), PerformedFiltrationInCurrentMode(), Performed(), Extrapolation(), ElapsedTime() {}
 };
 struct CommandVector {
  InData data;
  SystemMode Mode;
  DistanceData DD;
  struct NS_Data NS_Data;
  Spherical2DCoords InertialtrackingMismatch;
  bool AirTarget;
  CommandVector() :
   data(), Mode(), DD(), NS_Data(), InertialtrackingMismatch(), AirTarget() {}
 };
private:
 class FilterCaller;
 Module::Thread Calculator;
 status Calculate();
 CommandVector Command;
 StatusVector Status;
 // temp
 CommandVector cmd;
 StatusVector st;
 bool HaveAngleDataInCurrentMode;
 bool HaveNavigationDataInCurrentMode;
 Cartesian3DCoords ModeIT_Corrections;
 double Time;
 SystemMode PreviousMode;

 const Cartesian3DCoords OEUC_Location;
 struct MathVar { // переменные для математических вычислений
  static const size_t CoordsArraySize = 100; // Размерность скользящих выборок координат цели
  static const size_t HypothesesArraySize = 30; // Размерность скользящих выборок текущих
  const double Tau0; // period

  std::vector<Cartesian3DCoords> TargetCoeff; // Оценки коэф-тов полинома движения цели[номер коорд., номер коэффициента]

  std::vector<Cartesian3DCoords> Coordinates; // Скользящие массивы текущих вычисленных координат цели
  std::vector<Cartesian3DCoordsType<int> > Hypotheses; // Скользящие массивы текущих выбранных   гипотез

  // Константы системы ортогональных полиномов Чебышева
  std::vector<double> fi1, fi2, fi3, fi4; // Массивы значений ортогональных полиномов 1 - 4 степени
  double ff[5]; // Суммы квадратов значений ортогональных полиномов
  double a10; // Коэффициенты ортогонального полинома  1-ой степени
  double a21, a20; // Коэффициенты ортогонального полинома  2-ой степени
  double a32, a31, a30; // Коэффициенты ортогонального полинома  3-ой степени
  double a43, a42, a41, a40; // Коэффициенты ортогонального полинома  4-ой степени

  // Константы  для выбора гипотез
  double F0, F1, F2, F3, F4;

  unsigned long CountHp; // Счетчик тактов вычисленных гипотез
  Cartesian3DCoordsType<int> nHp_max;

  MathVar(double t) :
   Tau0(t),
   TargetCoeff(5), Coordinates(CoordsArraySize), Hypotheses(HypothesesArraySize),
   fi1(CoordsArraySize), fi2(CoordsArraySize), fi3(CoordsArraySize), fi4(CoordsArraySize),
   CountHp(), nHp_max { 1, 1, 1 } {
   PreCountConst();
  }
  void PreCountConst();
  void Push(const OutData&);
  void AutoHyp();
  double TimeOffset(int) const;
 } MV;

 const struct TestDump {
  const bool Input;
  const bool Output;
  const bool EvaluatedDistance;
  const bool ExtrapolationOK;
  const bool Time;
  const bool HaveAngleDataInCurrentMode;
  const bool ElapsedTime;
  TestDump(const ModuleInit&, const QString&);
 } TestDump;
 const struct Algorithm {
  const double DistanceMin;
  const bool AlwaysUseOEUC;
  const bool UseShipCoords;
  const bool AirTarget;
  const bool TargetTypeExternal;
  Algorithm(const ModuleInit&, const QString&);
 } Algorithm;
 bool AirTarget;
 bool AirTargetPrevious;
 const Rotations GetRotations() const;
 const Cartesian3DCoords TargetOffset(const Rotations&) const;
 void ModeIT_CorrectFilterOutput(const Cartesian3DCoords&);
 void SetInit(OutData&);
 void EvaluateGround(double);
 void Finalize();
public:
 explicit FilterModule(const ModuleInit&);
 virtual ~FilterModule() {}
 virtual void Start();
 virtual void Stop();
 void Interact(const CommandVector&, StatusVector&);
 bool UseOEUC() const { return Algorithm.AlwaysUseOEUC; }
 const Cartesian3DCoords GetOEUCLocation() const { return OEUC_Location; }
};

#endif /* FILTER_HH_ */
