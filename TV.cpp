#include "TV.h"
#include "DebugOut.h"

// TV exchange
// (C) I.Drandar, 2018, all rights reserved

ModuleTV::ModuleTV(const ModuleInit& _) : BASE(_) {}

std::ostream& operator<<(std::ostream& _, const TV::OutputData& T) {
 _ <<
  "focus:\nsmoothly:\t+: " << T.Focus.SmoothlyPlus << "\t-: " << T.Focus.SmoothlyMinus << "\n"
  "near: " << T.Focus.Near << "\tsmoothly:" << T.Focus.Smoothly <<
  "\tmanual: " << T.Focus.Manual << "\n"
  "field of view:\n+: " << T.FOV.SmoothlyPlus << "\t-: " << T.FOV.SmoothlyMinus << "\n"
  "narrow:\t" << T.FOV.Narrow << "\tsmoothly:\t" << T.FOV.Smoothly << "\n"
  "night node:\t" << T.Night << "\n"
  "brightness: " << T.Brightness << "\t"
  "contrast: " << T.Contrast << "\t"
  "center correction:\t" << T.CenterCorrection;
 return _;
}

bool operator!=(const TV::OutputData& l, const TV::OutputData& r) {
 return
  l.Focus.SmoothlyPlus != r.Focus.SmoothlyPlus || l.Focus.SmoothlyMinus != r.Focus.SmoothlyMinus ||
  l.Focus.Smoothly != r.Focus.Smoothly || l.Focus.Near != r.Focus.Near || l.Focus.Manual != r.Focus.Manual ||
  l.FOV.SmoothlyPlus != r.FOV.SmoothlyPlus || l.FOV.SmoothlyMinus != r.FOV.SmoothlyMinus ||
  l.FOV.Smoothly != r.FOV.Smoothly || l.FOV.Narrow != r.FOV.Narrow ||
  l.Brightness != r.Brightness || l.Contrast != r.Contrast || l.Night != r.Night ||
  l.CenterCorrection != r.CenterCorrection;
}

void ModuleTV::Act() {
 if (Command.Go)
  Transmit();
}

void ModuleTV::EncodePacket(bool dump) {
 // TV control
 output.Focus.SmoothlyPlus = Output.Focus.SmoothlyPlus;
 output.Focus.SmoothlyMinus = Output.Focus.SmoothlyMinus;
 output.Focus.Near = Output.Focus.Near;
 output.Focus.Smoothly = Output.Focus.Smoothly; // false - fixed, true - smoothly
 output.Focus.Manual = Output.Focus.Manual;
 output.Focus.Night = Output.Night; // false - day, true - night
 output.FOV.SmoothlyPlus = Output.FOV.SmoothlyPlus;
 output.FOV.SmoothlyMinus = Output.FOV.SmoothlyMinus;
 output.FOV.Narrow = Output.FOV.Narrow; // false - wide, true - narrow (zoom)
 output.FOV.Smoothly = Output.FOV.Smoothly;
 output.FOV.Manual = Output.FOV.Manual;
 output.Brightness = Output.Brightness;
 output.Contrast = Output.Contrast;
 output.CenterCorrection = Output.CenterCorrection;
 if (dump)
  std::cout <<
   "Dump of received " << GetName() << "::Input packet\n"
   "TV camera control:\n" << Output << "\n";
}

void ModuleTV::DecodePacket(bool) {}

