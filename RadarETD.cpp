// radar ETD mode
// (C) I.Drandar, 2019, all rights reserved
// Guidance and tracking controller - Radar

#include "RadarETD.h"
#include "DebugOut.h"

bool operator==(const RadarETD::OutputData& l, const RadarETD::InputData& r) {
 return
  l.EmissionPermitted == r.EmissionPermitted &&
  l.Antenna == r.Antenna && l.TT == r.TT &&
  l.Distance == r.Distance && l.LFM_SlopeNegative == r.LFM_SlopeNegative &&
  l.DS == r.DS && l.SignalCode == r.SignalCode && l.OFT == r.OFT &&
  l.FrequencyCode == r.FrequencyCode && l.RPA == r.RPA &&
  l.RepetitionPeriodCode == r.RepetitionPeriodCode &&
  l.MTS1 == r.MTS1 && l.MTS2 == r.MTS2 && l.FFT == r.FFT &&
  l.ActiveNoiseAnalysis == r.ActiveNoiseAnalysis &&
  l.OwnMovementCompensation == r.OwnMovementCompensation &&
  l.ActiveInterferenceAngularTracking == r.ActiveInterferenceAngularTracking &&
  l.SumDeltaChannelsCalibration == r.SumDeltaChannelsCalibration &&
  l.RangeCalibration == r.RangeCalibration && l.PulseCount == r.PulseCount;
}

template <>
constexpr std::array enum_rep<RadarETD::TargetType> = {
 "not determined", "air", "low flying", "helicopter", "surface", "coastal"
};

std::ostream& operator<<(std::ostream& _, const RadarETD::InputData& d) {
 _ <<
  "emission permission: " << d.EmissionPermitted << "\t"
  "emission permission physical: " << d.EmissionPermittedPhysical << "\t"
  "antenna id on: " << d.Antenna << "\ttarget type: " << enum2str(d.TT) << "\n"
  "distance: " << d.Distance << " m\tLFM slope is negative: " << d.LFM_SlopeNegative << "\n"
  "distance scale: " << enum2str(d.DS) << "\tsignal code #" << d.SignalCode << "\n"
  "operating frequency tuning: " << enum2str(d.OFT) << "\t"
  "frequency code #" << d.FrequencyCode << "\n"
  "repetition period adjustment: " << enum2str(d.RPA) << "\t"
  "repetition period code #" << d.RepetitionPeriodCode << "\n"
  "moving target selection 1: " << enum2str(d.MTS1) << "\t"
  "moving target selection 2: " << enum2str(d.MTS2) << "\n"
  "fast Fourier transform: " << enum2str(d.FFT) << "\t"
  "active noise analysis enabled: " << d.ActiveNoiseAnalysis << "\n"
  "own movement compensation enabled: " << d.OwnMovementCompensation << "\t"
  "active interference angular tracking enabled: " << d.ActiveInterferenceAngularTracking << "\n"
  "sum-delta channels calibration enabled: " << d.SumDeltaChannelsCalibration << "\t"
  "range calibration enabled: " << d.RangeCalibration << "\tpulse count: " << d.PulseCount;
 return _;
}

std::ostream& operator<<(std::ostream& _, const RadarETD::OutputData& d) {
 _ <<
  "emission permission: " << d.EmissionPermitted << "\t"
  "antenna id on: " << d.Antenna << "\ttarget type: " << enum2str(d.TT) << "\n"
  "distance: " << d.Distance << " m\tLFM slope is negative: " << d.LFM_SlopeNegative << "\n"
  "distance scale: " << enum2str(d.DS) << "\tsignal code #" << d.SignalCode << "\n"
  "operating frequency tuning: " << enum2str(d.OFT) << "\t"
  "frequency code #" << d.FrequencyCode << "\n"
  "repetition period adjustment: " << enum2str(d.RPA) << "\t"
  "repetition period code #" << d.RepetitionPeriodCode << "\n"
  "moving target selection 1: " << enum2str(d.MTS1) << "\t"
  "moving target selection 2: " << enum2str(d.MTS2) << "\n"
  "fast Fourier transform: " << enum2str(d.FFT) << "\t"
  "active noise analysis enabled: " << d.ActiveNoiseAnalysis << "\n"
  "own movement compensation enabled: " << d.OwnMovementCompensation << "\t"
  "active interference angular tracking enabled: " << d.ActiveInterferenceAngularTracking << "\n"
  "sum-delta channels calibration enabled: " << d.SumDeltaChannelsCalibration << "\t"
  "range calibration enabled: " << d.RangeCalibration << "\tpulse count: " << d.PulseCount;
 return _;
}

ModuleRadarETD::FakeData::FakeData(const ModuleInit& init, const QString& s) :
 use(init.GetConst(s, "use")) {
}

ModuleRadarETD::ModuleRadarETD(const ModuleInit& init) :
  BASE(init), Fake(init, "fake") {
}

void ModuleRadarETD::Act() {
 if (Command.Go) {
  Transmit();
 }
 CommitReception();
}

void ModuleRadarETD::EncodePacket(bool dump) {
 output.EmissionPermitted = Output.EmissionPermitted;
 output.Antenna = Output.Antenna; // 0 - equivalent, 1 - antenna
 output.TT = static_cast<unsigned char>(Output.TT);
 output.Distance = Output.Distance;
 output.LFM_SlopeNegative = Output.LFM_SlopeNegative;
 output.DS = static_cast<unsigned char>(Output.DS);
 output.SignalCode = Output.SignalCode;
 output._.OFT = static_cast<unsigned char>(Output.OFT);
 output._.FrequencyCode = Output.FrequencyCode;
 output.__.RPA = static_cast<unsigned char>(Output.RPA);
 output.__.RepetitionPeriodCode = Output.RepetitionPeriodCode;
 output.MTS1 = static_cast<unsigned char>(Output.MTS1);
 output.___.MTS2 = static_cast<unsigned char>(Output.MTS2);
 output.FFT = static_cast<unsigned char>(Output.FFT);
 output.ActiveNoiseAnalysis = Output.ActiveNoiseAnalysis;
 output.OwnMovementCompensation = Output.OwnMovementCompensation;
 output.___.ActiveInterferenceAngularTracking = Output.ActiveInterferenceAngularTracking;
 output.___.SumDeltaChannelsCalibration = Output.SumDeltaChannelsCalibration; // 0 - disabled, 1 - enabled
 output.___.RangeCalibration = Output.RangeCalibration; // 0 - disabled, 1 - enabled
 output.___.PulseCount = Output.PulseCount;
 if (dump) {
  std::cout <<
   "Dump of transmitted " << GetName() << "::Output packet\n" <<
   Output << "\n";
 }
}

void ModuleRadarETD::DecodePacket(bool dump) {
 Input.EmissionPermitted = input.EmissionPermitted;
 Input.EmissionPermittedPhysical = input.EmissionPermittedPhysical;
 Input.Antenna = input.Antenna; // 0 - equivalent, 1 - antenna
 Input.TT = static_cast<Types::TargetType>(input.TT);
 Input.Distance = input.Distance;
 Input.LFM_SlopeNegative = input.LFM_SlopeNegative;
 Input.DS = static_cast<Radar::DistanceScale>(input.DS);
 Input.SignalCode = input.SignalCode;
 Input.OFT = static_cast<Radar::OperatingFrequencyTuning>(input._.OFT);
 Input.FrequencyCode = input._.FrequencyCode;
 Input.RPA = static_cast<Radar::RepetitionPeriodAdjustment>(input.__.RPA);
 Input.RepetitionPeriodCode = input.__.RepetitionPeriodCode;
 Input.MTS1 = static_cast<Radar::MovingTargetSelection>(input.MTS1);
 Input.MTS2 = static_cast<Radar::MovingTargetSelection>(input.___.MTS2);
 Input.FFT = static_cast<Radar::FastFourierTransform>(input.FFT);
 Input.ActiveNoiseAnalysis = input.ActiveNoiseAnalysis;
 Input.OwnMovementCompensation = input.OwnMovementCompensation;
 Input.ActiveInterferenceAngularTracking = input.___.ActiveInterferenceAngularTracking;
 Input.SumDeltaChannelsCalibration = input.___.SumDeltaChannelsCalibration; // 0 - disabled, 1 - enabled
 Input.RangeCalibration = input.___.RangeCalibration; // 0 - disabled, 1 - enabled
 Input.PulseCount = input.___.PulseCount;
 if (dump) {
  std::cout <<
   "Dump of received " << GetName() << "::Input packet\n" <<
   Input << "\n";
 }
}

void ModuleRadarETD::PrepareStatusVector(bool) {
 if (Fake.use) {
  Status.NewInfo = true;
  Status.ExchangeFault = false;
  auto& s = Status.data;
  const auto& c = Command.data;
  s.EmissionPermitted = c.EmissionPermitted;
  s.EmissionPermittedPhysical = c.EmissionPermitted;
  s.Antenna = c.Antenna; // 0 - equivalent, 1 - antenna
  s.TT = c.TT;
  s.Distance = c.Distance;
  s.LFM_SlopeNegative = c.LFM_SlopeNegative;
  s.DS = c.DS;
  s.SignalCode = c.SignalCode;
  s.OFT = c.OFT;
  s.FrequencyCode = c.FrequencyCode;
  s.RPA = c.RPA;
  s.RepetitionPeriodCode = c.RepetitionPeriodCode;
  s.MTS1 = c.MTS1;
  s.MTS2 = c.MTS2;
  s.FFT = c.FFT;
  s.ActiveNoiseAnalysis = c.ActiveNoiseAnalysis;
  s.OwnMovementCompensation = c.OwnMovementCompensation;
  s.ActiveInterferenceAngularTracking = c.ActiveInterferenceAngularTracking;
  s.SumDeltaChannelsCalibration = c.SumDeltaChannelsCalibration; // 0 - disabled, 1 - enabled
  s.RangeCalibration = c.RangeCalibration; // 0 - disabled, 1 - enabled
  s.PulseCount = c.PulseCount;
 }
}
