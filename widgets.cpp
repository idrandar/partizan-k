// "Sarmat" project
// List of widgets
// (C) I.Drandar, 2010-2018, all rights reserved
// (C) I.Drandar, for Qt + C++1x, 2018-2020, all rights reserved

#include "widgets.h"
#include "ui_mainwindow.h"
#include "Setup.h"
#include "ui_trendswindow.h"
#include "common/CoordinatesConversion.h"
#include "DebugOut.h"

// Submodes
const ColorStringPair VI::SubModeStr[] = { { "", "grey" }, // Initial
 { "ЦВ", "lime" }, { "РС", "lime" }, { "АС", "lime" }, { "ІС", "lime" }, { "", "grey" }, // SYSMODE_Testing
 { "ВД", "lime" }, { "РН", "lime" }, { "Завершення", "lime" }, { "УІС", "lime" }, { nullptr, nullptr } };
// System functional faults
const ColorStringPair VI::SystemFuncFaultStr[] = {
 { "", "grey" }, // no error
 { "Обмiн RS-422", "red" }, { "Обмiн Ethernet", "red" },
 { "Датчик локального часу", "red" }, { "Відсутні навігаційні дані", "red" },
 { "Контроль закінчено\nВиявлено несправність", "red" },
 { "Контроль закінчено\nНесправностей не виявлено",  "lime" },
 { nullptr, nullptr } };

VI::VI(ui _) : System(_), Main(_) {}

VI::System::System(ui _) :
// ShowAuxiliaryTrends(ABW_btnAuxiliaryTrends),
 SubMode(_.lblSubMode, ColorStringTab(SubModeStr)),
 FuncFault(_.lblSystemFuncFault, ColorStringTab(SystemFuncFaultStr)), TD(_) {
}

VI::System::TD::TD(ui _) :
 Accept(), Pane(_.grpETD),
 Distance(_.lblETDDistance), Bearing(_.lblETDHA), Elevation(_.lblETDElevation) {
 Pane.Hide();
}

void VI::System::TD::Assign(const struct Interface::Command::_System::TD& _) {
 if (_.Accept != Accept) {
  Accept = _.Accept;
  if (Accept)
   Pane.Show();
  else
   Pane.Hide();
 }
 if (Accept) {
  Distance.Assign(_.Distance);
  Bearing.Assign(_.Bearing);
  Elevation.Assign(_.Elevation);
 }
}

const ColorStringPair VI::Main::SolutionResultStr[] = {
 { "обчислення виконано", "lime" },
 { "занадто низько", "red" },
 { "занадто високо", "red" },
 { "занадто близько", "red" },
 { "занадто далеко", "red" },
 { "помилка у таблицi", "red" },
 { "помилка у вхiдних даних", "red" },
 { "завеликий час польоту", "red" },
 { "завелика кiлькiсть iтерацiй", "red" },
 { "обчислення не потрiбнi", "grey" },
 { "неможливо обрахувати координати цiлi", "red" },
 { "невiдома причина", "yellow" }, { nullptr, nullptr } };

VI::Main::Main(ui _) :
 External(_), Internal(_), Evaluations(_) {
}

void VI::Main::Assign(const struct Interface::Command::Main& _) {
// FaultsPane.Assign(_.Faults);
 Internal.Assign(_.Internal);
 External.Assign(_.External);
 Evaluations.Assign(_.Evaluations);
}

void VI::Main::GTC_ORU::Assign(const struct ::GTC::InputData::ORU& _) {
 DriveOn.Assign(_.DriveOn);
 ElevLimitHigh.Assign(_.ElevLimitHigh);
 ElevLimitLow.Assign(_.ElevLimitLow);
 Ground.Assign(_.Ground);
 Ship.Assign(_.Ship); // deck coordinate system OEUC position
}

void VI::Main::GTC_Faults::Assign(const GTC2::Faults& _) {
 // ПФ1.1
 DeviceController.Assign(_.DeviceController);
 LRF.Assign(_.LRF);
 TV.Assign(_.TV);
 IR.Assign(_.IR);
 ASS.Assign(_.ASS);
 EncoderExchange.Assign(_.EncoderExchange);
 // motors
 Encoder.Assign(_.Encoder);
 // ПФ4.1
 GTC.Assign(_.GTC);
 ExchangeDAT.Assign(_.ExchangeDAT);
 ExchangeComp.Assign(_.ExchangeComp);
 ExchangeDC.Assign(_.ExchangeDC);
 ExchangeLTS_DC.Assign(_.ExchangeLTS_DC);
 ExchangeNavig.Assign(_.ExchangeNavig);
 // ПФ7.2
 Overheat.Assign(_.Overheat);
 No24V.Assign(_.No24V);
 No7_2.Assign(_.No7_2);
 NoPhase.Assign(_.NoPhase);
 Overcurrent.Assign(_.Overcurrent);
 // Доп. неисп. НК КС
 NS_NoH.Assign(_.NS_NoH); // не заг.
 NS_NoD.Assign(_.NS_NoD); // не дан.
 // ПФ1.1.2
 AmplifierHAEngine.Assign(_.AmplifierHAEngine);
 AmplifierHAA1.Assign(_.AmplifierHAA1);
 AmplifierElevEngine.Assign(_.AmplifierElevEngine);
 AmplifierElevA3.Assign(_.AmplifierElevA3);
 ExchangeQ.Assign(_.ExchangeQ);
 ExchangeE.Assign(_.ExchangeE);
}

void VI::Main::TV_Control::Assign(const TV::OutputData& _) {
 Focus.Assign(_.Focus);
 FOV.Assign(_.FOV);
 Night.Assign(_.Night); // false - day, true - night
 CenterCorrection.Assign(_.CenterCorrection);
 Brightness.Assign(_.Brightness);
 Contrast.Assign(_.Contrast);
}

void VI::Main::TV_Control::FocusControl::Assign(const struct ::TV::OutputData::Focus& _) {
 SmoothlyPlus.Assign(_.SmoothlyPlus);
 SmoothlyMinus.Assign(_.SmoothlyMinus);
 Near.Assign(_.Near);
 Smoothly.Assign(_.Smoothly);
 Manual.Assign(_.Manual);
}

void VI::Main::TV_Control::FieldOfView::Assign(const ::TV::OutputData::FieldOfView& _) {
 SmoothlyPlus.Assign(_.SmoothlyPlus);
 SmoothlyMinus.Assign(_.SmoothlyMinus);
 Narrow.Assign(_.Narrow); // false - wide, true - narrow (zoom)
 Smoothly.Assign(_.Smoothly);
}

void VI::Main::Gun_Faults::Assign(const struct _Gun::InputData::Faults& _) {
 ExchangeComp.Assign(_.ExchangeComp);
 ExchangeGun.Assign(_.ExchangeGun);
 PowerSupply.Assign(_.PowerSupply);
 URef.Assign(_.URef);
}

void VI::Main::LRF_Faults::Assign(const struct LRF::InputData::Faults& _) {
 FRAM.Assign(_.FRAM);
 CRC.Assign(_.CRC);
 IntervalMeasurer.Assign(_.IntervalMeasurer);
 QSwitch.Assign(_.QSwitch);
 LPS.Assign(_.LPS); // БПЛ
 WTF.Assign(_.WTF); // ФПУ
 OpticalStart.Assign(_.OpticalStart);
}

void VI::Main::LRF_State::Assign(const struct LRF::InputData::State& _) {
 Targets.Assign(_.Targets);
 LowPeriod.Assign(_.LowPeriod);
 OnboardNetwork.Assign(_.OnboardNetwork);
 WTF.Assign(_.WTF); // Ошибка драйв. ТЕС
 TestingCompleted.Assign(_.TestingCompleted);
 Failure.Assign(_.Failure);
}

void VI::Main::WeatherType::Assign(const struct Weather& _) {
 WindSpeed.Assign(_.WindSpeed);
 WindBearing.Assign(_.WindBearing);
 Pressure.Assign(_.Pressure);
 Temperature.Assign(_.Temperature);
 ChargeTemperature.Assign(_.ChargeTemperature);
}

void VI::Main::NS_Faults::Assign(const struct NS::InputData::Faults& _) {
 HeaderChecksum.Assign(_.HeaderChecksum);
 DataChecksum.Assign(_.DataChecksum);
 Count.Assign(_.Count);
 General.Assign(_.General);
 Off.Assign(_.Off);
}

void VI::Main::ETD_In::Assign(const RadarETD::OutputData& _) {
 EmissionPermitted.Assign(_.EmissionPermitted);
 Antenna.Assign(_.Antenna); // 0 - equivalent, 1 - antenna
 TT.Assign(_.TT);
 Distance.Assign(_.Distance);
 LFM_SlopeNegative.Assign(_.LFM_SlopeNegative);
 DS.Assign(_.DS);
 SignalCode.Assign(_.SignalCode);
 OFT.Assign(_.OFT);
 FrequencyCode.Assign(_.FrequencyCode);
 RPA.Assign(_.RPA);
 RepetitionPeriodCode.Assign(_.RepetitionPeriodCode);
 MTS1.Assign(_.MTS1);
 MTS2.Assign(_.MTS2);
 FFT.Assign(_.FFT);
 ActiveNoiseAnalysis.Assign(_.ActiveNoiseAnalysis);
 OwnMovementCompensation.Assign(_.OwnMovementCompensation);
 ActiveInterferenceAngularTracking.Assign(_.ActiveInterferenceAngularTracking);
 SumDeltaChannelsCalibration.Assign(_.SumDeltaChannelsCalibration); // 0 - disabled, 1 - enabled
 RangeCalibration.Assign(_.RangeCalibration); // 0 - disabled, 1 - enabled
 PulseCount.Assign(_.PulseCount);
}

void VI::Main::ETD_Out::Assign(const RadarETD::InputData& _) {
 EmissionPermitted.Assign(_.EmissionPermitted);
 EmissionPermittedPhysical.Assign(_.EmissionPermittedPhysical);
 Antenna.Assign(_.Antenna); // 0 - equivalent, 1 - antenna
 TT.Assign(_.TT);
 Distance.Assign(_.Distance);
 LFM_SlopeNegative.Assign(_.LFM_SlopeNegative);
 DS.Assign(_.DS);
 SignalCode.Assign(_.SignalCode);
 OFT.Assign(_.OFT);
 FrequencyCode.Assign(_.FrequencyCode);
 RPA.Assign(_.RPA);
 RepetitionPeriodCode.Assign(_.RepetitionPeriodCode);
 MTS1.Assign(_.MTS1);
 MTS2.Assign(_.MTS2);
 FFT.Assign(_.FFT);
 ActiveNoiseAnalysis.Assign(_.ActiveNoiseAnalysis);
 OwnMovementCompensation.Assign(_.OwnMovementCompensation);
 ActiveInterferenceAngularTracking.Assign(_.ActiveInterferenceAngularTracking);
 SumDeltaChannelsCalibration.Assign(_.SumDeltaChannelsCalibration); // 0 - disabled, 1 - enabled
 RangeCalibration.Assign(_.RangeCalibration); // 0 - disabled, 1 - enabled
 PulseCount.Assign(_.PulseCount);
}

void VI::Main::SS_In::Assign(const RadarSS::OutputData& _) {
 EmissionPermitted.Assign(_.EmissionPermitted);
 Antenna.Assign(_.Antenna); // 0 - equivalent, 1 - antenna
 DistanceBegin.Assign(_.DistanceBegin); // of passive noise strobe
 DistanceEnd.Assign(_.DistanceEnd);   // of passive noise strobe
 LFM_SlopeNegative.Assign(_.LFM_SlopeNegative);
 DS.Assign(_.DS);
 SignalCode.Assign(_.SignalCode);
 OFT.Assign(_.OFT);
 FrequencyCode.Assign(_.FrequencyCode);
 RPA.Assign(_.RPA);
 RepetitionPeriodCode.Assign(_.RepetitionPeriodCode);
 MTS1.Assign(_.MTS1);
 MTS2.Assign(_.MTS2);
 FFT.Assign(_.FFT);
 ActiveNoiseAnalysis.Assign(_.ActiveNoiseAnalysis);
 OwnMovementCompensation.Assign(_.OwnMovementCompensation);
 RangeCalibration.Assign(_.RangeCalibration); // 0 - disabled, 1 - enabled
 PulseCount.Assign(_.PulseCount);
}

void VI::Main::SS_Out::Assign(const RadarSS::InputData& _) {
 EmissionPermitted.Assign(_.EmissionPermitted);
 EmissionPermittedPhysical.Assign(_.EmissionPermittedPhysical);
 Antenna.Assign(_.Antenna); // 0 - equivalent, 1 - antenna
 DistanceBegin.Assign(_.DistanceBegin); // of passive noise strobe
 DistanceEnd.Assign(_.DistanceEnd);   // of passive noise strobe
 LFM_SlopeNegative.Assign(_.LFM_SlopeNegative);
 DS.Assign(_.DS);
 SignalCode.Assign(_.SignalCode);
 OFT.Assign(_.OFT);
 FrequencyCode.Assign(_.FrequencyCode);
 RPA.Assign(_.RPA);
 RepetitionPeriodCode.Assign(_.RepetitionPeriodCode);
 MTS1.Assign(_.MTS1);
 MTS2.Assign(_.MTS2);
 FFT.Assign(_.FFT);
 ActiveNoiseAnalysis.Assign(_.ActiveNoiseAnalysis);
 OwnMovementCompensation.Assign(_.OwnMovementCompensation);
 RangeCalibration.Assign(_.RangeCalibration); // 0 - disabled, 1 - enabled
 PulseCount.Assign(_.PulseCount);
}

void VI::Main::Control_Data::Assign(
 const RadarControl::OutputData& _) {
 TAGC.Assign(_.TAGC);
 RangeCalibrationInput.Assign(_.RangeCalibrationInput);
 PIC.Assign(_.PIC);
 CFAR.Assign(_.CFAR);
 ManualIF.Assign(_.ManualIF);
 ManualUHF.Assign(_.ManualUHF);
 DetectionThreshold.Assign(_.DetectionThreshold);
 ANI_Threshold.Assign(_.ANI_Threshold);
 UHF_ControlThreshold.Assign(_.UHF_ControlThreshold);
 IF_ControlThreshold.Assign(_.IF_ControlThreshold);
 SignalCode.Assign(_.SignalCode);
 CalibrationRangeCorrection.Assign(_.CalibrationRangeCorrection);
 PassiveInterferenceCoherenceCoefficient.Assign(_.PassiveInterferenceCoherenceCoefficient);
 MinimumPassiveInterferenceThreshold.Assign(_.MinimumPassiveInterferenceThreshold);
}

void VI::Main::RadarTest_Data::Assign(
 const RadarTestResult::InputData& _) {
 Faults.Assign(_.Faults);
 TransmitterPulsePower.Assign(_.TransmitterPulsePower);
 SUMNoisePower.Assign(_.SUMNoisePower);
 DF1NoisePower.Assign(_.DF1NoisePower);
 DF2NoisePower.Assign(_.DF2NoisePower);
 SUMCalibrationPower.Assign(_.SUMCalibrationPower);
 DF1CalibrationPower.Assign(_.DF1CalibrationPower);
 DF2CalibrationPower.Assign(_.DF2CalibrationPower);
 SUMDirectWaveLevel.Assign(_.SUMDirectWaveLevel);
 SUMBackwardWaveLevel.Assign(_.SUMBackwardWaveLevel);
 SUM_DF1AmplitudeDifference.Assign(_.SUM_DF1AmplitudeDifference);
 SUM_DF2AmplitudeDifference.Assign(_.SUM_DF2AmplitudeDifference);
 SUM_DF1PhaseDifference.Assign(_.SUM_DF1PhaseDifference);
 SUM_DF2PhaseDifference.Assign(_.SUM_DF2PhaseDifference);
 Completed.Assign(_.Completed);
}

void VI::Main::PlannedFrequenciesItem::Assign(const PlannedFrequencies::Item& _) {
 Permitted.Assign(_.Permitted);
 Code.Assign(_.Code);
}

void VI::Main::RadarTest_Data::Faults_Data::Assign(
 const struct RadarTestResult::InputData::Faults& _) {
 TransmitterPulsePower.Assign(_.TransmitterPulsePower);
 SUMNoisePower.Assign(_.SUMNoisePower);
 DF1NoisePower.Assign(_.DF1NoisePower);
 DF2NoisePower.Assign(_.DF2NoisePower);
 SUMCalibrationPower.Assign(_.SUMCalibrationPower);
 DF1CalibrationPower.Assign(_.DF1CalibrationPower);
 DF2CalibrationPower.Assign(_.DF2CalibrationPower);
 SUMDirectWaveLevel.Assign(_.SUMDirectWaveLevel);
 SUMBackwardWaveLevel.Assign(_.SUMBackwardWaveLevel);
 SUM_DF1AmplitudeDifference.Assign(_.SUM_DF1AmplitudeDifference);
 SUM_DF2AmplitudeDifference.Assign(_.SUM_DF2AmplitudeDifference);
 SynthesizerPLL_LoopCapture.Assign(_.SynthesizerPLL_LoopCapture);
 A_E_Switch.Assign(_.A_E_Switch);
 TransmitterTemperature.Assign(_.TransmitterTemperature);
}

VI::Main::External::External(ui _) :
 NS(_), GunX(_),
 GunHA {
  {
   Gun_Faults {
    _.lblGunInputHAFaultInterfaceComp, _.lblGunInputHAFaultExchangeGun,
    _.lblGunInputHAFaultPowerSupply, _.lblGunInputHAFaultURef
   },
   _.lblGunHAReceived, _.lblGunHAReceptionFaults
  }, {
   _.lblGunHAOutputMode, _.lblGunHAOutputPosition,
   _.lblGunHAOutputElev, _.lblGunHATransmitted
  }
 },
 GunElev {
  {
   Gun_Faults {
    _.lblGunInputElevFaultInterfaceComp, _.lblGunInputElevFaultExchangeGun,
    _.lblGunInputElevFaultPowerSupply, _.lblGunInputElevFaultURef
   },
   _.lblGunElevReceived, _.lblGunElevReceptionFaults
  }, {
   _.lblGunElevOutputMode, _.lblGunElevOutputPosition,
   _.lblGunElevOutputElev, _.lblGunElevTransmitted
  }
 },
 GunTimer {
  {
   Gun_Faults {
    _.lblGunInputTimerFaultInterfaceComp, _.lblGunInputTimerFaultExchangeGun,
    _.lblGunInputTimerFaultPowerSupply, _.lblGunInputTimerFaultURef
   },
   _.lblGunTimerReceived, _.lblGunTimerReceptionFaults
  }, {
   _.lblGunTimerOutputMode, _.lblGunTimerOutputTime,
   _.lblGunTimerTransmitted
  }
 },
 GTC_Radar(_) {
}

void VI::Main::External::Assign(const struct Interface::Command::Main::External& _) {
 GunHA.Assign(_.GunHA);
 GunElev.Assign(_.GunElev);
 GunTimer.Assign(_.GunTimer);
 GunX.Assign(_.GunX);
 NS.Assign(_.NS.Out);
 GTC_Radar.Assign(_.Radar);
}

VI::Main::External::NS::NS(ui _) :
 Rot { _.lblHeading, _.lblRolling, _.lblPitch },
 RotChange { _.lblHeadingChange, _.lblRollingChange, _.lblPitchChange },
 SpeedPart { _.lblSpeedX, _.lblSpeedY, _.lblSpeedZ },
 SpeedAbsolute(_.lblSpeedAbsolute),
 Faults {
  _.lblNSFaultHeaderChecksum, _.lblNSFaultDataChecksum,
  _.lblNSFaultCount, _.lblNSFaultGeneral, _.lblNSOff
 },
 LTS(_.lblNSLTS), Received(_.lblNSReceived), ReceptionFaults(_.lblNSReceptionFaults) {
}

void VI::Main::External::NS::Assign(const Interface::DisplayRecord<ModuleNS>::OutRecord& _) {
 Rot.Assign(_.Rot);
 RotChange.Assign(_.RotChange);
 SpeedPart.Assign(_.SpeedPart);
 SpeedAbsolute.Assign(Absolute(_.SpeedPart));
 Faults.Assign(_.Faults);
 LTS.Assign(_.LTSTime);
 Received.Assign(_.Received);
 ReceptionFaults.Assign(_.ReceptionFaults);
}

VI::Main::External::GunX::GunX(ui _) :
 Out(_), In(_) {}

void VI::Main::External::GunX::Assign(const Interface::DisplayRecord<ModuleGunX>& _) {
// const size_t Index = ModuleIndex.Get();
 Out.Assign(_.Out);
 In.Assign(_.In);
}

VI::Main::External::GunX::Out::Out(ui _) :
 Ship_HA { _.lblGunXInputShipHeading0, _.lblGunXInputShipHeading1 },
 ADC { _.lblGunXInputADC0, _.lblGunXInputADC1 },
 FaultCS(_.lblGunXInputFaultCS), FaultPV(_.lblGunXInputFaultPV),
 FaultADC(_.lblGunXInputFaultADC),
 Received(_.lblGunXReceived), ReceptionFaults(_.lblGunXReceptionFaults) {
}

void VI::Main::External::GunX::Out::Assign(
 const Interface::DisplayRecord<ModuleGunX>::OutRecord& _) {
 for (size_t i = 0; i < Ship_HA.size(); ++i) {
  Ship_HA[i].Assign(_.Ship_HA[i]);
  ADC[i].Assign(_.ADC[i]);
 }
 FaultCS.Assign(_.Faults.CS);
 FaultPV.Assign(_.Faults.PV);
 FaultADC.Assign(_.Faults.ADC);
 Received.Assign(_.Received);
 ReceptionFaults.Assign(_.ReceptionFaults);
}

VI::Main::External::GunX::In::In(ui _) :
 Test(_.lblGunXOutputTest),
 Transmitted(_.lblGunXTransmitted) {
}

void VI::Main::External::GunX::In::Assign(
 const Interface::DisplayRecord<ModuleGunX>::InRecord& _) {
 Test.Assign(_.Test);
 Transmitted.Assign(_.Transmitted);
}

void VI::Main::External::Gun::Assign(const Interface::DisplayRecord<ModuleGun>& _) {
 Out.Assign(_.Out);
 In.Assign(_.In);
}

template <>
constexpr auto enum_tab<_Gun::Workmode> = { "робота", "контроль" };

void VI::Main::External::Gun::Out::Assign(
 const Interface::DisplayRecord<ModuleGun>::OutRecord& _) {
 Faults.Assign(_.Faults);
 Received.Assign(_.Received);
 ReceptionFaults.Assign(_.ReceptionFaults);
}

void VI::Main::External::Gun::In::Assign(
 const Interface::DisplayRecord<ModuleGun>::InRecord& _) {
 Mode.Assign(_.Mode);
 Position.Assign(_.Position);
 IsElevChannel.Assign(_.IsElevChannel);
 Transmitted.Assign(_.Transmitted);
}

void VI::Main::External::GunTimer::Assign(const Interface::DisplayRecord<ModuleGunTimer>& _) {
 Out.Assign(_.Out);
 In.Assign(_.In);
}

void VI::Main::External::GunTimer::Out::Assign(
 const Interface::DisplayRecord<ModuleGunTimer>::OutRecord& _) {
 Faults.Assign(_.Faults);
 Received.Assign(_.Received);
 ReceptionFaults.Assign(_.ReceptionFaults);
}

void VI::Main::External::GunTimer::In::Assign(
 const Interface::DisplayRecord<ModuleGunTimer>::InRecord& _) {
 Mode.Assign(_.Mode);
 Time.Assign(_.Time);
 Transmitted.Assign(_.Transmitted);
}

VI::Main::External::GTC_Radar::Mode::Mode(ui _) :
 Out(_), In(_) {
}

void VI::Main::External::GTC_Radar::Mode::Assign(
 const Interface::DisplayRecord<ModuleGTCRMode>& _) {
 Out.Assign(_.Out);
 In.Assign(_.In);
}

template<>
constexpr auto enum_tab<GTCRMode::Mode> = {
 "", "робота", "тренаж", "контроль"
};
template<>
constexpr auto enum_tab<GTCRMode::Submode> = { "черговий", "АП", "ЦВ", "" };

VI::Main::External::GTC_Radar::Mode::Out::Out(ui _) :
 Mode(_.lblGTCRModeInputMode), Submode(_.lblGTCRModeInputSubmode),
 Received(_.lblGTCRModeReceived), ReceptionFaults(_.lblGTCRModeReceptionFaults) {
}

void VI::Main::External::GTC_Radar::Mode::Out::Assign(
 const Interface::DisplayRecord<ModuleGTCRMode>::OutRecord& _) {
 Mode.Assign(_.Mode);
 Submode.Assign(_.Submode);
 Received.Assign(_.Received);
 ReceptionFaults.Assign(_.ReceptionFaults);
}

VI::Main::External::GTC_Radar::Mode::In::In(ui _) :
 Mode(_.lblGTCRModeOutputMode), Submode(_.lblGTCRModeOutputSubmode),
 Transmitted(_.lblGTCRModeTransmitted) {
}

void VI::Main::External::GTC_Radar::Mode::In::Assign(
 const Interface::DisplayRecord<ModuleGTCRMode>::InRecord& _) {
 Mode.Assign(_.Mode);
 Submode.Assign(_.Submode);
 Transmitted.Assign(_.Transmitted);
}

VI::Main::External::GTC_Radar::RadialSpeed::RadialSpeed(ui _) :
 Target(_.lblRadialSpeedTarget), PassiveInterference(_.lblRadialSpeedPassiveInterference),
 Transmitted(_.lblRadialSpeedTransmitted) {
}

void VI::Main::External::GTC_Radar::RadialSpeed::Assign(
 const Interface::DisplayRecord<ModuleRadialSpeed>::InRecord& _) {
 Target.Assign(_.Target);
 PassiveInterference.Assign(_.PassiveInterference);
 Transmitted.Assign(_.Transmitted);
}

VI::Main::External::GTC_Radar::PlannedFrequencies::PlannedFrequencies(ui _) :
 Out(_), In(_) {
}

void VI::Main::External::GTC_Radar::PlannedFrequencies::Assign(
 const Interface::DisplayRecord<ModulePlannedFrequencies>& _) {
 Out.Assign(_.Out);
 In.Assign(_.In);
}

void VI::Main::External::GTC_Radar::PlannedFrequencies::Plan::Assign(
 const ::PlannedFrequencies::OutputData& _) {
 Count.Assign(_.Count);
 for (size_t i = 0; i < Array.size(); ++i) {
  Array[i].Assign(_.Array[i]);
  Array[i].Permitted.Assign(_.Array[i].Permitted);
  Array[i].Code.Assign(_.Array[i].Code);
 }
}

VI::Main::External::GTC_Radar::PlannedFrequencies::Out::Out(ui _) :
 Plan {
  _.lblPlannedFrequenciesInputCount,
  {
   PlannedFrequenciesItem {
    _.lblPlannedFrequenciesInputPermitted0, _.lblPlannedFrequenciesInputCode0
   },
   { _.lblPlannedFrequenciesInputPermitted1, _.lblPlannedFrequenciesInputCode1 },
   { _.lblPlannedFrequenciesInputPermitted2, _.lblPlannedFrequenciesInputCode2 },
   { _.lblPlannedFrequenciesInputPermitted3, _.lblPlannedFrequenciesInputCode3 },
   { _.lblPlannedFrequenciesInputPermitted4, _.lblPlannedFrequenciesInputCode4 },
   { _.lblPlannedFrequenciesInputPermitted5, _.lblPlannedFrequenciesInputCode5 }
  }
 },
 Received(_.lblPlannedFrequenciesReceived),
 ReceptionFaults(_.lblPlannedFrequenciesReceptionFaults) {
}

void VI::Main::External::GTC_Radar::PlannedFrequencies::Out::Assign(
 const Interface::DisplayRecord<ModulePlannedFrequencies>::OutRecord& _) {
 Plan.Assign(_);
 Received.Assign(_.Received);
 ReceptionFaults.Assign(_.ReceptionFaults);
}

VI::Main::External::GTC_Radar::PlannedFrequencies::In::In(ui _) :
 Plan {
  _.lblPlannedFrequenciesOutputCount,
  {
   PlannedFrequenciesItem {
    _.lblPlannedFrequenciesOutputPermitted0, _.lblPlannedFrequenciesOutputCode0
   },
   { _.lblPlannedFrequenciesOutputPermitted1, _.lblPlannedFrequenciesOutputCode1 },
   { _.lblPlannedFrequenciesOutputPermitted2, _.lblPlannedFrequenciesOutputCode2 },
   { _.lblPlannedFrequenciesOutputPermitted3, _.lblPlannedFrequenciesOutputCode3 },
   { _.lblPlannedFrequenciesOutputPermitted4, _.lblPlannedFrequenciesOutputCode4 },
   { _.lblPlannedFrequenciesOutputPermitted5, _.lblPlannedFrequenciesOutputCode5 }
  }
 },
 Transmitted(_.lblPlannedFrequenciesTransmitted) {
}

void VI::Main::External::GTC_Radar::PlannedFrequencies::In::Assign(
 const Interface::DisplayRecord<ModulePlannedFrequencies>::InRecord& _) {
 Plan.Assign(_);
 Transmitted.Assign(_.Transmitted);
}

VI::Main::External::GTC_Radar::ETD::ETD(ui _) : Out(_), In(_) {}

void VI::Main::External::GTC_Radar::ETD::Assign(
 const Interface::DisplayRecord<ModuleRadarETD>& _) {
 Out.Assign(_.Out);
 In.Assign(_.In);
}

template <>
constexpr auto enum_tab<RadarETD::TargetType> = {
 "не визначена", "повітряна", "та, що низько летить", "вертоліт", "надводна", "берегова"
};

template <>
constexpr auto enum_tab<Radar::DistanceScale> = {
 "не визначений", "6 км", "12 км", "18 км", "24 км", "36 км"
};

template <>
constexpr auto enum_tab<Radar::OperatingFrequencyTuning> = {
 "не визначена", "фіксована", "адаптивна", "випадкова"
};

template <>
constexpr auto enum_tab<Radar::RepetitionPeriodAdjustment> = {
 "фіксований", "вобуляція"
};

template <>
constexpr auto enum_tab<Radar::MovingTargetSelection> = {
 "вимкнено", "фільтр СРЦ №1", "фільтр СРЦ №2", "фільтр СРЦ №3"
};

template <>
constexpr auto enum_tab<Radar::FastFourierTransform> = {
 "вимкнено", "16-точкове", "32-точкове", "64-точкове", "128-точкове", "256-точкове"
};

VI::Main::External::GTC_Radar::ETD::Out::Out(ui _) :
 Data {
  _.lblETD_InputEmissionPermitted, _.lblETD_InputEmissionPermittedPhysical,
  _.lblETD_InputAntenna, _.lblETD_InputTT, _.lblETD_InputDistance,
  _.lblETD_InputLFM_SlopeNegative, _.lblETD_InputDS,
  _.lblETD_InputSignalCode, _.lblETD_InputOFT, _.lblETD_InputFrequencyCode,
  _.lblETD_InputRPA, _.lblETD_InputRepetitionPeriodCode,
  _.lblETD_InputMTS1, _.lblETD_InputMTS2,
  _.lblETD_InputFFT, _.lblETD_InputActiveNoiseAnalysis,
  _.lblETD_InputOwnMoventCompensation,
  _.lblETD_InputActiveInterferenceAngularTracking,
  _.lblETD_InputSumDeltaChannelsCalibration,
  _.lblETD_InputRangeCalibration, _.lblETD_InputRepetitionPulseCount
 },
 Received(_.lblETDReceived), ReceptionFaults(_.lblETDReceptionFaults) {
}

void VI::Main::External::GTC_Radar::ETD::Out::Assign(
 const Interface::DisplayRecord<ModuleRadarETD>::OutRecord& _) {
 Data.Assign(_);
}

VI::Main::External::GTC_Radar::ETD::In::In(ui _) :
 Data {
  _.lblETD_OutputEmissionPermitted,
  _.lblETD_OutputAntenna, _.lblETD_OutputTT,
  _.lblETD_OutputDistance, _.lblETD_OutputLFM_SlopeNegative,
  _.lblETD_OutputDS, _.lblETD_OutputSignalCode,
  _.lblETD_OutputOFT, _.lblETD_OutputFrequencyCode,
  _.lblETD_OutputRPA, _.lblETD_OutputRepetitionPeriodCode,
  _.lblETD_OutputMTS1, _.lblETD_OutputMTS2,
  _.lblETD_OutputFFT, _.lblETD_OutputActiveNoiseAnalysis,
  _.lblETD_OutputOwnMoventCompensation,
  _.lblETD_OutputActiveInterferenceAngularTracking,
  _.lblETD_OutputSumDeltaChannelsCalibration,
  _.lblETD_OutputRangeCalibration, _.lblETD_OutputRepetitionPulseCount
 },
 Transmitted(_.lblETDTransmitted) {
}

void VI::Main::External::GTC_Radar::ETD::In::Assign(
 const Interface::DisplayRecord<ModuleRadarETD>::InRecord& _) {
 Data.Assign(_);
 Transmitted.Assign(_.Transmitted);
}

VI::Main::External::GTC_Radar::SS::SS(ui _) : Out(_), In(_) {}

void VI::Main::External::GTC_Radar::SS::Assign(
 const Interface::DisplayRecord<ModuleRadarSS>& _) {
 Out.Assign(_.Out);
 In.Assign(_.In);
}

VI::Main::External::GTC_Radar::SS::Out::Out(ui _) :
 Data {
  _.lblSS_InputEmissionPermitted,
  _.lblSS_InputEmissionPermittedPhysical,
  _.lblSS_InputAntenna,
  _.lblSS_InputDistanceBegin, _.lblSS_InputDistanceEnd,
  _.lblSS_InputLFM_SlopeNegative, _.lblSS_InputDS,
  _.lblSS_InputSignalCode, _.lblSS_InputOFT,
  _.lblSS_InputFrequencyCode, _.lblSS_InputRPA,
  _.lblSS_InputRepetitionPeriodCode,
  _.lblSS_InputMTS1, _.lblSS_InputMTS2,
  _.lblSS_InputFFT, _.lblSS_InputActiveNoiseAnalysis,
  _.lblSS_InputOwnMoventCompensation,
  _.lblSS_InputRangeCalibration, _.lblSS_InputPulseCount
 },
 Received(_.lblSSReceived), ReceptionFaults(_.lblSSReceptionFaults) {
}

void VI::Main::External::GTC_Radar::SS::Out::Assign(
 const Interface::DisplayRecord<ModuleRadarSS>::OutRecord& _) {
 Data.Assign(_);
 Received.Assign(_.Received);
 ReceptionFaults.Assign(_.ReceptionFaults);
}

VI::Main::External::GTC_Radar::SS::In::In(ui _) :
 Data {
  _.lblSS_OutputEmissionPermitted, _.lblSS_OutputAntenna,
  _.lblSS_OutputDistanceBegin, _.lblSS_OutputDistanceEnd,
  _.lblSS_OutputLFM_SlopeNegative, _.lblSS_OutputDS,
  _.lblSS_OutputSignalCode, _.lblSS_OutputOFT,
  _.lblSS_OutputFrequencyCode, _.lblSS_OutputRPA,
  _.lblSS_OutputRepetitionPeriodCode,
  _.lblSS_OutputMTS1, _.lblSS_OutputMTS2,
  _.lblSS_OutputFFT, _.lblSS_OutputActiveNoiseAnalysis,
  _.lblSS_OutputOwnMoventCompensation,
  _.lblSS_OutputRangeCalibration, _.lblSS_OutputPulseCount
 },
 Transmitted(_.lblSSTransmitted) {
}

void VI::Main::External::GTC_Radar::SS::In::Assign(
 const Interface::DisplayRecord<ModuleRadarSS>::InRecord& _) {
 Data.Assign(_);
 Transmitted.Assign(_.Transmitted);
}

template <>
constexpr auto enum_tab<RadarControl::PassiveInterferenceCompensating> = {
 "вимкнена", "автоматична", "ручна"
};

VI::Main::External::GTC_Radar::Control::Control(ui _) :
 Out(_), In(_) {
}

void VI::Main::External::GTC_Radar::Control::Assign(
 const Interface::DisplayRecord<ModuleRadarControl>& _) {
 Out.Assign(_.Out);
 In.Assign(_.In);
}

VI::Main::External::GTC_Radar::Control::Out::Out(ui _) :
 Data {
  _.lblRadarControl_InputTAGC, _.lblRadarControl_InputRangeInputCalibration,
  _.lblRadarControl_InputPIC, _.lblRadarControl_InputCFAR,
  _.lblRadarControl_InputManualIF, _.lblRadarControl_InputManualUHF,
  _.lblRadarControl_InputDetectionThreshold, _.lblRadarControl_InputANI_Threshold,
  _.lblRadarControl_InputUHF_ControlThreshold, _.lblRadarControl_InputIF_ControlThreshold,
  _.lblRadarControl_InputSignalCode, _.lblRadarControl_InputCalibrationRangeCorrection,
  _.lblRadarControl_InputPassiveInterferenceCoherenceCoefficient,
  _.lblRadarControl_InputMinimumPassiveInterferenceThreshold
 },
 Received(_.lblRadarControlReceived), ReceptionFaults(_.lblRadarControlReceptionFaults) {
}

void VI::Main::External::GTC_Radar::Control::Out::Assign(
 const Interface::DisplayRecord<ModuleRadarControl>::OutRecord& _) {
 Data.Assign(_);
 Received.Assign(_.Received);
 ReceptionFaults.Assign(_.ReceptionFaults);
}

VI::Main::External::GTC_Radar::Control::In::In(ui _) :
 Data {
  _.lblRadarControl_OutputTAGC, _.lblRadarControl_OutputRangeInputCalibration,
  _.lblRadarControl_OutputPIC, _.lblRadarControl_OutputCFAR,
  _.lblRadarControl_OutputManualIF, _.lblRadarControl_OutputManualUHF,
  _.lblRadarControl_OutputDetectionThreshold, _.lblRadarControl_OutputANI_Threshold,
  _.lblRadarControl_OutputUHF_ControlThreshold, _.lblRadarControl_OutputIF_ControlThreshold,
  _.lblRadarControl_OutputSignalCode, _.lblRadarControl_OutputCalibrationRangeCorrection,
  _.lblRadarControl_OutputPassiveInterferenceCoherenceCoefficient,
  _.lblRadarControl_OutputMinimumPassiveInterferenceThreshold
 },
 Transmitted(_.lblRadarControlTransmitted) {
}

void VI::Main::External::GTC_Radar::Control::In::Assign(
 const Interface::DisplayRecord<ModuleRadarControl>::InRecord& _) {
 Data.Assign(_);
 Transmitted.Assign(_.Transmitted);
}

VI::Main::External::GTC_Radar::Training::Training(ui _) :
 Distance(_.lblRadarTrainingDistance),
 RadialSpeed(_.lblRadarTrainingRadialSpeed),
 DopplerShift(_.lblRadarTrainingDopplerShift),
 Transmitted(_.lblRadarTrainingTransmitted) {
}

void VI::Main::External::GTC_Radar::Training::Assign(
 const Interface::DisplayRecord<ModuleRadarTraining>::InRecord& _) {
 Distance.Assign(_.Distance);
 RadialSpeed.Assign(_.RadialSpeed);
 DopplerShift.Assign(_.DopplerShift);
 Transmitted.Assign(_.Transmitted);
}

template <>
constexpr auto enum_tab<Radar::ActiveNoisePresence> = {
 "відсутня", "прицільна", "загороджувальна"
};

VI::Main::External::GTC_Radar::ETD_Detection::ETD_Detection(ui _) :
 OFT(_.lblETD_DetectionOFT), FrequencyCode(_.lblETD_DetectionFrequencyCode),
 RepetitionPeriodCode(_.lblETD_DetectionRepetitionPeriodCode),
 RPA(_.lblETD_DetectionRPA), TargetCount(_.lblETD_DetectionTargetCount),
 ANP(_.lblETD_DetectionActiveNoisePresence),
 ActiveNoiseInterferenceProcessing(_.lblETD_DetectionActiveNoiseInterferenceProcessing),
 First(_),
 Others{
  Target {
   _.lblETD_DetectionRangeTrackingState1, _.lblETD_DetectionDistance1,
   {
    _.lblETD_DetectionCommonModeDeviationHA1,
    _.lblETD_DetectionCommonModeDeviationElev1
   },
   _.lblETD_DetectionSignalToNoiseRatio1, _.lblETD_DetectionRadialSpeed1
  },
  Target {
   _.lblETD_DetectionRangeTrackingState2, _.lblETD_DetectionDistance2,
   {
    _.lblETD_DetectionCommonModeDeviationHA2,
    _.lblETD_DetectionCommonModeDeviationElev2
   },
   _.lblETD_DetectionSignalToNoiseRatio2, _.lblETD_DetectionRadialSpeed2
  },
  Target {
   _.lblETD_DetectionRangeTrackingState3, _.lblETD_DetectionDistance3,
   {
    _.lblETD_DetectionCommonModeDeviationHA3,
    _.lblETD_DetectionCommonModeDeviationElev3
   },
   _.lblETD_DetectionSignalToNoiseRatio3, _.lblETD_DetectionRadialSpeed3
  },
 },
 LTS(_.lblETD_DetectionLTS),
 Received(_.lblETD_DetectionReceived), ReceptionFaults(_.lblETD_DetectionReceptionFaults) {
}

void VI::Main::External::GTC_Radar::ETD_Detection::Assign(
 const Interface::DisplayRecord<ModuleETD_Detection>::OutRecord& _) {
 FrequencyCode.Assign(_.FrequencyCode);
 OFT.Assign(_.OFT);
 RepetitionPeriodCode.Assign(_.RepetitionPeriodCode);
 RPA.Assign(_.RPA);
 ANP.Assign(_.ANP);
 TargetCount.Assign(_.TargetCount);
 ActiveNoiseInterferenceProcessing.Assign(_.ActiveNoiseInterferenceProcessing);
 First.Assign(_.First);
 for (size_t i = 0; i < Others.size(); ++i) {
  Others[i].Assign(_.Others[i]);
 }
 LTS.Assign(_.LTS);
 Received.Assign(_.Received);
 ReceptionFaults.Assign(_.ReceptionFaults);
}

template <>
constexpr auto enum_tab<::ETD_Detection::RangeTrackingState> = {
 "немає", "АС", "ІС", "зрив"
};

VI::Main::External::GTC_Radar::ETD_Detection::FirstTarget::FirstTarget(ui _) :
 RTS(_.lblETD_DetectionRangeTrackingState0), Distance(_.lblETD_DetectionDistance0),
 CommonModeDeviation {
  _.lblETD_DetectionCommonModeDeviationHA0, _.lblETD_DetectionCommonModeDeviationElev0
 },
 QuadratureDeviation {
  _.lblETD_DetectionQuadratureDeviationHA0, _.lblETD_DetectionQuadratureDeviationElev0
 },
 SignalToNoiseRatio(_.lblETD_DetectionSignalToNoiseRatio0),
 RadialSpeed(_.lblETD_DetectionRadialSpeed0) {
}

void VI::Main::External::GTC_Radar::ETD_Detection::FirstTarget::Assign(
 const ::ETD_Detection::FirstTarget& _) {
 RTS.Assign(_.RTS);
 Distance.Assign(_.Distance);
 CommonModeDeviation.Assign(_.CommonModeDeviation);
 QuadratureDeviation.Assign(_.QuadratureDeviation);
 SignalToNoiseRatio.Assign(_.SignalToNoiseRatio);
 RadialSpeed.Assign(_.RadialSpeed);
}

void VI::Main::External::GTC_Radar::ETD_Detection::Target::Assign(
 const ::ETD_Detection::Target& _) {
 RTS.Assign(_.RTS);
 Distance.Assign(_.Distance);
 CommonModeDeviation.Assign(_.CommonModeDeviation);
 SignalToNoiseRatio.Assign(_.SignalToNoiseRatio);
 RadialSpeed.Assign(_.RadialSpeed);
}

VI::Main::External::GTC_Radar::SS_Data::SS_Data(ui _) :
 OFT(_.lblSS_DataOFT), FrequencyCode(_.lblSS_DataFrequencyCode),
 RepetitionPeriodCode(_.lblSS_DataRepetitionPeriodCode),
 RPA(_.lblSS_DataRPA), TargetCount(_.lblSS_DataTargetCount),
 ANP(_.lblSS_DataActiveNoisePresence),
 ActiveNoiseInterferenceProcessing(_.lblSS_DataActiveNoiseInterferenceProcessing),
 LTS(_.lblSS_DataLTS),
 Received(_.lblSS_DataReceived), ReceptionFaults(_.lblSS_DataReceptionFaults) {
}

void VI::Main::External::GTC_Radar::SS_Data::Assign(
 const Interface::DisplayRecord<ModuleSS_Data>::OutRecord& _) {
 FrequencyCode.Assign(_.FrequencyCode);
 OFT.Assign(_.OFT);
 RepetitionPeriodCode.Assign(_.RepetitionPeriodCode);
 RPA.Assign(_.RPA);
 ANP.Assign(_.ANP);
 TargetCount.Assign(_.TargetCount);
 ActiveNoiseInterferenceProcessing.Assign(_.ActiveNoiseInterferenceProcessing);
 LTS.Assign(_.LTS);
 Received.Assign(_.Received);
 ReceptionFaults.Assign(_.ReceptionFaults);
}

VI::Main::External::GTC_Radar::Test::Test(ui _) :
 Data {
  {
   _.lblRadarTestFaultTransmitterPulsePower,
   _.lblRadarTestFaultSUMNoisePower,
   _.lblRadarTestFaultDF1NoisePower, _.lblRadarTestFaultDF2NoisePower,
   _.lblRadarTestFaultSUMCalibrationPower,
   _.lblRadarTestFaultDF1CalibrationPower, _.lblRadarTestFaultDF2CalibrationPower,
   _.lblRadarTestFaultSUMDirectWaveLevel, _.lblRadarTestFaultSUMBackwardWaveLevel,
   _.lblRadarTestFaultSUM_DF1AmplitudeDifference,
   _.lblRadarTestFaultSUM_DF2AmplitudeDifference,
   _.lblRadarTestFaultSynthesizerPLL_LoopCapture,
   _.lblRadarTestFaultA_E_Switch,
   _.lblRadarTestFaultTransmitterTemperature
  },
  _.lblRadarTestTransmitterPulsePower,
  _.lblRadarTestSUMNoisePower, _.lblRadarTestDF1NoisePower,
  _.lblRadarTestDF2NoisePower,
  _.lblRadarTestSUMCalibrationPower,
  _.lblRadarTestDF1CalibrationPower, _.lblRadarTestDF2CalibrationPower,
  _.lblRadarTestSUMDirectWaveLevel,
  _.lblRadarTestSUMBackwardWaveLevel,
  _.lblRadarTestSUM_DF1AmplitudeDifference,
  _.lblRadarTestSUM_DF2AmplitudeDifference,
  _.lblRadarTestSUM_DF1PhaseDifference,
  _.lblRadarTestSUM_DF2PhaseDifference,
  _.lblRadarTestCompleted
 },
 Received(_.lblRadarTestReceived), ReceptionFaults(_.lblRadarTestReceptionFaults) {
}

void VI::Main::External::GTC_Radar::Test::Assign(
 const Interface::DisplayRecord<ModuleRadarTest>::OutRecord& _) {
 Data.Assign(_);
 Received.Assign(_.Received);
 ReceptionFaults.Assign(_.ReceptionFaults);
}

VI::Main::External::GTC_Radar::GTC_Radar(ui _) :
 Mode(_), PlannedFrequencies(_), RadialSpeed(_), ETD(_), SS(_),
 Control(_), Training(_), ETD_Detection(_), SS_Data(_), Test(_) {
}

void VI::Main::External::GTC_Radar::Assign(
 const struct Interface::Command::Main::External::Radar& _) {
 Mode.Assign(_.Mode);
 PlannedFrequencies.Assign(_.PlannedFrequencies);
 RadialSpeed.Assign(_.RadialSpeed.In);
 ETD.Assign(_.ETD);
 SS.Assign(_.SS);
 Control.Assign(_.Control);
 Training.Assign(_.Training.In);
 ETD_Detection.Assign(_.ETD_Detection.Out);
 SS_Data.Assign(_.SS_Data.Out);
 Test.Assign(_.Test.Out);
}

VI::Main::Internal::Internal(ui _) :
 GTC(_), DAT(_), Comp2(_), LTS(_), TV(_), IR(_), GTC2(_), LRF(_), Training(_),
 // partizan stuff
 RadarComp2(_) {}

void VI::Main::Internal::Assign(const struct Interface::Command::Main::Internal& _) {
 LRF.Assign(_.LRF);
 LTS.Assign(_.LTS);
 Comp2.Assign(_.Comp2);
 DAT.Assign(_.DAT);
 GTC.Assign(_.GTC);
 GTC2.Assign(_.GTC2);
 TV.Assign(_.TV);
 IR.Assign(_.IR);
 Training.Assign(_.Training.In);
 RadarComp2.Assign(_.Comp2Radar);
}

void VI::Main::Internal::Lock() {
 DAT.Lock();
}

void VI::Main::Internal::Unlock() {
 DAT.Unlock();
}

VI::Main::Internal::GTC::GTC(ui _) : Out(_), In(_) {}

void VI::Main::Internal::GTC::Assign(const Interface::DisplayRecord<ModuleGTC>& _) {
 In.Assign(_.In);
 Out.Assign(_.Out);
}

template <>
constexpr auto enum_tab<GTC::Mode> = {
 "початковий", "1:??", "ЦВ", "3:??", "РН", "5:??", "AC по ЦАС", "7:??",
 "допошук по спіралі", "9:??", "круговий огляд", "B:??", "НЛЦ", "IC", "AC по РЛС", "контроль"
};

VI::Main::Internal::GTC::Out::Out(ui _) :
 Mode(_.lblGTC_InputMode), ModeIsChanging(_.lblGTC_InputModeIsChanging),
 TVReady(_.lblGTC_InputTVReady), IRReady(_.lblGTC_InputIRReady),
 LRFReady(_.lblGTC_InputLRFReady),
 Power(_.lblGTC_InputPower), IIT_Permit(_.lblGTC_InputInnerIT),
 DATChannel(_.lblGTC_InputDATChannelNumber), _7_2(_.lblGTC_Input7_2),
 ORU {
  { _.lblGTC_InputHADriveOn, _.lblGTC_InputElevDriveOn },
  _.lblGTC_InputElevDriveLimitHi, _.lblGTC_InputElevDriveLimitLow,
  {
   _.lblGTC_InputGCSBearing, AngleValue(_.lblGTC_InputGCSElev, AngleValue::NORMALIZE)
  },
  {
   _.lblGTC_InputSCSHA, _.lblGTC_InputSCSElev
  }
 },
 Sine(_.lblGTC_InputSine), Cosine(_.lblGTC_InputCosine),
 Mismatch { _.lblGTC_InputMismatchHA, _.lblGTC_InputMismatchElev },
 Detent { _.lblGTC_InputArresterHA, _.lblGTC_InputArresterElev },
 DetentError { _.lblGTC_InputArresterHAError, _.lblGTC_InputArresterElevError },
 SyncRequest(_.lblGTC_InputSyncRequest), SyncDone(_.lblGTC_InputSyncDone),
 LTS(_.lblGTC_InputLTS), Received(_.lblGTC_Received), ReceptionFaults(_.lblGTC_ReceptionFault) {
}

void VI::Main::Internal::GTC::Out::Assign(
 const Interface::DisplayRecord<ModuleGTC>::OutRecord& _) {
 Mode.Assign(_.State.Mode);
 ModeIsChanging.Assign(_.State.ModeIsChanging);
 TVReady.Assign(_.TVReady);
 IRReady.Assign(_.IRReady);
 LRFReady.Assign(_.LRFReady);
 Power.Assign(_.Power);
 IIT_Permit.Assign(_.IIT_Permit);
 DATChannel.Assign(_.DATChannel);
 _7_2.Assign(_._7_2);
 ORU.Assign(_.ORU);
 Sine.Assign(_.Sine);
 Cosine.Assign(_.Cosine);
 Mismatch.Assign(_.Mismatch);
 Detent.Assign(_.State.Detent);
 DetentError.Assign(_.DetentError);
 SyncRequest.Assign(_.SyncRequest);
 SyncDone.Assign(_.SyncDone);
 LTS.Assign(_.LTS);
 Received.Assign(_.Received);
 ReceptionFaults.Assign(_.ReceptionFaults);
}

VI::Main::Internal::GTC::In::In(ui _) :
 Mode(_.lblGTC_OutputMode),
 WiperOn(_.lblGTC_OutputWiperOn), HeatingOn(_.lblGTC_OutputHeatingOn),
 Fire(_.lblGTC_OutputFire), HeatStandby(_.lblGTC_OutputHeatStandby),
 IIT_Enable(_.lblGTC_OutputIIT_Enable), IROn(_.lblGTC_OutputIROn),
 DATChannel(_.lblGTC_OutputDATChannel), DAT1(_.lblGTC_OutputDAT1),
 ORU(_), Handle(_),
 Transmitted(_.lblGTCTransmitted) {
}

void VI::Main::Internal::GTC::In::Assign(const Interface::DisplayRecord<ModuleGTC>::InRecord& _) {
 Mode.Assign(_.Mode);
 WiperOn.Assign(_.WiperOn);
 HeatingOn.Assign(_.HeatingOn);
 Fire.Assign(_.Fire);
 HeatStandby.Assign(_.HeatStandby);
 IIT_Enable.Assign(_.IIT_Enable);
 IROn.Assign(_.IROn);
 DATChannel.Assign(_.DATChannel);
 DAT1.Assign(_.DAT1);
 // ORU guidance
 ORU.Assign(_.ORU);
 // handle control
 Handle.Assign(_.Handle);
 Transmitted.Assign(_.Transmitted);
}

VI::Main::Internal::GTC::In::ORU_Control::ORU_Control(ui _) :
 ETD { _.lblGTC_OutputETDBearing, _.lblGTC_OutputETDElevation } {
}

void VI::Main::Internal::GTC::In::ORU_Control::Assign(const ::GTC::ORU_Control& _) {
 ETD.Assign(_.ETD);
}

VI::Main::Internal::GTC::In::Handle_Control::Handle_Control(ui _) :
 Offset { _.lblGTC_OutputHandleHA, _.lblGTC_OutputHandleElev } {
}

void VI::Main::Internal::GTC::In::Handle_Control::Assign(const ::GTC::Handle_Control& _) {
 Offset.Assign(_.Deviation);
}

VI::Main::Internal::DAT::DAT(ui _) : Out(_), In(_) {}

void VI::Main::Internal::DAT::Assign(const Interface::DisplayRecord<ModuleDAT> _[]) {
 Out.Assign(_);
 In.Assign(_);
}

void VI::Main::Internal::DAT::Lock() {
 Out.ChannelNo.Lock();
 In.ChannelNo.Lock();
}

void VI::Main::Internal::DAT::Unlock() {
 Out.ChannelNo.Unlock();
 In.ChannelNo.Unlock();
}

template<>
constexpr auto enum_tab<DAT::Workmode> = { "робота", "контроль" };
template<>
constexpr auto enum_tab<DAT::TargetType> = { "ПЦ", "ТЦ" };

VI::Main::Internal::DAT::Out::Out(ui _) :
 Fault(_.lblDATFault),
 Mode(_.lblDATMode), TT(_.lblDATTT),
 Method(_.lblDATProcessingMethod), Break(_.lblDATBreak), Capture(_.lblDATCapture),
 Credibility(_.lblDATCredibility), Detection(_.lblDATDetection),
 TestingCompleted(_.lblDATTestingCompleted),
 AutotrackingMismatch(
  { _.lblDATAutotrackingMismatchHA, _.lblDATAutotrackingMismatchElev }),
 LTS(_.lblDATLTS), ChannelNo(_.cmbDATChannel),
 Received(_.lblDATReceived), ReceptionFaults(_.lblDATReceptionFaults) {
}

void VI::Main::Internal::DAT::Out::Assign(const Interface::DisplayRecord<ModuleDAT> _[]) {
 size_t ChNo = ChannelNo.GetCurrent();
 if (ChNo >= DAT_CHANNELS_COUNT) {
  ChNo = DAT_CHANNEL_1;
  ChannelNo.SetCurrent(ChNo);
 }
 const Interface::DisplayRecord<ModuleDAT>::OutRecord& __ = _[ChNo].Out;
 Fault.Assign(__.Fault); // channel is invalid
 Mode.Assign(__.Mode);
 TT.Assign(__.TT);
 Method.Assign(__.Method);
 Break.Assign(__.Break);
 Capture.Assign(__.Capture); // transition to autotracking mode
 Credibility.Assign(__.Credibility); // in percent
 Detection.Assign(__.Detection);
 TestingCompleted.Assign(__.TestingCompleted);
 AutotrackingMismatch.Assign(__.AutotrackingMismatch);
 LTS.Assign(__.LTS);
 Received.Assign(__.Received);
 ReceptionFaults.Assign(__.ReceptionFaults);
}

template<>
constexpr auto enum_tab<DAT::CameraType> = { "ТВК", "ТПК", "дод. 1-й канал", "дод. 2-й канал" };

VI::Main::Internal::DAT::In::In(ui _) :
 Video1(_.lblDAT_OutputVideo1), Video2(_.lblDAT_OutputVideo2),
 Video3(_.lblDAT_OutputVideo3),
 Rotation(_.lblDAT_OutputRotation), Mark(_.lblDAT_OutputMark),
 Processing(_.lblDAT_OutputProcessingMethod),
 TT(_.lblDAT_OutputTargetType), Source(_.lblDAT_OutputSource),
 Capture(_.lblDAT_OutputCapture), CaptureEnable(_.lblDAT_OutputCaptureEnable), On(_.lblDAT_OutputOn),
 ReserveMethodCoords { _.lblDAT_OutputReserveMethodX, _.lblDAT_OutputReserveMethodY },
 ChannelNo(_.cmbDATControlChannel),
 Transmitted(_.lblDATTransmitted) {
}

void VI::Main::Internal::DAT::In::Assign(
 const Interface::DisplayRecord<ModuleDAT> _[]) {
 size_t ChNo = ChannelNo.GetCurrent();
 if (ChNo >= DAT_CHANNELS_COUNT) {
  ChNo = DAT_CHANNEL_1;
  ChannelNo.SetCurrent(ChNo);
 }
 const Interface::DisplayRecord<ModuleDAT>::InRecord& __ = _[ChNo].In;
 Video1.Assign(__.Video1);
 Video2.Assign(__.Video2);
 Video3.Assign(__.Video3);
 Rotation.Assign(__.Rotation);
 Mark.Assign(__.Mark);
 Processing.Assign(__.Processing);
 TT.Assign(__.TT);
 Source.Assign(__.Source);
 Capture.Assign(__.Capture);
 CaptureEnable.Assign(__.CaptureEnable);
 On.Assign(__.On);
 ReserveMethodCoords.Assign(__.ReserveMethodCoords);
 Transmitted.Assign(__.Transmitted);
}

VI::Main::Internal::Comp2::Comp2(ui _) : Out(_), In(_) {
}

void VI::Main::Internal::Comp2::Assign(const Interface::DisplayRecord<ModuleComp2>& _) {
 In.Assign(_.In);
 Out.Assign(_.Out);
}

VI::Main::Internal::Comp2::Out::Out(ui _) :
 Control(_),
 Weather {
  _.lblComp2WindSpeed, _.lblComp2WindBearing, _.lblComp2Pressure,
  _.lblComp2Temperature, _.lblComp2ChargeTemperature
 },
 DAT_Control(_),
 TV {
   {
    _.lblComp2FocusSmoothlyPlus, _.lblComp2FocusSmoothlyMinus, _.lblComp2FocusNear,
    _.lblComp2FocusSmoothly, _.lblComp2FocusManual
   },
   {
    _.lblComp2FOVSmoothlyPlus, _.lblComp2FOVSmoothlyMinus, _.lblComp2FOVNarrow,
    _.lblComp2FOVSmoothly, _.lblComp2FOVManual
   },
   _.lblComp2TVNight, _.lblComp2TVBrightness, _.lblComp2TVContrast,
   { _.lblComp2CorrectionTVX, _.lblComp2CorrectionTVY }
 },
 IR {
  _.lblComp2IR_OutputHotBlack, _.lblComp2IR_OutputDigitalZoom,
  _.lblComp2IR_OutputOpticalZoomPlus, _.lblComp2IR_OutputOpticalZoomMinus,
  {
   _.lblComp2IR_OutputMenuOn
  },
  _.lblComp2IR_OutputContrastPlus, _.lblComp2IR_OutputContrastMinus,
  _.lblComp2IR_OutputBrightnessPlus, _.lblComp2IR_OutputBrightnessMinus,
  _.lblComp2IR_OutputFocusFar, _.lblComp2IR_OutputFocusNear, _.lblComp2IR_OutputCalibration,
  _.lblComp2IR_OutputBrightness, _.lblComp2IR_OutputContrast,
  { _.lblComp2IR_OutputCorrectionX, _.lblComp2IR_OutputCorrectionY }
 },
 LRF(_), GC(_), Time(_.lblComp2InputTime),
 Received(_.lblComp2InputReceived), ReceptionFaults(_.lblComp2InputReceptionFaults) {
}

void VI::Main::Internal::Comp2::Out::Assign(const Interface::DisplayRecord<ModuleComp2>::OutRecord& _) {
 Control.Assign(_.Control);
 Weather.Assign(_.Weather);
 DAT_Control.Assign(_.DAT_Control);
 TV.Assign(_.TV);
 // DAT.Assign(_.DAT);
 IR.Assign(_.IR);
 LRF.Assign(_.LRF);
 GC.Assign(_.GC);
 Time.Assign(_.Time);
 Received.Assign(_.Received);
 ReceptionFaults.Assign(_.ReceptionFaults);
}

template<>
constexpr auto enum_tab<Comp2::SysMode> = { "Робота", "Контроль", "Тренаж" };
template<>
constexpr auto enum_tab<Comp2::SubMode> = { "Початковий", "РН", "ЦВ", "АС", "ІС", "АП" };

VI::Main::Internal::Comp2::Out::Control::Control(ui _) :
 Mode(_.lblComp2InputMode), Submode(_.lblComp2InputSubmode),
 IIT_Enable(_.lblComp2IIT_Enable), AdditionalSearch(_.lblComp2AdditionalSearch),
 IROn(_.lblComp2IROn), WiperOn(_.lblComp2WiperOn),
 InternalHeatingOn(_.lblComp2InternalHeatingOn),
 Camera(_.lblComp2Camera), Fire(_.lblComp2Fire),
 AirTarget(_.lblComp2AirTarget), HeatStandby(_.lblComp2HeatStandby),
 DATChannel(_.lblComp2DATChannel), Radar(_.lblComp2Radar), LowFlyingTarget(_.lblComp2LFT),
 Training(_),
 ExternalTarget {
  _.lblComp2ExternalTargetDistance, _.lblComp2ExternalTargetBearing, _.lblComp2ExternalTargetElevation
 },
 HandleX(_.lblComp2HandleX), HandleY(_.lblComp2HandleY),
 ManualDistanceOn(_.lblComp2ManualDistanceOn), ManualDistance(_.lblComp2ManualDistanceCode),
 OEUC { _.lblComp2OEUC_OrientationHA, _.lblComp2OEUC_OrientationElev } {
}

void VI::Main::Internal::Comp2::Out::Control::Assign(
 const struct ::Comp2::InputData::Control& _) {
 Mode.Assign(_.Mode);
 Submode.Assign(_.SubMode);
 IIT_Enable.Assign(_.IIT_Enable);
 AdditionalSearch.Assign(_.AdditionalSearch);
 IROn.Assign(_.IROn);
 WiperOn.Assign(_.WiperOn);
 InternalHeatingOn.Assign(_.InternalHeatingOn);
 Camera.Assign(_.Camera);
 Fire.Assign(_.Fire);
 HeatStandby.Assign(_.HeatStandby);
 AirTarget.Assign(_.AirTarget);
 DATChannel.Assign(_.DATChannel);
 Radar.Assign(_.Radar);
 LowFlyingTarget.Assign(_.LowFlyingTarget);
 Training.Assign(_.Training);
 ExternalTarget.Assign(_.ExternalTarget);
 HandleX.Assign(_.Joystick.X);
 HandleY.Assign(_.Joystick.Y);
 ManualDistanceOn.Assign(_.ManualDistanceOn);
 ManualDistance.Assign(_.ManualDistance);
 OEUC.Assign(_.OEUC);
}

VI::Main::Internal::Comp2::Out::DAT_Control::DAT_Control(ui _) :
 Rotation(_.lblComp2DATControlRotation), Mark(_.lblComp2DATControlMark),
 DAT1 {
  _.lblComp2DAT1ProcessingMethod, _.lblComp2DAT1TargetType, _.lblComp2DAT1Source,
  _.lblComp2DAT1Capture, _.lblComp2DAT1On
 },
 DAT2 {
  _.lblComp2DAT2ProcessingMethod, _.lblComp2DAT2TargetType, _.lblComp2DAT2Source,
  _.lblComp2DAT2Capture, _.lblComp2DAT2On
 } {
}

void VI::Main::Internal::Comp2::Out::DAT_Control::Assign(const struct ::Comp2::InputData::DAT_Control& _) {
 Rotation.Assign(_.Rotation);
 Mark.Assign(_.Mark);
 DAT1.Assign(_.DAT1);
 DAT2.Assign(_.DAT2);
}

void VI::Main::Internal::Comp2::Out::DAT_Control::Channel::Assign(
 const ::DAT::OutputData& _) {
 Processing.Assign(_.Processing);
 TT.Assign(_.TT);
 Source.Assign(_.Source);
 Capture.Assign(_.Capture);
 On.Assign(_.On);
}

VI::Main::Internal::Comp2::Out::Control::Training::Training(ui _) :
 Code(_.lblComp2TrainingCode), StationaryTarget(_.lblComp2TrainingStationaryTarget), On(_.lblComp2TrainingOn),
 StationaryTargetCoords {
  _.lblComp2TrainingDistance, _.lblComp2TrainingBearing, _.lblComp2TrainingElevation
 } {
}

void VI::Main::Internal::Comp2::Out::Control::Training::Assign(
 const struct ::Comp2::InputData::Control::Training& _) {
 Code.Assign(_.Code);
 StationaryTarget.Assign(_.StationaryTarget);
 On.Assign(_.On);
 StationaryTargetCoords.Assign(_.StationaryTargetCoords);
}

void VI::Main::Internal::Comp2::Out::IR_Control::Assign(
 const struct ::Comp2::InputData::IR& _) {
 HotBlack.Assign(_.HotBlack);
 DigitalZoom.Assign(_.DigitalZoom);
 OpticalZoomPlus.Assign(_.OpticalZoomPlus);
 OpticalZoomMinus.Assign(_.OpticalZoomMinus);
 Menu.Assign(_.Menu);
 if (_.Menu.On) {
  ContrastPlus.SetText("вгору");
  ContrastMinus.SetText("вниз");
  BrightnessPlus.SetText("вправо");
  BrightnessMinus.SetText("вліво");
 } else {
  ContrastPlus.SetText("контраст+");
  ContrastMinus.SetText("контраст-");
  BrightnessPlus.SetText("яскравість+");
  BrightnessMinus.SetText("яскравість-");
 }
 ContrastPlus.Assign(_.ContrastPlus);
 ContrastMinus.Assign(_.ContrastMinus);
 BrightnessPlus.Assign(_.BrightnessPlus);
 BrightnessMinus.Assign(_.BrightnessMinus);
 FocusFar.Assign(_.FocusFar);
 FocusNear.Assign(_.FocusNear);
 Calibration.Assign(_.Calibration);
 Brightness.Assign(_.Brightness);
 Contrast.Assign(_.Contrast);
 CenterCorrection.Assign(_.CenterCorrection);
}

void VI::Main::Internal::Comp2::Out::IR_Control::_Menu::Assign(
 const struct ::Comp2::InputData::IR::Menu& _) {
 On.Assign(_.On);
}

VI::Main::Internal::Comp2::Out::LRF_Control::LRF_Control(ui _) :
 TargetNumber(_.lblComp2LRFTargetNumber),
 Work(_.lblComp2LRFWork), Single(_.lblComp2LRFSingle), Frequent(_.lblComp2LRFFrequent),
 PilotLaser(_.lblComp2LRFPilot), Frequency(_.lblComp2LRFFrequency) {
}

void VI::Main::Internal::Comp2::Out::LRF_Control::Assign(
 const struct ::Comp2::InputData::LRF& _) {
 Work.Assign(_.Signals.Work);
 Single.Assign(_.Signals.Single);
 Frequent.Assign(_.Signals.Frequent);
 PilotLaser.Assign(_.Signals.PilotLaser);
 TargetNumber.Assign(_.TargetNumber);
 Frequency.Assign(_.Frequency);
}

VI::Main::Internal::Comp2::Out::GunControl::GunControl(ui _) :
 DriveControl(_), DriveCorrections(_),
 BallisticData { _.lblComp2CMCStartSpeedMismatch, _.lblComp2CMCMassMismatch } {
}

void VI::Main::Internal::Comp2::Out::GunControl::Assign(const ::Comp2::InputData::GunControl& _) {
// const size_t Index = ModuleIndex.Get();
 DriveControl.Assign(_.DriveControl);
 DriveCorrections.Assign(_.DriveCorrections);
 BallisticData.Assign(_.BalData[0]);
}

VI::Main::Internal::Comp2::Out::GunControl::DriveControl::DriveControl(ui _) :
 GunControl(_.lblComp2CMCGunControl), RemoteFuseControl(_.lblComp2CMCRemoteFuseControl),
 JointAlignment(_.lblComp2CMCDriveJointAlignment),
 ParallaxJointAlignment(_.lblComp2CMCDriveParallaxJointAlignment),
 Distance(_.lblComp2CMCDriveDistance) {
}

void VI::Main::Internal::Comp2::Out::GunControl::DriveControl::Assign(
 const struct ::Comp2::InputData::GunControl::DriveControl& _) {
 GunControl.Assign(_.GunControl);
 RemoteFuseControl.Assign(_.RemoteFuseControl);
 JointAlignment.Assign(_.JointAlignment);
 ParallaxJointAlignment.Assign(_.ParallaxJointAlignment);
 Distance.Assign(_.Distance);
}

VI::Main::Internal::Comp2::Out::GunControl::DriveCorrections::DriveCorrections(ui _) :
 Orientation {
  _.lblComp2CMCCorrectionsOrientationHA, _.lblComp2CMCCorrectionsOrientationElev
 },
 PointingAngleMismatch {
  _.lblComp2CMCCorrectionsPointingAngleHA, _.lblComp2CMCCorrectionsPointingAngleElev
 },
 SituationAngleMismatch {
  _.lblComp2CMCCorrectionsSituationAngleHA, _.lblComp2CMCCorrectionsSituationAngleElev
 },
 Miss { _.lblComp2CMCCorrectionsMissHA, _.lblComp2CMCCorrectionsMissElev } {
}

void VI::Main::Internal::Comp2::Out::GunControl::DriveCorrections::Assign(
 const struct ::Comp2::DriveCorrections& _) {
 Orientation.Assign(_.Orientation);
 PointingAngleMismatch.Assign(_.PointingAngleMismatch);
 SituationAngleMismatch.Assign(_.SituationAngleMismatch);
 Miss.Assign(_.Miss);
}

void VI::Main::Internal::Comp2::Out::GunControl::BallisticData::Assign(
 const ::Comp2::InputData::GunControl::BallisticData& _) {
 StartSpeedMismatch.Assign(_.StartSpeedMismatch);
 MassMismatch.Assign(_.MassMismatch);
}

VI::Main::Internal::Comp2::In::In(ui _) :
 State(_), GTC(_), TV(_), IR(_), LRF(_),
 BallisticModule(_), Gun(_),
 Navig {
  { _.lblComp2NSHeading, _.lblComp2NSRolling, _.lblComp2NSPitch },
  { _.lblComp2NSHeadingChange, _.lblComp2NSRollingChange, _.lblComp2NSPitchChange },
  { _.lblComp2NSSpeedX, _.lblComp2NSSpeedY, _.lblComp2NSSpeedZ },
  NS_Faults {
   _.lblComp2NSFaultHeaderChecksum, _.lblComp2NSFaultDataChecksum,
   _.lblComp2NSFaultCount, _.lblComp2NSFaultGeneral, _.lblComp2NSOff
  }
 },
 Time(_.lblComp2OutputTime), Transmitted(_.lblComp2OutputTransmitted) {
}

void VI::Main::Internal::Comp2::In::Assign(const Interface::DisplayRecord<ModuleComp2>::InRecord& _) {
 State.Assign(_.State);
 GTC.Assign(_.GTC);
 TV.Assign(_.GTC.TV);
 IR.Assign(_.GTC.IR, _.GTC.IR_Temperature);
 LRF.Assign(_.LRF);
 BallisticModule.Assign(_.BallisticModule);
 Gun.Assign(_.Gun);
 Navig.Assign(_.Navig);
 Time.Assign(_.Time);
 Transmitted.Assign(_.Transmitted);
}

VI::Main::Internal::Comp2::In::State::State(ui _) :
 Mode(_.lblComp2OutputMode), SubMode(_.lblComp2OutputSubmode),
 AdditionalSearch(_.lblComp2OutputAdditionalSearch),
 LowFlyingTarget(_.lblComp2OutputLowFlyingTarget),
 ModeIsChanging(_.lblComp2OutputModeIsChanging), Ready(_) {
}

void VI::Main::Internal::Comp2::In::State::Assign(const struct ::Comp2::OutputData::State& _) {
 Mode.Assign(_.Mode);
 SubMode.Assign(_.SubMode);
 AdditionalSearch.Assign(_.AdditionalSearch);
 LowFlyingTarget.Assign(_.LowFlyingTarget);
 ModeIsChanging.Assign(_.ModeIsChanging);
 Ready.Assign(_.Ready);
}

VI::Main::Internal::Comp2::In::State::Ready::Ready(ui _) :
 TV(_.lblComp2OutputReadyTV), IR(_.lblComp2OutputReadyIR),
 Navig(_.lblComp2OutputReadyNavig) {
}

void VI::Main::Internal::Comp2::In::State::Ready::Assign(
 const struct ::Comp2::OutputData::State::Ready& _) {
 TV.Assign(_.TV);
 IR.Assign(_.IR);
 Navig.Assign(_.Navig);
}

VI::Main::Internal::Comp2::In::GTC_State::GTC_State(ui _) :
 Mode(_.lblComp2GTCMode), ModeIsChanging(_.lblComp2GTCDATModeIsChanging),
 Detent {
  _.lblComp2DetentHA, _.lblComp2DetentElev
 },
 Power(_.lblComp2GTCPower), _7_2(_.lblComp2GTC7_2),
 IIT(_.lblComp2GTCDATIIT),
 SyncRequest(_.lblComp2GTCSyncRequest), SyncDone(_.lblComp2GTCSyncDone),
 DATChannel(_.lblComp2GTCDATChannel),
 Faults(_),
 Distance(_.lblComp2OutputDistance),
 OEUC {
  { _.lblComp2HADriveOn, _.lblComp2ElevDriveOn },
  _.lblComp2ElevDriveLimitHi, _.lblComp2ElevDriveLimitLow,
  {
   _.lblComp2GCSBearing, AngleValue(_.lblComp2GCSElev, AngleValue::NORMALIZE)
  },
  { _.lblComp2SCSHA, _.lblComp2SCSElev }
 },
 Mismatch { _.lblComp2MismatchHA, _.lblComp2MismatchElev },
 Training {
  _.lblComp2OutputTrainingDistance,
  _.lblComp2OutputTrainingBearing, _.lblComp2OutputTrainingElevation
 },
 DAT(_) {
}

void VI::Main::Internal::Comp2::In::GTC_State::Assign(
 const ::Comp2::OutputData::GTC_State& _) {
 Mode.Assign(_.State.Mode);
 ModeIsChanging.Assign(_.State.ModeIsChanging);
 Detent.Assign(_.State.Detent);
 Power.Assign(_.Power);
 _7_2.Assign(_._7_2);
 IIT.Assign(_.IIT);
 SyncRequest.Assign(_.SyncRequest);
 SyncDone.Assign(_.SyncDone);
 DATChannel.Assign(_.DATChannel);
 Faults.Assign(_.Faults);
 Distance.Assign(_.Distance);
 OEUC.Assign(_.OEUC);
 Mismatch.Assign(_.Mismatch);
 Training.Assign(_.TrainingTarget);
 DAT.Assign(_.DAT);
}

VI::Main::Internal::Comp2::In::GTC_State::Faults::Faults(ui _) :
 // 1.1
 DeviceController(_.lblComp2FaultA_PC), LRF(_.lblComp2FaultA_LRF),
 TV(_.lblComp2FaultA_TV), IR(_.lblComp2FaultA_IR), ASS(_.lblComp2FaultA_ASS),
 EncoderExchange { _.lblComp2FaultEncoderHAExchange, _.lblComp2FaultEncoderElevExchange },
 Encoder { _.lblComp2FaultEncoderHA, _.lblComp2FaultEncoderElev },
 Detent { _.lblComp2FaultDetentHA, _.lblComp2FaultDetentElev },
 // 4.1
 GTC(_.lblComp2FaultA_GTC),
 ExchangeDAT(_.lblComp2FaultExchangeDAT), ExchangeComp(_.lblComp2FaultExchangeComp),
 ExchangeDC(_.lblComp2FaultExchangeDC), ExchangeLTS_DC(_.lblComp2FaultExchangeLTS_DC),
 ExchangeNavig(_.lblComp2FaultExchangeNavig),
 // 7.2
 Overheat(_.lblComp2FaultOverheat), No24V(_.lblComp2FaultNo24V),
 NoPhase(_.lblComp2FaultNoPhase), Overcurrent(_.lblComp2FaultOvercurrent),
 No7_2(_.lblComp2FaultNo7_2),
 AmplifierHAEngine(_.lblComp2FaultAmplifierHAEngine),
 AmplifierHAA1(_.lblComp2FaultAmplifierHAA1),
 AmplifierElevEngine(_.lblComp2FaultAmplifierElevEngine),
 AmplifierElevA3(_.lblComp2FaultAmplifierElevA3),
 ExchangeQ(_.lblComp2FaultExchangeQ), ExchangeE(_.lblComp2FaultExchangeE) {
}

void VI::Main::Internal::Comp2::In::GTC_State::Faults::Assign(
 const struct ::Comp2::OutputData::GTC_State::Faults& _) {
 // 1.1
 DeviceController.Assign(_.DeviceController);
 LRF.Assign(_.LRF);
 TV.Assign(_.TV);
 IR.Assign(_.IR);
 ASS.Assign(_.ASS);
 EncoderExchange.Assign(_.EncoderExchange);
 //
 Encoder.Assign(_.Encoder);
 Detent.Assign(_.Detent);
 // 4.1
 GTC.Assign(_.GTC);
 ExchangeDAT.Assign(_.ExchangeDAT);
 ExchangeComp.Assign(_.ExchangeComp);
 ExchangeDC.Assign(_.ExchangeDC);
 ExchangeLTS_DC.Assign(_.ExchangeLTS_DC);
 ExchangeNavig.Assign(_.ExchangeNavig);
 // 7.2
 Overheat.Assign(_.Overheat);
 No24V.Assign(_.No24V);
 NoPhase.Assign(_.NoPhase);
 Overcurrent.Assign(_.Overcurrent);
 No7_2.Assign(_.No7_2);
 // ПФ1.1.2
 AmplifierHAEngine.Assign(_.AmplifierHAEngine);
 AmplifierHAA1.Assign(_.AmplifierHAA1);
 AmplifierElevEngine.Assign(_.AmplifierElevEngine);
 AmplifierElevA3.Assign(_.AmplifierElevA3);
 ExchangeQ.Assign(_.ExchangeQ);
 ExchangeE.Assign(_.ExchangeE);
}

VI::Main::Internal::Comp2::In::GTC_State::DAT_State::DAT_State(ui _) :
 Channel(_) {
}

void VI::Main::Internal::Comp2::In::GTC_State::DAT_State::Assign(
 const ::Comp2::OutputData::GTC_State::DAT_State& _) {
 Channel.Assign(_.Channel);
}

VI::Main::Internal::Comp2::In::GTC_State::DAT_State::Channel::Channel(ui _) :
 ChannelNo(_.cmbComp2DATChannel),
 Method(_.lblComp2OutputDATProcessingMetod), Break(_.lblComp2OutputDATBreak),
 Capture(_.lblComp2OutputDATCapture),
 Credibility(_.lblComp2OutputDATCredibility),
 Detection(_.lblComp2OutputDATDetection), Fault(_.lblComp2OutputDATFault),
 TestingCompleted(_.lblComp2OutputDATTestingCompleted),
 Mode(_.lblComp2OutputDATMode), TT(_.lblComp2OutputDATTT),
 AutoTrackingMismatch(
  {
   _.lblComp2OutputDATAutotrackingMismatchHA, _.lblComp2OutputDATAutotrackingMismatchElev
  }) {
}

void VI::Main::Internal::Comp2::In::GTC_State::DAT_State::Channel::Assign(
 const struct ::Comp2::OutputData::GTC_State::DAT_State::Channel _[]) {
 size_t ChNo = ChannelNo.GetCurrent();
 if (ChNo >= DAT_CHANNELS_COUNT) {
  ChNo = DAT_CHANNEL_1;
  ChannelNo.SetCurrent(ChNo);
 }
 const struct ::Comp2::OutputData::GTC_State::DAT_State::Channel& __ = _[ChNo];
 Method.Assign(__.Method);
 Break.Assign(__.Break);
 Capture.Assign(__.Capture);
 Credibility.Assign(__.Credibility);
 Detection.Assign(__.Detection);
 Fault.Assign(__.Fault);
 TT.Assign(__.TT);
 Mode.Assign(__.Mode);
 TestingCompleted.Assign(__.TestingCompleted);
 AutoTrackingMismatch.Assign(__.AutoTrackingMismatch);
}

VI::Main::Internal::Comp2::In::TV_State::TV_State(ui _) :
 FOV { _.lblComp2TVFOVHA, _.lblComp2TVFOVElev } {}

void VI::Main::Internal::Comp2::In::TV_State::Assign(
 const Spherical2DCoords& _) {
 FOV.Assign(_);
}

VI::Main::Internal::Comp2::In::IR_State::IR_State(ui _) :
 FOV { _.lblComp2IRFOVHA, _.lblComp2IRFOVElev }, Temperature(_.lblComp2IRTemperature) {}

void VI::Main::Internal::Comp2::In::IR_State::Assign(const Spherical2DCoords& F, double T) {
 FOV.Assign(F);
 Temperature.Assign(T);
}

VI::Main::Internal::Comp2::In::LRF_State::LRF_State(ui _) :
 Mode(_.lblComp2LRFMode, {
  "початковий", "робота", "вимiрювання", "контроль", "технологічний", "", "", "стан"
 }),
 Readiness(_.lblComp2LRFReadiness),
 ReceptionFault(_.lblComp2LRFReceptionFault), ControlFault(_.lblComp2LRFControlFault),
 TemperatureReady(_.lblComp2LRFTemperatureReadiness),
 Faults {
  _.lblComp2LRFFaultFRAM, _.lblComp2LRFFaultCRC,
  _.lblComp2LRFFaultLowPeriod, _.lblComp2LRFFaultQSwitch,
  _.lblComp2LRFFaultLPS, _.lblComp2LRFFaultWTF, _.lblComp2LRFFaultOpticalStart
  },
  State {
  _.lblComp2LRFTargets, _.lblComp2LRFStateLowPeriod, _.lblComp2LRFStateOnboardNetwork,
  _.lblComp2LRFStateWTF, _.lblComp2LRFStateTestingCompleted, _.lblComp2LRFStateFault
  },
 Distance1(_.lblComp2LRFD1), Distance2(_.lblComp2LRFD2), Distance3(_.lblComp2LRFD3),
 Measuring(_.lblComp2LRFMeasuring),
 Resource(_.lblComp2LRFResource) {
}

void VI::Main::Internal::Comp2::In::LRF_State::Assign(
 const ::Comp2::OutputData::LRF_State& _) {
 Mode.Assign(_.Mode);
 Readiness.Assign(_.Readiness);
 ReceptionFault.Assign(_.ReceptionFault);
 ControlFault.Assign(_.ControlFault);
 TemperatureReady.Assign(_.TemperatureReady);
 Faults.Assign(_.Faults);
 State.Assign(_.State);
 Distance1.Assign(_.Distance1);
 Distance2.Assign(_.Distance2);
 Distance3.Assign(_.Distance3);
 Measuring.Assign(_.Measuring);
 Resource.Assign(_.Resource);
}

VI::Main::Internal::Comp2::In::BallisticModule::BallisticModule(ui _) :
 Mode(_.lblComp2BallisticMode), SubMode(_.lblComp2BallisticSubmode),
 State(_), ExchangeFaults(_), Limits(_), Faults(_) {
}

void VI::Main::Internal::Comp2::In::BallisticModule::Assign(
 const struct ::Comp2::OutputData::BallisticModule& _) {
 Mode.Assign(_.Mode);
 SubMode.Assign(_.SubMode);
 State.Assign(_.State);
 ExchangeFaults.Assign(_.ExchangeFaults);
 Limits.Assign(_.Limits);
 Faults.Assign(_.Faults);
}

VI::Main::Internal::Comp2::In::BallisticModule::State::State(ui _) :
 TestingCompleted(_.lblComp2BallisticTestingIsOver),
 PointingAngleReady(_.lblComp2BallisticPointingAngleReady),
 ITReady(_.lblComp2BallisticIT_Ready) {
}

void VI::Main::Internal::Comp2::In::BallisticModule::State::Assign(
 const struct ::Comp2::OutputData::BallisticModule::State& _) {
 TestingCompleted.Assign(_.TestingCompleted);
 PointingAngleReady.Assign(_.PointingAngleReady);
 ITReady.Assign(_.ITReady);
}

VI::Main::Internal::Comp2::In::BallisticModule::ExchangeFaults::ExchangeFaults(ui _) :
 Gun(_.lblComp2BallisticExchangeFaultGun), Navig(_.lblComp2BallisticExchangeFaultNavig),
 GTC(_.lblComp2BallisticExchangeFaultGTC), Comp2(_.lblComp2BallisticExchangeFaultComp2) {}

void VI::Main::Internal::Comp2::In::BallisticModule::ExchangeFaults::Assign(
 const struct ::Comp2::OutputData::BallisticModule::ExchangeFaults& _) {
 Gun.Assign(_.Gun);
 Navig.Assign(_.Navig);
 GTC.Assign(_.GTC);
 Comp2.Assign(_.Comp2);
}

VI::Main::Internal::Comp2::In::BallisticModule::Limits::Limits(ui _) :
 RightHA(_.lblComp2BallisticLimitHARight), LeftHA(_.lblComp2BallisticLimitHALeft),
 HighElev(_.lblComp2BallisticLimitElevHigh), LowElev(_.lblComp2BallisticLimitElevLow) {
}

void VI::Main::Internal::Comp2::In::BallisticModule::Limits::Assign(
 const struct ::Comp2::OutputData::BallisticModule::Limits& _) {
 RightHA.Assign(_.RightHA);
 LeftHA.Assign(_.LeftHA);
 HighElev.Assign(_.HighElev);
 LowElev.Assign(_.LowElev);
}

VI::Main::Internal::Comp2::In::BallisticModule::Faults::Faults(ui _) :
 GunXExchange(_.lblComp2BallisticCommandGunXExchange),
 GunHAExchange(_.lblComp2BallisticCommandGunHAExchange),
 GunHA(_.lblComp2BallisticCommandGunHAFault),
 GunElevExchange(_.lblComp2BallisticCommandGunElevExchange),
 GunElev(_.lblComp2BallisticCommandGunElevFault),
 GunTimerExchange(_.lblComp2BallisticCommandGunTimerExchange),
 GunTimer(_.lblComp2BallisticCommandGunTimerFault) {
}

void VI::Main::Internal::Comp2::In::BallisticModule::Faults::Assign(
 const struct ::Comp2::OutputData::BallisticModule::Faults& _) {
 GunXExchange.Assign(_.GunXExchange);
 GunHAExchange.Assign(_.GunHAExchange);
 GunHA.Assign(_.GunHA);
 GunElevExchange.Assign(_.GunElevExchange);
 GunElev.Assign(_.GunElev);
 GunTimerExchange.Assign(_.GunTimerExchange);
 GunTimer.Assign(_.GunTimer);
}

VI::Main::Internal::Comp2::In::GunState::GunState(ui _) :
 TowerPosition { _.lblComp2CMStatePositionHA, _.lblComp2CMStatePositionElev },
 DesiredAngle { _.lblComp2CMStateDesiredAngleHA, _.lblComp2CMStateDesiredAngleElev },
 CurrentDistance(_.lblComp2CMStateDistanceCurrent), MeetDistance(_.lblComp2CMStateDistanceMeet),
 FlightTime(_.lblComp2CMStateFlightTime) {
}

void VI::Main::Internal::Comp2::In::GunState::Assign(
 const ::Comp2::OutputData::GunState& _) {
 TowerPosition.Assign(_.TowerPosition);
 DesiredAngle.Assign(_.DesiredAngle);
 CurrentDistance.Assign(_.CurrentDistance);
 MeetDistance.Assign(_.MeetDistance);
 FlightTime.Assign(_.FlightTime);
}

void VI::Main::Internal::Comp2::In::Navig::Assign(
 const struct ::Comp2::OutputData::Navig& _) {
 Rot.Assign(_.Rot);
 Speed.Assign(_.SpeedPart);
 RotChange.Assign(_.RotChange);
 Faults.Assign(_.Faults);
}

VI::Main::Internal::LTS::LTS(ui _) :
 Time(_.lblLTS),
 Received(_.lblLTSReceived), ReceptionFaults(_.lblLTSReceptionFaults), Transmitted(_.lblLTSTransmitted) {}

void VI::Main::Internal::LTS::Assign(const Interface::DisplayRecord<ModuleLTS>& _) {
 Time.Assign(_.Out.LTS);
 Received.Assign(_.Out.Received);
 ReceptionFaults.Assign(_.Out.ReceptionFaults);
 Transmitted.Assign(_.In.Transmitted);
}

VI::Main::Internal::TV::TV(ui _) : In(_) {}

void VI::Main::Internal::TV::Assign(const Interface::DisplayRecord<ModuleTV>& _) {
 In.Assign(_.In);
}

VI::Main::Internal::TV::In::In(ui _) :
 Data {
  {
   _.lblTV_OutputFocusSmoothlyPlus, _.lblTV_OutputFocusSmoothlyMinus, _.lblTV_OutputFocusNear,
   _.lblTV_OutputFocusSmoothly, _.lblTV_OutputFocusManual
  },
  {
   _.lblTV_OutputFOVSmoothlyPlus, _.lblTV_OutputFOVSmoothlyMinus, _.lblTV_OutputFOVNarrow,
   _.lblTV_OutputFOVSmoothly, _.lblTV_OutputFOVManual
  },
  _.lblTV_OutputNight, _.lblTV_OutputBrightness, _.lblTV_OutputContrast,
  { _.lblTV_OutputCorrectionX, _.lblTV_OutputCorrectionY }
 },
 Transmitted(_.lblTVTransmitted) {
}

void VI::Main::Internal::TV::In::Assign(const Interface::DisplayRecord<ModuleTV>::InRecord& _) {
 Data.Focus.Assign(_.Focus);
 Data.FOV.Assign(_.FOV);
 Data.Night.Assign(_.Night);
 Data.Brightness.Assign(_.Brightness);
 Data.Contrast.Assign(_.Contrast);
 Data.CenterCorrection.Assign(_.CenterCorrection);
 Transmitted.Assign(_.Transmitted);
}

VI::Main::Internal::IR::IR(ui _) : In(_) {}

void VI::Main::Internal::IR::Assign(const Interface::DisplayRecord<ModuleIR>& _) {
 In.Assign(_.In);
}

VI::Main::Internal::IR::In::In(ui _) :
 Data(_), Transmitted(_.lblIRTransmitted) {}

void VI::Main::Internal::IR::In::Assign(const Interface::DisplayRecord<ModuleIR>::InRecord& _) {
 Data.Assign(_);
 Transmitted.Assign(_.Transmitted);
}

VI::Main::Internal::IR::In::Control::Control(ui _) :
 HotBlack(_.lblIR_OutputHotBlack),
 DigitalZoom(_.lblIR_OutputDigitalZoom),
 OpticalZoomPlus(_.lblIR_OutputOpticalZoomPlus), OpticalZoomMinus(_.lblIR_OutputOpticalZoomMinus),
 MenuOn(_.lblIR_OutputMenuOn),
 ContrastPlus(_.lblIR_OutputContrastPlus), ContrastMinus(_.lblIR_OutputContrastMinus),
 BrightnessPlus(_.lblIR_OutputBrightnessPlus), BrightnessMinus(_.lblIR_OutputBrightnessMinus),
 FocusFar(_.lblIR_OutputFocusFar), FocusNear(_.lblIR_OutputFocusNear),
 Calibration(_.lblIR_OutputCalibration),
 Brightness(_.lblIR_OutputBrightness), Contrast(_.lblIR_OutputContrast),
 CenterCorrection { _.lblIR_OutputCorrectionX, _.lblIR_OutputCorrectionY } {
}

void VI::Main::Internal::IR::In::Control::Assign(const ::IR::OutputData& _) {
 HotBlack.Assign(_.HotBlack);
 DigitalZoom.Assign(_.DigitalZoom);
 OpticalZoomPlus.Assign(_.OpticalZoomPlus);
 OpticalZoomMinus.Assign(_.OpticalZoomMinus);
 MenuOn.Assign(_.MenuOn);
 ContrastPlus.Assign(_.ContrastPlus);
 ContrastMinus.Assign(_.ContrastMinus);
 BrightnessPlus.Assign(_.BrightnessPlus);
 BrightnessMinus.Assign(_.BrightnessMinus);
 FocusFar.Assign(_.FocusFar);
 FocusNear.Assign(_.FocusNear);
 Calibration.Assign(_.Calibration);
 Brightness.Assign(_.Brightness);
 Contrast.Assign(_.Contrast);
 CenterCorrection.Assign(_.CenterCorrection);
}

VI::Main::Internal::GTC2::GTC2(ui _) : Out(_) {}

void VI::Main::Internal::GTC2::Assign(const Interface::DisplayRecord<ModuleGTC2>& _) {
 Out.Assign(_.Out);
}

VI::Main::Internal::GTC2::Out::Out(ui _) :
 TV {_.lblGTC2_InputTVFOVHA, _.lblGTC2_InputTVFOVElev },
 IR { _.lblGTC2_InputIRFOVHA, _.lblGTC2_InputIRFOVElev },
 FRAM(_.lblGTC2FRAM), CRC(_.lblGTC2CRC),
 IntervalMeasurer(_.lblGTC2IntervalMeasurer),
 QSwitch(_.lblGTC2QSwitch), LPS(_.lblGTC2LPS),
 WTF0(_.lblGTC2WTF0), OpticalStart(_.lblGTC2OpticalStart),
 Targets(_.lblGTC2Targets), LowPeriod(_.lblGTC2LowPeriod),
 OnboardNetwork(_.lblGTC2OnboardNetwork), WTF1(_.lblGTC2WTF1),
 TestingCompleted(_.lblGTC2TestingCompleted), Failure(_.lblGTC2Fault),
 Faults {
  _.lblGTC2FaultA_PC, _.lblGTC2FaultA_LRF,
  _.lblGTC2FaultA_TV, _.lblGTC2FaultA_IR, _.lblGTC2FaultA_ASS,
  _.lblGTC2FaultEncoderHAExchange, _.lblGTC2FaultEncoderElevExchange,
  _.lblGTC2FaultEncoderHA, _.lblGTC2FaultEncoderElev,
  _.lblGTC2FaultA_GTC,
  _.lblGTC2FaultExchangeDAT, _.lblGTC2FaultExchangeComp, _.lblGTC2FaultExchangeDC,
  _.lblGTC2FaultExchangeLTS_DC,_.lblGTC2FaultExchangeNavig,
  _.lblGTC2FaultOverheat, _.lblGTC2FaultNo24V, _.lblGTC2FaultNo7_2,
  _.lblGTC2FaultNS_NoH, _.lblGTC2FaultNS_NoD,
  _.lblGTC2FaultNoPhase, _.lblGTC2FaultOvercurrent,
  _.lblGTC2FaultAmplifierHAEngine, _.lblGTC2FaultAmplifierHAA1,
  _.lblGTC2FaultAmplifierElevEngine, _.lblGTC2FaultAmplifierElevA3,
  _.lblGTC2FaultExchangeQ, _.lblGTC2FaultExchangeE
 },
 Received(_.lblGTC2Received), ReceptionFaults(_.lblGTC2ReceptionFaults) {
}

void VI::Main::Internal::GTC2::Out::Assign(const Interface::DisplayRecord<ModuleGTC2>::OutRecord& _) {
 TV.Assign(_.TV);
 IR.Assign(_.IR);
 FRAM.Assign(_.Something.FRAM);
 CRC.Assign(_.Something.CRC);
 IntervalMeasurer.Assign(_.Something.IntervalMeasurer);
 QSwitch.Assign(_.Something.QSwitch);
 LPS.Assign(_.Something.LPS);
 WTF0.Assign(_.Something.WTF0);
 OpticalStart.Assign(_.Something.OpticalStart);
 Targets.Assign(_.Something.Targets);
 LowPeriod.Assign(_.Something.LowPeriod);
 OnboardNetwork.Assign(_.Something.OnboardNetwork);
 WTF1.Assign(_.Something.WTF1); // Ошибка драйв. ТЕС
 TestingCompleted.Assign(_.Something.TestingCompleted);
 Failure.Assign(_.Something.Failure);
 Faults.Assign(_.Faults);
 Received.Assign(_.Received);
 ReceptionFaults.Assign(_.ReceptionFaults);
}

VI::Main::Internal::LRF::LRF(ui _) : Out(_), In(_) {
}

void VI::Main::Internal::LRF::Assign(const Interface::DisplayRecord<ModuleLRF>& _) {
 In.Assign(_.In);
 Out.Assign(_.Out);
}

VI::Main::Internal::LRF::Out::Out(ui _) :
 Mode(_.lblLRFMode), Readiness(_.lblLRFReadiness),
 ReceptionFault(_.lblLRFReceptionFault), ControlFault(_.lblLRFControlFault), TemperatureReady(_.lblLRFTemperatureReady),
 D1(_.lblLRFD1), D2(_.lblLRFD2), D3(_.lblLRFD3),
 Resource(_.lblLRFResource),
 Faults {
  _.lblLRFFaultFRAM, _.lblLRFFaultCRC, _.lblLRFFaultIntervalMeasurer, _.lblLRFFaultQSwitch,
  _.lblLRFFaultLPS, _.lblLRFFaultWTF, _.lblLRFFaultOpticalStart
 },
 State {
  _.lblLRFTargets, _.lblLRFStateLowPeriod, _.lblLRFStateOnboardNetwork,
  _.lblLRFStateWTF, _.lblLRFStateTestingCompleted, _.lblLRFStateFault
 },
 LTS(_.lblLRFLTS),
 Received(_.lblLRFReceived), ReceptionFaults(_.lblLRFReceptionFaults) {}

void VI::Main::Internal::LRF::Out::Assign(const Interface::DisplayRecord<ModuleLRF>::OutRecord& _) {
 Mode.Assign(_.Mode);
 Readiness.Assign(_.Readiness);
 ReceptionFault.Assign(_.ReceptionFault);
 ControlFault.Assign(_.ControlFault);
 TemperatureReady.Assign(_.TemperatureReady);
 D1.Assign(_.Distance1);
 D2.Assign(_.Distance2);
 D3.Assign(_.Distance3);
 Resource.Assign(_.Resource);
 Faults.Assign(_.Faults);
 State.Assign(_.State);
 LTS.Assign(_.LTS);
 Received.Assign(_.Received);
 ReceptionFaults.Assign(_.ReceptionFaults);
}

VI::Main::Internal::LRF::In::In(ui _) :
 Frequency(_.lblLRFFrequency), Work(_.lblLRFWork),
 Single(_.lblLRFSingle), Frequent(_.lblLRFFrequent),
 Camera(_.lblLRFCamera), Pilot(_.lblLRFPilot),
 Transmitted(_.lblLRFTransmitted){
}

void VI::Main::Internal::LRF::In::Assign(const Interface::DisplayRecord<ModuleLRF>::InRecord& _) {
 Frequency.Assign(_.Frequency);
 Frequent.Assign(_.Signals.Frequent);
 Single.Assign(_.Signals.Single);
 Work.Assign(_.Signals.Work);
 Camera.Assign(_.Signals.Camera);
 Pilot.Assign(_.Signals.Pilot);
 Transmitted.Assign(_.Transmitted);
}

VI::Main::Internal::Training::Training(ui _) :
 On(_.lblFakeTargetOn), Square(_.lblFakeTargetSquare),
 Inversion(_.lblFakeTargetInversion), DirectionLeft(_.lblFakeTargetDirection),
 Type(_.lblFakeTargetPictureType, {
  "прямокутник", "вертоліт", "літак", "корабель", "еліпс"
 }),
 Size(_.lblFakeTargetSize),
 Sph { _.lblFakeTargetDistance, _.lblFakeTargetBearing, _.lblFakeTargetElevation },
 Transmitted(_.lblFakeTargetTransmitted) {
}

void VI::Main::Internal::Training::Assign(
 const Interface::DisplayRecord<ModuleTraining>::InRecord& _) {
 On.Assign(_.On);
 Square.Assign(_.Square);
 Inversion.Assign(_.Inversion);
 DirectionLeft.Assign(_.DirectionLeft);
 Type.Assign(_.Type);
 Size.Assign(_.Size);
 Sph.Assign(_.Sph);
 Transmitted.Assign(_.Transmitted);
}

VI::Main::Internal::Comp2Radar::Comp2Radar(ui _) : Out(_), In(_) {}

void VI::Main::Internal::Comp2Radar::Assign(
 const Interface::DisplayRecord<ModuleComp2Radar>& _) {
 Out.Assign(_.Out);
 In.Assign(_.In);
}

VI::Main::Internal::Comp2Radar::Out::Out(ui _) :
 Mode(_.lblComp2RModeInputMode), Submode(_.lblComp2RModeInputSubmode),
 War(_.lblComp2InputWar),
 ETD {
  _.lblComp2InputETD_EmissionPermitted,
  _.lblComp2InputETD_Antenna, _.lblComp2InputETD_TT,
  _.lblComp2InputETD_Distance, _.lblComp2InputETD_LFM_SlopeNegative,
  _.lblComp2InputETD_DS, _.lblComp2InputETD_SignalCode,
  _.lblComp2InputETD_OFT, _.lblComp2InputETD_FrequencyCode,
  _.lblComp2InputETD_RPA, _.lblComp2InputETD_RepetitionPeriodCode,
  _.lblComp2InputETD_MTS1, _.lblComp2InputETD_MTS2,
  _.lblComp2InputETD_FFT, _.lblComp2InputETD_ActiveNoiseAnalysis,
  _.lblComp2InputETD_OwnMoventCompensation,
  _.lblComp2InputETD_ActiveInterferenceAngularTracking,
  _.lblComp2InputETD_SumDeltaChannelsCalibration,
  _.lblComp2InputETD_RangeCalibration, _.lblComp2InputETD_RepetitionPulseCount
 },
 SS {
  _.lblComp2InputSS_EmissionPermitted, _.lblComp2InputSS_Antenna,
  _.lblComp2InputSS_DistanceBegin, _.lblComp2InputSS_DistanceEnd,
  _.lblComp2InputSS_LFM_SlopeNegative, _.lblComp2InputSS_DS,
  _.lblComp2InputSS_SignalCode, _.lblComp2InputSS_OFT,
  _.lblComp2InputSS_FrequencyCode, _.lblComp2InputSS_RPA,
  _.lblComp2InputSS_RepetitionPeriodCode,
  _.lblComp2InputSS_MTS1, _.lblComp2InputSS_MTS2,
  _.lblComp2InputSS_FFT, _.lblComp2InputSS_ActiveNoiseAnalysis,
  _.lblComp2InputSS_OwnMoventCompensation,
  _.lblComp2InputSS_RangeCalibration, _.lblComp2InputSS_PulseCount
 },
 Control {
  _.lblComp2InputRadarControlTAGC, _.lblComp2InputRadarControlRangeInputCalibration,
  _.lblComp2InputRadarControlPIC, _.lblComp2InputRadarControlCFAR,
  _.lblComp2InputRadarControlManualIF, _.lblComp2InputRadarControlManualUHF,
  _.lblComp2InputRadarControlDetectionThreshold, _.lblComp2InputRadarControlANI_Threshold,
  _.lblComp2InputRadarControlUHF_ControlThreshold, _.lblComp2InputRadarControlIF_ControlThreshold,
  _.lblComp2InputRadarControlSignalCode, _.lblComp2InputRadarControlCalibrationRangeCorrection,
  _.lblComp2InputRadarControlPassiveInterferenceCoherenceCoefficient,
  _.lblComp2InputRadarControlMinimumPassiveInterferenceThreshold
 },
 Distance(_.lblComp2InputRadarTrainingDistance),
 RadialSpeed(_.lblComp2InputRadarTrainingRadialSpeed),
 DopplerShift(_.lblComp2InputRadarTrainingDopplerShift),
 PF {
  PlannedFrequenciesItem {
   _.lblComp2PlannedFrequenciesInputPermitted0, _.lblComp2PlannedFrequenciesInputCode0
  },
  { _.lblComp2PlannedFrequenciesInputPermitted1, _.lblComp2PlannedFrequenciesInputCode1 },
  { _.lblComp2PlannedFrequenciesInputPermitted2, _.lblComp2PlannedFrequenciesInputCode2 },
  { _.lblComp2PlannedFrequenciesInputPermitted3, _.lblComp2PlannedFrequenciesInputCode3 },
  { _.lblComp2PlannedFrequenciesInputPermitted4, _.lblComp2PlannedFrequenciesInputCode4 },
  { _.lblComp2PlannedFrequenciesInputPermitted5, _.lblComp2PlannedFrequenciesInputCode5 }
 },
 Time(_.lblRadarComp2InputTime),
 Received(_.lblRadarComp2InputReceived),
 ReceptionFaults(_.lblRadarComp2InputReceptionFaults) {
}

void VI::Main::Internal::Comp2Radar::Out::Assign(
 const Interface::DisplayRecord<ModuleComp2Radar>::OutRecord& _) {
 Mode.Assign(_.Mode.Mode);
 Submode.Assign(_.Mode.Submode);
 War.Assign(_.War);
 ETD.Assign(_.ETD);
 SS.Assign(_.SS);
 Control.Assign(_.Control);
 Distance.Assign(_.Training.Distance);
 RadialSpeed.Assign(_.Training.RadialSpeed);
 DopplerShift.Assign(_.Training.DopplerShift);
 for (size_t i = 0; i < PF.size(); ++i) {
  PF[i].Assign(_.PF[i]);
 }
 Time.Assign(_.Time);
 Received.Assign(_.Received);
 ReceptionFaults.Assign(_.ReceptionFaults);
}

VI::Main::Internal::Comp2Radar::In::In(ui _) :
 Mode(_.lblComp2RModeOutputMode), Submode(_.lblComp2RModeOutputSubmode),
 War(_.lblComp2OutputWar),
 ETD {
  _.lblComp2OutputETD_EmissionPermitted,
  _.lblComp2OutputETD_EmissionPermittedPhysical,
  _.lblComp2OutputETD_Antenna, _.lblComp2OutputETD_TT,
  _.lblComp2OutputETD_Distance, _.lblComp2OutputETD_LFM_SlopeNegative,
  _.lblComp2OutputETD_DS, _.lblComp2OutputETD_SignalCode,
  _.lblComp2OutputETD_OFT, _.lblComp2OutputETD_FrequencyCode,
  _.lblComp2OutputETD_RPA, _.lblComp2OutputETD_RepetitionPeriodCode,
  _.lblComp2OutputETD_MTS1, _.lblComp2OutputETD_MTS2,
  _.lblComp2OutputETD_FFT, _.lblComp2OutputETD_ActiveNoiseAnalysis,
  _.lblComp2OutputETD_OwnMoventCompensation,
  _.lblComp2OutputETD_ActiveInterferenceAngularTracking,
  _.lblComp2OutputETD_SumDeltaChannelsCalibration,
  _.lblComp2OutputETD_RangeCalibration,
  _.lblComp2OutputETD_RepetitionPulseCount
 },
 SS {
  _.lblComp2OutputSS_EmissionPermitted,
  _.lblComp2OutputSS_EmissionPermittedPhysical,
  _.lblComp2OutputSS_Antenna,
  _.lblComp2OutputSS_DistanceBegin, _.lblComp2OutputSS_DistanceEnd,
  _.lblComp2OutputSS_LFM_SlopeNegative, _.lblComp2OutputSS_DS,
  _.lblComp2OutputSS_SignalCode, _.lblComp2OutputSS_OFT,
  _.lblComp2OutputSS_FrequencyCode, _.lblComp2OutputSS_RPA,
  _.lblComp2OutputSS_RepetitionPeriodCode,
  _.lblComp2OutputSS_MTS1, _.lblComp2OutputSS_MTS2,
  _.lblComp2OutputSS_FFT, _.lblComp2OutputSS_ActiveNoiseAnalysis,
  _.lblComp2OutputSS_OwnMoventCompensation,
  _.lblComp2OutputSS_RangeCalibration, _.lblComp2OutputSS_PulseCount
 },
 Control {
  _.lblComp2OutputRadarControlTAGC,
  _.lblComp2OutputRadarControlRangeInputCalibration,
  _.lblComp2OutputRadarControlPIC, _.lblComp2OutputRadarControlCFAR,
  _.lblComp2OutputRadarControlManualIF,
  _.lblComp2OutputRadarControlManualUHF,
  _.lblComp2OutputRadarControlDetectionThreshold,
  _.lblComp2OutputRadarControlANI_Threshold,
  _.lblComp2OutputRadarControlUHF_ControlThreshold,
  _.lblComp2OutputRadarControlIF_ControlThreshold,
  _.lblComp2OutputRadarControlSignalCode,
  _.lblComp2OutputRadarControlCalibrationRangeCorrection,
  _.lblComp2OutputRadarControlPassiveInterferenceCoherenceCoefficient,
  _.lblComp2OutputRadarControlMinimumPassiveInterferenceThreshold
 },
 Test {
  {
   _.lblComp2OutputRadarTestFaultTransmitterPulsePower,
   _.lblComp2OutputRadarTestFaultSUMNoisePower,
   _.lblComp2OutputRadarTestFaultDF1NoisePower,
   _.lblComp2OutputRadarTestFaultDF2NoisePower,
   _.lblComp2OutputRadarTestFaultSUMCalibrationPower,
   _.lblComp2OutputRadarTestFaultDF1CalibrationPower,
   _.lblComp2OutputRadarTestFaultDF2CalibrationPower,
   _.lblComp2OutputRadarTestFaultSUMDirectWaveLevel,
   _.lblComp2OutputRadarTestFaultSUMBackwardWaveLevel,
   _.lblComp2OutputRadarTestFaultSUM_DF1AmplitudeDifference,
   _.lblComp2OutputRadarTestFaultSUM_DF2AmplitudeDifference,
   _.lblComp2OutputRadarTestFaultSynthesizerPLL_LoopCapture,
   _.lblComp2OutputRadarTestFaultA_E_Switch,
   _.lblComp2OutputRadarTestFaultTransmitterTemperature
  },
  _.lblComp2OutputRadarTestTransmitterPulsePower,
  _.lblComp2OutputRadarTestSUMNoisePower,
  _.lblComp2OutputRadarTestDF1NoisePower,
  _.lblComp2OutputRadarTestDF2NoisePower,
  _.lblComp2OutputRadarTestSUMCalibrationPower,
  _.lblComp2OutputRadarTestDF1CalibrationPower,
  _.lblComp2OutputRadarTestDF2CalibrationPower,
  _.lblComp2OutputRadarTestSUMDirectWaveLevel,
  _.lblComp2OutputRadarTestSUMBackwardWaveLevel,
  _.lblComp2OutputRadarTestSUM_DF1AmplitudeDifference,
  _.lblComp2OutputRadarTestSUM_DF2AmplitudeDifference,
  _.lblComp2OutputRadarTestSUM_DF1PhaseDifference,
  _.lblComp2OutputRadarTestSUM_DF2PhaseDifference,
  _.lblComp2OutputRadarTestCompleted
 },
 ETD_Detection(_), SS_Data(_),
 PF {
  PlannedFrequenciesItem {
   _.lblComp2PlannedFrequenciesOutputPermitted0, _.lblComp2PlannedFrequenciesOutputCode0
  },
  { _.lblComp2PlannedFrequenciesOutputPermitted1, _.lblComp2PlannedFrequenciesOutputCode1 },
  { _.lblComp2PlannedFrequenciesOutputPermitted2, _.lblComp2PlannedFrequenciesOutputCode2 },
  { _.lblComp2PlannedFrequenciesOutputPermitted3, _.lblComp2PlannedFrequenciesOutputCode3 },
  { _.lblComp2PlannedFrequenciesOutputPermitted4, _.lblComp2PlannedFrequenciesOutputCode4 },
  { _.lblComp2PlannedFrequenciesOutputPermitted5, _.lblComp2PlannedFrequenciesOutputCode5 }
 },
 Time(_.lblRadarComp2OutputTime),
 Transmitted(_.lblRadarComp2OutputTransmitted) {
}

void VI::Main::Internal::Comp2Radar::In::Assign(
 const Interface::DisplayRecord<ModuleComp2Radar>::InRecord& _) {
 Mode.Assign(_.Mode.Mode);
 Submode.Assign(_.Mode.Submode);
 War.Assign(_.War);
 ETD.Assign(_.ETD);
 SS.Assign(_.SS);
 Control.Assign(_.Control);
 Test.Assign(_.TestResult);
 ETD_Detection.Assign(_.ETD_Detection);
 SS_Data.Assign(_.SS_Data);
 for (size_t i = 0; i < PF.size(); ++i) {
  PF[i].Assign(_.PF[i]);
 }
 Time.Assign(_.Time);
 Transmitted.Assign(_.Transmitted);
}

VI::Main::Internal::Comp2Radar::In::ETD_Detection::ETD_Detection(ui _) :
 ETD_Mode(_.lblComp2OutputETD_DetectionETD_Mode),
 TargetCount(_.lblComp2OutputETD_DetectionTargetCount),
 Targets {
  TargetData {
   _.lblComp2OutputETD_DetectionRTS0, _.lblComp2OutputETD_DetectionDistance0,
   { _.lblComp2OutputETD_DetectionMismatchHA0, _.lblComp2OutputETD_DetectionMismatchElev0 }
  },
  TargetData {
   _.lblComp2OutputETD_DetectionRTS1, _.lblComp2OutputETD_DetectionDistance1,
   { _.lblComp2OutputETD_DetectionMismatchHA1, _.lblComp2OutputETD_DetectionMismatchElev1 }
  },
  TargetData {
   _.lblComp2OutputETD_DetectionRTS2, _.lblComp2OutputETD_DetectionDistance2,
   { _.lblComp2OutputETD_DetectionMismatchHA2, _.lblComp2OutputETD_DetectionMismatchElev2 }
  },
  TargetData {
   _.lblComp2OutputETD_DetectionRTS3, _.lblComp2OutputETD_DetectionDistance3,
   { _.lblComp2OutputETD_DetectionMismatchHA3, _.lblComp2OutputETD_DetectionMismatchElev3 }
  }
 },
 ORU { _.lblComp2OutputETD_ORU_HA, _.lblComp2OutputETD_ORU_Elev },
 Time(_.lblComp2OutputETD_DetectionLTS) {
}

void VI::Main::Internal::Comp2Radar::In::ETD_Detection::Assign(
 const struct ::Comp2Radar::OutputData::ETD_Detection& _) {
 ETD_Mode.Assign(_.ETD_Mode);
 TargetCount.Assign(_.TargetCount);
 for (size_t i = 0; i < Targets.size(); ++i) {
  Targets[i].Assign(_.Targets[i]);
 }
 ORU.Assign(_.ORU);
 Time.Assign(_.Time);
}

VI::Main::Internal::Comp2Radar::In::SS_Data::SS_Data(ui _) :
 SS_Mode(_.lblComp2OutputSS_DataSS_Mode), OFT(_.lblComp2OutputSS_DataOFT),
 FrequencyCode(_.lblComp2OutputSS_DataFrequencyCode),
 RepetitionPeriodCode(_.lblComp2OutputSS_DataRepetitionPeriodCode),
 RPA(_.lblComp2OutputSS_DataRPA), ANP(_.lblComp2OutputSS_DataActiveNoisePresence),
 ActiveNoiseInterferenceProcessing(_.lblComp2OutputSS_DataActiveNoiseInterferenceProcessing),
 Time(_.lblComp2OutputSS_DataLTS) {
}

void VI::Main::Internal::Comp2Radar::In::SS_Data::Assign(
 const struct ::Comp2Radar::OutputData::SS_Data& _) {
 SS_Mode.Assign(_.SS_Mode);
 OFT.Assign(_.OFT);
 FrequencyCode.Assign(_.FrequencyCode);
 RepetitionPeriodCode.Assign(_.RepetitionPeriodCode);
 RPA.Assign(_.RPA);
 ANP.Assign(_.ANP);
 ActiveNoiseInterferenceProcessing.Assign(_.ActiveNoiseInterferenceProcessing);
 Time.Assign(_.Time);
}

void VI::Main::Internal::Comp2Radar::In::ETD_Detection::TargetData::Assign(
 const ::Comp2Radar::OutputData::ETD_Detection::TargetData& _) {
 RTS.Assign(_.RTS);
 Distance.Assign(_.Distance);
 Mismatch.Assign(_.Mismatch);
}

template <>
inline constexpr auto enum_tab<ExtrapolationPolynomials::Angles::ReadyState> = {
 "не готово", "частково", "готово"
};

VI::Main::Evaluations::Evaluations(ui _) :
  FilterOptical {
   {
    {
     _.lblFilterInputAnglesTime,
     { _.lblFilterInputOEUCHA, _.lblFilterInputOEUCElev },
     { _.lblFilterInputAutotrackingMismatchHA, _.lblFilterInputAutotrackingMismatchElev }
    },
    _.lblFilterInputMode,
    {
     _.lblFilterInputDistanceOK, _.lblFilterInputDistanceTime,
     _.lblFilterInputDistance, _.lblFilterInputDistanceSpeed,
     _.lblFilterInputDistanceAcceleration
    },
    {
     _.lblFilterInputNavigOK, _.lblFilterInputNavigTime,
     {
      _.lblFilterInputNavigSpeedX, _.lblFilterInputNavigSpeedY, _.lblFilterInputNavigSpeedZ
     },
     { _.lblFilterInputHeading, _.lblFilterInputRolling, _.lblFilterInputPitch },
     {
      _.lblFilterInputHeadingChange, _.lblFilterInputRollingChange, _.lblFilterInputPitchChange
     }
    }
   },
   {
    {
     _.lblFilterOutputTime, _.lblFilterOutputCompleted,
     _.lblFilterOutputCoordinatesCount,
     {
      { _.lblFilterOutputTargetX, _.lblFilterOutputTargetY, _.lblFilterOutputTargetZ },
      _.lblFilterOutputTargetTime
     },
     {
      CoordinateCluster::Cartesian3DCoords {
       _.lblFilterOutputPolynomialCoeffX0, _.lblFilterOutputPolynomialCoeffY0,
       _.lblFilterOutputPolynomialCoeffZ0
      },
      {
       _.lblFilterOutputPolynomialCoeffX1, _.lblFilterOutputPolynomialCoeffY1,
       _.lblFilterOutputPolynomialCoeffZ1
      },
      {
       _.lblFilterOutputPolynomialCoeffX2, _.lblFilterOutputPolynomialCoeffY2,
       _.lblFilterOutputPolynomialCoeffZ2
      },
      {
       _.lblFilterOutputPolynomialCoeffX3, _.lblFilterOutputPolynomialCoeffY3,
       _.lblFilterOutputPolynomialCoeffZ3
      },
      {
       _.lblFilterOutputPolynomialCoeffX4, _.lblFilterOutputPolynomialCoeffY4,
       _.lblFilterOutputPolynomialCoeffZ4
      }
     },
     {
      CoordinateCluster::Cartesian3DCoords {
       _.lblFilterOutputPolynomialTestX1, _.lblFilterOutputPolynomialTestY1, _.lblFilterOutputPolynomialTestZ1,
      },
      { _.lblFilterOutputPolynomialTestX2, _.lblFilterOutputPolynomialTestY2, _.lblFilterOutputPolynomialTestZ2 }
     }
    },
    _.lblFilterOutputPerformedFiltrationInCurrentMode, _.lblFilterOutputPerformed,
    {
     {
      {
       { _.lblFilterOutputNavigSpeedX, _.lblFilterOutputNavigSpeedY, _.lblFilterOutputNavigSpeedZ },
       { _.lblFilterOutputNavigX0, _.lblFilterOutputNavigY0, _.lblFilterOutputNavigZ0 },
       { _.lblFilterOutputNavigX1, _.lblFilterOutputNavigY1, _.lblFilterOutputNavigZ1 },
       { _.lblFilterOutputNavigHeading0, _.lblFilterOutputNavigRolling0, _.lblFilterOutputNavigPitch0 },
       { _.lblFilterOutputNavigHeading1, _.lblFilterOutputNavigRolling1, _.lblFilterOutputNavigPitch1 }
      },
      _.lblFilterOutputNSTime, _.lblFilterOutputNSOK,
      { _.lblFilterOutputNavigTestX, _.lblFilterOutputNavigTestY, _.lblFilterOutputNavigTestZ },
      {
       _.lblFilterOutputNavigTestHeading1, _.lblFilterOutputNavigTestRolling1,
       _.lblFilterOutputNavigTestPitch1
      }
     },
     {
      _.lblFilterOutputAnglesTime,
      _.lblFilterOutputAnglesReady,
      _.lblFilterOutputAnglesCount, _.lblFilterOutputAnglesDegree,
      {
       CoordinateCluster::Spherical2DShipCoords {
        _.lblFilterOutputAnglesShipHeading0, _.lblFilterOutputAnglesShipElevation0
       },
       { _.lblFilterOutputAnglesShipHeading1, _.lblFilterOutputAnglesShipElevation1 },
       { _.lblFilterOutputAnglesShipHeading2, _.lblFilterOutputAnglesShipElevation2 },
       { _.lblFilterOutputAnglesShipHeading3, _.lblFilterOutputAnglesShipElevation3 },
       { _.lblFilterOutputAnglesShipHeading4, _.lblFilterOutputAnglesShipElevation4 }
      },
      {
       CoordinateCluster::Spherical2DGroundCoords {
        _.lblFilterOutputAnglesGroundBearing0, _.lblFilterOutputAnglesGroundElevation0
       },
       { _.lblFilterOutputAnglesGroundBearing1, _.lblFilterOutputAnglesGroundElevation1 },
       { _.lblFilterOutputAnglesGroundBearing2, _.lblFilterOutputAnglesGroundElevation2 },
       { _.lblFilterOutputAnglesGroundBearing3, _.lblFilterOutputAnglesGroundElevation3 },
       { _.lblFilterOutputAnglesGroundBearing4, _.lblFilterOutputAnglesGroundElevation4 }
      },
      { _.lblFilterOutputAnglesShipHeadingTest, _.lblFilterOutputAnglesShipElevationTest },
      { _.lblFilterOutputAnglesGroundBearingTest, _.lblFilterOutputAnglesGroundElevationTest }
     }
    },
    FloatValue(_.lblFilterOutputElapsedTime, 1E-5)
   }
  },
  FilterRadar {
   {
    {
     _.lblFilterRadarInputAnglesTime,
     { _.lblFilterRadarInputOEUCHA, _.lblFilterRadarInputOEUCElev },
     { _.lblFilterRadarInputAutotrackingMismatchHA, _.lblFilterRadarInputAutotrackingMismatchElev }
    },
    _.lblFilterRadarInputMode,
    {
     _.lblFilterRadarInputDistanceOK, _.lblFilterRadarInputDistanceTime,
     _.lblFilterRadarInputDistance, _.lblFilterRadarInputDistanceSpeed,
     _.lblFilterRadarInputDistanceAcceleration
    },
    {
     _.lblFilterRadarInputNavigOK, _.lblFilterRadarInputNavigTime,
     {
      _.lblFilterRadarInputNavigSpeedX, _.lblFilterRadarInputNavigSpeedY,
      _.lblFilterRadarInputNavigSpeedZ
     },
     { _.lblFilterRadarInputHeading, _.lblFilterRadarInputRolling, _.lblFilterRadarInputPitch },
     {
      _.lblFilterRadarInputHeadingChange, _.lblFilterRadarInputRollingChange,
      _.lblFilterRadarInputPitchChange
     }
    }
   },
   {
    {
     _.lblFilterRadarOutputTime, _.lblFilterRadarOutputCompleted,
     _.lblFilterRadarOutputCoordinatesCount,
     {
      {
       _.lblFilterRadarOutputTargetX, _.lblFilterRadarOutputTargetY, _.lblFilterRadarOutputTargetZ
      },
      _.lblFilterRadarOutputTargetTime
     },
     {
      CoordinateCluster::Cartesian3DCoords {
       _.lblFilterRadarOutputPolynomialCoeffX0, _.lblFilterRadarOutputPolynomialCoeffY0,
       _.lblFilterRadarOutputPolynomialCoeffZ0
      },
      {
       _.lblFilterRadarOutputPolynomialCoeffX1, _.lblFilterRadarOutputPolynomialCoeffY1,
       _.lblFilterRadarOutputPolynomialCoeffZ1
      },
      {
       _.lblFilterRadarOutputPolynomialCoeffX2, _.lblFilterRadarOutputPolynomialCoeffY2,
       _.lblFilterRadarOutputPolynomialCoeffZ2
      },
      {
       _.lblFilterRadarOutputPolynomialCoeffX3, _.lblFilterRadarOutputPolynomialCoeffY3,
       _.lblFilterRadarOutputPolynomialCoeffZ3
      },
      {
       _.lblFilterRadarOutputPolynomialCoeffX4, _.lblFilterRadarOutputPolynomialCoeffY4,
       _.lblFilterRadarOutputPolynomialCoeffZ4
      }
     },
     {
      CoordinateCluster::Cartesian3DCoords {
       _.lblFilterRadarOutputPolynomialTestX1, _.lblFilterRadarOutputPolynomialTestY1,
       _.lblFilterRadarOutputPolynomialTestZ1,
      },
      {
       _.lblFilterRadarOutputPolynomialTestX2, _.lblFilterRadarOutputPolynomialTestY2,
       _.lblFilterRadarOutputPolynomialTestZ2
      }
     }
    },
    _.lblFilterRadarOutputPerformedFiltrationInCurrentMode, _.lblFilterRadarOutputPerformed,
    {
     {
      {
       {
        _.lblFilterRadarOutputNavigSpeedX, _.lblFilterRadarOutputNavigSpeedY,
        _.lblFilterRadarOutputNavigSpeedZ
       },
       {
        _.lblFilterRadarOutputNavigX0, _.lblFilterRadarOutputNavigY0,
        _.lblFilterRadarOutputNavigZ0
       },
       {
        _.lblFilterRadarOutputNavigX1, _.lblFilterRadarOutputNavigY1,
        _.lblFilterRadarOutputNavigZ1
       },
       {
        _.lblFilterRadarOutputNavigHeading0, _.lblFilterRadarOutputNavigRolling0,
        _.lblFilterRadarOutputNavigPitch0
       },
       {
        _.lblFilterRadarOutputNavigHeading1, _.lblFilterRadarOutputNavigRolling1,
        _.lblFilterRadarOutputNavigPitch1
       }
      },
      _.lblFilterRadarOutputNSTime, _.lblFilterRadarOutputNSOK,
      {
       _.lblFilterRadarOutputNavigTestX, _.lblFilterRadarOutputNavigTestY,
       _.lblFilterRadarOutputNavigTestZ
      },
      {
       _.lblFilterRadarOutputNavigTestHeading1, _.lblFilterRadarOutputNavigTestRolling1,
       _.lblFilterRadarOutputNavigTestPitch1
      }
     },
     {
      _.lblFilterRadarOutputAnglesTime, _.lblFilterRadarOutputAnglesReady,
      _.lblFilterRadarOutputAnglesCount, _.lblFilterRadarOutputAnglesDegree,
      {
       CoordinateCluster::Spherical2DShipCoords {
        _.lblFilterRadarOutputAnglesShipHeading0, _.lblFilterRadarOutputAnglesShipElevation0
       },
       { _.lblFilterRadarOutputAnglesShipHeading1, _.lblFilterRadarOutputAnglesShipElevation1 },
       { _.lblFilterRadarOutputAnglesShipHeading2, _.lblFilterRadarOutputAnglesShipElevation2 },
       { _.lblFilterRadarOutputAnglesShipHeading3, _.lblFilterRadarOutputAnglesShipElevation3 },
       { _.lblFilterRadarOutputAnglesShipHeading4, _.lblFilterRadarOutputAnglesShipElevation4 }
      },
      {
       CoordinateCluster::Spherical2DGroundCoords {
        _.lblFilterRadarOutputAnglesGroundBearing0, _.lblFilterRadarOutputAnglesGroundElevation0
       },
       {
        _.lblFilterRadarOutputAnglesGroundBearing1, _.lblFilterRadarOutputAnglesGroundElevation1
       },
       {
        _.lblFilterRadarOutputAnglesGroundBearing2, _.lblFilterRadarOutputAnglesGroundElevation2
       },
       {
        _.lblFilterRadarOutputAnglesGroundBearing3, _.lblFilterRadarOutputAnglesGroundElevation3
       },
       {
        _.lblFilterRadarOutputAnglesGroundBearing4, _.lblFilterRadarOutputAnglesGroundElevation4
       }
      },
      {
       _.lblFilterRadarOutputAnglesShipHeadingTest,
       _.lblFilterRadarOutputAnglesShipElevationTest
      },
      {
       _.lblFilterRadarOutputAnglesGroundBearingTest,
       _.lblFilterRadarOutputAnglesGroundElevationTest
      }
     }
    },
    FloatValue(_.lblFilterRadarOutputElapsedTime, 1E-5)
   }
  },
 CollisionOptical {
  {
   _.lblCollisionInputFitrationPerformed, _.lblCollisionInputShotTime,
   {
    _.lblCollisionInputMode, _.lblCollisionInputPerformedFiltrationInCurrentMode
   }
  },
  {
   _.lblCollisionInputStartSpeedMismatch, _.lblCollisionInputMassMismatch
  },
  {
   _.lblCollisionOutputPerformed
  },
  {
   _.lblCollisionOutputSolverPerformed,
   ColorSelectValue<CollisionTable::SolutionResult>(
    _.lblCollisionOutputSolutionResult, SolutionResultStr),
   {
    _.lblCollisionOutputHaveSolution,
    {
     _.lblCollisionOutputShotTime,
     _.lblCollisionOutputFlightTime,
     {
      _.lblCollisionOutputGroundBearing, _.lblCollisionOutputGroundElevation
     },
     {
      _.lblCollisionOutputShipHeading, _.lblCollisionOutputShipElevation
     },
     _.lblCollisionOutputDistanceCurrent, _.lblCollisionOutputDistanceMeet,
     {
      CoordinateCluster::Spherical2DShipCoords {
       _.lblCollisionOutputShipHeading0, _.lblCollisionOutputShipElevation0
      },
      {
       _.lblCollisionOutputShipHeading1, _.lblCollisionOutputShipElevation1
      },
      {
       _.lblCollisionOutputShipHeading2, _.lblCollisionOutputShipElevation2
      },
      {
       _.lblCollisionOutputShipHeading3, _.lblCollisionOutputShipElevation3
      },
      {
       _.lblCollisionOutputShipHeading4, _.lblCollisionOutputShipElevation4
      }
     },
     {
      CoordinateCluster::Spherical2DGroundCoords {
      _.lblCollisionOutputGroundBearing0, _.lblCollisionOutputGroundElevation0
      },
      {
       _.lblCollisionOutputGroundBearing1, _.lblCollisionOutputGroundElevation1
      },
      {
       _.lblCollisionOutputGroundBearing2, _.lblCollisionOutputGroundElevation2
      },
      {
       _.lblCollisionOutputGroundBearing3, _.lblCollisionOutputGroundElevation3
      },
      {
       _.lblCollisionOutputGroundBearing4, _.lblCollisionOutputGroundElevation4
      }
     },
     {
      CoordinateCluster::Spherical2DShipCoords {
       _.lblCollisionOutputShipHeadingTest0, _.lblCollisionOutputShipElevationTest0
      },
      {
       _.lblCollisionOutputShipHeadingTest1, _.lblCollisionOutputShipElevationTest1
      }
     },
     {
      CoordinateCluster::Spherical2DGroundCoords {
       _.lblCollisionOutputGroundBearingTest0, _.lblCollisionOutputGroundElevationTest0
      },
      {
       _.lblCollisionOutputGroundBearingTest1, _.lblCollisionOutputGroundElevationTest1
      }
     }
    },
    {
     _.lblCorrectionDistanceTailWind, _.lblCorrectionHeightTailWind,
     _.lblCorrectionDistanceDencity, _.lblCorrectionHeightDencity,
     _.lblCorrectionDistanceSpeedStart, _.lblCorrectionHeightSpeedStart,
     { _.lblCorrectionShipMotionHA, _.lblCorrectionShipMotionElev },
     _.lblCorrectionDerivation, _.lblCorrectionCrosswind, _.lblCorrectionAimingAngle
    }
   },
   FloatValue(_.lblCollisionOutputElapsedTime, 1E-06)
  }
 },
 CollisionRadar {
  {
   _.lblCollisionRadarInputFitrationPerformed, _.lblCollisionRadarInputShotTime,
   {
    _.lblCollisionRadarInputMode, _.lblCollisionRadarInputPerformedFiltrationInCurrentMode
   }
  },
  {
   _.lblCollisionRadarInputStartSpeedMismatch, _.lblCollisionRadarInputMassMismatch
  },
  {
   _.lblCollisionRadarOutputPerformed
  },
  {
   _.lblCollisionRadarOutputSolverPerformed,
   ColorSelectValue<CollisionTable::SolutionResult>(
    _.lblCollisionRadarOutputSolutionResult, SolutionResultStr),
   {
    _.lblCollisionRadarOutputHaveSolution,
    {
     _.lblCollisionRadarOutputShotTime,
     _.lblCollisionRadarOutputFlightTime,
     {
      _.lblCollisionRadarOutputGroundBearing, _.lblCollisionRadarOutputGroundElevation
     },
     {
      _.lblCollisionRadarOutputShipHeading, _.lblCollisionRadarOutputShipElevation
     },
     _.lblCollisionRadarOutputDistanceCurrent, _.lblCollisionRadarOutputDistanceMeet,
     {
      CoordinateCluster::Spherical2DShipCoords {
       _.lblCollisionRadarOutputShipHeading0, _.lblCollisionRadarOutputShipElevation0
      },
      {
       _.lblCollisionRadarOutputShipHeading1, _.lblCollisionRadarOutputShipElevation1
      },
      {
       _.lblCollisionRadarOutputShipHeading2, _.lblCollisionRadarOutputShipElevation2
      },
      {
       _.lblCollisionRadarOutputShipHeading3, _.lblCollisionRadarOutputShipElevation3
      },
      {
       _.lblCollisionRadarOutputShipHeading4, _.lblCollisionRadarOutputShipElevation4
      }
     },
     {
      CoordinateCluster::Spherical2DGroundCoords {
      _.lblCollisionRadarOutputGroundBearing0, _.lblCollisionRadarOutputGroundElevation0
      },
      {
       _.lblCollisionRadarOutputGroundBearing1, _.lblCollisionRadarOutputGroundElevation1
      },
      {
       _.lblCollisionRadarOutputGroundBearing2, _.lblCollisionRadarOutputGroundElevation2
      },
      {
       _.lblCollisionRadarOutputGroundBearing3, _.lblCollisionRadarOutputGroundElevation3
      },
      {
       _.lblCollisionRadarOutputGroundBearing4, _.lblCollisionRadarOutputGroundElevation4
      }
     },
     {
      CoordinateCluster::Spherical2DShipCoords {
       _.lblCollisionRadarOutputShipHeadingTest0, _.lblCollisionRadarOutputShipElevationTest0
      },
      {
       _.lblCollisionRadarOutputShipHeadingTest1, _.lblCollisionRadarOutputShipElevationTest1
      }
     },
     {
      CoordinateCluster::Spherical2DGroundCoords {
       _.lblCollisionRadarOutputGroundBearingTest0, _.lblCollisionRadarOutputGroundElevationTest0
      },
      {
       _.lblCollisionRadarOutputGroundBearingTest1, _.lblCollisionRadarOutputGroundElevationTest1
      }
     }
    },
    {
     _.lblCorrectionDistanceTailWindRadar, _.lblCorrectionHeightTailWindRadar,
     _.lblCorrectionDistanceDencityRadar, _.lblCorrectionHeightDencityRadar,
     _.lblCorrectionDistanceSpeedStartRadar, _.lblCorrectionHeightSpeedStartRadar,
     { _.lblCorrectionShipMotionHARadar, _.lblCorrectionShipMotionElevRadar },
     _.lblCorrectionDerivationRadar, _.lblCorrectionCrosswindRadar,
     _.lblCorrectionAimingAngleRadar
    }
   },
   FloatValue(_.lblCollisionRadarOutputElapsedTime, 1E-06)
  }
 } {
}

void VI::Main::Evaluations::Assign(const struct Interface::Command::Main::Evaluations& _) {
 FilterOptical.Assign(_.FilterOptical);
 FilterRadar.Assign(_.FilterRadar);
 CollisionOptical.Assign(_.CollisionOptical);
 CollisionRadar.Assign(_.CollisionRadar);
}

void VI::Main::Evaluations::Filter::Assign(
 const struct Interface::Command::Main::Evaluations::Filter& _) {
 In.Assign(_.In);
 Out.Assign(_.Out);
}

template <>
constexpr auto enum_tab<SystemMode> = {
 "початковий", "ЦВ", "РC", "AC", "IC", "контроль", "ВД", "PH", "завершення", "УIC"
};

void VI::Main::Evaluations::Filter::In::Assign(const FilterModule::CommandVector& _) {
 data.Assign(_.data);
 Mode.Assign(_.Mode);
 DD.Assign(_.DD);
 NS_Data.Assign(_.NS_Data);
}

void VI::Main::Evaluations::Filter::In::AnglesData::Assign(const Angles_Data& _) {
 Time.Assign(_.Time);
 OEUC.Assign(_.Ship);
 AutotrackingMismatch.Assign(_.AutotrackingMismatch);
}

void VI::Main::Evaluations::Filter::In::DistanceData::Assign(const ::DistanceData& _) {
 OK.Assign(_.OK);
 Time.Assign(_.Time);
 Distance.Assign(_.Distance);
 Speed.Assign(_.Speed);
 Acceleration.Assign(_.Acceleration);
}

void VI::Main::Evaluations::Filter::In::NS_Data::Assign(const ::NS_Data& _) {
 NewInfo.Assign(_.NewInfo);
 Time.Assign(_.Time);
 SpeedPart.Assign(_.SpeedPart);
 Rot.Assign(_.Rot);
 RotChange.Assign(_.RotChange);
}

void VI::Main::Evaluations::Filter::Out::Assign(const FilterModule::StatusVector& _) {
 data.Assign(_.data);
 PerformedFiltrationInCurrentMode.Assign(_.PerformedFiltrationInCurrentMode);
 Performed.Assign(_.Performed);
 Extrapolation.Assign(_.Extrapolation);
 ElapsedTime.Assign(_.ElapsedTime);
}

void VI::Main::Evaluations::Filter::Out::Extrapolation::Assign(const ExtrapolationPolynomials::All& _) {
 NS.Assign(_.NS);
 Angles.Assign(_.Angles);
}

void VI::Main::Evaluations::Filter::Out::Extrapolation::NS::Assign(const ExtrapolationPolynomials::NS& _) {
 CV.Assign(_.GetCV());
 Time.Assign(_.Time);
 OK.Assign(_.OK);
 ShipTest1.Assign(_.GetShipCart(0.1));
 RotationsTest1.Assign(_.GetRotations(0.1));
}

void VI::Main::Evaluations::Filter::Out::Extrapolation::NS::CurrentValues::Assign(
 const ExtrapolationPolynomials::NS::CurrentValues& _) {
 SpeedPart.Assign(_.SpeedPart);
 ShipCart0.Assign(_.ShipCart0);
 ShipCart1.Assign(_.ShipCart1);
 Rotations0.Assign(_.Rotations0);
 Rotations1.Assign(_.Rotations1);
}

void VI::Main::Evaluations::Filter::Out::Extrapolation::Angles::Assign(
 const ExtrapolationPolynomials::Angles& _) {
 Time.Assign(_.Time);
 Ready.Assign(_.Ready);
 Count.Assign(_.Count);
 DP_Cur.Assign(_.DP_Cur);
 for (size_t i = 0; i < PolyDegree; ++i) {
  PnShip[i].Assign(_.GetPnShip(i));
  PnGround[i].Assign(_.GetPnGround(i));
 }
 ShipTest.Assign(_.ShipVal(0.1));
 GroundTest.Assign(_.GroundVal(0.1));
}

void VI::Main::Evaluations::Filter::Out::OutData::Assign(const FilterModule::OutData& _) {
 Time.Assign(_.Time);
 Completed.Assign(_.Completed);
 CoordinatesCount.Assign(_.CoordinatesCount);
 TargetGroundCart.Assign(_.TargetGroundCart);
 for (size_t i = 0; i < PolynomialCoeff.size(); ++i) {
  PolynomialCoeff[i].Assign(_.PolynomialCoeffs[i]);
 }
 PolynomialTest[0].Assign(_.Cart(0.5));
 PolynomialTest[1].Assign(_.Cart(1.0));
}

void VI::Main::Evaluations::Filter::Out::OutData::Cartesian3DCoordsExtended::Assign(
 const FilterModule::OutData::Cartesian3DCoordsExtended& _) {
 Coords.Assign(_.Coords);
 Time.Assign(_.Time);
}

void VI::Main::Evaluations::Collision::Assign(
 const struct Interface::Command::Main::Evaluations::Collision& _) {
 In.Assign(_.In);
 Solver_In.Assign(_.Solver_In);
 Out.Assign(_.Out);
 Solver_Out.Assign(_.Solver_Out);
}

void VI::Main::Evaluations::Collision::In::Assign(const CollisionTaskModule::CommandVector& _) {
 FiltrationPerformedCount.Assign(_.FiltrationPerformedCount);
 ShotTime.Assign(_.ShotTime);
 Extra.Assign(_.Extra);
}

void VI::Main::Evaluations::Collision::In::ExtraData::Assign(const CollisionTaskModule::ExtraData& _) {
 Mode.Assign(_.Mode);
 PerformedFiltrationInCurrentMode.Assign(_.PerformedFiltrationInCurrentMode);
}

void VI::Main::Evaluations::Collision::Solver_In::Assign(
 const CollisionTaskSolverModule::CommandVector& _) {
 StartSpeedMismatch.Assign(_.Ballistic.StartSpeedMismatch);
 MassMismatch.Assign(_.Ballistic.MassMismatch);
}

void VI::Main::Evaluations::Collision::Out::Assign(const CollisionTaskModule::StatusVector& _) {
 Performed.Assign(_.Performed);
}

void VI::Main::Evaluations::Collision::Solver_Out::Assign(
 const CollisionTaskSolverModule::StatusVector& _) {
 Performed.Assign(_.Performed);
 Result.Assign(_.Result);
 data.Assign(_.data);
 ElapsedTime.Assign(_.ElapsedTime);
}

void VI::Main::Evaluations::Collision::Solver_Out::OutData::Assign(
 const CollisionTaskSolverModule::OutData& _) {
 HaveSolution.Assign(_.HaveSolution);
 Extrapolation.Assign(_.Extrapolation);
 Corrections.Assign(_.Extrapolation.Corrections);
}

void VI::Main::Evaluations::Collision::Solver_Out::OutData::Extrapolation::Assign(
 const struct ExtrapolationPolynomials::Collision::InOut& _) {
 ShotTime.Assign(_.ShotTime);
 FlightTime.Assign(_.FlightTime);
 Ground.Assign(_.Ground);
 Ship.Assign(_.Ship);
 DistanceCurrent.Assign(_.DistanceCurrent);
 DistanceMeet.Assign(_.DistanceMeet);
 for (size_t i = 0; i < PolyDegree; ++i) {
  PnShip[i].Assign(_.GetPnShip(i));
  PnGround[i].Assign(_.GetPnGround(i));
 }
 constexpr std::array<double, Tests> TestVal = { 0.5, 1.0 };
 for (size_t i = 0; i < Tests; ++i) {
  ShipTest[i].Assign(_.ShipVal(TestVal[i]));
  GroundTest[i].Assign(_.GroundVal(TestVal[i]));
 }
}

void VI::Main::Evaluations::Collision::Solver_Out::OutData::Corrections::Assign(
 const CollisionTable::AllCorrections& _) {
 TailwindDistance.Assign(_.Tailwind.ToDistance.value);
 TailwindHeight.Assign(_.Tailwind.ToHeight.value);
 DencityDistance.Assign(_.Dencity.ToDistance.value);
 DencityHeight.Assign(_.Dencity.ToHeight.value);
 SpeedStartDistance.Assign(_.SpeedMismatch.ToDistance.value);
 SpeedStartHeight.Assign(_.SpeedMismatch.ToHeight.value);
 ShipMotion.Assign(_.ShipMotion);
 Derivation.Assign(_.Derivation);
 Crosswind.Assign(_.Crosswind);
 AimingAngle.Assign(_.AimingAngle);
}

TW::TW(td _) : External(_), Internal(_), Evaluations(_), Trajectories(_),
 SysTimer(_.trIntervals, "інтервали системного таймера", "c", "с"),
 Tick(SysTimer, "c", QPen(Qt::green)) {
}

void TW::Assign(const struct Interface::Command::Main& _) {
 External.Assign(_.External);
 External.Weather.Assign(_.Internal.Comp2.Out);
 Internal.Assign(_.Internal);
 Evaluations.Assign(_.Evaluations);
 Trajectories.Assign(_.Evaluations);
 auto n = _.Internal.LTS.Out.LTS;
 Tick.Add(n, _.Interval);
 SysTimer.Replot();
}

void TW::Suspend() {
 External.Suspend();
 Internal.Suspend();
 Evaluations.Suspend();
 Trajectories.Suspend();
 SysTimer.Suspend();
}

void TW::Resume() {
 External.Resume();
 Internal.Resume();
 Evaluations.Resume();
 Trajectories.Resume();
 SysTimer.Resume();
}

TW::External::External(td _) : NS(_), Weather(_), Radar(_), Gun(_) {
}

void TW::External::Assign(const struct Interface::Command::Main::External& _) {
 NS.Assign(_.NS.Out);
 Radar.Assign(_.Radar);
 Gun.Assign(_);
}

void TW::External::Suspend() {
 NS.Suspend();
 Weather.Suspend();
 Radar.Suspend();
 Gun.Suspend();
}

void TW::External::Resume() {
 NS.Resume();
 Weather.Resume();
 Radar.Resume();
 Gun.Resume();
}

TW::External::NS::NS(td _) :
 Speed(_.trNSSpeed, "складові швидкості", "с", "м/с"),
 SpeedPart(Speed), SpeedAbs(Speed, "abs", QPen(Qt::black)),
 Rot(_.trNSRotations, "кути обертання корабля", "с", "°"), Rotations(Rot) {
}

void TW::External::NS::Assign(const Interface::DisplayRecord<ModuleNS>::OutRecord& _) {
 auto t = _.LTSTime;
 SpeedPart.Add(t, _.SpeedPart);
 SpeedAbs.Add(t, Absolute(_.SpeedPart));
 Speed.Replot();
 Rotations.Add(t, _.Rot);
 Rot.Replot();
}

void TW::External::NS::Suspend() {
 Speed.Suspend();
 Rot.Suspend();
}

void TW::External::NS::Resume() {
 Speed.Resume();
 Rot.Resume();
}

TW::External::Weather::Weather(td _) :
 WindSpeed(_.trWeatherWindSpeed, "швидкість вітру", "с", "м/с"),
 WindSpeedLine(WindSpeed, "", QPen(Qt::black)),
 WindBearing(_.trWeatherWindBearing, "азимут вітру", "с", "°"),
 WindBearingLine(WindBearing, "", QPen(Qt::green)),
 Pressure(_.trWeatherPressure, "тиск", "с", "бар"),
 PressureLine(Pressure, "", QPen(Qt::darkYellow)),
 Temperature(_.trWeatherTemperature, "температура", "с", "°C"),
 AirTemperature(Temperature, "повітря", QPen(Qt::blue)),
 ChargeTemperature(Temperature, "заряд", QPen(Qt::cyan)) {
}

void TW::External::Weather::Assign(const ::Comp2::InputData& _) {
 auto t = _.Time;
 auto w = _.Weather;
 WindSpeedLine.Add(t, w.WindSpeed);
 WindSpeed.Replot();
 WindBearingLine.Add(t, w.WindBearing);
 WindBearing.Replot();
 PressureLine.Add(t, w.Pressure);
 Pressure.Replot();
 AirTemperature.Add(t, w.Temperature);
 ChargeTemperature.Add(t, w.ChargeTemperature);
 Temperature.Replot();
}

void TW::External::Weather::Suspend() {
 WindSpeed.Suspend();
 WindBearing.Suspend();
 Pressure.Suspend();
 Temperature.Suspend();
}

void TW::External::Weather::Resume() {
 WindSpeed.Resume();
 WindBearing.Resume();
 Pressure.Resume();
 Temperature.Resume();
}

TW::External::Radar::Radar(td _) : ETD_Detection(_) {}

void TW::External::Radar::Assign(const struct Interface::Command::Main::External::Radar& _) {
 ETD_Detection.Assign(_.ETD_Detection.Out);
}

void TW::External::Radar::Suspend() {
 ETD_Detection.Suspend();
}

void TW::External::Radar::Resume() {
 ETD_Detection.Resume();
}

TW::External::Radar::ETD_Detection::ETD_Detection(td _) :
 Distance(_.trRadarETDDistance, "дальність", "с", "м"), D(Distance, "дальність", QPen(Qt::green)),
 Angles(_.trRadarETDAngles, "кути відхилення", "с", "°"),
 Common(Angles, "синфазне:", QPen(Qt::green), QPen(Qt::darkGreen)),
 Quadrature(Angles, "квадратурне: ") {
}

void TW::External::Radar::ETD_Detection::Assign(const ::ETD_Detection::InputData& _) {
 auto t = _.LTS;
 D.Add(t, _.First.Distance);
 Distance.Replot();
 Common.Add(t, _.First.CommonModeDeviation);
 Quadrature.Add(t, _.First.QuadratureDeviation);
 Angles.Replot();
}

void TW::External::Radar::ETD_Detection::Suspend() {
 Distance.Suspend();
 Angles.Suspend();
}

void TW::External::Radar::ETD_Detection::Resume() {
 Distance.Resume();
 Angles.Resume();
}

TW::External::Gun::Gun(td _) :
 Pos(_.trGun, "положення гармати", "с", "°"), Position(Pos) {}

void TW::External::Gun::Assign(const struct Interface::Command::Main::External& _) {
 auto t = _.NS.Out.LTSTime;
 Position.Add(t, { _.GunHA.In.Position, _.GunElev.In.Position });
 Pos.Replot();
}

void TW::External::Gun::Suspend() {
 Pos.Suspend();
}

void TW::External::Gun::Resume() {
 Pos.Resume();
}

TW::Internal::Internal(td _) : GTC(_), DAT(_), LRF(_), Training(_) {
}

void TW::Internal::Assign(const struct Interface::Command::Main::Internal& _) {
 GTC.Assign(_.GTC);
 DAT.Assign(_.DAT[0]);
 LRF.Assign(_.LRF.Out);
 Training.Assign(_.Training);
}

void TW::Internal::Suspend() {
 GTC.Suspend();
 DAT.Suspend();
 LRF.Suspend();
 Training.Suspend();
}

void TW::Internal::Resume() {
 GTC.Resume();
 DAT.Resume();
 LRF.Resume();
 Training.Resume();
}

TW::Internal::GTC::GTC(td _) :
 ORU(_.trORU, "координати приладу", "с", "°"), Ground(ORU, "ЗСК: "), Ship(ORU, "ПСК: "),
 ETD(_.trORU_ETD, "координати цілевказування", "с", "°"), ETD_Ground(ETD) {
}

void TW::Internal::GTC::Assign(const Interface::DisplayRecord<ModuleGTC>& _) {
 auto t = _.Out.LTS;
 Ground.Add(t, _.Out.ORU.Ground);
 Ship.Add(t, _.Out.ORU.Ship);
 ORU.Replot();
 ETD_Ground.Add(t, _.In.ORU.ETD);
 ETD.Replot();
}

void TW::Internal::GTC::Suspend() {
 ORU.Suspend();
 ETD.Suspend();
}

void TW::Internal::GTC::Resume() {
 ORU.Resume();
 ETD.Resume();
}

TW::Internal::DAT::DAT(td _) :
 AM(_.trDAT, "похибки автосупроводу", "с", "°"), AutotrackingMismatch(AM) {
}

void TW::Internal::DAT::Assign(const Interface::DisplayRecord<ModuleDAT>& _) {
 auto t = _.Out.LTS;
 AutotrackingMismatch.Add(t, _.Out.AutotrackingMismatch);
 AM.Replot();
}

void TW::Internal::DAT::Suspend() {
 AM.Suspend();
}

void TW::Internal::DAT::Resume() {
 AM.Resume();
}

TW::Internal::LRF::LRF(td _) :
 Distances(_.trLRF, "виміряні дальності", "с", "м"),
 D1(Distances, "дальність №1", QPen(Qt::green)),
 D2(Distances, "дальність №2", QPen(Qt::darkGreen)),
 D3(Distances, "дальність №3", QPen(Qt::darkCyan)) {
}

void TW::Internal::LRF::Assign(const ::LRF::InputData& _) {
 auto t = _.LTS;
 D1.Add(t, _.Distance1);
 D2.Add(t, _.Distance2);
 D3.Add(t, _.Distance3);
 Distances.Replot();
}

void TW::Internal::LRF::Suspend() {
 Distances.Suspend();
}

void TW::Internal::LRF::Resume() {
 Distances.Resume();
}

TW::Internal::Training::Training(td _) :
 Distance(_.trTrainingDistance, "дальність", "с", "м"), D(Distance, "", QPen(Qt::green)),
 Angles(_.trTrainingAngles, "кути", "с", "°"), A(Angles) {
}

void TW::Internal::Training::Assign(const Interface::DisplayRecord<ModuleTraining>& _) {
 auto t = _.Command.LTS;
 D.Add(t, _.In.Sph.Distance);
 A.Add(t, { _.In.Sph.Bearing, _.In.Sph.Elevation} );
 Distance.Replot();
 Angles.Replot();
}

void TW::Internal::Training::Suspend() {
 Distance.Suspend();
 Angles.Suspend();
}

void TW::Internal::Training::Resume() {
 Distance.Resume();
 Angles.Resume();
}

TW::Evaluations::Evaluations(td _) :
 FilterOptical {
  {
   Trend(_.trFilterOpticalInAngles, "кути оптичного каналу", "с", "°"),
   TrendLine::Spherical2DGround(FilterOptical.In.Angles, "ЗСК: "),
   TrendLine::Spherical2DShip(FilterOptical.In.Angles, "ПСК: "),
   TrendLine::Spherical2D(FilterOptical.In.Angles, "помилки автосупроводу: "),
   Trend(_.trFilterOpticalInDistance, "дальність оптичного каналу", "с", "м"),
   TrendLine::Simple(FilterOptical.In.Distance, "дальність", QPen(Qt::green))
  }, {
   Trend(_.trFilterOpticalOutAngles, "кути оптичного каналу", "с", "°"),
   TrendLine::Spherical2DGround(FilterOptical.Out.Angles, "ЗСК: "),
   TrendLine::Spherical2DShip(FilterOptical.Out.Angles, "ПСК: "),
   Trend(_.trFilterOpticalOutCoordinates, "координати цілі оптичного каналу", "с", "м"),
   TrendLine::Cartesian3D(FilterOptical.Out.Target),
   Trend(_.trFilterOpticalOutNS, "координати корабля оптичного каналу", "с", "м"),
   TrendLine::Cartesian3D(FilterOptical.Out.ShipMotion)
  }
 },
 FilterRadar {
  {
   Trend(_.trFilterRadarInAngles, "кути радіолокаційного каналу", "с", "°"),
   TrendLine::Spherical2DGround(FilterRadar.In.Angles, "ЗСК: "),
   TrendLine::Spherical2DShip(FilterRadar.In.Angles, "ПСК: "),
   TrendLine::Spherical2D(FilterRadar.In.Angles, "помилки автосупроводу: "),
   Trend(_.trFilterRadarInDistance, "дальність радіолокаційного каналу", "с", "м"),
   TrendLine::Simple(FilterRadar.In.Distance, "дальність", QPen(Qt::green)),
  }, {
   Trend(_.trFilterRadarOutAngles, "кути радіолокаційного каналу", "с", "°"),
   TrendLine::Spherical2DGround(FilterRadar.Out.Angles, "ЗСК: "),
   TrendLine::Spherical2DShip(FilterRadar.Out.Angles, "ПСК: "),
   Trend(_.trFilterRadarOutCoordinates, "координати цілі радіолокаційного каналу", "с", "м"),
   TrendLine::Cartesian3D(FilterRadar.Out.Target),
   Trend(_.trFilterRadarOutNS, "координати корабля радіолокаційного каналу", "с", "м"),
   TrendLine::Cartesian3D(FilterRadar.Out.ShipMotion)
  }
 },
 CollisionOptical {
  {
   Trend(_.trCollisionOpticalOutAngles, "кути оптичного каналу", "с", "°"),
   TrendLine::Spherical2DGround(CollisionOptical.Out.Angles, "ЗСК: "),
   TrendLine::Spherical2DShip(CollisionOptical.Out.Angles, "ПСК: "),
   Trend(_.trCollisionOpticalOutDistances, "дальності оптичного каналу", "с", "м"),
   TrendLine::Simple(CollisionOptical.Out.Distances, "ціль", QPen(Qt::green)),
   TrendLine::Simple(CollisionOptical.Out.Distances, "зустріч", QPen(Qt::darkGreen)),
   Trend(_.trCollisionOpticalOutFlightTime, "час польоту снаряду оптичного каналу", "с", "с"),
   TrendLine::Simple(CollisionOptical.Out.Time, "", QPen(Qt::black)),
   {
    Trend(_.trCollisionOpticalCorrectionsToDistance, "до дальності оптичного каналу", "с", "м"),
    TrendLine::Simple(
     CollisionOptical.Out.Corrections.ToDistance, "на задній вітер", QPen(Qt::black)),
    TrendLine::Simple(
     CollisionOptical.Out.Corrections.ToDistance, "на щільність повітря", QPen(Qt::green)),
    TrendLine::Simple(
     CollisionOptical.Out.Corrections.ToDistance,
     "на відхилення початкової швидкості", QPen(Qt::blue)),
    Trend(_.trCollisionOpticalCorrectionsToHeight, "до висоти оптичного каналу", "с", "м"),
    TrendLine::Simple(
     CollisionOptical.Out.Corrections.ToHeight, "на задній вітер", QPen(Qt::black)),
    TrendLine::Simple(
     CollisionOptical.Out.Corrections.ToHeight, "на щільність повітря", QPen(Qt::green)),
    TrendLine::Simple(
     CollisionOptical.Out.Corrections.ToHeight,
     "на відхилення початкової швидкості", QPen(Qt::blue)),
    Trend(_.trCollisionOpticalCorrectionsToBearing, "до азимута оптичного каналу", "с", "°"),
    TrendLine::Degrees(CollisionOptical.Out.Corrections.ToBearing, "деривація", QPen(Qt::black)),
    TrendLine::Degrees(
     CollisionOptical.Out.Corrections.ToBearing, "на боковий вітер", QPen(Qt::green)),
    TrendLine::Degrees(
     CollisionOptical.Out.Corrections.ToBearing, "на рух корабля", QPen(Qt::blue)),
    Trend(_.trCollisionOpticalCorrectionsToElevation, "до кута місця оптичного каналу", "с", "°"),
    TrendLine::Degrees(
     CollisionOptical.Out.Corrections.ToElevation, "кут прицілювання", QPen(Qt::black)),
    TrendLine::Degrees(
     CollisionOptical.Out.Corrections.ToElevation, "на рух корабля", QPen(Qt::blue))
   }
  }
 },
 CollisionRadar {
  {
   Trend(_.trCollisionRadarOutAngles, "кути радіолокаційного каналу", "с", "°"),
   TrendLine::Spherical2DGround(CollisionRadar.Out.Angles, "ЗСК: "),
   TrendLine::Spherical2DShip(CollisionRadar.Out.Angles, "ПСК: "),
   Trend(_.trCollisionRadarOutDistances, "дальності радіолокаційного каналу", "с", "м"),
   TrendLine::Simple(CollisionRadar.Out.Distances, "ціль", QPen(Qt::green)),
   TrendLine::Simple(CollisionRadar.Out.Distances, "зустріч", QPen(Qt::darkGreen)),
   Trend(
    _.trCollisionRadarOutFlightTime, "час польоту снаряду радіолокаційного каналу", "с", "с"),
   TrendLine::Simple(CollisionRadar.Out.Time, "", QPen(Qt::black)),
   {
    Trend(
     _.trCollisionRadarCorrectionsToDistance, "до дальності радіолокаційного каналу", "с", "м"),
    TrendLine::Simple(
     CollisionRadar.Out.Corrections.ToDistance, "на задній вітер", QPen(Qt::black)),
    TrendLine::Simple(
     CollisionRadar.Out.Corrections.ToDistance, "на щільність повітря", QPen(Qt::green)),
    TrendLine::Simple(
     CollisionRadar.Out.Corrections.ToDistance,
     "на відхилення початкової швидкості", QPen(Qt::blue)),
    Trend(_.trCollisionRadarCorrectionsToHeight, "до висоти радіолокаційного каналу", "с", "м"),
    TrendLine::Simple(
     CollisionRadar.Out.Corrections.ToHeight, "на задній вітер", QPen(Qt::black)),
    TrendLine::Simple(
     CollisionRadar.Out.Corrections.ToHeight, "на щільність повітря", QPen(Qt::green)),
    TrendLine::Simple(
     CollisionRadar.Out.Corrections.ToHeight,
     "на відхилення початкової швидкості", QPen(Qt::blue)),
    Trend(_.trCollisionRadarCorrectionsToBearing, "до азимута радіолокаційного каналу", "с", "°"),
    TrendLine::Degrees(CollisionRadar.Out.Corrections.ToBearing, "деривація", QPen(Qt::black)),
    TrendLine::Degrees(
     CollisionRadar.Out.Corrections.ToBearing, "на боковий вітер", QPen(Qt::green)),
    TrendLine::Degrees(
     CollisionRadar.Out.Corrections.ToBearing, "на рух корабля", QPen(Qt::blue)),
    Trend(
     _.trCollisionRadarCorrectionsToElevation, "до кута місця радіолокаційного каналу", "с", "°"),
    TrendLine::Degrees(
     CollisionRadar.Out.Corrections.ToElevation, "кут прицілювання", QPen(Qt::black)),
    TrendLine::Degrees(
     CollisionRadar.Out.Corrections.ToElevation, "на рух корабля", QPen(Qt::blue))
   }
  }
 },
 Service(_) {
}

void TW::Evaluations::Assign(const struct Interface::Command::Main::Evaluations& _) {
 FilterOptical.Assign(_.FilterOptical);
 FilterRadar.Assign(_.FilterRadar);
 CollisionOptical.Assign(_.CollisionOptical);
 CollisionRadar.Assign(_.CollisionRadar);
 Service.Assign(_);
}

void TW::Evaluations::Suspend() {
 FilterOptical.Suspend();
 FilterRadar.Suspend();
 CollisionOptical.Suspend();
 CollisionRadar.Suspend();
 Service.Suspend();
}

void TW::Evaluations::Resume() {
 FilterOptical.Resume();
 FilterRadar.Resume();
 CollisionOptical.Resume();
 CollisionRadar.Resume();
 Service.Resume();
}

void TW::Evaluations::Filter::Assign(
 const struct Interface::Command::Main::Evaluations::Filter& _) {
 In.Assign(_.In);
 Out.Assign(_.Out);
}

void TW::Evaluations::Filter::Suspend() {
 In.Suspend();
 Out.Suspend();
}

void TW::Evaluations::Filter::Resume() {
 In.Resume();
 Out.Resume();
}

void TW::Evaluations::Filter::In::Assign(const FilterModule::CommandVector& _) {
 auto ta = _.data.Time;
 Ground.Add(ta, _.data.Ground);
 Ship.Add(ta, _.data.Ship);
 AutotrackingMismatch.Add(ta, _.data.AutotrackingMismatch);
 Angles.Replot();
 auto td = _.DD.Time;
 D.Add(td, _.DD.Distance);
 Distance.Replot();
}

void TW::Evaluations::Filter::In::Suspend() {
 Angles.Suspend();
 Distance.Suspend();
}

void TW::Evaluations::Filter::In::Resume() {
 Angles.Resume();
 Distance.Resume();
}

void TW::Evaluations::Filter::Out::Assign(const FilterModule::StatusVector& _) {
 auto t = _.data.Time;
 Ground.Add(t, _.Extrapolation.Angles.GroundVal(0));
 Ship.Add(t, _.Extrapolation.Angles.ShipVal(0));
 Angles.Replot();
 Coords.Add(t, _.data.Cart(0));
 Target.Replot();
 ShipCoords.Add(t, _.Extrapolation.NS.GetShipCart(0));
 ShipMotion.Replot();
}

void TW::Evaluations::Filter::Out::Suspend() {
 Angles.Suspend();
 Target.Suspend();
 ShipMotion.Suspend();
}

void TW::Evaluations::Filter::Out::Resume() {
 Angles.Resume();
 Target.Resume();
 ShipMotion.Resume();
}

void TW::Evaluations::Collision::Assign(
 const Interface::Command::Main::Evaluations::Collision& _) {
 Out.Assign(_.Solver_Out);
}

void TW::Evaluations::Collision::Suspend() {
 Out.Suspend();
}

void TW::Evaluations::Collision::Resume() {
 Out.Resume();
}

void TW::Evaluations::Collision::Out::Assign(const CollisionTaskSolverModule::StatusVector& _) {
 const auto& e = _.data.Extrapolation;
 auto t = e.ShotTime;
 Ground.Add(t, e.GroundVal(0));
 Ship.Add(t, e.ShipVal(0));
 Angles.Replot();
 Target.Add(t, e.DistanceCurrent);
 Meet.Add(t, e.DistanceMeet);
 Distances.Replot();
 Flight.Add(t, e.FlightTime);
 Time.Replot();
 Corrections.Assign(t, e.Corrections);
}

void TW::Evaluations::Collision::Out::Suspend() {
 Angles.Suspend();
 Distances.Suspend();
 Time.Suspend();
 Corrections.Suspend();
}

void TW::Evaluations::Collision::Out::Resume() {
 Angles.Resume();
 Distances.Resume();
 Time.Resume();
 Corrections.Resume();
}

void TW::Evaluations::Collision::Out::Corrections::Assign(
 double t, const CollisionTable::AllCorrections& _) {
 ToDistanceTailwind.Add(t, _.Tailwind.ToDistance.value);
 ToDistanceDencity.Add(t, _.Dencity.ToDistance.value);
 ToDistanceStartSpeed.Add(t, _.SpeedMismatch.ToDistance.value);
 ToDistance.Replot();
 ToHeightTailwind.Add(t, _.Tailwind.ToHeight.value);
 ToHeightDencity.Add(t, _.Dencity.ToHeight.value);
 ToHeightStartSpeed.Add(t, _.SpeedMismatch.ToHeight.value);
 ToHeight.Replot();
 Derivation.Add(t, _.Derivation);
 Crosswind.Add(t, _.Crosswind);
 ShipMotionHA.Add(t, _.ShipMotion.HA);
 ToBearing.Replot();
 AimingAngle.Add(t, _.AimingAngle);
 ShipMotionElev.Add(t, _.ShipMotion.Elev);
 ToElevation.Replot();
}

void TW::Evaluations::Collision::Out::Corrections::Suspend() {
 ToDistance.Suspend();
 ToHeight.Suspend();
 ToBearing.Suspend();
 ToElevation.Suspend();
}

void TW::Evaluations::Collision::Out::Corrections::Resume() {
 ToDistance.Resume();
 ToHeight.Resume();
 ToBearing.Resume();
 ToElevation.Resume();
}

TW::Evaluations::Service::Service(td _) :
 ElapsedTime(_.trElapsedTime, "час обрахунку задач", "с", "с"),
 FilterOptical(ElapsedTime, "фільтрація оптичного каналу", QPen(Qt::black)),
 FilterRadar(ElapsedTime, "фільтрація радіолокаційного каналу", QPen(Qt::green)),
 CollisionOptical(ElapsedTime, "задача зустрічі оптичного каналу", QPen(Qt::blue)),
 CollisionRadar(ElapsedTime, "задача зустрічі радіолокаційного каналу", QPen(Qt::yellow)),
 Cumulative(ElapsedTime, "сумарний", QPen(Qt::red)) {
}

void TW::Evaluations::Service::Assign(const struct Interface::Command::Main::Evaluations& _) {
 FilterOptical.Add(_.FilterOptical.In.data.Time, _.FilterOptical.Out.ElapsedTime);
 FilterRadar.Add(_.FilterRadar.In.data.Time, _.FilterRadar.Out.ElapsedTime);
 CollisionOptical.Add(_.CollisionOptical.In.ShotTime, _.CollisionOptical.Solver_Out.ElapsedTime);
 CollisionRadar.Add(_.CollisionRadar.In.ShotTime, _.CollisionRadar.Solver_Out.ElapsedTime);
 Cumulative.Add(
  std::max(
   std::max(_.FilterOptical.In.data.Time, _.FilterRadar.In.data.Time),
   std::max(_.CollisionOptical.In.ShotTime, _.CollisionRadar.In.ShotTime)),
   _.FilterOptical.Out.ElapsedTime + _.FilterRadar.Out.ElapsedTime +
    _.CollisionOptical.Solver_Out.ElapsedTime + _.CollisionRadar.Solver_Out.ElapsedTime
 );
 ElapsedTime.Replot();
}

void TW::Evaluations::Service::Suspend() {
 ElapsedTime.Suspend();
}

void TW::Evaluations::Service::Resume() {
 ElapsedTime.Resume();
}

TW::Trajectories::Trajectories(td _) :
 XY(_.trXY, "вигляд згори", "схід, м", "північ, м", -1),
 XY_Optical(XY, "ціль, оптичний канал", QPen(Qt::green)),
 XY_Radar(XY, "ціль, радіолокаційний канал", QPen(Qt::blue)),
 XY_Ship(XY, "корабель", QPen(Qt::red)),
 XZ(_.trXZ, "вигляд зі сходу", "північ, м", "висота, м", -1),
 XZ_Optical(XZ, "ціль, оптичний канал", QPen(Qt::green)),
 XZ_Radar(XZ, "ціль, радіолокаційний канал", QPen(Qt::blue)),
 XZ_Ship(XZ, "корабель", QPen(Qt::red)),
 YZ(_.trYZ, "вигляд з півдня", "схід, м", "висота, м", -1),
 YZ_Optical(YZ, "ціль, оптичний канал", QPen(Qt::green)),
 YZ_Radar(YZ, "ціль, радіолокаційний канал", QPen(Qt::blue)),
 YZ_Ship(YZ, "корабель", QPen(Qt::red)) {
}

void TW::Trajectories::Assign(const struct Interface::Command::Main::Evaluations& _) {
 auto CoordOptical = _.FilterOptical.Out.data.Cart(0.0);
 auto CoordRadar = _.FilterRadar.Out.data.Cart(0.0);
 auto CoordShip = _.FilterOptical.Out.Extrapolation.NS.GetShipCart(0.0);
 XY_Optical.Add(CoordOptical.Y, CoordOptical.X);
 XY_Radar.Add(CoordRadar.Y, CoordRadar.X);
 XY_Ship.Add(CoordShip.Y, CoordShip.X);
 XY.Replot();
 XZ_Optical.Add(CoordOptical.X, CoordOptical.Z);
 XZ_Radar.Add(CoordRadar.X, CoordRadar.Z);
 XZ_Ship.Add(CoordShip.X, CoordShip.Z);
 XZ.Replot();
 YZ_Optical.Add(CoordOptical.Y, CoordOptical.Z);
 YZ_Radar.Add(CoordRadar.Y, CoordRadar.Z);
 YZ_Ship.Add(CoordShip.Y, CoordShip.Z);
 YZ.Replot();
}

void TW::Trajectories::Suspend() {
 XY.Suspend();
 XZ.Suspend();
 YZ.Suspend();
}

void TW::Trajectories::Resume() {
 XY.Resume();
 XZ.Resume();
 YZ.Resume();
}
