#ifndef SS_DETECTIONDATA_H
#define SS_DETECTIONDATA_H

// radar SS mode data
// (C) I.Drandar, 2019, all rights reserved
// Guidance and tracking controller - Radar

#include "common/ExchangeModule.h"
#include "RadarCommon.h"
#include "data_types.h"

struct SS_Data {

 struct InputData {
  Radar::OperatingFrequencyTuning OFT;
  unsigned int FrequencyCode;
  unsigned int RepetitionPeriodCode;
  Radar::RepetitionPeriodAdjustment RPA;
  unsigned int TargetCount;
  Radar::ActiveNoisePresence ANP;
  bool ActiveNoiseInterferenceProcessing;
  double LTS; // LTS time
 };

 struct InputPacket {
  union {
   struct {
    unsigned char : 5;
    unsigned char OFT : 2;
   };
   Bits7Shared<unsigned int, 9> FrequencyCode;
  };
  union {
   Bits7Shared<unsigned int, 6> RepetitionPeriodCode;
   struct {
    unsigned char : 6;
    unsigned char RPA : 1;
   };
  };
  unsigned char TargetCount : 3;
  unsigned char : 1;
  unsigned char ANP : 2;
  bool ActiveNoiseInterferenceProcessing : 1;
  Packed::LTS_Type LTS; // LTS time
 };

 using OutputData = NOTHING;
 using OutputPacket = NOTHING;

 using CommandVector = NOTHING;
 struct StatusVector {};
};

class ModuleSS_Data : public ExchangeModuleImpl<SS_Data> {
 const struct FakeData {
  const bool use;
  FakeData(const ModuleInit&, const QString&);
 } Fake;
 void DecodePacket(bool) override;
 void EncodePacket(bool) override {}
 void PrepareStatusVector(bool) override;
public:
 ModuleSS_Data(const ModuleInit&);
 void Act() override;
};

#endif // SS_DETECTIONDATA_H
