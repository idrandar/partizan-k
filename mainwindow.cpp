#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDateTime>
#include <iostream>
#include "widgets.h"

Main::Main(QWidget *parent) :
 QMainWindow(parent),
 ui(new Ui::MainWindow), tmr(this),
 app(std::make_unique<Application>()),
 itf(nullptr) {
 ui->setupUi(this);
 connect(&tmr, SIGNAL(timeout()), SLOT(Update()));
// connect(&tmr, &QTimer::timeout,
//  [this]() {
//   const auto s = QDateTime::currentDateTime().toString("dddd d MMMM yyyy hh:mm:ss");
//   ui->lblDateTime->setText(s);
//  });
 tmr.setTimerType(Qt::PreciseTimer);
 tmr.start(200);
 ui->status->addPermanentWidget(ui->lblSubMode);
 ui->status->addPermanentWidget(ui->lblDateTime);
 ui->status->addWidget(ui->lblSystemFuncFault);
 itf = std::make_unique<VI>(*ui);
 try {
  app->Start();
 } catch (std::exception& e) {
  ui->lblSystemFuncFault->setText(e.what());
  itf->System.FuncFault.SetColor("red");
  QApplication::exit(-1);
 }
}

Main::~Main() {
 std::cout << __PRETTY_FUNCTION__ << std::endl;
 delete ui;
}

void Main::Update() {
// double _gone = counter.Gone();
// counter.Start();
// std::cout << __PRETTY_FUNCTION__ << ": time gone: " << std::fixed << _gone << " s" << std::endl;
 const auto s = QDateTime::currentDateTime().toString("dddd d MMMM yyyy hh:mm:ss");
 ui->lblDateTime->setText(s);

 Interface::Command cmd;
 StatusVector::GetCommand(cmd);

 itf->System.SubMode.Assign(cmd.System.State.IntegralMode());
 if (cmd.System.State.Mode == SYSMODE_Initial || cmd.System.State.Mode == SYSMODE_Testing)
  itf->System.SubMode.SetMode(true);
 else
  itf->System.SubMode.SetMode(cmd.System.State.ModeComplete);
 if (cmd.System.State.Mode == SYSMODE_Testing) {
  if (cmd.System.State.TestingCompleted) {
   if (cmd.System.State.Fault)
    itf->System.FuncFault.Assign(Interface::Command::_System::ffTestingCompletedFault);
   else
    itf->System.FuncFault.Assign(Interface::Command::_System::ffTestingCompletedSuccess);
  } else
   itf->System.FuncFault.Assign(Interface::Command::_System::ffNone);
 } else
  itf->System.FuncFault.Assign(cmd.System.FuncFault);
 itf->System.TD.Assign(cmd.System.TD);
// if (i != NULL && !Aux->Stop.IsSet()) {
 itf->Main.Assign(cmd.Main);
// }
}

void Main::on_MainWindow_destroyed() {
 std::cout << __PRETTY_FUNCTION__ << std::endl;
}

void Main::on_btnTrends_clicked() {
 trends = std::make_unique<TrendsWindow>();
 if (!trends->isVisible()) {
  trends->show();
 }
}
