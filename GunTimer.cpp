/*
 * CombatModule.cc
 *
 *  Created on: 12.03.2015
 *      Author: Drandar
 */
// Processing data of "GUN" Timer channel
// (C) I.Drandar, 2019, all rights reserved

#include "GunTimer.h"
#include "DebugOut.h"

ModuleGunTimer::ModuleGunTimer(const ModuleInit& init) :
 BASE(init) {
}

void ModuleGunTimer::DecodePacket(bool dump) {
 Input.Faults.ExchangeGun = input.ExchangeGunFault;
 Input.Faults.ExchangeComp = input.ExchangeCompFault;
 Input.Faults.PowerSupply = input.PowerSupplyFault;
 Input.Faults.URef = input.URefFault;
 // testing stuff
 if (dump) {
  std::cout << "Dump of received " << Name << "::Input packet\n"
   "faults:\n" << Input.Faults << "\n";
 }
}

void ModuleGunTimer::EncodePacket(bool dump) {
 if (Output.Mode == _Gun::gmWork) {
  output.Mode = 0;
  output.ModeNext = false;
 } else {
  output.Mode = 1;
  output.ModeNext = true;
 }
 output.Defence = true;
 output.SpeedNegative = true;
 output.AutoTimer = true;
 output.IsElevChannel = false;
 output.Time = Output.Time;
 output.Offset = 0;
 output.AngleSpeed = 0;
 // testing stuff
 if (dump)
  std::cout << "Dump of transmitted " << GetName() << "::Output packet\n"
   "mode: " << Output.Mode << "\t"
   "time: " << Output.Time << " s\n";
}

void ModuleGunTimer::Act() {
 CommitReception();
 Transmit();
} // end ccRun
