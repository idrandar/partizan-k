// (C) I.Drandar, 2019-2020, all rights reserved

#include "GTC-2.h"
#include "DebugOut.h"

std::ostream& operator<<(std::ostream& _, const struct GTC2::Faults& F) {
 _ <<
  "ПФ1.1:\n"
  "\tKП A6: " << F.DeviceController << "\tЛД A9: " << F.LRF << "\tДУC A11: " << F.ASS << "\t"
  "TBK A13: " << F.TV << "\tTПK A12: " << F.IR << "\n"
  "encoder exchange:\t" << F.EncoderExchange << "\n"
  "motors:\tencoder:\t" << F.Encoder << "\n"
  "ПФ4.1:\n" <<
  "\tA-ЦK.27: " << F.GTC << "\n"
  "\texhange DAT: " << F.ExchangeDAT << "\texchange comp: " << F.ExchangeComp << "\t"
  "exchange DC: " << F.ExchangeDC << "\texchange LTS DC: " << F.ExchangeLTS_DC << "\t"
  "exchange navig: " << F.ExchangeNavig << "\n"
  "ПФ6.2:\n"
  "\tнеиспр. ПФ7.2:\t"
  "no phase: " << F.NoPhase << "\toverheat: " << F.Overheat <<"\t"
  "overcurrent: " << F.Overcurrent << "\tno 24 V: " << F.No24V << "\tno 7.2: " << F.No7_2 << "\n"
  "\tДоп. неисп. НК КС:\t"
  "не заг: " << F.NS_NoH << "\tне дан: " << F.NS_NoD << "\n"
  "ПФ1.1.2:\n"
  "amplifier HA:\tengine: " << F.AmplifierHAEngine << "\tЦП.06: " << F.AmplifierHAA1 << "\t"
  "amplifier Elev:\tengine: " << F.AmplifierElevEngine << "\tЦП.06: " << F.AmplifierElevA3 << "\n"
  "exchange Q: " << F.ExchangeQ << "\texchange E: " << F.ExchangeE;
 return _;
}

ModuleGTC2::ModuleGTC2(const ModuleInit& _) : BASE(_) {
}

void ModuleGTC2::Act() {
 CommitReception();
}

void ModuleGTC2::EncodePacket(bool) {}

void ModuleGTC2::DecodePacket(bool dump) {
 Input.TV = input.TV;
 Input.IR = input.IR;
 //
 Input.Something.FRAM = input.Something.FRAM;
 Input.Something.CRC = input.Something.CRC;
 Input.Something.IntervalMeasurer = input.Something.IntervalMeasurer;
 Input.Something.QSwitch = input.Something.QSwitch;
 Input.Something.LPS = input.Something.LPS; // БПЛ
 Input.Something.WTF0 = input.Something.WTF0;
 Input.Something.OpticalStart = input.Something.OpticalStart;
 Input.Something.Targets = input.Something.Targets;
 Input.Something.LowPeriod = input.Something.LowPeriod;
 Input.Something.OnboardNetwork = input.Something.OnboardNetwork;
 Input.Something.WTF1 = input.Something.WTF1; // Ошибка драйв. ТЕС
 Input.Something.TestingCompleted = input.Something.TestingCompleted;
 Input.Something.Failure = input.Something.Failure;
 // ПФ1.1
 Input.Faults.DeviceController = input.Faults.DeviceController; // КП
 Input.Faults.LRF = input.Faults.LRF;
 Input.Faults.TV = input.Faults.TV;
 Input.Faults.IR = input.Faults.IR;
 Input.Faults.ASS = input.Faults.ASS;
 Input.Faults.EncoderExchange.HA = input.Faults.EncoderHAExchange;
 Input.Faults.EncoderExchange.Elev = input.Faults.EncoderElevExchange;
 // motors
 Input.Faults.Encoder.HA = input.Faults.EncoderHA;
 Input.Faults.Encoder.Elev = input.Faults.EncoderElev;
 // ПФ4.1
 Input.Faults.GTC = input.Faults.GTC; // ЦК.27
 Input.Faults.ExchangeDAT = input.Faults.ExchangeDAT;
 Input.Faults.ExchangeComp = input.Faults.ExchangeComp;
 Input.Faults.ExchangeDC = input.Faults.ExchangeDC;
 Input.Faults.ExchangeLTS_DC = input.Faults.ExchangeLTS_DC;
 Input.Faults.ExchangeNavig = input.Faults.ExchangeNavig;
 // ПФ7.2
 Input.Faults.NoPhase = input.Faults.NoPhase;
 Input.Faults.Overcurrent = input.Faults.Overcurrent;
 Input.Faults.Overheat = input.Faults.Overheat;
 Input.Faults.No24V = input.Faults.No24V;
 Input.Faults.No7_2 = input.Faults.No7_2;
 // Доп. неисп. НК КС
 Input.Faults.NS_NoH = input.Faults.NS_NoH; // не заг.
 Input.Faults.NS_NoD = input.Faults.NS_NoD; // не дан.
 // ПФ1.1.2
 Input.Faults.AmplifierHAEngine = input.Faults.AmplifierHAEngine;
 Input.Faults.AmplifierHAA1 = input.Faults.AmplifierHAA1;
 Input.Faults.AmplifierElevEngine = input.Faults.AmplifierElevEngine;
 Input.Faults.AmplifierElevA3 = input.Faults.AmplifierElevA3;
 Input.Faults.ExchangeQ = input.Faults.ExchangeQ;
 Input.Faults.ExchangeE = input.Faults.ExchangeE;
 if (dump)
  std::cout <<
   "Dump of received " << GetName() << "::Input packet\n"
   "TV FOV:\n" << Input.TV << "\n"
   "IR FOV: " << Input.IR << "\n"
   "...\n"
   "FRAM: " << Input.Something.FRAM << "\tCRC: " << Input.Something.CRC << "\t"
   "interval measurer: " << Input.Something.IntervalMeasurer << "\t"
   "qswitch: " << Input.Something.QSwitch << "\tLPS: " << Input.Something.LPS << "\t"
   "WTF: " << Input.Something.WTF0 << "\toptical start: " << Input.Something.OpticalStart << "\n"
   "number of targets: " << Input.Something.Targets << "\t"
   "low measuring period: " << Input.Something.LowPeriod << "\t"
   "onboard network fail: " << Input.Something.OnboardNetwork << "\t"
   "Ошибка драйв. ТЕС: " << Input.Something.WTF1 << "\t"
   "testing completed: " << Input.Something.TestingCompleted << "\t"
   "failure: " << Input.Something.Failure << "\t"
   "faults\n" << Input.Faults << "\n";
}
