/*
 * Filter.cc
 *
 *  Created on: 14.09.2015
 *      Author: Drandar
 */

#include "Filter.h"
#include "common/Polynomial.h"
#include "common/CoordinatesConversion.h"
#include "Setup.h"
#include <iostream>
#include "DebugOut.h" // for SysMode
#include <iomanip>

// Filter class
// (C) I.Drandar, 2015-2016, all rights reserved
// (C) I.Drandar, for Qt + C++1x, 2018-2019, all rights reserved

void FilterModule::OutData::Set_FilterNotNeeded(double t) {
 std::fill_n(PolynomialCoeffs, 5, Cartesian3DCoords());
 Completed = false;
 Time = t;
}

void FilterModule::OutData::Set_FilterNeededNotDone(double t) {
 std::fill_n(PolynomialCoeffs, 5, Cartesian3DCoords());
 Completed = false;
 Time = t;
}

void FilterModule::OutData::Set_FilterSimple(double t, const Cartesian3DCoordsExtended& Coord) {
 std::fill_n(PolynomialCoeffs, 5, Cartesian3DCoords());
 CoordinatesCount = 0;
 Time = t;
 Completed = true;
 PolynomialCoeffs[0] = Coord.Coords;
}

const size_t FilterModule::MathVar::HypothesesArraySize;

///////////////////////////////////////////////////////////////////////////
//                Функция выполнения одного шага фильтрации              //
//                HPS - координат цели : x, y, z.                        //
//  На 14.08.2006 г. функция доработана в части скользящего запоминания  //
//  выбранных гипотез                                                    //
///////////////////////////////////////////////////////////////////////////
void FilterModule::MathVar::Push(const OutData& Out) {
 // Сдвиг координат
 Coordinates.erase(Coordinates.begin());

 // Запись последних значений координат
 Coordinates.push_back(Out.TargetGroundCart.Coords);

 // Сдвиг запомненных гипотез
 for (size_t i = 1; i < HypothesesArraySize; i++) {
  Hypotheses[i - 1] = Hypotheses[i];
 }

 if (Out.CoordinatesCount >= CoordsArraySize) {
  CountHp++;
  AutoHyp();
 } // Фильтруем. Выборка готова.
}

double FilterModule::MathVar::TimeOffset(int count) const {
  return -(static_cast<int>(CoordsArraySize) - 1 - count) * Tau0;
}

/////////////////////////////////////////////////////////////////////////////////
//    функция вычисляет следующие константы:                                   //
//      - коэффициенты ортогональных полиномов Чебышева:                       //
//        a10,a20,a21,a30,a31,a32,a40,a41,a42,a43;                             //
//      - суммы f0f,f1f,f2f,f3f,f4f;                                           //
//      - значения ортогональных полиномов fi1[i],fi2[i],fi3[i],fi4[i]         //
//        i=0,...,Ns-1;                                                        //
//      - коэффициенты для выбора гипотез: F0,F1,F2,F3,F4.                   //
/////////////////////////////////////////////////////////////////////////////////
void FilterModule::MathVar::PreCountConst() {
 // Константы системы ортогональных полиномов Чебышева
 double x1 = 0;
 double x2 = 0;
 double x3 = 0;
 double x4 = 0;
 for (unsigned int i = 0; i < CoordsArraySize; i++) {
  const double t = TimeOffset(i);
  const double a = t;
  const double b = a * a;
  x1 += a;
  x2 += b;
  x3 += a * b;
  x4 += b * b;
 }

 ff[0] = CoordsArraySize;
 ff[1] = x2 - x1 * x1 / CoordsArraySize;
 a10 = x1 / CoordsArraySize;
 a21 = (x3 - x1 * x2 / CoordsArraySize) / ff[1];
 a20 = x2 / CoordsArraySize - a21 * a10;

 double x3f1 = 0;
 double x3f2 = 0;
 ff[2] = 0;
 for (unsigned int i = 0; i < CoordsArraySize; i++) {
  const double t = TimeOffset(i);
  const double a = t * t;
  const double b = a * t;
  fi1[i] = t - a10;
  fi2[i] = a - a21 * t - a20;
  x3f1 += b * fi1[i];
  x3f2 += b * fi2[i];
  ff[2] += fi2[i] * fi2[i];
 }
 a32 = x3f2 / ff[2];
 a31 = x3f1 / ff[1] - a32 * a21;
 a30 = x3 / CoordsArraySize - a32 * a20 - a10 * x3f1 / ff[1];

 double x4f1 = 0;
 double x4f2 = 0;
 double x4f3 = 0;
 ff[3] = 0;
 for (unsigned int i = 0; i < CoordsArraySize; i++) {
  const double t = TimeOffset(i);
  const double a = t * t;
  const double b = a * a;
  fi3[i] = a * t - a32 * a - a31 * t - a30;
  x4f1 += b * fi1[i];
  x4f2 += b * fi2[i];
  x4f3 += b * fi3[i];
  ff[3] += fi3[i] * fi3[i];
 }
 a43 = x4f3 / ff[3];
 a42 = x4f2 / ff[2] - a43 * a32;
 a41 = x4f1 / ff[1] - a43 * a31 - a21 * x4f2 / ff[2];
 a40 = x4 / CoordsArraySize - a43 * a30 - a20 * x4f2 / ff[2] - a10 * x4f1 / ff[1];

 ff[4] = 0;
 for (unsigned int i = 0; i < CoordsArraySize; i++) {
  const double t = TimeOffset(i);
  const double a = t * t;
  const double b = a * a;
  fi4[i] = b - a43 * a * t - a42 * a - a41 * t - a40;
  ff[4] += fi4[i] * fi4[i];
 }

 // Константы для выбора гипотез
 F0 = 1.0 / ff[0];
 F1 = F0 + a10 * a10 / ff[1];
 F2 = F1 + a20 * a20 / ff[2];
 F3 = F2 + a30 * a30 / ff[3];
 F4 = F3 + a40 * a40 / ff[4];
}

///////////////////////////////////////////////////////////////////////////////
//     Фильтр со скользящей выборкой и с автоматическим выбором гипотезы.    //
// На 14.08.2006 г. функция доработана в части скользящего запоминания выб-  //
// ранных гипотез и на основании этой предистории вычисление взвешенных      //
// коэффициентов полинома экстраполяции.                                     //
///////////////////////////////////////////////////////////////////////////////
void FilterModule::MathVar::AutoHyp() {
 const double K_Psi = 1.0 / 20.5;

 // Обнуление сумм
 // Вычисление сумм
 //скалярные произведения векторов координат цели на вектор значений полинома Чебышева 0-й ст.)
 const Cartesian3DCoords f0 = std::accumulate(Coordinates.begin(), Coordinates.end(), Cartesian3DCoords());
 //скалярные произведения векторов координат цели на вектор значений полинома Чебышева 1-й ст.)
 const Cartesian3DCoords f1 = std::inner_product(fi1.begin(), fi1.end(), Coordinates.begin(), Cartesian3DCoords());
 //скалярные произведения векторов координат цели на вектор значений полинома Чебышева 2-й ст.)
 const Cartesian3DCoords f2 = std::inner_product(fi2.begin(), fi2.end(), Coordinates.begin(), Cartesian3DCoords());
 //скалярные произведения векторов координат цели на вектор значений полинома Чебышева 3-й ст.)
 const Cartesian3DCoords f3 = std::inner_product(fi3.begin(), fi3.end(), Coordinates.begin(), Cartesian3DCoords());
 //скалярные произведения векторов координат цели на вектор значений полинома Чебышева 4-й ст.)
 const Cartesian3DCoords f4 = std::inner_product(fi4.begin(), fi4.end(), Coordinates.begin(), Cartesian3DCoords());
 const Cartesian3DCoords __ =
  std::inner_product(Coordinates.begin(), Coordinates.end(), Coordinates.begin(), Cartesian3DCoords());

 Cartesian3DCoords C[5]; //коэффициенты разложения выборок координат по базису ортогональнальных полиномов Чебышева
 // Оценки коэффициентов полиномов - коэффициенты разложения векторов координат по базису ортогональнальных полиномов Чебышева
 C[0] = f0 / ff[0];
 C[1] = f1 / ff[1];
 C[2] = f2 / ff[2];
 C[3] = f3 / ff[3];
 C[4] = f4 / ff[4];

 // Вычисление по выборке сумм квадратов невязки для каждой координаты по гипотезам
 // X // Y // Z
 Cartesian3DCoords ll[5];
 // X // Y // Z
 ll[0] = __ - f0 * f0 / ff[0]; // для 0-й гипотезы
 ll[1] = ll[0] - f1 * f1 / ff[1]; // для 1-й гипотезы
 ll[2] = ll[1] - f2 * f2 / ff[2]; // для 2-й гипотезы
 ll[3] = ll[2] - f3 * f3 / ff[3]; // для 3-й гипотезы
 ll[4] = ll[3] - f4 * f4 / ff[4]; // для 4-й гипотезы

 // Вычисление оценок дисперсии обобщенной ошибки исходных декартовых координат,
 // (вычисленных по измеренным сферическим координатам с ипользованием навигационных данных)
 const Cartesian3DCoords D = ll[4] / (CoordsArraySize - 5);


 // Оценки дисперсии ошибок отфильтрованных координат на текущий момент времени по всем гипотезам
 Cartesian3DCoords Sig[5];
 Sig[0] = D * F0;
 Sig[1] = D * F1;
 Sig[2] = D * F2;
 Sig[3] = D * F3;
 Sig[4] = D * F4;

 Cartesian3DCoords Sm[5];
 // Вычисление усредненной суммы квадратов детерминированной составляющей невязки
 // (для каждой гипотезы каждой координаты)
 for (size_t i = 0; i < 5; i++) {
  Sm[i] = ll[i] / (CoordsArraySize - i - 1) - D;
 }

 // Выбор гипотезы
 // Вычисление значений функций штрафа (для каждой гипотезы каждой координаты)
 Cartesian3DCoords Psi[5]; //функции штрафа
 for (size_t i = 0; i < 5; i++) {
  Psi[i] = K_Psi * Sm[i] + Sig[i];
 }

 // Собсвенно выбор: выбор для каждой координаты той гипотезы,
 // у которой минимально  значение функции штрафа
 Cartesian3DCoords abc = Psi[0];

 const int nr = HypothesesArraySize - 1;
 Hypotheses[nr] = {};
 for (size_t i = 1; i < 5; i++) {
  for (size_t j = 0; j < std::size(abc.CoordList); ++j) {
   const auto& c = Hypotheses[0].CoordList[j];
   const auto& cc = abc.CoordList[j];
   if (Psi[i].*cc < abc.*cc) {
    abc.*cc = Psi[i].*cc;
    Hypotheses[nr].*c = i;
   }
  }
 }

 // Принудительное исключение гипотез высокого порядка
 Hypotheses[nr].X = std::min(Hypotheses[nr].X, nHp_max.X);
 Hypotheses[nr].Y = std::min(Hypotheses[nr].Y, nHp_max.Y);
 Hypotheses[nr].Z = std::min(Hypotheses[nr].Z, nHp_max.Z);

 // Вычисление усредненных коэффициентов полиномов экстраполяции координат цели
 // с использованием данных в скользящих выборках вычисленных гипотез;
 auto Mo = std::min(CountHp, HypothesesArraySize);
 for (size_t j = 0; j < 3; j++) {
  const Cartesian3DCoordsType<int>::Coordinate c = Cartesian3DCoordsType<int>::CoordList[j];
  const Cartesian3DCoords::Coordinate cc = Cartesian3DCoords::CoordList[j];
  int k1 = 0;
  int k2 = 0;
  int k3 = 0;
  int k4 = 0;

  for (size_t i = HypothesesArraySize - 1; i + 1 > HypothesesArraySize - Mo; --i) {
   if (Hypotheses[i].*c == 1)
    k1++;
   if (Hypotheses[i].*c == 2)
    k2++;
   if (Hypotheses[i].*c == 3)
    k3++;
   if (Hypotheses[i].*c == 4)
    k4++;
  }
  // Вычисление коэффициентов полиномов экстраполяции координат цели
  // как взвешенных (усредненных) значений

  // Вычисление весовых коэффициентов
  const double p1 = (double)(k1 + k2 + k3 + k4) / (double)Mo;
  const double p2 = (double)(k2 + k3 + k4) / (double)Mo;
  const double p3 = (double)(k3 + k4) / (double)Mo;
  const double p4 = (double)(k4) / (double)Mo;

  // Собственно вычисление коэффициентов полиномов экстраполяции координат цели
  TargetCoeff[0].*cc = C[0].*cc - C[1].*cc * a10 * p1 - C[2].*cc * a20 * p2 - C[3].*cc * a30 * p3 - C[4].*cc * a40 * p4;
  TargetCoeff[1].*cc = C[1].*cc * p1 - C[2].*cc * a21 * p2 - C[3].*cc * a31 * p3 - C[4].*cc * a41 * p4;
  TargetCoeff[2].*cc = C[2].*cc * p2 - C[3].*cc * a32 * p3 - C[4].*cc * a42 * p4;
  TargetCoeff[3].*cc = C[3].*cc * p3 - C[4].*cc * a43 * p4;
  TargetCoeff[4].*cc = C[4].*cc * p4;
 }
}

class FilterModule::FilterCaller : public Caller {
 virtual status call() {
  return dynamic_cast<FilterModule&>(owner).Calculate();
 }
public:
 FilterCaller(FilterModule& _) : Caller(_) {}
 virtual ~FilterCaller() {}
};

FilterModule::TestDump::TestDump(const ModuleInit& init, const QString& s) :
 Input(init.GetConst(s, "input")), Output(init.GetConst(s, "output")),
 EvaluatedDistance(init.GetConst(s, "evaluated-distance")),
 ExtrapolationOK(init.GetConst(s, "extrapolation-OK")),
 Time(init.GetConst(s, "time")), HaveAngleDataInCurrentMode(init.GetConst(s, "have-angle-data-in-current-mode")),
 ElapsedTime(init.GetConst(s, "elapsed-time")) {
}

FilterModule::Algorithm::Algorithm(const ModuleInit& init, const QString& s) :
 DistanceMin(init.GetConst(s, "distance-min")), AlwaysUseOEUC(init.GetConst(s, "always-use-OEUC")),
 UseShipCoords(init.GetConst(s, "use-ship-coords")),
 AirTarget(init.GetConst(s, "air-target")),
 TargetTypeExternal(init.GetConst(s, "target-type-external")) {
}

FilterModule::FilterModule(const ModuleInit& init) :
 Module(init), Calculator(init.Thread("go"), new FilterCaller(*this)),
 Command(), Status(), cmd(), st(),
 HaveAngleDataInCurrentMode(), HaveNavigationDataInCurrentMode(), ModeIT_Corrections(),
 Time(NAN), PreviousMode(),
 OEUC_Location(setup->GetConst("constructive", "OEU location")), MV(TimeStep()),
 TestDump(init, "test-dump"), Algorithm(init, "algorithm") {
}

/////////////////////////////////////////////////////////////////////////////////
// Производит предварительную установку массивов фильтрации и массива гипотез  //
/////////////////////////////////////////////////////////////////////////////////
void FilterModule::SetInit(OutData& _) {
 _.Set_FilterNotNeeded(Time);
}

const Rotations FilterModule::GetRotations() const {
 const double te_Sh = Time - st.Extrapolation.NS.Time; // Time interval  for extrapolation ship   coordinates
 // Angle  coordinates ship         in time of packet TgAnState
 return st.Extrapolation.NS.GetRotations(te_Sh);
}

const Cartesian3DCoords FilterModule::TargetOffset(const Rotations& Rot) const {
// const double te_Tg = Time - st.data.Time; // Time interval  for extrapolation target coordinates
 const double te_Tg = cmd.data.Time - st.data.Time; // Time interval  for extrapolation target coordinates
// std::cout << te_Tg << std::endl;
 // Topocentrical coordinates target in time of packet TgAnState
 const Cartesian3DCoords Tg = st.data.Cart(te_Tg, 2);
 // Topocentrical  coordinates ship in time of packet TgAnState
 const double te_Sh = cmd.data.Time - st.Extrapolation.NS.Time; // Time interval  for extrapolation Ship coordinates
 const Cartesian3DCoords ShipCart = st.Extrapolation.NS.GetShipCart(te_Sh);
 // Topocentrical  coordinates OED in time of packet TgAnState
 const Cartesian3DCoords a = ShipCart + ConvertShipToGroundCartesian(OEUC_Location, Rot);
 // Topocentrical coordinates vector "OED-Target"
 return Tg - a;
}

void FilterModule::ModeIT_CorrectFilterOutput(const Cartesian3DCoords& Offset) {
 // sin, cos  of Azimuth and angle elevation of above vector
 const double de = AbsoluteXY(Offset);
 const double De = Absolute(Offset);
 const double sA = Offset.Y / de;
 const double cA = Offset.X / de;
 const double sB = Offset.Z / De;
 const double cB = de / De;

 // Corrections to topocentrical coordinates target
 const Spherical2DCoords InertialtrackingMismatch = cmd.InertialtrackingMismatch;

 const Cartesian3DCoords dl {
  -De * (InertialtrackingMismatch.HA * sA * cB + InertialtrackingMismatch.Elev * cA * sB),
   De * (InertialtrackingMismatch.HA * cA * cB - InertialtrackingMismatch.Elev * sA * sB),
   De * InertialtrackingMismatch.Elev * cB
 };

 // Corrections to filter
 ModeIT_Corrections = dl;
}

Module::status FilterModule::Calculate() {
 Calculator.Wait();
 Calculator.Lock();
 cmd = Command;
 Calculator.Unlock();

 CTime calcTime; // start of elapsed time measuring
 calcTime.Start();
 if (PreviousMode != cmd.Mode) {
  if (!(PreviousMode == SYSMODE_AT && cmd.Mode == SYSMODE_IT)) {
   HaveNavigationDataInCurrentMode = false;
   HaveAngleDataInCurrentMode = false;
   st.PerformedFiltrationInCurrentMode = false;
   st.Performed = 0;
   if (!Algorithm.AlwaysUseOEUC) {
    st.Extrapolation.Angles.Reset();
   }
  }
  PreviousMode = cmd.Mode;
 }
 AirTarget =
  (Algorithm.TargetTypeExternal && cmd.AirTarget) ||
  (!Algorithm.TargetTypeExternal && Algorithm.AirTarget);
 st.AirTargetChanged = AirTarget != AirTargetPrevious;
 AirTargetPrevious = AirTarget;
 dump_if(TestDump.Input) <<
  "\tMode: " << cmd.Mode << "\n\t"
  "input-data:\ttime: " << std::fixed << std::setprecision(2) << cmd.data.Time << " s\t"
  "ship coordinates: " << cmd.data.Ship << "\n\t\t\t"
  "autotracking mismatch: " << cmd.data.AutotrackingMismatch << "\n\t"
  "navigation data:\tnew info: " << cmd.NS_Data.NewInfo << "\ttime: " << cmd.NS_Data.Time << " s\n\t\t\t"
  "rotations:\t" << cmd.NS_Data.Rot << "\n\t\t\t"
  "rotations change:\t" << cmd.NS_Data.RotChange << "\n\t\t\t"
  "speed: (m/s)\t" << cmd.NS_Data.SpeedPart << "\n\t"
  "distance data:\tOK: " << cmd.DD.OK << "\ttime: " << cmd.DD.Time << " s\n\t\t\t"
  "distance: "<< cmd.DD.Distance << " m\tspeed: " << cmd.DD.Speed << " m/s\t"
  "acceleration: " << cmd.DD.Acceleration << " m/s²\n";

 // NS
 st.Extrapolation.NS.Prepare(cmd.NS_Data, cmd.Mode, HaveNavigationDataInCurrentMode);
 if (cmd.Mode == SYSMODE_IT) {
  Rotations Rot = GetRotations();
  Cartesian3DCoords offset = TargetOffset(Rot);
  ModeIT_CorrectFilterOutput(offset);
  const SphericalCoords _ = CartesianToSpherical(offset);
  Spherical2DGroundCoords Ground { _.Bearing,  _.Elevation };
  NormalizeRadians(Ground.Bearing);
  Ground.Elevation = std::min(Ground.Elevation, 1.3);
  if (Ground.Elevation >= 1.3)
   ModeIT_Corrections = Cartesian3DCoords();

  const Spherical2DShipCoords Ship = ConvertGroundToShipSpherical(Ground, Rot);
  cmd.data.Ship = Ship;
  cmd.data.Ground = Ground;
  cmd.data.AutotrackingMismatch = Spherical2DCoords();

  st.ETD_IT = Ground;
  st.data.PolynomialCoeffs[0] += ModeIT_Corrections;
 }

 // distance
 if (cmd.DD.OK)
  st.Extrapolation.Distance.Assign(cmd.DD);
 double TargetDs;
 if (st.Extrapolation.Distance.OK) {
  const double dt = cmd.data.Time - st.Extrapolation.Distance.Time;
  TargetDs = st.Extrapolation.Distance.Evaluate(dt);
 } else
  TargetDs = INFINITY;

 if (TargetDs < Algorithm.DistanceMin)
  TargetDs = INFINITY;

 dump_if(TestDump.EvaluatedDistance) << "\tevaluated distance: " << TargetDs << " m\n";

 // angles
 if (cmd.Mode != SYSMODE_AT)
  cmd.data.AutotrackingMismatch = Spherical2DCoords();
 dump_if(TestDump.Time) <<
  "\tTime: " << std::fixed << cmd.data.Time << std::endl;
 if (fabs(cmd.data.Time - Time) < 0.000001) {
//  std::cout << std::setprecision(5) << cmd.data.Time << "\t" << Time << std::endl;
//  return THREAD_FAIL;
 }
 st.Extrapolation.Angles.Evaluate(cmd.data, TargetDs, st.Extrapolation.NS, Algorithm.UseShipCoords);
 if (st.Extrapolation.Angles.Ready != ExtrapolationPolynomials::Angles::ReadyState::Empty)
  HaveAngleDataInCurrentMode = true;
 dump_if(TestDump.HaveAngleDataInCurrentMode) <<
  "\tHave angle data in current mode: " << HaveAngleDataInCurrentMode << "\n";

 if (!std::isnan(Time) && fabs(Time - st.Extrapolation.Angles.Time) < (TimeStep() / 2)) {
  Time += TimeStep();
 } else
  Time = st.Extrapolation.Angles.Time;
 const bool OK = st.Extrapolation.OK();

 dump_if(TestDump.ExtrapolationOK) <<
  "\tNS.OK: " << st.Extrapolation.NS.OK << "\tDistance.OK: " << st.Extrapolation.Distance.OK << "\t"
  "Angles ready: " << enum2str(st.Extrapolation.Angles.Ready) << "\n";
 if (!OK) return THREAD_FAIL;
 const SystemMode Mode = cmd.Mode;

 enum FiltrationStage {
  fsUNDEFINED,
  fsNO_FILTRATION_NO_DATA,
  fsNO_FILTRATION_HAVE_DATA,
  fsFILTRATION_STARTED
 };
 FiltrationStage NmCase = fsUNDEFINED;

 if (Mode == SYSMODE_Initial || Mode == SYSMODE_Testing ||
     Mode == SYSMODE_Shutdown || Mode == SYSMODE_SS) {
  st.data.Set_FilterNotNeeded(Time);
  st.PerformedFiltrationInCurrentMode = true;
  //return;
 }

 if (Mode == SYSMODE_IT) {
  st.data.SetTargetCoord(Time);
  st.PerformedFiltrationInCurrentMode = true;
  //return;
 }

 if (Mode == SYSMODE_MP || Mode == SYSMODE_ETD || Mode == SYSMODE_AT) {
  if (!st.PerformedFiltrationInCurrentMode)
   if ((!HaveNavigationDataInCurrentMode) || (!HaveAngleDataInCurrentMode))
    NmCase = fsNO_FILTRATION_NO_DATA;
   else
    NmCase = fsNO_FILTRATION_HAVE_DATA;
  else
   NmCase = fsFILTRATION_STARTED;

  if (!OK) {
   NmCase = fsNO_FILTRATION_NO_DATA;
  }

  switch (NmCase) {
  case fsNO_FILTRATION_NO_DATA:

   SetInit(st.data);
   st.data.Set_FilterNeededNotDone(Time);
   st.data.CoordinatesCount = 0;
   st.data.TargetGroundCart.Time = Time;
   st.data.TargetGroundCart.Coords = { 20000.0, 20000.0, 20000.0 };

   break;

  case fsNO_FILTRATION_HAVE_DATA:

   SetInit(st.data);
   st.data.CoordinatesCount = 1;
   EvaluateGround(Time);
   if (cmd.Mode == SYSMODE_AT) {
    MV.Push(st.data);
    Finalize();
   } else {
    st.data.Set_FilterSimple(Time, st.data.TargetGroundCart);
   }
   st.PerformedFiltrationInCurrentMode = true;
   break;

  case fsFILTRATION_STARTED:
   if (cmd.Mode == SYSMODE_AT) {
    st.data.CoordinatesCount++;
   } else {
    st.data.CoordinatesCount = 1;
   }
   EvaluateGround(Time);
   if (cmd.Mode == SYSMODE_AT) {
    MV.Push(st.data);
    Finalize();
   } else {
    st.data.Set_FilterSimple(Time, st.data.TargetGroundCart);
   }
   st.PerformedFiltrationInCurrentMode = true;
   break;

  default:
   ;
  }
 }
 ++st.Performed;
 dump_if(TestDump.Output) << "\n\t"
  "output-data:\tperformed: " << st.Performed << "\t"
  "performed in current mode: " << st.PerformedFiltrationInCurrentMode << "\n\t\t\t"
  "time: " << st.data.Time << "\tcompleted: " << st.data.Completed << "\t"
  "coordinates count: " << st.data.CoordinatesCount << "\n\t"
  "target in ground coordinates:\n\t\t"
  "time: " << st.data.TargetGroundCart.Time << " s\t"
  "cartesian coords (m): " << st.data.TargetGroundCart.Coords << "\n\t"
  "polynomial coefficients:\n\t\t[ 0: " << st.data.PolynomialCoeffs[0] << ", "
  "1: " << st.data.PolynomialCoeffs[1] << ",\n\t\t"
  "  2: " << st.data.PolynomialCoeffs[2] << ", 3: " << st.data.PolynomialCoeffs[3] << ",\n\t\t"
  "  4: " << st.data.PolynomialCoeffs[4] << "]\n";

 calcTime.Stop();
 dump_if(TestDump.ElapsedTime) << "calculation performed in " << std::scientific << calcTime.Span() << " s\n";
 st.ElapsedTime = calcTime.Span();
 Calculator.Lock();
 Status = st;
 Calculator.Unlock();
 return THREAD_OK;
}

void FilterModule::Start() {
 Module::Start();
 Calculator.Start();
}

void FilterModule::Stop() {
 Calculator.Stop();
 Module::Stop();
}

void FilterModule::Interact(const CommandVector& cmd, StatusVector& st) {
 Calculator.Lock();
 Command = cmd;
 Calculator.Unlock();
 Calculator.WakeUp();
 Calculator.Lock();
 st = Status;
 Calculator.Unlock();
}

Cartesian3DCoords FilterModule::OutData::Cart(double dt, size_t range) const {
 range = std::min(range, 4UL);
 return {
  EvaluatePolynomial(PolynomialCoeffs, &Cartesian3DCoords::X, dt, range),
  EvaluatePolynomial(PolynomialCoeffs, &Cartesian3DCoords::Y, dt, range),
  EvaluatePolynomial(PolynomialCoeffs, &Cartesian3DCoords::Z, dt, range)
 };
}

void FilterModule::EvaluateGround(double Time) {
 const double NS_TimeOffset = st.NS_TimeOffset(Time);

 const double Angles_TimeOffset = st.Angles_TimeOffset(Time);

 const double Distance_TimeOffset = st.Distance_TimeOffset(Time);

 // Шаг 1
 const Cartesian3DCoords ShipCart = st.Extrapolation.NS.GetShipCart(NS_TimeOffset);
 //printf("<CFlt.EvaluateGround>   Tm=%8.3f  dt_Flt_Sh=%8.3f  ShX=%8.2f  ShY=%8.2f  ShZ=%8.2f\n",Tm,M.dt_Flt_ShPkPnFt,ShX,ShY,ShZ);

 Cartesian3DCoords TargetGroundCartesian;

 // Шаг 2
 const Rotations ShRot = st.Extrapolation.NS.GetRotations(NS_TimeOffset);

 if (Algorithm.UseShipCoords) {
  // Шаг 3
  const Spherical2DShipCoords tmpSph = st.Extrapolation.Angles.ShipVal(Angles_TimeOffset);
  const SphericalCoords TargetShipSphericalFromOEUC {
   st.Extrapolation.Distance.Evaluate(Distance_TimeOffset), tmpSph.Heading, tmpSph.Elevation
  };

  // Шаг 4
  const Cartesian3DCoords TargetShipCartesianFromOEUC = SphericalToCartesian(TargetShipSphericalFromOEUC);

  // Шаг 5
  Cartesian3DCoords TargetShipCartesian = TargetShipCartesianFromOEUC + OEUC_Location;

  // Шаг 6
  // TgCrDct_Ship_Main.InvertZ();
  TargetGroundCartesian = ConvertShipToGroundCartesian(TargetShipCartesian, ShRot);
  // TgCrDct_Ship_Main.InvertZ();
 } else {
  // Шаг 3
  Spherical2DGroundCoords tmpSph = st.Extrapolation.Angles.GroundVal(Angles_TimeOffset);
  if (!AirTarget) {
   tmpSph.Elevation = 0.0;
  }

  const SphericalCoords TargetGroundSphericalFromOEUC {
   st.Extrapolation.Distance.Evaluate(Distance_TimeOffset), tmpSph.Bearing, tmpSph.Elevation
  };

  // Шаг 4
  const Cartesian3DCoords TargetGroundCartesianFromOEUC = SphericalToCartesian(TargetGroundSphericalFromOEUC);

  // Шаг 5, 6
  TargetGroundCartesian = TargetGroundCartesianFromOEUC + ConvertShipToGroundCartesian(OEUC_Location, ShRot);
 }

 // Шаг 7
 st.data.TargetGroundCart.Time = Time;
 st.data.TargetGroundCart.Coords = TargetGroundCartesian + ShipCart;

 if (!AirTarget) {
  st.data.TargetGroundCart.Coords.Z = 0;
 }
}

void FilterModule::Finalize() {
 // Запоминание результатов выполнения шага фильтрации
 if (st.data.CoordinatesCount >= MV.CoordsArraySize) {
  st.data.Time = Time;

  st.data.Completed = true;
  std::copy(MV.TargetCoeff.begin(), MV.TargetCoeff.end(), st.data.PolynomialCoeffs);
 } else {
  st.data.Set_FilterNeededNotDone(Time);
 }
}

