#ifndef IR_H
#define IR_H

// IR exchange
// (C) I.Drandar, 2018-2020, all rights reserved

#include "common/ExchangeModule.h"

struct IR {

 using InputData = NOTHING;

 struct OutputData {
  bool FocusFar;
  bool FocusNear;
  bool MenuOn;
  bool ContrastPlus;
  bool ContrastMinus;
  bool BrightnessPlus;
  bool BrightnessMinus;
  bool HotBlack;
  bool Calibration;
  bool OpticalZoomPlus;
  bool OpticalZoomMinus;
  bool DigitalZoom;
  unsigned int Brightness;
  unsigned int Contrast;
  Cartesian2DCoordsType<int> CenterCorrection;
 };

 using InputPacket = NOTHING;

 struct OutputPacket {
  struct { // for bitwise alignment
   bool FocusFar : 1;
   bool FocusNear : 1;
   bool MenuOn : 1;
   bool ContrastPlus : 1;
   bool ContrastMinus : 1;
   bool BrightnessMinus : 1;
   bool Calibration : 1;
  };
  struct {
   bool OpticalZoomPlus : 1;
   bool OpticalZoomMinus : 1;
   bool DigitalZoom : 1;
   bool : 2;
   bool BrightnessPlus : 1;
   bool HotBlack : 1;
  };
  Bits7<unsigned int, 7> Brightness;
  Bits7<unsigned int, 7> Contrast;
  Cartesian2DCoordsType<Bits7<int, 7> > CenterCorrection;
 };

 struct CommandVector {
  bool Go;
 };

 struct StatusVector {
 };

};

std::ostream& operator<<(std::ostream&, const IR::OutputData&);
std::ostream& operator<<(std::ostream&, const IR::InputData&) = delete;

class ModuleIR : public ExchangeModuleImpl<IR> {
 void DecodePacket(bool) override;
 void EncodePacket(bool) override;
public:
 ModuleIR(const ModuleInit&);
 void Act() override;
};

#endif // IR_H
