#ifndef TIME_HH_
#define TIME_HH_

/* The author: Alexander Ketner        */
/* Date of creation: 26 April 2007     */
/* Management local system time        */
/* IDE Momentics & QNX Neutrino 6.3.0  */
// (C) I.Drandar, 2018-2019, all rights reserved

#include <unistd.h>
#include <time.h>
#include <signal.h>
#include <map>
#include <semaphore.h>

class AbstractTimer { // abstract generic class

public:
 AbstractTimer() = default;
 virtual ~AbstractTimer() = default;
 virtual void Start() {}
 virtual void Stop() {}
 virtual void Wait() = 0;
};

class UsleepTimer : public AbstractTimer {
 const useconds_t usec;
public:
 UsleepTimer(double period);
 void Wait() override;
 ~UsleepTimer() override {}
};

class NanosleepTimer : public AbstractTimer {
 const timespec req;
public:
 NanosleepTimer(double period);
 void Wait() override;
 ~NanosleepTimer() override {}
};

class ClockNanosleepTimer : public AbstractTimer {
 const clockid_t clock;
 const int type;
 timespec req;
 const double period;
 void advance(double);
public:
 ClockNanosleepTimer(clockid_t, int, double);
 void Wait() override;
 ~ClockNanosleepTimer() override {}
};

class Timer : public AbstractTimer {
 const timespec req;
 timer_t t;
 sem_t wait_sem;
 static void handler(int, siginfo_t*, void*);
 static std::map<timer_t, Timer*> timers;
public:
 Timer(double);
 void Start() override;
 void Stop() override;
 void Wait() override;
 ~Timer() override;
};

#endif /*TIME_HH_*/
