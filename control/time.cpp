/* The author: Alexander Ketner           */
/* Date of creation: 11 October 2006      */
/* Management local system time           */
/* System (service) of a local time       */
/* Measuring of time responses            */
/* timekeeping critical sections          */
/* of a programm code                     */
/* IDE Momentics & QNX Neutrino 6.3.0     */
// (C) I.Drandar, for LINUX, 2018-2019, all rigts reserved

#include "time.h"
#include "exception.h"
#include <math.h>
//#include <errno.h>

UsleepTimer::UsleepTimer(double period) : usec(1'000'000 * period) {
}

void UsleepTimer::Wait() {
 usleep(usec);
}

static constexpr int NSEC_PER_SEC = 1'000'000'000;

NanosleepTimer::NanosleepTimer(double period) :
 req {
  static_cast<time_t>(period), static_cast<time_t>(remainder(period, 1.0) * NSEC_PER_SEC)
 } {}

void NanosleepTimer::Wait() {
 timespec rem;
 nanosleep(&req, &rem);
}

ClockNanosleepTimer::ClockNanosleepTimer(clockid_t clock, int flags, double period) :
 clock(clock), type(flags), period(period) {
 if (type == 0) {
  req = {static_cast<time_t>(period), static_cast<time_t>(remainder(period, 1.0) * NSEC_PER_SEC)};
 } else if (type == TIMER_ABSTIME) {
  clock_gettime(clock, &req);
  advance(period);
 } else {
  throw TException(__PRETTY_FUNCTION__, ": invalid value of type: ", type);
 }
}

void ClockNanosleepTimer::advance(double add) {
 req.tv_nsec += add * NSEC_PER_SEC;
 if (req.tv_nsec >= NSEC_PER_SEC) {
  req.tv_nsec -= NSEC_PER_SEC;
  ++req.tv_sec;
 }
}

void ClockNanosleepTimer::Wait() {
 clock_nanosleep(clock, type, &req, nullptr);
 if (type == TIMER_ABSTIME) {
  advance(period);
 }
}

Timer::Timer(double period) :
 req {
  static_cast<time_t>(period), static_cast<time_t>(remainder(period, 1.0) * NSEC_PER_SEC)
 } {
 struct sigaction sa;
 sa.sa_flags = SA_SIGINFO;
 sa.sa_sigaction = handler;
 sigemptyset(&sa.sa_mask);
 if (sigaction(SIGRTMIN, &sa, nullptr) == -1) {
  throw TException(__PRETTY_FUNCTION__, ": failed to setup signal handling");
 }
 sigevent te;
 te.sigev_notify = SIGEV_SIGNAL;
 te.sigev_signo = SIGRTMIN;
 te.sigev_value.sival_ptr = &t;
 timer_create(CLOCK_REALTIME, &te, &t);
 sem_init(&wait_sem, 0, 0);
}

void Timer::handler(int, siginfo_t *si, void*) {
 timer_t* _t = reinterpret_cast<timer_t*>(si->si_value.sival_ptr);
 Timer* _T = timers[*_t];
 sem_post(&_T->wait_sem);
}

std::map<timer_t, Timer*> Timer::timers;

void Timer::Start() {
 timers[t] = this;
 itimerspec its = { req, req };
 timer_settime(t, 0, &its, nullptr);
}

void Timer::Stop() {
 itimerspec its { {}, {} };
 timer_settime(t, 0, &its, nullptr);
 sem_close(&wait_sem);
}

void Timer::Wait() {
 sem_wait(&wait_sem);
}

Timer::~Timer() {
 timer_delete(t);
 timers[t] = nullptr;
 sem_destroy(&wait_sem);
}
