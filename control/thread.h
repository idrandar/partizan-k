#ifndef THREAD_HH_
#define THREAD_HH_

/* The author: Alexander Ketner                       */
/* Date of creation: 03 May 2007                      */
/* Include class of control thread                    */
/* IDE Momentics & QNX Neutrino 6.3.0                 */
// Modified for "Sarmat" project
// (C) I.Drandar, 2010, 2018, all rights reserved
// (C) I.Drandar, for Linux, 2018, all rights reserved

#include <pthread.h>

class Thread {
 const int priority_thrd;
 void *arg;
 pthread_t id_thread;
 pthread_attr_t attr;
 bool is_creating;
 bool is_running;
 static void* ThreadFunc(Thread*);
 virtual void* DoRun(void* arg) = 0;
public:
 typedef void* (*FuncTYPE)(void*);

 Thread(int priority, void* = NULL);
 virtual ~Thread();
 int Cancel();
 void* Join(); // waiting for thread termination
 int Create();
 bool IsRunning() const;
 void CleanUp();
};

#endif /*THREAD_HH_*/
