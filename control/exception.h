#ifndef EXCEPTION_HH_
#define EXCEPTION_HH_

/* The author: Alexander Ketner                        */
/* Date of creation: 03 May 2007                       */
/* Implementation excaption class                      */
/* IDE Momentics & QNX Neutrino 6.3.0                  */
// Modified for Linux
// (C) I.Drandar, 2018-2019, for Qt + C++1x, all rights reserved

#include <sstream>
#include <QString>
#include <iostream>

class TException : public std::exception {
private:
 std::ostringstream _s;
 mutable char* ss;
 template <typename T>
 void MakeRep(T _) {
  _s << _ << std::ends; // end of recursion
 }
 template <typename T, typename... Ts>
 void MakeRep(T _, Ts&&... rest) {
  _s << _;
  MakeRep(std::forward<Ts>(rest)...); // continue of recursion
 }
 template <typename... Ts>
 void MakeRep(const QString& _, Ts&&... rest) { // specialization fo QString
  _s << _.toLocal8Bit().constData();
  MakeRep(std::forward<Ts>(rest)...); // continue of recursion
 }
 template <typename... Ts>
 void MakeRep(const QString& _) { // specialization fo QString
  _s << _.toLocal8Bit().constData(); // end of recursion
 }

public:
 template <typename... T>
 TException(T&&... _) : ss(nullptr) {
  MakeRep(_...);
 }
 TException(const TException&) = default;
 ~TException() override { if (ss != nullptr) free(ss); }
 const char* what() const noexcept override {
  ss = strdup(_s.str().c_str());
  return ss;
 }
};

#endif /*EXCEPTION_HH_*/
