/* The author: Alexander Ketner                       */
/* Date of creation: 03 May 2007                      */
/* Implementation class of control thread             */
/* IDE Momentics & QNX Neutrino 6.3.0                 */
// Modified for "Sarmat" project
// (C) I.Drandar, 2010, 2018, all rights reserved
// (C) I.Drandar, for Linux, 2018, all rights reserved

#include "thread.h"
#include "exception.h"
#include <errno.h>
#include <string.h>
#include <iostream>

constexpr int IS_FAILURE = -1;
// the constructor
// To create the class and a thread with preliminary tunings and
// to suspend {pause} operation of a thread, translation of a stream in state BLOCKED
// void * (*pthreadfunc) (void *) - the pointer on function of a thread
// int name - a symbolical name of a thread
// int priority - priority of a thread
// Remaining tunings by default
Thread::Thread(int priority, void* _arg) :
 priority_thrd(priority), arg(_arg) {
 is_creating = false;
 is_running = false;
 id_thread = 0;
 pthread_attr_init(&attr);
// std::cout << __PRETTY_FUNCTION__ << ": pthread_attr_init error: " << error << std::endl;
 //    Create();
} // end TThread

// Destructor - delete thread
Thread::~Thread() {
 Cancel();
}

// the soft terminated of a thread without a signal
int Thread::Cancel() {
 int error;
 if (is_creating) {
  error = pthread_cancel(id_thread);
  is_creating = false;
 }
 error = pthread_attr_destroy(&attr);
 return (error);
} // end Cancel

void* Thread::Join() { // waiting for thread termination
 void* retval;
 if (id_thread != 0) {
  pthread_join(id_thread, &retval);
  return retval;
 } else
  return nullptr;
}

// Creating of a thread if he has been terminated.
// It is applied to restart of a thread
// All tunings of a thread remain former
int Thread::Create() {
 if (is_creating) {
  return IS_FAILURE;
 }

 int oldstate;
 int error = pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &oldstate);
// std::cout << __PRETTY_FUNCTION__ << ": pthread_setcancelstate error: " << error << std::endl;
 sched_param param;
 param.sched_priority = priority_thrd;
 constexpr int detachstate = PTHREAD_CREATE_DETACHED;
 error = pthread_attr_setdetachstate(&attr, detachstate);
// std::cout << __PRETTY_FUNCTION__ << ": pthread_setdetachstate error: " << error << std::endl;
 constexpr int inheritsched = PTHREAD_EXPLICIT_SCHED;
 error = pthread_attr_setinheritsched(&attr, inheritsched);
// std::cout << __PRETTY_FUNCTION__ << ": pthread_attr_setinheritsched error: " << error << std::endl;
 constexpr int scope = PTHREAD_SCOPE_SYSTEM;
 error = pthread_attr_setscope(&attr, scope);
// std::cout << __PRETTY_FUNCTION__ << ": pthread_attr_setscope error: " << error << std::endl;
 constexpr int GUARD_SIZE = 4096;
 error = pthread_attr_setguardsize(&attr, GUARD_SIZE);
// std::cout << __PRETTY_FUNCTION__ << ": pthread_attr_setguardsize error: " << error << std::endl;
 constexpr int STACK_SIZE = 16384;
 void* stack = malloc(STACK_SIZE);
 error = pthread_attr_setstack(&attr, stack, STACK_SIZE);
// std::cout << __PRETTY_FUNCTION__ << ": pthread_attr_setstack error: " << error << std::endl;
 error = pthread_attr_setschedpolicy(&attr, SCHED_RR);
 if (error != 0) {
  std::cout << __PRETTY_FUNCTION__ << ": pthread_attr_setschedpolicy error: " << error << std::endl;
 }
 error = pthread_attr_setschedparam(&attr, &param);
// std::cout << __PRETTY_FUNCTION__ << ": pthread_attr_setschedparam error: " << error << std::endl;
 if (error != 0) {
  throw
   TException(__PRETTY_FUNCTION__, ": Error of thread attr initialization:\t", strerror(errno));
 }
 error = pthread_create(&id_thread, &attr,
  reinterpret_cast<FuncTYPE>(ThreadFunc), this);
 if (error != 0) {
  throw
   TException(__PRETTY_FUNCTION__, ": Impossible to create thread:\t", strerror(error));
 }
 is_creating = true;
 is_running = true;
 return 0;
} // end Create

// Returns a flag: the thread works or not
bool Thread::IsRunning() const {
 return is_running;
}

// Installation of the flags testifying to a state of a thread
void Thread::CleanUp() {
 int error;
 if (is_creating) {
  error = pthread_attr_destroy(&attr);
  if (error != 0) {
   is_creating = false;
   is_running = false;
  }
 }
}

void* Thread::ThreadFunc(Thread* pThis) {
 void* retval = pThis->DoRun(pThis->arg);
 pThis->CleanUp();
 return retval;
}
//=================== End of thread.cc file ===================================
