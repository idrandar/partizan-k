#ifndef ANGLEMC_HH_
#define ANGLEMC_HH_

/* The author: Vladislav Guyvoronskiy             */
/* Date of creation: 21 May 2007                  */
/* Processing data in angle tracking              */
/* IDE Momentics & QNX Neutrino 6.3.0             */
// Modified for "Sarmat" project
// Converted for "Owl" project to "GTC"
// (C) I.Drandar, 2010-2018, all rights reserved
// (C) I.Drandar, for Qt + C++1x, 2018-2020, all rights reserved

#include "data_types.h"
#include "common/ExchangeModule.h"

// Guidance an tracking controller
struct GTC { // works as namespace
 enum Mode {
  mInitial   = 0x00,
  mETD       = 0x02,
  mMP        = 0x04,
  mAT_DAT    = 0x06,
  mIT        = 0x0D,
  mTesting   = 0x0F,
  //
  mAS_Spiral = 0x08,
  mSS        = 0x0A, // round survey (aka standalone search)
  mLFT       = 0x0C, // low flying target
  mAT_Radar  = 0x0E
 };

 struct ORU_Control {
  Spherical2DGroundCoords ETD;
 };

 struct Handle_Control {
  Cartesian2DCoordsType<int> Deviation;
 };

 struct OutputData {
  // generic control
  enum Mode Mode;
  bool IIT_Enable;
  bool WiperOn;
  bool HeatingOn;
  bool Fire;
  bool IROn;
  unsigned int DATChannel;
  bool DAT1;
  bool HeatStandby;
  // OEUC guidance
  ORU_Control ORU;
  // handle control
  Handle_Control Handle;
 };

 struct General_State {
  Spherical2DCoordsType<bool> Detent;
  bool ModeIsChanging;
  enum Mode Mode;
 };

 struct InputPacket {
  // layout of state packet's fields
  // this structure is compiler-dependent
  // possible use of conditional compilation needed in case of
  // transferring to other platform

  // fields
  // generic state
  struct General_State {
   bool DetentHA : 1;
   bool DetentElev : 1;
   bool ModeIsChanging : 1;
   char Mode : 4;
   operator GTC::General_State() const;
   General_State() = default;
   General_State(const GTC::General_State&);
  } State;
  bool HADriveOn : 1;
  bool ElevDriveOn : 1;
  bool ElevLimitHigh : 1;
  bool ElevLimitLow : 1;
  bool TVReady : 1;
  bool IRReady : 1;
  bool LRFReady : 1;
  Packed::UnsignedAngle20 GroundBearing; // ground coordinate system
  Packed::SignedAngle20 GroundElevation; // ground coordinate system
  Spherical2DShipCoordsType<Packed::SignedAngle20> Ship; // deck coordinate system OEUC position
  using SineType = Bits7<Upscale<int, 262144>, 20>;
  SineType Sine;
  SineType Cosine;
  bool Power : 1;
  bool _7_2 : 1;
  bool : 1;
  unsigned char DATChannel : 1;
  bool InnerInertialTrackingPermittion : 1;
  bool MismatchHA : 1;
  bool MismatchElev : 1;
  bool : 1; // 8-th bit
  bool SyncRequest : 1;
  bool SyncDone : 1;
  bool : 3;
  bool DetentErrorHA : 1;
  bool DetentErrorElev : 1;
  Packed::LTS_Type LTS;
  InputPacket() = default;
 };

 struct InputData {
  General_State State;
  bool Power;
  bool IIT_Permit; // inner inertial tracking
  unsigned int DATChannel;
  bool IRReady;
  bool TVReady;
  bool LRFReady;
  bool _7_2;
  struct ORU {
   Spherical2DCoordsType<bool> DriveOn;
   bool ElevLimitHigh;
   bool ElevLimitLow;
   bool ElevLimit() const { return ElevLimitHigh || ElevLimitLow; }
   Spherical2DGroundCoords Ground; // ground coordinate system
   Spherical2DShipCoords Ship; // deck coordinate system OEUC position
  } ORU;
  double Sine; // sine of image rotation angle
  double Cosine; // cosine of image rotation angle
  Spherical2DCoordsType<bool> Mismatch;
  Spherical2DCoordsType<bool> DetentError;
  bool SyncRequest;
  bool SyncDone;
  double LTS;
  bool TestingCompleted() const { return !State.ModeIsChanging; }
 };

 struct CommandVector {
  Rotations Rot;
 };

 struct StatusVector {
 };

 struct OutputPacket {
  // layout of control packet's fields
  // this structure is compiler-dependent
  // possible use of conditional compilation needed in case of
  // transferring to other platform

  // constants

  // types
  typedef Cartesian2DCoordsType<Bits7<unsigned int, 7> > CenterCorrectionType;
  // fiels according to exchange protocol
  // DAT control generic
  // generic control
  struct GenericControl {
   struct {
    unsigned char DATChannel : 1;
    bool IIT_Enable : 1;
    bool DAT1 : 1;
    unsigned char Mode : 4;
   };
   bool WiperOn : 1;
   bool HeatingOn : 1;
   bool IROn : 1;
   bool Fire : 1;
   bool HeatStandby : 1;
  } Control;
  // DAT + video
  // ORU guidance
  struct ORU_Control {
   Packed::UnsignedAngle20 Bearing; // ETD;
   Packed::SignedAngle20 Elev;      // ETD;
  } ORU;
  Cartesian2DCoordsType<Bits7<int, 12> > HandleDeviation;
 };
};

std::ostream& operator<<(std::ostream&, const GTC::General_State&);
std::ostream& operator<<(std::ostream&, const struct GTC::InputData::ORU&);

class ModuleGTC : public ExchangeModuleImpl<GTC> {
 const struct FakeData {
  const bool use;
  const bool UseShipCoords;
  const Function Heading;
  const Function Elevation;
  FakeData(const ModuleInit&, const QString&);
 } Fake;
 const struct TestDump {
  const bool Ground;
  TestDump(const ModuleInit&, const QString&);
 } TestDump;
 virtual void DecodePacket(bool);
 virtual void EncodePacket(bool);
 void PrepareStatusVector(bool) override;
public:
 explicit ModuleGTC(const ModuleInit&);
 void Act();
};

#endif /*ANGLEMC_HH_*/
