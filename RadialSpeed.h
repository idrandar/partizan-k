#ifndef RADIALSPEED_H
#define RADIALSPEED_H

// radial speed control
// (C) I.Drandar, 2019, all rights reserved
// Guidance and tracking controller - Radar

#include "common/ExchangeModule.h"

struct RadialSpeed {
 using InputPacket = NOTHING;
 using InputData = NOTHING;

 struct OutputData {
  double Target;
  double PassiveInterference;
 };

 struct OutputPacket {
  using SpeedType = Bits7<Upscale<int, 256>, 14>;
  SpeedType Target;
  SpeedType PassiveInterference;
 };

 struct StatusVector {};
 struct CommandVector {};
};

class ModuleRadialSpeed : public ExchangeModuleImpl<RadialSpeed> {
 void DecodePacket(bool) override {}
 void EncodePacket(bool) override;
public:
 ModuleRadialSpeed(const ModuleInit&);
 void Act();
};

#endif // RADIALSPEED_H
