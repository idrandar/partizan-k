/*
 * FRAM.cc
 *
 *  Created on: 16.05.2017
 *      Author: Drandar
 */

// FRAM control class
// (C) I.Drandar, 2017, all rights reserved
// (C) I.Drandar, for C++1x, 2018, all rights reserved

#include "FRAM.h"
#include <iostream>
#include "DebugOut.h"

std::ostream& operator<<(std::ostream& _, const FRAM::Memory& F) {
 _ <<
  " Write: " << F.Write << " Address: " << F.Bus.Address() << " Data: " <<
  static_cast<unsigned int>(F.Bus.Data);
 return _;
}

const unsigned int ModuleFRAM::LocationBase = Base(Heading_ADR);
const size_t ModuleFRAM::CACHE_SIZE = sizeof(Spherical2DShipCoordsType<int>);

ModuleFRAM::ModuleFRAM(const ModuleInit& init) :
 BASE(init), LocationToWrite(), LocationVerified(),
 WriteCache(reinterpret_cast<char const*>(&LocationToWrite)),
 ReadCache(reinterpret_cast<char*>(&LocationVerified)),
 Current(), stage(READ), UpdateStarted() {}

// pack for transfer
void FRAM::LowLevelBus::Assign(const Memory& __) {
 _.Write = __.Write;
 Bus = MakeInt(__.Bus.Data, __.Bus.Address_Low, __.Bus.Address_High);
}

// unpack
void FRAM::Memory::Assign(const LowLevelBus& _) {
 Write = _._.Write;
 const unsigned int bus = _.Bus; // temp variable
 Bus.Data = Byte0(bus);
 Bus.Address_Low = Byte1(bus);
 Bus.Address_High = Byte2(bus);
}

void ModuleFRAM::DecodePacket(bool dump) {
 Input._.Assign(input._);
 if (dump)
  std::cout << "Dump of received " << GetName() << "::Input packet\n:"
   << Input._ << "\n";
}

void ModuleFRAM::EncodePacket(bool dump) {
 output._.Assign(Output._);
 if (dump)
  std::cout << "Dump of transmitted " << GetName() << "::Output packet\n:"
   << Output._ << "\n";
}

void ModuleFRAM::Act() {
 CommitReception();
 Transmit();
}

void ModuleFRAM::ProcessCommandVector() {
 if (UpdateStarted) {
  if (stage == WRITE) // were in writing stage
   stage = READ; // toggle to verification
 } else {
  LocationToWrite = Command.Location * Coeff;
  if (LocationVerified != LocationToWrite) {
   // start to update FRAM
   Current = 0;
   stage = WRITE;
   UpdateStarted = true;
  } else {
   stage = READ;
   UpdateStarted = false;
  }
 }
 Command.data._.Write = (stage == WRITE);
 Command.data._.Bus.SetAddress(LocationBase + Current);
 Command.data._.Bus.Data = WriteCache[Current];
}

void ModuleFRAM::PrepareStatusVector(bool GotData) {
 if (GotData) {
  if (UpdateStarted) {
   if (stage == READ) { // verification
    if (!Status.data._.Write && Status.data._.Bus.Address()) { // were in verification stage
     ReadCache[Current] = Status.data._.Bus.Data;
     if (ReadCache[Current] == WriteCache[Current]) { // go to next byte
      ++Current;
      if (Current < CACHE_SIZE) {
       stage = WRITE; // continue to write
      } else {
       UpdateStarted = false; // stop writing
      }
     }
    }
   }
  }
 }
}
