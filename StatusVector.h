#ifndef _VIRTCLASS_HH_
#define _VIRTCLASS_HH_
/* The author: Victor Poputnikov         		*/
/* Date of creation: 13 September 2007   */
/*  Virtual interface class					   		*/
/* IDE Momentics & QNX Neutrino 6.3.0    */
// Modified for "Sarmat" project
// Status vector class
// (C) I.Drandar, 2010-2014, all rights reserved

#include "Interface.h"
#include <pthread.h>

class StatusVector { // global shared data
public:
 static void GetCommand(Interface::Command&);
 static void SetCommand(const Interface::Command& _);
 static void GetStatus(Interface::Status&);
 static void SetStatus(const Interface::Status& _);
private:
 pthread_mutex_t mutex;
 Interface::Command Command;
 Interface::Status Status;
 static StatusVector vect;
 StatusVector();
 ~StatusVector();
 class Lock { // helper class
 public:
  Lock() {
   pthread_mutex_lock(&vect.mutex);
  }
  ~Lock() {
   pthread_mutex_unlock(&vect.mutex);
  }
 };
};
#endif //_VIRTCLASS_HH_
