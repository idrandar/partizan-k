/*
 * LTS.hh
 *  Created on: 17.03.2011
 */
// LTS module class
// (C) I.Drandar, 2011-2013, 2015, all rights reserved
// (C) I.Drandar, for Qt, 2018-2019, all rights reserved

#ifndef LTS_HH_
#define LTS_HH_

#include "common/ExchangeModule.h"
#include "data_types.h"
#include "common/ctime.h"

struct LTS { // works as namespace
 typedef NOTHING OutputData;
 struct InputData {
  double LTS;
 };

 typedef NOTHING CommandVector;
 struct StatusVector {
  double DiffTime; // aboutTimes.doc
 };

 // packets layouts
 struct InputPacket {
  // constants
  // fields
  Packed::LTS_Type LTS;
  InputPacket() = default;
 };

 struct OutputPacket {
  // constants
  // fields
  unsigned char Gap[2];
  OutputPacket() = default;
 };
};

// LTS exchange module
class ModuleLTS : public ExchangeModuleImpl<LTS> {
 CTime time;
 CTime WhenReceived;
 const struct FakeData {
  const bool use;
  FakeData(const ModuleInit&, const QString&);
 } Fake;
 virtual void DecodePacket(bool); // decode info from LTS
 virtual void EncodePacket(bool) {} // make output packet; empty
 virtual void PrepareStatusVector(bool); // corr_LTS_etc_funcs.cc
 virtual void PrepareTransmission();
public:
 explicit ModuleLTS(const ModuleInit&);
 void Act();
 double AdjustedTime() const; // LTS time corrected by time passed from last time arrival
};

#endif /* LTS_HH_ */
