/*
 * LTS.cc
 *
 *  Created on: 17.03.2011
 *      Author: Drandar
 */
// LTS module class
// (C) I.Drandar, 2011-2015, all rights reserved
// (C) I.Drandar, for Qt, 2018, all rights reserved

#include "LTS.h"
#include <iostream>
#include "DebugOut.h"

ModuleLTS::FakeData::FakeData(const ModuleInit& init, const QString& s) :
 use(init.GetConst(s, "use")) {
}

ModuleLTS::ModuleLTS(const ModuleInit& init) :
 BASE(init), Fake(init, "fake") {}

void ModuleLTS::DecodePacket(bool dump) { // decode info from LTS
 Input.LTS = input.LTS;
 // testing stuff
 if (dump) {
  std::cout << "Dump of received LTS::Input packet\n" << Input.LTS << "\n";
 }
}

void ModuleLTS::Act() { // corr_LTS_etc_funcs.cc
 Transmit(); // corr_LTS_etc_funcs.cc
 time.Start();
 CommitReception(); // corr_LTS_etc_funcs.cc
}

void ModuleLTS::PrepareStatusVector(bool GotData) { // corr_LTS_etc_funcs.cc
 if (Fake.use) {
  GotData = true;
  Status.NewInfo = true;
  Status.ExchangeFault = false;
  Status.data.LTS = counter.TimeFromProgramStart();
 }
 if (GotData) {
  Status.DiffTime = time.Gone() / 2; // corr_LTS_etc_funcs.cc
  WhenReceived.Start();
  SendedPackage = false;
 }
 return;
}

void ModuleLTS::PrepareTransmission() {
 time.Start();
}

double ModuleLTS::AdjustedTime() const {
 return Status.data.LTS + WhenReceived.Gone();
}
