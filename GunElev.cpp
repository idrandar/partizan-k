// Processing data of "GUN" elev channel
// (C) I.Drandar, 2015, 2017, all rights reserved
// (C) I.Drandar, for Qt, 2018-2020, all rights reserved

#include "GunElev.h"
#include "common/CoordinatesConversion.h"
#include "DebugOut.h"
#include <iomanip>

ModuleGunElev::ModuleGunElev(const ModuleInit& _) : ModuleGun(_) {}

void ModuleGunElev::ProcessCommandVector() {
 if (Command.CollisionTaskPerformed == 0) {
  ClearStatus();
 } else {
  if (Control.UseShipCoords) {
   EvaluateUsingShipCoords();
  } else {
   EvaluateUsingGroundCoords();
  }
  Status.EvaluatedTime = Command.EvaluatedTime;
  GroundFinal = ConvertShipToGroundSpherical(ShipFinal, Command.Rot);
  Constr.EvaluateLimitsAndSetBounds(ShipFinal, Prev, Status.Limits);
 }
 if (Control.UseShipCoords) {
  auto conclusive = ShipFinal.Elevation + Constr.Offset.Elevation;
  NormalizeRadians(conclusive);
  Command.data.Position = conclusive;
 } else {
  Command.data.Position = GroundFinal.Elevation;
 }
 ModuleGun::ProcessCommandVector();
}

double ModuleGunElev::GetDesiredAngle(bool AnglesOut, bool Offset) const {
 double desired = Constr.Offset.Elevation;
 if (AnglesOut || Command.data.Mode == Types::gmTesting) {
  if (Status.UseFinal) {
   desired = Status.Final.Elev;
  } else {
   desired = Status.Preliminary.Elev;
  }
 }
 if (Offset) {
  desired -= Constr.Offset.Elevation;
 }
 NormalizeRadiansSigned(desired);
 return desired;
}
