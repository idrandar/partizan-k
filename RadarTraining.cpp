// radar Trainig mode
// (C) I.Drandar, 2019, all rights reserved
// Guidance and tracking controller - Radar

#include "RadarTraining.h"
#include "DebugOut.h"

ModuleRadarTraining::ModuleRadarTraining(const ModuleInit& _) : BASE(_) {}

void ModuleRadarTraining::Act() {
 if (Command.Go) {
  Transmit();
 }
}

void ModuleRadarTraining::EncodePacket(bool dump) {
 output.Distance = Output.Distance;
 output.RadialSpeed = Output.RadialSpeed;
 output.DopplerShift = Output.DopplerShift;
 if (dump) {
  std::cout <<
   "Dump of transmitted " << GetName() << "::Output packet\n"
   "imitated target:\tdistance: " << Output.Distance << " m\t"
   "radial speed: " << Output.RadialSpeed << " m/s\t"
   "doppler shift: " << Output.DopplerShift << " Hz\n";
 }
}

