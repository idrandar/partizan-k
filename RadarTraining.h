#ifndef RADARTRAINING_H
#define RADARTRAINING_H

// radar Trainig mode
// (C) I.Drandar, 2019, all rights reserved
// Guidance and tracking controller - Radar

#include "common/ExchangeModule.h"

struct RadarTraining {

 using InputData = NOTHING;
 using InputPacket = NOTHING;

 struct OutputData {
  double Distance;     // m
  double RadialSpeed;  // m/s
  double DopplerShift; // Hz
 };

 struct OutputPacket {
  Bits7<unsigned int, 16> Distance;                // m
  Bits7<Upscale<unsigned int, 4>, 14> RadialSpeed; // m/s
  Bits7<Scale<unsigned int, 2>, 7> DopplerShift;   // Hz
 };

 struct CommandVector {
  bool Go;
 };
 using StatusVector = NOTHING;

};

class ModuleRadarTraining : public ExchangeModuleImpl<RadarTraining> {
 void DecodePacket(bool) override {}
 void EncodePacket(bool) override;
public:
 ModuleRadarTraining(const ModuleInit&);
 void Act() override;
};

#endif // RADARTRAINING_H
