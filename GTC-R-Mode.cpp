// GTC R mode control
// (C) I.Drandar, 2019, all rights reserved

#include "GTC-R-Mode.h"
#include "DebugOut.h"

ModuleGTCRMode::FakeData::FakeData(const ModuleInit& init, const QString& s) :
 use(init.GetConst(s, "use")) {
}

ModuleGTCRMode::ModuleGTCRMode(const ModuleInit& init) :
 BASE(init), Fake(init, "fake") {}

void ModuleGTCRMode::Act() {
 if (Command.Go) {
  Transmit();
 }
 CommitReception();
}

void ModuleGTCRMode::DecodePacket(bool dump) {
 Input.Mode = static_cast<Types::Mode>(input.Mode);
 Input.Submode = static_cast<Types::Submode>(input.Submode);
 if (dump) {
  std::cout << "Dump of received " << GetName() << "::Input packet\n"
   "mode: " << enum2str(Input.Mode) << "\t"
   "submode: " << enum2str(Input.Submode) << "\n";
 }
}

void ModuleGTCRMode::EncodePacket(bool dump) {
 output.Mode = static_cast<unsigned char>(Output.Mode);
 output.Submode = static_cast<unsigned char>(Output.Submode);
 if (dump)
  std::cout << "Dump of transmitted " << GetName() << "::Output packet\n"
   "mode: " << enum2str(Input.Mode) << "\t"
   "submode: " << enum2str(Input.Submode) << "\n";
}

void ModuleGTCRMode::PrepareStatusVector(bool) {
 if (Fake.use) {
  Status.NewInfo = true;
  Status.ExchangeFault = false;
  Status.data = Command.data;
 }
}
