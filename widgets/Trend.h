/*
 * Trend.hh
 *
 *  Created on: 19.08.2016
 *      Author: Drandar
 */

// Wrapper for QCustomPlot widget
// (C) I.Drandar, 2016, all rights reserved
// (C) I.Drandar for Qt, 2019, all rights reserved

#ifndef TREND_HH_
#define TREND_HH_

#include "Common.h"
#include "redist/qcustomplot.h"

namespace TrendLine {
 class Simple;
}

class Trend : public WidgClass {
 QCustomPlot* p;
 double xsize;
 bool suspended;
public:
 Trend(QCustomPlot*, const QString&, const QString&, const QString&, double = 10.0);
 void Replot();
 QCPGraph* AddLine();
 QCPCurve* AddCurve();
 void Suspend();
 void Resume();
};

#endif /* TREND_HH_ */
