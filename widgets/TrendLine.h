/*
 * TrendLine.hh
 *
 *  Created on: 10.11.2016
 *      Author: Drandar
 */

#ifndef TRENDLINE_HH_
#define TRENDLINE_HH_

// Classes implementing TrendLine class
// (C) I.Drandar, 2016, all rights reserved
// (C) I.Drandar for Qt, 2019, all rights reserved

#include "Trend.h"
#include "common/Coordinates.h"
#include "redist/qcustomplot.h"

namespace TrendLine {

class Simple {
 QCPGraph* me;
public:
 Simple(Trend&, const QString&, const QPen&);
 void Add(double, double);
};

class Degrees : public Simple {
public:
 Degrees(Trend&, const QString&, const QPen&);
 void Add(double, double);
};

class Binary: public Simple {
 int offset;
public:
 Binary(Trend&, const QString&, const QPen&, int = 0);
};

class Spherical2D {
 Degrees HA;
 Degrees Elev;
public:
 Spherical2D(Trend&, const QString& = "", const QPen& = QPen(Qt::lightGray), const QPen& = QPen(Qt::darkGray));
 void Add(double, const Spherical2DCoords&);
};

class Spherical2DBool {
 Binary HA;
 Binary Elev;
public:
 Spherical2DBool(Trend&, const QPen&, const QPen&, int = 0);
 void Add(double, const Spherical2DCoordsType<bool>&);
};

class Spherical2DGround {
 Degrees Bearing;
 Degrees Elevation;
public:
 Spherical2DGround(
  Trend&, const QString& = "", const QPen& = QPen(Qt::green), const QPen& = QPen(Qt::darkCyan));
 void Add(double, const Spherical2DGroundCoords&);
};

class Spherical2DShip {
 Degrees Heading;
 Degrees Elevation;
public:
 Spherical2DShip(
  Trend&, const QString& = "", const QPen& = QPen(Qt::darkYellow), const QPen& = QPen(Qt::yellow));
 void Add(double, const Spherical2DShipCoords&);
};

class Rotations  {
 Degrees Heading; // K
 Degrees Rolling;
 Degrees Pitch;   // Psi
public:
 Rotations(
  Trend&,
  const QPen& = QPen(Qt::blue), const QPen& = QPen(Qt::green), const QPen& = QPen(Qt::yellow));
 void Add(double, const ::Rotations&);
};

class Cartesian2D {
 Simple X;
 Simple Y;
public:
 Cartesian2D(Trend&, const QPen& = QPen(Qt::green), const QPen& = QPen(Qt::darkCyan));
 void Add(double, const Cartesian2DCoords&);
};

class Cartesian3D {
 Simple X;
 Simple Y;
 Simple Z;
public:
 Cartesian3D(
  Trend&,
  const QPen& = QPen(Qt::blue), const QPen& = QPen(Qt::green), const QPen& = QPen(Qt::darkGray));
 void Add(double, const Cartesian3DCoords&);
};

class Curve { // wrapper for QCPCurve
 QCPCurve* me;
public:
 Curve(Trend&, const QString&, const QPen&);
 ~Curve();
 void Add(double, double);
};

}

#endif /* TRENDLINE_HH_ */
