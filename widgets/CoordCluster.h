/*
 * CoordCluster.hh
 *
 *  Created on: 01.09.2015
 *      Author: Drandar
 */

#ifndef COORDCLUSTER_HH_
#define COORDCLUSTER_HH_

#include "Common.h"
#include "common/Coordinates.h"

// (C) I.Drandar, 2015-2016, all rights reserved
// reflection of Coordinate types
// (C) I.Drandar, for Qt + C++1x, 2018-2019, all rights reserved

namespace CoordinateCluster {

struct Spherical2DCoords {
 AngleValue HA;
 AngleValue Elev;
 void Assign(const ::Spherical2DCoords&);
};

struct Spherical2DBool {
 Highlight HA;
 Highlight Elev;
 void Assign(const ::Spherical2DCoordsType<bool>&);
};

struct SphericalCoords {
 FloatValue Distance;
 AngleValue Bearing;
 AngleValue Elevation;
 void Assign(const ::SphericalCoords&);
};

struct Spherical2DGroundCoords {
 AngleValue Bearing;
 AngleValue Elevation;
 void Assign(const ::Spherical2DGroundCoords&);
};

struct Spherical2DShipCoords {
 AngleValue Heading;
 AngleValue Elevation;
 void Assign(const ::Spherical2DShipCoords&);
};

struct Rotations {
 AngleValue Heading; // K
 AngleValue Rolling; // Teta
 AngleValue Pitch;   // Psi
 void Assign(const ::Rotations&);
};

struct Cartesian3DCoords {
 FloatValue X;
 FloatValue Y;
 FloatValue Z;
 void Assign(const ::Cartesian3DCoords&);
};

struct Cartesian2DCoordsInt {
 IntValue X;
 IntValue Y;
 void Assign(const ::Cartesian2DCoordsType<int>&);
};

}

#endif /* COORDCLUSTER_HH_ */
