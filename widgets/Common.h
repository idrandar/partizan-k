#ifndef _WIDG_HH_
#define _WIDG_HH_

/* The author: Victor Poputnikov         */
/* Date of creation: 27 March 2007       */
/* A basic class for widgets             */
/* IDE Momentics & QNX Neutrino 6.3.0    */
// Modified for "Sarmat" project
// (C) I.Drandar, 2010-2016, all rights reserved
// (C) I.Drandar, for Qt + C++1x, 2018-2019, 2020 all rights reserved

#include <vector>
#include <algorithm> // find
#include <iterator>  // distance
#include <QWidget>
#include <QLabel>
#include <QComboBox>

class WidgClass  {
 QWidget* obj;
public:
 explicit WidgClass(QWidget*);
 WidgClass(const WidgClass&);
 virtual ~WidgClass() = default;

	int Move(int x, int y); 		 // moves widget to (x,y) position
//	void SetDimensions(const PhDim_t&); // sets width and height
	void SetDimensions(int x, int y);

//	PhDim_t GetDimensions() const;
 char* GetValue() const;
									 // widget
//	virtual double GetDouble() const;
	int GetGaugeValue();           // gets Pt_ARG_GAUGE_VALUE
                                     // argument from widget
//	void SetValue(long int val); 	 // sets long int value to
									 // Pt_ARG_TEXT_STRING
//	virtual void SetValue(int val); 		 // sets int value to Pt_ARG_TEXT_STRING
// virtual void SetValue(double val);		 // sets double value to Pt_ARG_TEXT_STRING
//    void SetValue(int val, int arg); // sets int value to given argument arg
// PhRect_t GetRect();            //gets rectangle from the widget
 QString GetColor() const;      // gets widget color
 void SetColor(const QString&); // sets widget color
	int GetWidth() const;
	int GetHeight() const;
 int GetLeft();
 int GetTop();

 void Lock();
 void Unlock();
 void Hide();
 void Show();
 bool IsHidden() const;

 const WidgClass GetParent() const; // returns parent of current widget
 WidgClass GetParent(); // returns parent of current widget
 const std::vector<WidgClass> GetChildren() const;
 std::vector<WidgClass> GetChildren();
// const std::vector<WidgClass> GetChildrenByClass(const PtWidgetClassRef_t*) const;
// std::vector<WidgClass> GetChildrenByClass(const PtWidgetClassRef_t*);

};

//class RadioClass:public WidgClass
//{
//	public:
//  RadioClass(QWidget *widget);
//  ~RadioClass();
//   	void SetNumber(short number);
//   	short GetNumber();
//	int SetArg(bool val);
// bool IsSet() const;
// virtual void Lock();
// virtual void Unlock();
// void Select();
// void Select(bool); // set value
//	private:
//	short num;
//};

class TextLabel : public WidgClass {
 QLabel* l; // alias;
public:
 TextLabel(QLabel*);
 void SetText(const QString&);
};

class NumericFloatClass : public WidgClass {
 // value is not cached
public:
 explicit NumericFloatClass(QWidget* widget);
 double Get() const;
 void Assign(double val);
};

class NumericIntegerClass : public WidgClass {
 // value is not cached
public:
 explicit NumericIntegerClass(QWidget* widget);
 int Get() const;
 void Assign(int val);
 void SetMinimum(int);
 void SetMaximum(int);
};

class FloatValue : public TextLabel {
 double value; // cached value;
 const double epsilon;
public:
 FloatValue(QLabel*, double eps=0.01);
 void Assign(double newval);
 double Get() const { return value; }
};

class IntValue : public TextLabel {
 int value; // cached value;
public:
 IntValue(QLabel* w);
 void Assign(int newval);
 int Get() const { return value; }
};

class AngleValue : public TextLabel {
public:
 enum NormalizeType { NORMALIZE, NONNORMALIZE };
private:
 double value; // cached value;
 static constexpr double epsilon = 1.0 / 60.0; // 1 minute
 const NormalizeType norm_type;
 void Display();
 void Normalize();
public:
 AngleValue(QLabel*, NormalizeType = NONNORMALIZE);
 virtual ~AngleValue() = default;
 void Assign(double newval);
 operator double(); // not const because value is being readen from text field
};

class Highlight : public TextLabel {
 bool value; // cached value;
 QString active_color;
public:
 Highlight(QLabel* w);
 void Assign(bool newval);
};

//class InternationalMessageDatabase {
// ApMsgDBase_t* db;
//public:
// explicit InternationalMessageDatabase(const char* name) {
//  db = ApLoadMessageDB(NULL, name);
// }
// ~InternationalMessageDatabase() {
//  ApCloseMessageDB(db);
// }
// const char* String(const char* Id) const {
//  const char* _ = ApGetMessage(db, Id);
//  if (_ != NULL) return _;
//  else return "";
// }
//};

using StringTab = std::vector<const char*>;

template <typename T>
constexpr auto enum_tab = {};

template <typename T>
class SelectValue : public TextLabel {
 T value; // cached value;
 const StringTab NameList;
// enum { ACTIVE_COLOR = Pg_GREEN, BLINK_COLOR = Pg_YELLOW };
public:
 SelectValue(QLabel* w, const StringTab& names) : TextLabel(w), value(), NameList(names) {
  SetText(NameList[static_cast<size_t>(value)]);
 }
 SelectValue(QLabel* w) :
  TextLabel(w), value(), NameList(enum_tab<T>.begin(), enum_tab<T>.end()) {
  SetText(NameList[static_cast<size_t>(value)]);
 }
 void Assign(T newval) {
  auto index = static_cast<size_t>(newval);
  if (value != newval && index < NameList.size()) {
   value = newval;
   SetText(NameList[index]);
  }
 }
};

template <typename T>
class ModeSelectValue : public WidgClass {
 T value; // cached value;
 bool mode; // cached value;
 const StringTab NameList;
 constexpr static char YES_COLOR[] = "lime";
 constexpr static char NO_COLOR[] = "yellow";
public:
 ModeSelectValue(QWidget* w, const StringTab& names) : WidgClass(w), value(), mode(), NameList(names) {
  SetText(NameList[value]);
  SetColor("");
 }
 void Assign(T newval) {
  if (value != newval && static_cast<size_t>(newval) < NameList.size()) {
   value = newval;
   SetText(NameList[value]);
  }
 }
 void SetMode(bool _) {
  if (mode != _) {
   mode = _;
   if (_) SetColor(YES_COLOR);
   else SetColor(NO_COLOR);
  }
 }
};

struct ColorStringPair {
 const char* String;
 QString Color;
};

class ColorStringTab : public std::vector<ColorStringPair> {
 typedef std::vector<ColorStringPair> base;
public:
 ColorStringTab();
 ColorStringTab(const ColorStringPair[]);
// ColorStringTab(const InternationalMessageDatabase*, const ColorStringPair[]);
};

template <typename T>
constexpr auto color_enum_tab = {};

template <typename T>
class ColorSelectValue : public TextLabel {
 T value; // cached value;
 const ColorStringTab ColorStringMap;
public:
 ColorSelectValue(QLabel* w, const ColorStringTab& map) :
  TextLabel(w), value(), ColorStringMap(map) {
  SetText(ColorStringMap[value].String);
  SetColor(ColorStringMap[value].Color);
 }
 ColorSelectValue(QLabel* w) :
  TextLabel(w), value(),
  ColorStringMap(color_enum_tab<T>.begin(), color_enum_tab<T>.end()) {
  SetText(ColorStringMap[value].String);
  SetColor(ColorStringMap[value].Color);
 }
 void Assign(T newval) {
  if (value != newval && static_cast<size_t>(newval) < ColorStringMap.size()) {
   value = newval;
   SetText(ColorStringMap[value].String);
   SetColor(ColorStringMap[value].Color);
  }
 }
};

template <typename T>
class ColorModeSelectValue : public TextLabel {
 T value; // cached value;
 const ColorStringTab ColorStringMap;
 bool mode; // cached value;
 constexpr static char NO_COLOR[] = "yellow";
public:
 ColorModeSelectValue(QLabel* w, const ColorStringTab& map) :
  TextLabel(w), value(), ColorStringMap(map) {
  SetText(ColorStringMap[value].String);
  SetColor(NO_COLOR);
 }
 void Assign(T newval) {
  if (value != newval && static_cast<size_t>(newval) < ColorStringMap.size()) {
   value = newval;
   SetText(ColorStringMap[value].String);
   if (mode) SetColor(ColorStringMap[value].Color);
   else SetColor(NO_COLOR);
  }
 }
 void SetMode(bool _) {
  if (mode != _) {
   mode = _;
   if (mode) SetColor(ColorStringMap[value].Color);
   else SetColor(NO_COLOR);
  }
 }
};

class Progress : public WidgClass {
public:
 explicit Progress(QWidget* w) : WidgClass(w) {}
 void Assign(int newval);
};

class List : public WidgClass {
 QComboBox* cb;
public:
 explicit List(QComboBox*);
 virtual ~List() = default;
 void Clear(); // clear list
 void Insert(QString); // insert string into list
 void SetCurrent(int);
 int GetCurrent() const;
 char const* GetCurrentText() const;
};

//template <typename T>
//class ValueSelectList : public WidgClass {
// std::vector<T> values;
//public:
// ValueSelectList(QWidget* w, const T init[]) : WidgClass(w) {
//  for (size_t i = 0; init[i] != -1; ++i) {
//   values.push_back(init[i]);
//  }
// }
// ValueSelectList(int, const T[]);
// void Assign(T newval) {
//  typename std::vector<T>::iterator found = std::find(values.begin(), values.end(), newval);
//  auto l = qobject_cast<QListWidget*>(this);
//  if (found == values.end()) {
//   l->SelectItems(0);
//  } else {
//   l->SelectItems(std::distance(values.begin(), found) + 1, 0);
//  }
// }
// const T Get() const {
//  auto l = qobject_cast<QListWidget*>(this);
//  return values[l->currentRow()];
// }
//};

//class FormattedText : public WidgClass {
// const InternationalMessageDatabase* imb;
// const std::string format;
//public:
// explicit FormattedText(PtWidget_t*, const InternationalMessageDatabase* = NULL); // ABW-like constructor
// explicit FormattedText(int, const InternationalMessageDatabase* = NULL); // ABN-like constructor
// void WriteFormatText(const std::string&); // writes text using own caption as format string
//};

//end of widg.hh file
#endif //_WIDG_HH_
