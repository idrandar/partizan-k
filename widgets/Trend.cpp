/*
 * Trend.cc
 *
 *  Created on: 19.08.2016
 *      Author: Drandar
 */

// Wrapper for PtTrend widget
// (C) I.Drandar, 2016, all rights reserved
// (C) I.Drandar for Qt, 2019, all rights reserved

#include "Trend.h"
#include "TrendLine.h"

Trend::Trend(
 QCustomPlot* _, const QString& name, const QString& xlabel, const QString& ylabel, double lim) :
 WidgClass(_), p(_), xsize(lim), suspended() {
 p->xAxis2->setVisible(true);
 p->xAxis2->setTickLabels(false);
 p->yAxis2->setVisible(true);
 p->yAxis2->setTickLabels(false);
 p->xAxis2->setLabel(name);
 p->xAxis->setLabel(xlabel);
 p->xAxis->setLabelPadding(0);
 p->xAxis->setTickLabelSide(QCPAxis::lsInside);
 auto f = p->font();
 f.setPointSize(8);
 p->xAxis->setTickLabelFont(f);
 p->yAxis->setLabel(ylabel);
 p->yAxis->setTickLabelSide(QCPAxis::lsInside);
 p->yAxis->setTickLabelFont(f);
 p->yAxis->setLabelPadding(0);
 // make left and bottom axes always transfer their ranges to right and top axes:
 p->connect(p->xAxis, SIGNAL(rangeChanged(QCPRange)), p->xAxis2, SLOT(setRange(QCPRange)));
 p->connect(p->yAxis, SIGNAL(rangeChanged(QCPRange)), p->yAxis2, SLOT(setRange(QCPRange)));
}

void Trend::Replot() {
 if (!suspended) {
  p->rescaleAxes();
  auto r = p->xAxis->range();
  if (xsize > 0) {
   p->xAxis->setRange(r.upper, std::min(xsize, r.upper - r.lower), Qt::AlignRight);
  }
  p->replot();
 }
}

QCPGraph* Trend::AddLine() {
 if (p->graphCount() == 1) {
  p->legend->setVisible(true);
  QFont legendFont = p->font(); // start out with trend's font..
  legendFont.setPointSize(7); // and make a bit smaller for legend
  p->legend->setFont(legendFont);
  p->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignRight | Qt::AlignTop);
 }
 return p->addGraph();
}

QCPCurve* Trend::AddCurve() {
 if (p->plottableCount() == 1) {
  p->legend->setVisible(true);
  QFont legendFont = p->font(); // start out with trend's font..
  legendFont.setPointSize(7); // and make a bit smaller for legend
  p->legend->setFont(legendFont);
  p->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignLeading | Qt::AlignTop);
 }
 return new QCPCurve(p->xAxis, p->yAxis);
}

void Trend::Suspend() {
 suspended = true;
 p->setInteraction(QCP::iRangeDrag, true);
 p->setInteraction(QCP::iRangeZoom, true);
 p->setInteraction(QCP::iSelectPlottables, true);
}

void Trend::Resume() {
 suspended = false;
 p->setInteraction(QCP::iRangeDrag, false);
 p->setInteraction(QCP::iRangeZoom, false);
 p->setInteraction(QCP::iSelectPlottables, false);
}
