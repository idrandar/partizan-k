/*
 * TrendLine.cc
 *
 *  Created on: 10.11.2016
 *      Author: Drandar
 */

// Classes implementing TrendLine class
// (C) I.Drandar, 2016, all rights reserved
// (C) I.Drandar for Qt, 2019, all rights reserved

#include "TrendLine.h"
#include "common/MathConst.h"
#include "common/CoordinatesConversion.h"
#include <iostream>

TrendLine::Simple::Simple(Trend& owner, const QString& name, const QPen& pen) :
 me(owner.AddLine()) {
 me->setPen(pen);
 me->setName(name);
 me->setBrush(Qt::NoBrush);
}

void TrendLine::Simple::Add(double key, double value) {
 me->addData(key, value);
}

TrendLine::Degrees::Degrees(Trend& owner, const QString& name, const QPen& pen) :
 Simple(owner, name, pen) {
}

void TrendLine::Degrees::Add(double key, double value) {
Simple::Add(key, MATH_CONST::Rad2Deg(value));
}

TrendLine::Binary::Binary(Trend& owner, const QString& name, const QPen& pen, int offset) :
 Simple(owner, name, pen), offset(offset) {
}

TrendLine::Spherical2D::Spherical2D(
 Trend& owner, const QString& Prefix, const QPen& HAPen, const QPen& ElevPen) :
 HA(owner, Prefix + "КК", HAPen), Elev(owner, Prefix + "KM", ElevPen) {
}

void TrendLine::Spherical2D::Add(double key, const Spherical2DCoords& _) {
 auto __ = _.HA;
 NormalizeRadiansSigned(__);
 HA.Add(key, __);
 Elev.Add(key, _.Elev);
}

TrendLine::Spherical2DBool::Spherical2DBool(
 Trend& owner, const QPen& HAPen, const QPen& ElevPen, int offset) :
 HA(owner, "КК", HAPen, offset), Elev(owner, "КM", ElevPen, offset + 2) {
}

void TrendLine::Spherical2DBool::Add(double key, const Spherical2DCoordsType<bool>& _) {
 HA.Add(key, _.HA);
 Elev.Add(key, _.Elev);
}

TrendLine::Spherical2DGround::Spherical2DGround(
 Trend& owner, const QString& Prefix, const QPen& BearingPen, const QPen& ElevationPen) :
 Bearing(owner, Prefix + "пеленг", BearingPen), Elevation(owner, Prefix + "КM", ElevationPen) {
}

void TrendLine::Spherical2DGround::Add(double key, const Spherical2DGroundCoords& _) {
 auto __ = _.Bearing;
 NormalizeRadians(__);
 Bearing.Add(key, __);
 Elevation.Add(key, _.Elevation);
}

TrendLine::Spherical2DShip::Spherical2DShip(
 Trend& owner, const QString& Prefix, const QPen& HeadingPen, const QPen& ElevationPen) :
 Heading(owner, Prefix + "KK", HeadingPen), Elevation(owner, Prefix + "КM", ElevationPen) {
}

void TrendLine::Spherical2DShip::Add(double key, const Spherical2DShipCoords& _) {
 double __ = _.Heading;
 NormalizeRadiansSigned(__);
 Heading.Add(key, __);
 Elevation.Add(key, _.Elevation);
}

TrendLine::Rotations::Rotations(
 Trend& owner,
 const QPen& HPen, const QPen& RPen, const QPen& PPen) :
 Heading(owner, "курс корабля", HPen), Rolling(owner, "бортова хитавиця", RPen),
 Pitch(owner, "кільова хитавиця", PPen) {
}

void TrendLine::Rotations::Add(double key, const ::Rotations& _) {
 double __ = _.Heading;
 NormalizeRadiansSigned(__);
 Heading.Add(key, __);
 Rolling.Add(key, _.Rolling);
 Pitch.Add(key, _.Pitch);
}

TrendLine::Cartesian2D::Cartesian2D(Trend& owner, const QPen& XPen, const QPen& YPen) :
 X(owner, "X", XPen), Y(owner, "Y", YPen) {
}

void TrendLine::Cartesian2D::Add(double key, const Cartesian2DCoords& _) {
 X.Add(key, _.X);
 Y.Add(key, _.Y);
}

TrendLine::Cartesian3D::Cartesian3D(
 Trend& owner, const QPen& XPen, const QPen& YPen, const QPen& ZPen) :
 X(owner, "X", XPen), Y(owner, "Y", YPen), Z(owner, "Z", ZPen) {
}

void TrendLine::Cartesian3D::Add(double key, const Cartesian3DCoords& _) {
 X.Add(key, _.X);
 Y.Add(key, _.Y);
 Z.Add(key, _.Z);
}

TrendLine::Curve::Curve(Trend& owner, const QString& name, const QPen& pen) :
 me(owner.AddCurve()) {
 me->setPen(pen);
 me->setName(name);
 me->setBrush(Qt::NoBrush);
}

void TrendLine::Curve::Add(double key, double value) {
 me->addData(key, value);
}

TrendLine::Curve::~Curve() {
 // delete me;
}
