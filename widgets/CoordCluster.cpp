/*
 * CoordCluster.cc
 *
 *  Created on: 02.09.2015
 *      Author: Drandar
 */

// (C) I.Drandar, 2015-2016, all rights reserved
// reflection of Coordinate types
// (C) I.Drandar, for Qt, 2018, all rights reserved

#include "CoordCluster.h"

void CoordinateCluster::SphericalCoords::Assign(const ::SphericalCoords& _) {
 Distance.Assign(_.Distance);
 Bearing.Assign(_.Bearing);
 Elevation.Assign(_.Elevation);
}

void CoordinateCluster::Spherical2DCoords::Assign(const ::Spherical2DCoords& _) {
 HA.Assign(_.HA);
 Elev.Assign(_.Elev);
}

void CoordinateCluster::Spherical2DBool::Assign(const ::Spherical2DCoordsType<bool>& _) {
 HA.Assign(_.HA);
 Elev.Assign(_.Elev);
}

void CoordinateCluster::Spherical2DGroundCoords::Assign(const ::Spherical2DGroundCoords& _) {
 Bearing.Assign(_.Bearing);
 Elevation.Assign(_.Elevation);
}

void CoordinateCluster::Spherical2DShipCoords::Assign(const ::Spherical2DShipCoords& _) {
 Heading.Assign(_.Heading);
 Elevation.Assign(_.Elevation);
}

void CoordinateCluster::Rotations::Assign(const ::Rotations& _) {
 Heading.Assign(_.Heading);
 Rolling.Assign(_.Rolling);
 Pitch.Assign(_.Pitch);
}

void CoordinateCluster::Cartesian3DCoords::Assign(const ::Cartesian3DCoords& _) {
 X.Assign(_.X);
 Y.Assign(_.Y);
 Z.Assign(_.Z);
}

void CoordinateCluster::Cartesian2DCoordsInt::Assign(const ::Cartesian2DCoordsType<int>& _) {
 X.Assign(_.X);
 Y.Assign(_.Y);
}
