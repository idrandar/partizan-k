/* The author: Victor Poputnikov         */
/* Date of creation: 27 March 2007       */
/* A basic class for widgets             */
/* IDE Momentics & QNX Neutrino 6.3.0    */
// Modified for "Sarmat" project
// (C) I.Drandar, 2010-2016, all rights reserved
// (C) I.Drandar, for Qt, 2018, 2020, all rights reserved

#include "Common.h"
//#include <assert.h>
//#include <strstream>
#include "common/MathConst.h"
//#include <iostream>

//==============================================================//
//==============================================================//
WidgClass::WidgClass(QWidget* widget) : obj(widget) {}

WidgClass::WidgClass(const WidgClass& _) : obj(_.obj) {}

//==============================================================//
//==============================================================//
//int WidgClass :: Move(int x, int y)
//{
//	 PhArea_t invis = {{x, y},{0, 0}};                 //setting position
//     PtArg_t args[1];                                  //
//     PtSetArg(&args[0], Pt_ARG_POS, &invis.pos, 0);    //creating argument
//     PtSetResources(obj, 1, args);
//   	 return( Pt_CONTINUE );

//}

//==============================================================//
//==============================================================//
//void WidgClass::SetValue(double val) {
// char str[128];
// if (val == 0.0) {
//  strcpy(str, "0");
// } else if (fabs(val) >= 0.01 && fabs(val) < 1E63) {
//  sprintf(str, "%.2F", val);
// } else {
//  sprintf(str, "%.2E", val);
// }
// PtSetResource(obj, Pt_ARG_TEXT_STRING, &str, 0);
//}

/*
 * sets int value to a widget
 */
//void WidgClass::SetValue(int val) {
// char str[20];
//	PtArg_t args[1];
//	sprintf(str, "%d", val);
//    PtSetArg(&args[0], Pt_ARG_TEXT_STRING, &str, 0);
//    PtSetResources(obj, 1, args);
//}

/*
 * sets long int value to a widget
 */
//void WidgClass::SetValue(long int val) {
// char str[20];
//	PtArg_t args[1];
//	ltoa(val, str, 10);
//    PtSetArg(&args[0], Pt_ARG_TEXT_STRING, &str, 0);
//    PtSetResources(obj, 1, args);
//}

//==============================================================//
//==============================================================//
//void WidgClass::SetValue(int val, int arg) {
//	PtArg_t args[1];
//	PtSetArg(&args[0], arg, val, 0);
//	PtSetResources(obj, 1, args);
//}

//==============================================================//
//==============================================================//
//char* WidgClass::GetValue() const{
// PtArg_t args[1];
// char * str;
// PtSetArg(&args[0], Pt_ARG_TEXT_STRING, &str, 0);
// PtGetResources(obj, 1, args);
// return str;
//}

//==============================================================//
//==============================================================//
//double WidgClass::GetDouble() const {
//	PtArg_t args[1];
//    char * str;
//    PtSetArg(&args[0], Pt_ARG_TEXT_STRING, &str, 0);
//    PtGetResources(obj, 1, args);
//    return atof(str);
//}

//==============================================================//
//==============================================================//
//int WidgClass :: GetGaugeValue()
//{
//    int * gauge_value;
//	PtArg_t args[1];
//	PtSetArg(&args[0], Pt_ARG_GAUGE_VALUE, &gauge_value, 0);
//    PtGetResources(obj, 1, args);
//    return *gauge_value;
//}

//==============================================================//
//==============================================================//
QString WidgClass::GetColor() const {
 QString style = obj->styleSheet();
 QRegExp bkg_color("background-color\\s*:\\s*(.*);?\\s*");
 if (bkg_color.indexIn(style) != -1) {
  QString _ = bkg_color.cap(1);
  return _;
 } else {
  return "";
 }
}

//==============================================================//
//==============================================================//
void WidgClass::SetColor(const QString& color) {
 obj->setStyleSheet("background-color: " + color + ";");
}

void WidgClass::Lock() {
 obj->setEnabled(false);
}

void WidgClass::Unlock() {
 obj->setEnabled(true);
}

//PhRect_t WidgClass::GetRect() {
// PhRect_t r;
// PhDim_t *dim;
// PhPoint_t *pos;
// PtArg_t args[2];
// PtSetArg(&args[0], Pt_ARG_POS, &pos, 0);
// PtSetArg(&args[1], Pt_ARG_DIM, &dim, 0);
// PtGetResources(obj, 2, args);
// r.ul.x = pos->x;
// r.ul.y = pos->y;
// r.lr.x = pos->x + dim->w;
// r.lr.y = pos->y + dim->h;
// return r;
//}

//int WidgClass::GetWidth() const {
// PhDim_t *dim;
// PtGetResource(obj, Pt_ARG_DIM, &dim, 0);
// return dim->w;
//}

//int WidgClass::GetHeight() const {
// PhDim_t *dim;
// PtGetResource(obj, Pt_ARG_DIM, &dim, 0);
// return dim->h;
//}

//int WidgClass :: GetLeft()
//{
//    PhPoint_t *pos;
//    PtArg_t args[1];
//    PtSetArg(&args[0], Pt_ARG_POS, &pos, 0);
//    PtGetResources(obj, 1, args );
//    return pos->x;
//}

//int WidgClass :: GetTop()
//{
//    PhPoint_t *pos;
//    PtArg_t args[1];
//    PtSetArg(&args[0], Pt_ARG_POS, &pos, 0);
//    PtGetResources(obj, 1, args );
//    return pos->y;
//}

//void WidgClass::SetDimensions(const PhDim_t& dim) { // sets width and height
// PtSetResource(obj, Pt_ARG_DIM, &dim, 0);
//}

//void WidgClass::SetDimensions(int x, int y) {
// const PhDim_t _ = { x, y };
// SetDimensions(_);
//}

//PhDim_t WidgClass::GetDimensions() const {
// PhDim_t *dim;
// PtGetResource(obj, Pt_ARG_DIM, &dim, 0);
// return *dim;
//}

void WidgClass::Hide() {
 obj->hide();
}

void WidgClass::Show() {
 obj->show();
}

//bool WidgClass::IsHidden() const {
// return PtWidgetIsRealized(obj) == 0;
//}

//const WidgClass WidgClass::GetParent() const {
// return WidgClass(PtWidgetParent(obj));
//}

//WidgClass WidgClass::GetParent() {
// return WidgClass(PtWidgetParent(obj));
//}

//const std::vector<WidgClass> WidgClass::GetChildren() const {
// PtWidget_t* item = obj;
// std::vector<WidgClass> result;
// while (item = PtWidgetFamily(obj, item)) {
//  result.push_back(WidgClass(item));
// }
// return result;
//}

//std::vector<WidgClass> WidgClass::GetChildren() {
// PtWidget_t* item = obj;
// std::vector<WidgClass> result;
// while (item = PtWidgetFamily(obj, item)) {
//  result.push_back(WidgClass(item));
// }
// return result;
//}

//const std::vector<WidgClass> WidgClass::GetChildrenByClass(const PtWidgetClassRef_t* _) const {
// PtWidget_t* item = obj;
// std::vector<WidgClass> result;
// while (item = PtWidgetFamily(obj, item)) {
//  if (PtWidgetIsClass(item, _)) {
//   result.push_back(WidgClass(item));
//  }
// }
// return result;
//}

//std::vector<WidgClass> WidgClass::GetChildrenByClass(const PtWidgetClassRef_t* _) {
// PtWidget_t* item = obj;
// std::vector<WidgClass> result;
// while (item = PtWidgetFamily(obj, item)) {
//  if (PtWidgetIsClass(item, _)) {
//   result.push_back(WidgClass(item));
//  }
// }
// return result;
//}

//end of widg.cc file
//==============================================================//
//==============================================================//

//RadioClass::RadioClass(PtWidget_t *widget):WidgClass(widget) {}
//RadioClass::RadioClass(int id):WidgClass(AbGetABW(id)) {}

//==============================================================//
//==============================================================//
//RadioClass::~RadioClass()
//{
//}

//==============================================================//
//==============================================================//
//int RadioClass::SetArg(bool val)
//{
//	int flag;
//    if (val) flag = Pt_TRUE;
//    else flag = Pt_FALSE;
//    PtArg_t args[1];
//    PtSetArg(&args[0], Pt_ARG_FLAGS, flag , Pt_SET);
//    PtSetResources(obj, 1, args);
//    return( Pt_CONTINUE );
//}
//==============================================================//
//==============================================================//
//bool RadioClass::IsSet() const {
//	unsigned long *flags;
//    PtGetResource(obj, Pt_ARG_FLAGS,  &flags , 0);
//	return *flags & Pt_SET;
//}

//void RadioClass :: SetNumber(short number)
//{
//	num = number;
//}
//short RadioClass :: GetNumber()
//{
//	return num;
//}
//void RadioClass :: Lock()
//{
//PtArg_t args[1];
//PtSetArg(&args[0], Pt_ARG_FLAGS, Pt_FALSE, Pt_TOGGLE | Pt_SELECTABLE | Pt_GETS_FOCUS | Pt_SET );
//PtSetArg(&args[0], Pt_ARG_FLAGS, Pt_TRUE, Pt_GHOST | Pt_BLOCKED);
//PtSetResources(obj, 1, args);
//}
//void RadioClass :: Unlock()
//{
//PtArg_t args[1];
//PtSetArg(&args[0], Pt_ARG_FLAGS, Pt_TRUE, Pt_TOGGLE | Pt_SELECTABLE | Pt_GETS_FOCUS | Pt_GHOST | Pt_BLOCKED);
//PtSetArg(&args[0], Pt_ARG_FLAGS, Pt_FALSE, Pt_GHOST | Pt_BLOCKED | Pt_SET);
//PtSetResources(obj, 1, args);
//}

//void RadioClass::Select() {
// long* flags;
// PtGetResource(obj, Pt_ARG_FLAGS, &flags, 0);
// *flags |= Pt_SET;
// PtSetResource(obj, Pt_ARG_FLAGS, flags, 0);
// PtGiveFocus(obj, NULL);
//}

//void RadioClass::Select(bool value) {
// long* flags;
// PtGetResource(obj, Pt_ARG_FLAGS, &flags, 0);
// if (value) *flags |= Pt_SET;
// else *flags &= ~Pt_SET;
// PtSetResource(obj, Pt_ARG_FLAGS, flags, 0);
// PtGiveFocus(obj, NULL);
//}

TextLabel::TextLabel(QLabel* _) : WidgClass(_), l(_) {}

void TextLabel::SetText(const QString& _) {
 l->setText(_);
}

//NumericFloatClass::NumericFloatClass(PtWidget_t* w) : WidgClass(w) {
// assert(PtWidgetIsClass(obj, PtNumericFloat));
//}

//NumericFloatClass::NumericFloatClass(int w) : WidgClass(AbGetABW(w)) {
// assert(PtWidgetIsClass(obj, PtNumericFloat));
//}

//double NumericFloatClass::Get() const {
// double* val;
// PtGetResource(obj, Pt_ARG_NUMERIC_VALUE, &val, 0);
// return *val;
//}

//void NumericFloatClass::Assign(double val) {
// PtSetResource(obj, Pt_ARG_NUMERIC_VALUE, &val, 0);
//}

//NumericIntegerClass::NumericIntegerClass(PtWidget_t* w) : WidgClass(w) {
// assert(PtWidgetIsClass(obj, PtNumericInteger));
//}

//NumericIntegerClass::NumericIntegerClass(int w) : WidgClass(AbGetABW(w)) {
// assert(PtWidgetIsClass(obj, PtNumericInteger));
//}

//int NumericIntegerClass::Get() const {
// int* val;
// PtGetResource(obj, Pt_ARG_NUMERIC_VALUE, &val, 0);
// return *val;
//}

//void NumericIntegerClass::Assign(int val) {
// PtSetResource(obj, Pt_ARG_NUMERIC_VALUE, val, 0);
//}

//void NumericIntegerClass::SetMinimum(int _) {
// PtSetResource(obj, Pt_ARG_NUMERIC_MIN, _, 0);
//}

//void NumericIntegerClass::SetMaximum(int _) {
// PtSetResource(obj, Pt_ARG_NUMERIC_MAX, _, 0);
//}

FloatValue::FloatValue(QLabel* w, double eps) : TextLabel(w), value(), epsilon(eps) {
 SetText(QString::number(value, 'g', 2));
}

void FloatValue::Assign(double newval) {
 if (std::abs(value - newval) >= epsilon) {
  value = newval;
  if (std::abs(value) >= 0.01) {
   SetText(QString::number(value, 'f', 2));
  } else {
   SetText(QString::number(value, 'g', 4));
  }
 }
}

IntValue::IntValue(QLabel* w) : TextLabel(w), value() {
 SetText(QString::number(value));
}

void IntValue::Assign(int newval) {
 if (value != newval) {
  value = newval;
  SetText(QString::number(value));
 }
}

Highlight::Highlight(QLabel* w) : TextLabel(w), value() {
 active_color = GetColor();
 SetColor("");
}

void Highlight::Assign(bool newval) {
 if (value != newval) {
  value = newval;
  if (value) {
   SetColor(active_color);
  } else {
   SetColor("");
  }
 }
}

//void Flag::Assign(bool val) {
// SetArg(val);
//}

//void Progress::Assign(int value) {
// PtSetResource(obj, Pt_ARG_GAUGE_VALUE, value, 0);
//}

AngleValue::AngleValue(QLabel* w,  NormalizeType norm) : TextLabel(w), value(), norm_type(norm) {
 Display();
}

void AngleValue::Assign(double newval) {
 newval *= MATH_CONST::RAD2DEG; // convert to degrees
 if (std::abs(value - newval) >= epsilon) {
  value = newval;
  Normalize();
  Display();
 }
}

void AngleValue::Display() {
 QString s;
 int deg = trunc(value);
 unsigned int minute = round(std::abs(fmod(value, 1) * 60));
 if (minute == 60) {
  if (deg >= 0) ++deg; else --deg;
  minute = 0;
 }
 if ((deg == 0) && (value < 0)) s += "-";
 s.append(QString::number(deg)).append("°");
 if (minute < 10) s.append("0");
 s.append(QString::number(minute)).append("'");
 SetText(s);
}

//AngleValue::operator double() {
// std::string s(GetValue());
// bool minus = s[0] == '-';
// double deg = atof(s.c_str());
// int pos = s.find("°");
// s.erase(0, pos);
// pos = s.find_first_of("0123456789");
// s.erase(0, pos);
// double min = atof(s.c_str());
// value = abs(deg) + min / 60.0;
// if (deg < 0 || minus) value = -value;
// Normalize();
// Display(); // refresh display
// return value;
//}

void AngleValue::Normalize() {
 if (norm_type == NORMALIZE) {
  if (value >= 180) {
   value -= 360;
  }
 }
}

List::List(QComboBox* w) : WidgClass(w), cb(w) {
// cb->clear();
}

void List::Clear() {
 cb->clear();
}

//void List::Insert(const char* _) {
// PtListAddItems(obj, &_, 1, 0);
//}

int List::GetCurrent() const {
 return cb->currentIndex();
}

//char const* List::GetCurrentText() const {
// return GetValue();
//}

void List::SetCurrent(int val) {
 cb->setCurrentIndex(val);
}

//void List::Lock() {
// WidgClass::Lock();
// PtSetResource(obj, Pt_ARG_LIST_FLAGS, Pt_TRUE, Pt_LIST_NON_SELECT);
//}

//void List::Unlock() {
// PtSetResource(obj, Pt_ARG_LIST_FLAGS, Pt_FALSE, Pt_LIST_NON_SELECT);
// WidgClass::Unlock();
//}

ColorStringTab::ColorStringTab() : base() {}

ColorStringTab::ColorStringTab(const ColorStringPair init[]) : base() {
 for (const ColorStringPair* ptr = init; ptr->String != NULL; ++ptr) {
  push_back(*ptr);
 }
}

//ColorStringTab::ColorStringTab(const InternationalMessageDatabase* mdb, const ColorStringPair init[]) : base() {
// for (const ColorStringPair* ptr = init; ptr->String != NULL; ++ptr) {
//  ColorStringPair cs = { mdb->String(ptr->String), ptr->Color };
//  push_back(cs);
// }
//}

//FormattedText::FormattedText(PtWidget_t* w, const InternationalMessageDatabase* imb) :
// WidgClass(w), imb(imb), format(GetValue()) {}

//FormattedText::FormattedText(int id, const InternationalMessageDatabase* imb) :
// WidgClass(AbGetABW(id)), imb(imb), format(GetValue()) {}

//void FormattedText::WriteFormatText(const std::string& s) {
// std::string value;
// if (imb == NULL) {
//  value = s;
// } else {
//  value = imb->String(s.c_str());
// }
// char buffer[1024];
// snprintf(buffer, sizeof buffer, format.c_str(), value.c_str());
// SetValue(buffer);
//}
