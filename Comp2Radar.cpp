// radar data transferred via Ethernet
// (C) I.Drandar, 2019-2020, all rights reserved

#include "Comp2Radar.h"
#include "DebugOut.h"

ModuleComp2Radar::FakeData::FakeData(const ModuleInit& init, const QString& s) :
 use(init.GetConst(s, "use")),
 TrainingDistance(init.GetConst(s, "training-distance")),
 TrainingRadialSpeed(init.GetConst(s, "training-radial-speed")),
 TrainingDopplerShift(init.GetConst(s, "training-doppler-shift")) {
}

ModuleComp2Radar::ModuleComp2Radar(const ModuleInit& init) :
 BASE(init), Fake(init, "fake") {
}

void ModuleComp2Radar::Act() {
 Transmit();
 CommitReception();
}

void ModuleComp2Radar::DecodePacket(bool dump) {
 Input.Mode.Mode = GTCRMode::Mode{input.Mode.Mode};
 Input.Mode.Submode = GTCRMode::Submode{input.Mode.Submode};
 Input.War = input.War;
 Input.ETD.EmissionPermitted = input.ETD.EmissionPermitted;
 Input.ETD.Antenna = input.ETD.Antenna; // 0 - equivalent, 1 - antenna
 Input.ETD.TT = RadarETD::TargetType{input.ETD.TT};
 Input.ETD.Distance = input.ETD.Distance;
 Input.ETD.LFM_SlopeNegative = input.ETD.LFM_SlopeNegative;
 Input.ETD.DS = Radar::DistanceScale{input.ETD.DS};
 Input.ETD.SignalCode = input.ETD.SignalCode;
 Input.ETD.OFT = Radar::OperatingFrequencyTuning{input.ETD._.OFT};
 Input.ETD.FrequencyCode = input.ETD._.FrequencyCode;
 Input.ETD.RPA = Radar::RepetitionPeriodAdjustment{input.ETD.__.RPA};
 Input.ETD.RepetitionPeriodCode = input.ETD.__.RepetitionPeriodCode;
 Input.ETD.MTS1 = Radar::MovingTargetSelection{input.ETD.MTS1};
 Input.ETD.MTS2 = Radar::MovingTargetSelection{input.ETD.___.MTS2};
 Input.ETD.FFT = Radar::FastFourierTransform{input.ETD.FFT};
 Input.ETD.ActiveNoiseAnalysis = input.ETD.ActiveNoiseAnalysis;
 Input.ETD.OwnMovementCompensation = input.ETD.OwnMovementCompensation;
 Input.ETD.ActiveInterferenceAngularTracking = input.ETD.___.ActiveInterferenceAngularTracking;
 Input.ETD.SumDeltaChannelsCalibration = input.ETD.___.SumDeltaChannelsCalibration; // 0 - disabled, 1 - enabled
 Input.ETD.RangeCalibration = input.ETD.___.RangeCalibration; // 0 - disabled, 1 - enabled
 Input.ETD.PulseCount = input.ETD.___.PulseCount;
 Input.SS.EmissionPermitted = input.SS.EmissionPermitted;
 Input.SS.Antenna = input.SS.Antenna; // 0 - equivalent, 1 - antenna
 Input.SS.DistanceBegin = input.SS.DistanceBegin;
 Input.SS.DistanceEnd = input.SS.DistanceEnd;
 Input.SS.LFM_SlopeNegative = input.SS.LFM_SlopeNegative;
 Input.SS.DS = Radar::DistanceScale{input.SS.DS};
 Input.SS.SignalCode = input.SS.SignalCode;
 Input.SS.OFT = Radar::OperatingFrequencyTuning{input.SS._.OFT};
 Input.SS.FrequencyCode = input.SS._.FrequencyCode;
 Input.SS.RPA = Radar::RepetitionPeriodAdjustment{input.SS.__.RPA};
 Input.SS.RepetitionPeriodCode = input.SS.__.RepetitionPeriodCode;
 Input.SS.MTS1 = Radar::MovingTargetSelection{input.SS.MTS1};
 Input.SS.MTS2 = Radar::MovingTargetSelection{input.SS.___.MTS2};
 Input.SS.FFT = Radar::FastFourierTransform{input.SS.FFT};
 Input.SS.ActiveNoiseAnalysis = input.SS.ActiveNoiseAnalysis;
 Input.SS.OwnMovementCompensation = input.SS.OwnMovementCompensation;
 Input.SS.RangeCalibration = input.SS.___.RangeCalibration; // 0 - disabled, 1 - enabled
 Input.SS.PulseCount = input.SS.___.PulseCount;
 Input.Control.TAGC = input.Control.TAGC; // temporary auto gain control
 Input.Control.RangeCalibrationInput = input.Control.RangeCalibrationInput;
 Input.Control.PIC = RadarControl::PassiveInterferenceCompensating{input.Control.PIC};
 Input.Control.CFAR = input.Control.CFAR;
 Input.Control.ManualIF = input.Control.ManualIF;
 Input.Control.ManualUHF = input.Control.ManualUHF;
 Input.Control.DetectionThreshold = input.Control.DetectionThreshold; // dB
 Input.Control.ANI_Threshold = input.Control.ANI_Threshold; // dB
 Input.Control.UHF_ControlThreshold = input.Control.UHF_ControlThreshold; // dB
 Input.Control.IF_ControlThreshold = input.Control.IF_ControlThreshold; // dB
 Input.Control.SignalCode = input.Control.SignalCode;
 Input.Control.CalibrationRangeCorrection = input.Control.CalibrationRangeCorrection; // m
 Input.Control.PassiveInterferenceCoherenceCoefficient =
  input.Control.PassiveInterferenceCoherenceCoefficient; // γ
 Input.Control.MinimumPassiveInterferenceThreshold =
  input.Control.MinimumPassiveInterferenceThreshold; // β dB
 Input.Training.Distance = input.Training.Distance;
 Input.Training.RadialSpeed = input.Training.RadialSpeed;
 Input.Training.DopplerShift = input.Training.DopplerShift;
 for (size_t i = 0; i < Input.PF.size(); ++i) {
  Input.PF[i].Permitted = input.PF[i].Permitted;
  Input.PF[i].Code = input.PF[i].Code;
 }
 Input.Time = input.Time;
 if (dump) {
  std::cout << "Dump of received " << GetName() << "::Input packet\n"
   "mode: " << enum2str(Input.Mode.Mode) << "\t"
   "submode: " << enum2str(Input.Mode.Submode) << "\t"
   "war: " << Input.War << "\n"
   "ETD mode data:\n" << Input.ETD << "\n"
   "SS mode data:\n" << Input.SS << "\n"
   "control:\n" << Input.Control << "\n"
   "training:\nimitated target:\tdistance: " << Input.Training.Distance << " m\t"
   "radial speed: " << Input.Training.RadialSpeed << " m/s\t"
   "doppler shift: " << Input.Training.DopplerShift << " Hz\n"
   "packet sendiing time: " << Input.Time << " s\n"
   "planned frequencies array:";
  for (const auto& f : Input.PF) {
   std::cout <<
    "\t(permitted: " << f.Permitted << " code: " << f.Code << ")";
  }
  std::cout << "\n";
 }
}

void ModuleComp2Radar::EncodePacket(bool dump) {
 output.ModeToken = std::byte{0xDA};
 output.Mode.Mode = static_cast<unsigned char>(Output.Mode.Mode);
 output.Mode.Submode = static_cast<unsigned char>(Output.Mode.Submode);
 output.War = Output.War;
 output.ETDToken = std::byte{0xDC};
 output.ETD.EmissionPermitted = Output.ETD.EmissionPermitted;
 output.ETD.EmissionPermittedPhysical = Output.ETD.EmissionPermittedPhysical;
 output.ETD.Antenna = Output.ETD.Antenna; // 0 - equivalent, 1 - antenna
 output.ETD.TT = static_cast<unsigned char>(Output.ETD.TT);
 output.ETD.Distance = Output.ETD.Distance;
 output.ETD.LFM_SlopeNegative = Output.ETD.LFM_SlopeNegative;
 output.ETD.DS = static_cast<unsigned char>(Output.ETD.DS);
 output.ETD.SignalCode = Output.ETD.SignalCode;
 output.ETD._.OFT = static_cast<unsigned char>(Output.ETD.OFT);
 output.ETD._.FrequencyCode = Output.ETD.FrequencyCode;
 output.ETD.__.RPA = static_cast<unsigned char>(Output.ETD.RPA);
 output.ETD.__.RepetitionPeriodCode = Output.ETD.RepetitionPeriodCode;
 output.ETD.MTS1 = static_cast<unsigned char>(Output.ETD.MTS1);
 output.ETD.___.MTS2 = static_cast<unsigned char>(Output.ETD.MTS2);
 output.ETD.FFT = static_cast<unsigned char>(Output.ETD.FFT);
 output.ETD.ActiveNoiseAnalysis = Output.ETD.ActiveNoiseAnalysis;
 output.ETD.OwnMovementCompensation = Output.ETD.OwnMovementCompensation;
 output.ETD.___.ActiveInterferenceAngularTracking = Output.ETD.ActiveInterferenceAngularTracking;
 output.ETD.___.SumDeltaChannelsCalibration = Output.ETD.SumDeltaChannelsCalibration; // 0 - disabled, 1 - enabled
 output.ETD.___.RangeCalibration = Output.ETD.RangeCalibration; // 0 - disabled, 1 - enabled
 output.ETD.___.PulseCount = Output.ETD.PulseCount;
 output.SSToken = std::byte{0xDD};
 output.SS.EmissionPermitted = Output.SS.EmissionPermitted;
 output.SS.EmissionPermittedPhysical = Output.SS.EmissionPermittedPhysical;
 output.SS.Antenna = Output.SS.Antenna; // 0 - equivalent, 1 - antenna
 output.SS.DistanceBegin = Output.SS.DistanceBegin;
 output.SS.DistanceEnd = Output.SS.DistanceEnd;
 output.SS.LFM_SlopeNegative = Output.SS.LFM_SlopeNegative;
 output.SS.DS = static_cast<unsigned char>(Output.SS.DS);
 output.SS.SignalCode = Output.SS.SignalCode;
 output.SS._.OFT = static_cast<unsigned char>(Output.SS.OFT);
 output.SS._.FrequencyCode = Output.SS.FrequencyCode;
 output.SS.__.RPA = static_cast<unsigned char>(Output.SS.RPA);
 output.SS.__.RepetitionPeriodCode = Output.SS.RepetitionPeriodCode;
 output.SS.MTS1 = static_cast<unsigned char>(Output.SS.MTS1);
 output.SS.___.MTS2 = static_cast<unsigned char>(Output.SS.MTS2);
 output.SS.FFT = static_cast<unsigned char>(Output.SS.FFT);
 output.SS.ActiveNoiseAnalysis = Output.SS.ActiveNoiseAnalysis;
 output.SS.OwnMovementCompensation = Output.SS.OwnMovementCompensation;
 output.SS.___.RangeCalibration = Output.SS.RangeCalibration; // 0 - disabled, 1 - enabled
 output.SS.___.PulseCount = Output.SS.PulseCount;
 output.ControlToken = std::byte{0xDE};
 output.Control.TAGC = Output.Control.TAGC; // temporary auto gain control
 output.Control.RangeCalibrationInput = Output.Control.RangeCalibrationInput;
 output.Control.PIC = static_cast<unsigned char>(Output.Control.PIC);
 output.Control.CFAR = Output.Control.CFAR;
 output.Control.ManualIF = Output.Control.ManualIF;
 output.Control.ManualUHF = Output.Control.ManualUHF;
 output.Control.DetectionThreshold = Output.Control.DetectionThreshold; // dB
 output.Control.ANI_Threshold = Output.Control.ANI_Threshold; // dB
 output.Control.UHF_ControlThreshold = Output.Control.UHF_ControlThreshold; // dB
 output.Control.IF_ControlThreshold = Output.Control.IF_ControlThreshold; // dB
 output.Control.SignalCode = Output.Control.SignalCode;
 output.Control.CalibrationRangeCorrection = Output.Control.CalibrationRangeCorrection; // m
 output.Control.PassiveInterferenceCoherenceCoefficient =
  Output.Control.PassiveInterferenceCoherenceCoefficient; // γ
 output.Control.MinimumPassiveInterferenceThreshold =
  Output.Control.MinimumPassiveInterferenceThreshold; // β dB
 output.TestToken = std::byte{0xD5};
 // start of runtime data
 output.TestResult.Faults.TransmitterPulsePower = Output.TestResult.Faults.TransmitterPulsePower;
 output.TestResult.Faults.SUMNoisePower = Output.TestResult.Faults.SUMNoisePower;
 output.TestResult.Faults.DF1NoisePower = Output.TestResult.Faults.DF1NoisePower;
 output.TestResult.Faults.DF2NoisePower = Output.TestResult.Faults.DF2NoisePower;
 output.TestResult.Faults.SUMCalibrationPower = Output.TestResult.Faults.SUMCalibrationPower;
 output.TestResult.Faults.DF1CalibrationPower = Output.TestResult.Faults.DF1CalibrationPower;
 output.TestResult.Faults.DF2CalibrationPower = Output.TestResult.Faults.DF2CalibrationPower;
 output.TestResult.Faults.SUMDirectWaveLevel = Output.TestResult.Faults.SUMDirectWaveLevel;
 output.TestResult.Faults.SUMBackwardWaveLevel = Output.TestResult.Faults.SUMBackwardWaveLevel;
 output.TestResult.Faults.SUM_DF1AmplitudeDifference = Output.TestResult.Faults.SUM_DF1AmplitudeDifference;
 output.TestResult.Faults.SUM_DF2AmplitudeDifference = Output.TestResult.Faults.SUM_DF2AmplitudeDifference;
 output.TestResult.Faults.SynthesizerPLL_LoopCapture = Output.TestResult.Faults.SynthesizerPLL_LoopCapture;
 output.TestResult.Faults.A_E_Switch = Output.TestResult.Faults.A_E_Switch;
 output.TestResult.Faults.TransmitterTemperature = Output.TestResult.Faults.TransmitterTemperature;
 // end of runtime data
 output.TestResult.TransmitterPulsePower = Output.TestResult.TransmitterPulsePower;           // W
 output.TestResult.SUMNoisePower = Output.TestResult.SUMNoisePower;                           // dB
 output.TestResult.DF1NoisePower = Output.TestResult.DF1NoisePower;                           // dB
 output.TestResult.DF2NoisePower = Output.TestResult.DF2NoisePower;                           // dB
 output.TestResult.SUMCalibrationPower = Output.TestResult.SUMCalibrationPower;               // dB
 output.TestResult.DF1CalibrationPower = Output.TestResult.DF1CalibrationPower;               // dB
 output.TestResult.DF2CalibrationPower = Output.TestResult.DF2CalibrationPower;               // dB
 output.TestResult.SUMDirectWaveLevel = Output.TestResult.SUMDirectWaveLevel;                 // mV
 output.TestResult.SUMBackwardWaveLevel = Output.TestResult.SUMBackwardWaveLevel;             // mV
 output.TestResult.SUM_DF1AmplitudeDifference = Output.TestResult.SUM_DF1AmplitudeDifference; // dB
 output.TestResult.SUM_DF2AmplitudeDifference = Output.TestResult.SUM_DF2AmplitudeDifference; // dB
 output.TestResult.SUM_DF1PhaseDifference = Output.TestResult.SUM_DF1PhaseDifference;         // °
 output.TestResult.SUM_DF2PhaseDifference = Output.TestResult.SUM_DF2PhaseDifference;         // °
 output.TestResult.Completed = Output.TestResult.Completed;
 // ETD detection data
 output.ETD_Detection.ETD_Mode = Output.ETD_Detection.ETD_Mode;
 output.ETD_Detection.TargetCount = Output.ETD_Detection.TargetCount;
 output.ETD_Detection.ORU = Output.ETD_Detection.ORU;
 output.ETD_Detection.Time = Output.ETD_Detection.Time;
 for (size_t i = 0; i < Output.ETD_Detection.Targets.size(); ++i) {
  output.ETD_Detection.RTS[i] = Output.ETD_Detection.Targets[i].RTS;
  output.ETD_Detection.Targets[i].Distance = Output.ETD_Detection.Targets[i].Distance;
  output.ETD_Detection.Targets[i].Mismatch = Output.ETD_Detection.Targets[i].Mismatch;
 }
 output.SS_Data.SS_Mode = Output.SS_Data.SS_Mode;
 output.SS_Data.OFT = static_cast<unsigned char>(Output.SS_Data.OFT);
 output.SS_Data.FrequencyCode = Output.SS_Data.FrequencyCode;
 output.SS_Data.RepetitionPeriodCode = Output.SS_Data.RepetitionPeriodCode;
 output.SS_Data.RPA = static_cast<unsigned char>(Output.SS_Data.RPA);
 output.SS_Data.ANP = static_cast<unsigned char>(Output.SS_Data.ANP);
 output.SS_Data.ActiveNoiseInterferenceProcessing = Output.SS_Data.ActiveNoiseInterferenceProcessing;
 output.SS_Data.Time = Output.SS_Data.Time;
 for (size_t i = 0; i < Output.PF.size(); ++i) {
  output.PF[i].Permitted = Output.PF[i].Permitted;
  output.PF[i].Code = Output.PF[i].Code;
 }
 output.Time = Output.Time;
 if (dump) {
  std::cout << "Dump of transmitted " << GetName() << "::Output packet\n"
   "mode: " << enum2str(Output.Mode.Mode) << "\t"
   "submode: " << enum2str(Output.Mode.Submode) << "\t"
   "war: " << Output.War << "\n"
   "ETD mode data:\n" << Output.ETD << "\n"
   "SS mode data:\n" << Output.SS << "\n"
   "control:\n" << Output.Control << "\n"
   "test result:\n" << Output.TestResult << "\n"
   "ETD mode detection data:\nETD mode: " << Output.ETD_Detection.ETD_Mode << "\t"
   "target count: " << Output.ETD_Detection.TargetCount << "\n";
  for (size_t i = 0; i < Output.ETD_Detection.Targets.size(); ++i) {
   std::cout <<
    "target #" << i + 1 << ":\n"
    "range tracking state: " << enum2str(Output.ETD_Detection.Targets[i].RTS) << "\t"
    "distance: " << Output.ETD_Detection.Targets[i].Distance << " m\n"
    "mismatch: (°):\t" << Output.ETD_Detection.Targets[i].Mismatch << "\n";
  }
  std::cout <<
   "ORU coordinates: (°): " << Output.ETD_Detection.ORU << "\n"
   "measuring time: " << Output.ETD_Detection.Time << " s\n"
   "SS mode data:\nSS mode" << Output.SS_Data.SS_Mode << "\t"
   "operating frequency tuning: " << enum2str(Output.SS_Data.OFT) << "\t"
   "frequency code: " << Output.SS_Data.FrequencyCode << "\n"
   "repetition period code: " << Output.SS_Data.RepetitionPeriodCode << "\t"
   "repetition period adjustment: " << enum2str(Output.SS_Data.RPA) << "\n"
   "active noise presence: " << enum2str(Output.SS_Data.ANP) << "\t"
   "active noise interference processing: " << Output.SS_Data.ActiveNoiseInterferenceProcessing << "\n"
   "measuring time: " << Output.SS_Data.Time << " s\n"
   "packet sending time: " << Output.Time << " s\n"
   "planned frequencies:";
  for (const auto& f : Input.PF) {
   std::cout <<
    "\t (permitted: "  << f.Permitted << " code: " << f.Code << ")";
  }
  std::cout << "\n";
 }
}

void ModuleComp2Radar::PrepareStatusVector(bool) {
 if (Fake.use) {
  Status.NewInfo = true;
  Status.ExchangeFault = false;
  auto& _ = Status.data;
  _.Mode.Mode = GTCRMode::Mode::Work;
  _.Mode.Submode = GTCRMode::Submode::ETD;
  _.War = false;
  _.ETD = {};
  _.ETD.Distance = 1200.0;
  _.SS = {};
  _.Control = {};
  auto t = counter.TimeFromProgramStart();
  _.Training.Distance = Fake.TrainingDistance(t);
  _.Training.RadialSpeed = Fake.TrainingRadialSpeed(t);
  _.Training.DopplerShift = Fake.TrainingDopplerShift(t);
  _.Time = t;
 }
}
