/* The author: Lysenko Nikolay Fyodorovich             */
/* Date of creation: 24 September 2007                 */
/* Mathematical model of conditions and absent devices */
/* IDE Momentics & QNX Neutrino 6.3.0                  */
// Modified for "Sarmat" project
// (C) I.Drandar, 2010-2013, 2015, 2018-2020 all rights reserved

#include "Training.h"
#include <math.h>
#include <iostream>
#include "common/CFreeFun.h"
#include "common/CoordinatesConversion.h"
#include "DebugOut.h"
#include "control/exception.h"

ModuleTraining::ModuleTraining(const ModuleInit& init) try :
 BASE(init),
 Variations([init, this](){
  std::vector<Variation> _;
  auto VariationsDescriptions = init.GetAll("variation");
  std::copy(VariationsDescriptions.rbegin(), VariationsDescriptions.rend(), std::back_inserter(_));
  return _;
 }()) {
} catch (TException& e) {
 std::cout << "exception in " << __PRETTY_FUNCTION__ << ":\t" << e.what() << std::endl;
 throw
  TException(GetName(), ":\t", e.what());
}

void ModuleTraining::Act() {
 if (Command.Go) {
  Transmit();
 }
} // end Run

ModuleTraining::Variation::Variation(const ConstCategoryTable& _) try :
 Coord { _.Get("X"), _.Get("Y"), _.Get("Z") },
 Size(_.Get("size")), PT(_.Get("picture")), Duration(_.Get("time")) {
} catch (TException& e) {
 std::cout << "exception in " << __PRETTY_FUNCTION__ << ":\t" << e.what() << std::endl;
 throw TException(__func__, ":\t", e.what());
}

template <>
inline constexpr std::array enum_rep<Training::PictureType> = {
 "rectangle", "helicopter", "plane", "ship", "ellipse"
};

void ModuleTraining::EncodePacket(bool dump) {
 output.On = Output.On;
 output.Inversion = Output.Inversion;
 output.Square = Output.Square;
 output.Direction = Output.DirectionLeft;
 output.Type = static_cast<unsigned char>(Output.Type);
 output.Size = Output.Size;
 output.Distance = Output.Sph.Distance;
 output.Bearing = Output.Sph.Bearing;
 output.Elevation = Output.Sph.Elevation;
 if (dump)
  std::cout <<
   "fake target control:\n"
    "on: " << Output.On << "\ttarget is square: " << Output.Square << "\t"
    "inversion: " << Output.Inversion << "\n"
    "direction: left: " << Output.DirectionLeft << "\tpicture type: " << enum2str(Output.Type) << "\n"
    "size: " << Output.Size << " m\tspherical coordinates: " << Output.Sph << " m\n";
}

void ModuleTraining::DecodePacket(bool) {}

void ModuleTraining::PerformEvaluations(Types::OutputData& _) {
 if (Command.Start) {
  auto time = LocalTime.Gone(); // time gone from seance start
  const auto& v = Variations[var_index]; // current variation
  if (v.Duration >= 0) {
   // <0 means infinite time
   time = std::min(time, v.Duration);
  }
  // evaluate location of target in cartesian
  Cartesian3DCoords coord { v.Coord.X(time), v.Coord.Y(time), v.Coord.Z(time) };
  if (Command.StationaryTarget.On) {
   coord += Origin;
  }
  // convert evaluated target to spherical
  _.Sph = CartesianToSpherical(coord);
  double diff = _.Sph.Bearing - bearing;
  NormalizeRadiansSigned(diff);
  _.DirectionLeft = diff < 0;
  bearing = _.Sph.Bearing;
  _.Size = v.Size(time);
  _.Type = static_cast<Types::PictureType>(v.PT(time));
  //
  std::cout << time << "\t" << coord << std::endl;
 } else {
  // prepare initial data for evaluations
  Origin = SphericalToCartesian(Command.StationaryTarget.Coords);
  LocalTime.Start();
  var_index = std::min(Command.TargetType, Variations.size() - 1);
 }
}

