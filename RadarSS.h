#ifndef RADARSS_H
#define RADARSS_H

// radar SS mode
// (C) I.Drandar, 2019, all rights reserved
// Guidance and tracking controller - Radar

#include "RadarCommon.h"
#include "common/ExchangeModule.h"

struct RadarSS {

 struct InputData {
  bool EmissionPermitted;
  bool EmissionPermittedPhysical;
  bool Antenna; // 0 - equivalent, 1 - antenna
  double DistanceBegin; // of passive noise strobe
  double DistanceEnd;   // of passive noise strobe
  bool LFM_SlopeNegative;
  Radar::DistanceScale DS;
  unsigned int SignalCode;
  Radar::OperatingFrequencyTuning OFT;
  unsigned int FrequencyCode;
  Radar::RepetitionPeriodAdjustment RPA;
  unsigned int RepetitionPeriodCode;
  Radar::MovingTargetSelection MTS1;
  Radar::MovingTargetSelection MTS2;
  Radar::FastFourierTransform FFT;
  bool ActiveNoiseAnalysis;
  bool OwnMovementCompensation;
  bool RangeCalibration; // 0 - disabled, 1 - enabled
  unsigned int PulseCount;
 };

 struct InputPacket {
  bool : 1;
  bool EmissionPermittedPhysical : 1;
  bool EmissionPermitted : 1;
  bool Antenna : 1; // 0 - equivalent, 1 - antenna
  Bits7<Scale<unsigned int, 4>, 14> DistanceBegin;
  unsigned char SignalCode : 3;
  unsigned char DS : 3;
  bool LFM_SlopeNegative : 1;
  union {
   struct {
    unsigned char : 5;
    unsigned char OFT : 2;
   };
   Bits7Shared<unsigned int, 9> FrequencyCode;
  } _;
  union {
   struct {
    unsigned char : 6;
    unsigned char RPA : 1;
   };
   Bits7Shared<unsigned int, 6> RepetitionPeriodCode;
  } __;
  bool OwnMovementCompensation : 1;
  bool ActiveNoiseAnalysis : 1;
  unsigned char FFT : 3;
  unsigned char MTS1 : 2;
  union {
   struct {
    bool : 3;
    bool RangeCalibration : 1; // 0 - disabled, 1 - enabled
    bool : 1;
    unsigned char MTS2 : 2;
   };
   Bits7Shared<unsigned int, 9> PulseCount;
  } ___;
  Bits7<Scale<unsigned int, 4>, 14> DistanceEnd;
 };

 struct OutputData {
  bool EmissionPermitted;
  bool Antenna; // 0 - equivalent, 1 - antenna
  double DistanceBegin; // of passive noise strobe
  double DistanceEnd;   // of passive noise strobe
  bool LFM_SlopeNegative;
  Radar::DistanceScale DS;
  unsigned int SignalCode;
  Radar::OperatingFrequencyTuning OFT;
  unsigned int FrequencyCode;
  Radar::RepetitionPeriodAdjustment RPA;
  unsigned int RepetitionPeriodCode;
  Radar::MovingTargetSelection MTS1;
  Radar::MovingTargetSelection MTS2;
  Radar::FastFourierTransform FFT;
  bool ActiveNoiseAnalysis;
  bool OwnMovementCompensation;
  bool RangeCalibration; // 0 - disabled, 1 - enabled
  unsigned int PulseCount;
 };

 struct OutputPacket {
  bool Gap0 : 2;
  bool EmissionPermitted : 1;
  bool Antenna : 1; // 0 - equivalent, 1 - antenna
  Bits7<Scale<unsigned int, 4>, 14> DistanceBegin;
  unsigned char SignalCode : 3;
  unsigned char DS : 3;
  bool LFM_SlopeNegative : 1;
  union {
   struct {
    unsigned char Gap : 5;
    unsigned char OFT : 2;
   };
   Bits7Shared<unsigned int, 9> FrequencyCode;
  } _;
  union {
   struct {
    unsigned char Gap : 6;
    unsigned char RPA : 1;
   };
   Bits7Shared<unsigned int, 6> RepetitionPeriodCode;
  } __;
  bool OwnMovementCompensation : 1;
  bool ActiveNoiseAnalysis : 1;
  unsigned char FFT : 3;
  unsigned char MTS1 : 2;
  union {
   struct {
    bool Gap0 : 3;
    bool RangeCalibration : 1; // 0 - disabled, 1 - enabled
    bool Gap1 : 1;
    unsigned char MTS2 : 2;
   };
   Bits7Shared<unsigned int, 9> PulseCount;
  } ___;
  Bits7<Scale<unsigned int, 4>, 14> DistanceEnd;
 };

 struct CommandVector {
  bool Go;
 };
 struct StatusVector {};
};

bool operator==(const RadarSS::OutputData&, const RadarSS::InputData&);
std::ostream& operator<<(std::ostream&, const RadarSS::InputData&);
std::ostream& operator<<(std::ostream&, const RadarSS::OutputData&);

class ModuleRadarSS : public ExchangeModuleImpl<RadarSS> {
 const struct FakeData {
  const bool use;
  FakeData(const ModuleInit&, const QString&);
 } Fake;
 void DecodePacket(bool) override;
 void EncodePacket(bool) override;
 void PrepareStatusVector(bool) override;
public:
 ModuleRadarSS(const ModuleInit&);
 void Act() override;
};

#endif // RADARSS_H
