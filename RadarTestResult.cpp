// radar testing results
// (C) I.Drandar, 2019, all rights reserved
// Guidance and tracking controller - Radar

#include "RadarTestResult.h"
#include "DebugOut.h"

std::ostream& operator<<(std::ostream& _, const RadarTestResult::InputData& d) {
 return
  _ <<
  "runtime data:\tfaults:\ntransmitter pulse power: " << d.Faults.TransmitterPulsePower << "\t"
  "SUM channel noise power: " << d.Faults.SUMNoisePower << "\n"
  "DF1 channel noise power: " << d.Faults.DF1NoisePower << "\t"
  "DF2 channel noise power: " << d.Faults.DF2NoisePower << "\n"
  "SUM channel calibration signal power: " << d.Faults.SUMCalibrationPower << "\n"
  "DF1 channel calibration signal power: " << d.Faults.DF1CalibrationPower << "\t"
  "DF2 channel calibration signal power: " << d.Faults.DF2CalibrationPower << "\n"
  "SUM channel direct wave level: " << d.Faults.SUMDirectWaveLevel << "\t"
  "SUM channel backward wave level: " << d.Faults.SUMBackwardWaveLevel << "\n"
  "amplitude difference SUM - DF1: " << d.Faults.SUM_DF1AmplitudeDifference << "\t"
  "amplitude difference SUM - DF2: " << d.Faults.SUM_DF2AmplitudeDifference << "\n"
  "synthesizer PLL loop capture: " << d.Faults.SynthesizerPLL_LoopCapture << "\t"
  "A-E Switch: " << d.Faults.A_E_Switch << "\t"
  "transmitter temperature: " << d.Faults.TransmitterTemperature << "\n"
  "test mode data:\n"
  "transmitter pulse power: " << d.TransmitterPulsePower << " W\t"
  "SUM channel noise power: " << d.SUMNoisePower << " dB\n"
  "DF1 channel noise power: " << d.DF1NoisePower << " dB\t"
  "DF2 channel noise power: " << d.DF2NoisePower << " dB\n"
  "SUM channel calibration signal power: " << d.SUMCalibrationPower << " dB\n"
  "DF1 channel calibration signal power: " << d.DF1CalibrationPower << " dB\t"
  "DF2 channel calibration signal power: " << d.DF2CalibrationPower << " dB\n"
  "SUM channel direct wave level: " << d.SUMDirectWaveLevel << " mV\t"
  "SUM channel backward wave level: " << d.SUMBackwardWaveLevel << " mV\n"
  "amplitude difference SUM - DF1: " << d.SUM_DF1AmplitudeDifference << " dB\t"
  "amplitude difference SUM - DF2: " << d.SUM_DF2AmplitudeDifference << " dB\n"
  "phase difference SUM - DF1: " << d.SUM_DF1PhaseDifference << "°\t"
  "phase difference SUM - DF2: " << d.SUM_DF2PhaseDifference << "°\n"
  "completed: " << d.Completed;
}

ModuleRadarTest::FakeData::FakeData(const ModuleInit& init, const QString& s) :
 use(init.GetConst(s, "use")) {
}

ModuleRadarTest::ModuleRadarTest(const ModuleInit& init) :
 BASE(init), Fake(init, "fake") {
}

void ModuleRadarTest::Act() {
 CommitReception();
}

void ModuleRadarTest::DecodePacket(bool dump) {
 // start of runtime data
 Input.Faults.TransmitterPulsePower = input.Faults.TransmitterPulsePower;
 Input.Faults.SUMNoisePower = input.Faults.SUMNoisePower;
 Input.Faults.DF1NoisePower = input.Faults.DF1NoisePower;
 Input.Faults.DF2NoisePower = input.Faults.DF2NoisePower;
 Input.Faults.SUMCalibrationPower = input.Faults.SUMCalibrationPower;
 Input.Faults.DF1CalibrationPower = input.Faults.DF1CalibrationPower;
 Input.Faults.DF2CalibrationPower = input.Faults.DF2CalibrationPower;
 Input.Faults.SUMDirectWaveLevel = input.Faults.SUMDirectWaveLevel;
 Input.Faults.SUMBackwardWaveLevel = input.Faults.SUMBackwardWaveLevel;
 Input.Faults.SUM_DF1AmplitudeDifference = input.Faults.SUM_DF1AmplitudeDifference;
 Input.Faults.SUM_DF2AmplitudeDifference = input.Faults.SUM_DF2AmplitudeDifference;
 Input.Faults.SynthesizerPLL_LoopCapture = input.Faults.SynthesizerPLL_LoopCapture;
 Input.Faults.A_E_Switch = input.Faults.A_E_Switch;
 Input.Faults.TransmitterTemperature = input.Faults.TransmitterTemperature;
 // end of runtime data
 Input.TransmitterPulsePower = input.TransmitterPulsePower;           // W
 Input.SUMNoisePower = input.SUMNoisePower;                           // dB
 Input.DF1NoisePower = input.DF1NoisePower;                           // dB
 Input.DF2NoisePower = input.DF2NoisePower;                           // dB
 Input.SUMCalibrationPower = input.SUMCalibrationPower;               // dB
 Input.DF1CalibrationPower = input.DF1CalibrationPower;               // dB
 Input.DF2CalibrationPower = input.DF2CalibrationPower;               // dB
 Input.SUMDirectWaveLevel = input.SUMDirectWaveLevel;                 // mV
 Input.SUMBackwardWaveLevel = input.SUMBackwardWaveLevel;             // mV
 Input.SUM_DF1AmplitudeDifference = input.SUM_DF1AmplitudeDifference; // dB
 Input.SUM_DF2AmplitudeDifference = input.SUM_DF2AmplitudeDifference; // dB
 Input.SUM_DF1PhaseDifference = input.SUM_DF1PhaseDifference;         // °
 Input.SUM_DF2PhaseDifference = input.SUM_DF2PhaseDifference;         // °
 Input.Completed = input.Completed;
 if (dump) {
  std::cout <<
   "Dump of received " << GetName() << "::Input packet\n" << Input << "\n";
 }
}

void ModuleRadarTest::PrepareStatusVector(bool) {
 if (Fake.use) {
  Status.NewInfo = true;
  Status.ExchangeFault = false;
  auto& s = Status.data;
  std::uniform_int_distribution b(0, 1);
  auto& f = s.Faults;
  f.TransmitterPulsePower = {};
  f.SUMNoisePower = {};
  f.DF1NoisePower = {};
  f.DF2NoisePower = {};
  f.SUMCalibrationPower = {};
  f.DF1CalibrationPower = {};
  f.DF2CalibrationPower = {};
  f.SUMDirectWaveLevel = {};
  f.SUMBackwardWaveLevel = {};
  f.SUM_DF1AmplitudeDifference = {};
  f.SUM_DF2AmplitudeDifference = {};
  f.SynthesizerPLL_LoopCapture = {};
  f.A_E_Switch = {};
  f.TransmitterTemperature = {};
  // end of runtime data
  s.TransmitterPulsePower = {}; // W
  s.SUMNoisePower = {};
  s.DF1NoisePower = {};
  s.DF2NoisePower = {};
  s.SUMCalibrationPower = {};
  s.DF1CalibrationPower = {};
  s.DF2CalibrationPower = {};
  s.SUMDirectWaveLevel = {};
  s.SUMBackwardWaveLevel = {};
  s.SUM_DF1AmplitudeDifference = {};
  s.SUM_DF2AmplitudeDifference = {};
  s.SUM_DF1PhaseDifference = {};
  s.SUM_DF2PhaseDifference = {};
  s.Completed = true;
 }
}
