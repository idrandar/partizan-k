// Processing data of "GUN-Extended" channel
// (C) I.Drandar, 2015, 2017-2019, all rights reserved

#include "GunX.h"
#include "DebugOut.h"

ModuleGunX::ModuleGunX(const ModuleInit& init) : BASE(init) {} // end

void ModuleGunX::DecodePacket(bool dump) { //decode info from comp #2
 for (size_t i = 0; i < Input.Ship_HA.size(); ++i) {
  Input.Ship_HA[i] = input.Ship_HA[i];
  Input.ADC[i] = input.ADC[i];
 }
 Input.Faults.CS = input.Faults.CS;
 Input.Faults.PV = input.Faults.PV;
 Input.Faults.ADC = input.Faults.ADC;
 if (dump) {
  std::cout << "Dump of received " << Name << "::Input packet:\n";
  for (size_t i = 0; i < Input.Ship_HA.size(); ++i) {
   std::cout <<
    "channel " << i << ":\tHA : " << MATH_CONST::Rad2Deg(Input.Ship_HA[i]) << "° "
    "ADC: " << Input.ADC[i] << "V\n";
  }
  std::cout <<
   "faults:\tCS: " << Input.Faults.CS << "\tPV: " << Input.Faults.PV << "\t"
   "ADC" << Input.Faults.ADC << "\n";
 }
}

void ModuleGunX::EncodePacket(bool dump) { // code info to comp #2
 // testing stuff
 output.Test = Output.Test;
 if (dump) {
  std::cout << "Dump of " << GetName() << "::Output packet to be transmitted:\n"
   "test: " << Output.Test << "\n";
 }
}

void ModuleGunX::Act() {
 CommitReception();
 Transmit();
}
