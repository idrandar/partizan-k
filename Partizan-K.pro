#-------------------------------------------------
#
# Project created by QtCreator 2019-07-18T00:12:28
#
#-------------------------------------------------

QT       += core gui xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = Partizan-K
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    widgets/CoordCluster.cpp \
    widgets/Common.cpp \
    common/Sockets.cpp \
    common/Protocol.cpp \
    common/PropertyBag.cpp \
    common/Polynomial.cpp \
    common/module.cpp \
    common/lock.cpp \
    common/ExchDevice.cpp \
    common/ExchangeModule.cpp \
    common/CoordinatesConversion.cpp \
    common/cmp_ports.cpp \
    common/CFreeFun.cpp \
    control/time.cpp \
    control/thread.cpp \
    collision/TaskSolver.cpp \
    collision/Task.cpp \
    collision/Table.cpp \
    collision/Extrapolation.cpp \
    collision/AK630Table.cpp \
    collision/AK100Table.cpp \
    common/ctime.cpp \
    Application.cpp \
    StatusVector.cpp \
    Interface.cpp \
    widgets.cpp \
    ExtrapolationPolynomials.cpp \
    DebugOut.cpp \
    Setup.cpp \
    Filter.cpp \
    systemClass.cpp \
    cmxStrct.cpp \
    LTS.cpp \
    TV.cpp \
    IR.cpp \
    LRF.cpp \
    Comp2.cpp \
    Gun.cpp \
    NS.cpp \
    GTC.cpp \
    GTC-2.cpp \
    Training.cpp \
    DAT.cpp \
    GunX.cpp \
    FRAM.cpp \
    GTC-R-Mode.cpp \
    PlannedFrequencies.cpp \
    RadialSpeed.cpp \
    RadarETD.cpp \
    RadarSS.cpp \
    RadarControl.cpp \
    RadarTraining.cpp \
    ETD_DetectionData.cpp \
    SS_Data.cpp \
    RadarTestResult.cpp \
    Comp2Radar.cpp \
    GunHA.cpp \
    GunElev.cpp \
    TrendsWindow.cpp \
    widgets/TrendLine.cpp \
    widgets/Trend.cpp \
    redist/qcustomplot.cpp \
    common/PrimitiveFunctions.cpp \
    GunTimer.cpp

HEADERS += \
        mainwindow.h \
    widgets.h \
    widgets/CoordCluster.h \
    widgets/Common.h \
    common/StringKeyMap.h \
    common/Sockets.h \
    common/Record.h \
    common/Protocol.h \
    common/PropertyBag.h \
    common/Polynomial.h \
    common/module.h \
    common/MathConst.h \
    common/lock.h \
    common/ExchDevice.h \
    common/ExchangeModule.h \
    common/data_convert.h \
    common/ctime.h \
    common/CoordinatesConversion.h \
    common/Coordinates.h \
    common/cmp_ports.h \
    common/CFreeFun.h \
    data_types.h \
    Interface.h \
    LRF.h \
    control/time.h \
    control/thread.h \
    control/exception.h \
    NS.h \
    Comp2.h \
    GTC.h \
    collision/TaskSolver.h \
    collision/Task.h \
    collision/Table.h \
    collision/Extrapolation.h \
    collision/AK630Table.h \
    collision/AK100Table.h \
    Filter.h \
    ExtrapolationPolynomials.h \
    GunX.h \
    IR.h \
    TV.h \
    DAT.h \
    GTC-2.h \
    LTS.h \
    Training.h \
    Application.h \
    cmxStrct.h \
    StatusVector.h \
    FRAM.h \
    Setup.h \
    DebugOut.h \
    systemClass.h \
    Gun.h \
    GTC-R-Mode.h \
    PlannedFrequencies.h \
    RadialSpeed.h \
    RadarETD.h \
    RadarSS.h \
    RadarCommon.h \
    RadarControl.h \
    RadarTraining.h \
    ETD_DetectionData.h \
    SS_Data.h \
    RadarTestResult.h \
    Comp2Radar.h \
    GunHA.h \
    GunElev.h \
    TrendsWindow.h \
    widgets/TrendLine.h \
    widgets/Trend.h \
    redist/qcustomplot.h \
    common/PrimitiveFunctions.h \
    GunTimer.h

FORMS += \
        mainwindow.ui \
    trendswindow.ui

CONFIG += c++1z
CONFIG(release) {
 CONFIG += optimize_full
}

unix|win32: LIBS += -lrt

DISTFILES += \
    Partizan-K.xml
