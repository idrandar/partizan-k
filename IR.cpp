// IR exchange
// (C) I.Drandar, 2018-2020, all rights reserved

#include "IR.h"
#include "DebugOut.h"

std::ostream& operator<<(std::ostream& _, const IR::OutputData& I) {
 _ <<
  "menu:\ton: " << I.MenuOn << "\t"
  "hot black: " << I.HotBlack << "\n"
  "contrast\t+: " << I.ContrastPlus << "\t- " << I.ContrastMinus << "\n"
  "brightness\t+: " << I.BrightnessPlus << "\t-: " << I.BrightnessMinus << "\n"
  "optical zoom:\t+: " << I.OpticalZoomPlus << "\t-:" << "\t"
  "digital zoom: " << I.DigitalZoom << "\n"
  "focus far: " << I.FocusFar << "\tfocus near: " << I.FocusNear << "\t"
  "calibration: " << I.Calibration << "\n"
  "brightness: " << I.Brightness << "\t"
  "conrast: " << I.Contrast << "\t"
  "center correction: " << I.CenterCorrection;
 return _;
}

ModuleIR::ModuleIR(const ModuleInit& _) : BASE(_) {}

void ModuleIR::Act() {
 if (Command.Go)
  Transmit();
}

void ModuleIR::EncodePacket(bool dump) {
 // IR control
 output.HotBlack = Output.HotBlack;
 output.DigitalZoom = Output.DigitalZoom;
 output.OpticalZoomPlus = Output.OpticalZoomPlus;
 output.OpticalZoomMinus = Output.OpticalZoomMinus;
 output.MenuOn = Output.MenuOn;
 output.ContrastPlus = Output.ContrastPlus;
 output.ContrastMinus = Output.ContrastMinus;
 output.BrightnessPlus = Output.BrightnessPlus;
 output.BrightnessMinus = Output.BrightnessMinus;
 output.FocusFar = Output.FocusFar;
 output.FocusNear = Output.FocusNear;
 output.Calibration = Output.Calibration;
 output.Brightness = Output.Brightness;
 output.Contrast = Output.Contrast;
 output.CenterCorrection = Output.CenterCorrection;
 if (dump)
  std::cout <<
   "Dump of received " << GetName() << "::Input packet\n"
   "infrared camera control:\n" << Output << "\n";
}

void ModuleIR::DecodePacket(bool) {
}
