/*
 * Setup.cc
 *
 *  Created on: 04.02.2011
 */
// load setup settings from external file
// (C) I.Drandar, 2011-2017, all rights reserved
// (C) I.Drandar, for Qt, 2018-2020, all rights reserved

#include "Setup.h"
#include <stdio.h>
#include <iostream>
#include "control/exception.h"
#include "DebugOut.h"
#include "stdlib.h"
#include "common/MathConst.h"
#include <QFile>
//#include <QXmlSchema>
//#include <QXmlSchemaValidator>

Setup::Setup(const char* name) {

 QFile file(name);
 if (!file.open(QIODevice::ReadOnly))
  throw TException(__PRETTY_FUNCTION__, ":\tfile ", name, " not opened");

 /*QXmlSchema schema;
 QUrl schemaUrl = QUrl::fromLocalFile("setup-schema.xsd");
 QXmlSchemaValidator validator(schema);
 if (schema.load(schemaUrl) != true) {
  throw TException(__PRETTY_FUNCTION__, "schema ", schemaUrl.toString(), " is invalid");
 }

 if (!validator.validate(&file))
  std::cout << name << " is invalid" << std::endl;*/

 QDomDocument doc; // create document object
 QString errMsg;
 int errLine;
 if (!doc.setContent(&file, &errMsg, &errLine)) { // load document
  file.close();
  throw
   TException(__PRETTY_FUNCTION__, ":\tfile ", name, " not parsed, error: ", errMsg, " at line ", errLine);
 }
 std::cout << "Loading setup data from file " << name <<  "...\n" << std::boolalpha;
 QDomElement root = doc.documentElement(); // get root element
 this->name = root.attribute("name");
 std::cout << "Project name: " << Name() << std::endl;
 // load system setting
 std::cout << "Loading system settings...\n";
 QDomElement system = root.firstChildElement("system"); // system section
 // create devices list
 QDomElement devices_root = system.firstChildElement("devices");
 if (!devices_root.isNull()) {
  std::cout << "Loading devices descriptions...\n";
  QDomNodeList _devices = devices_root.elementsByTagName("device");
  for (int i = 0; i < _devices.count(); ++i) { // browse devices list
   QDomElement _device = _devices.item(i).toElement(); // current device;
   QString dev_name = _device.attribute("name"); // ID
   QString dev_class = _device.attribute("class");
   std::cout <<
    "Device #" << i << ":\t" <<
    dev_name << " (" << dev_class << ")" << std::endl;
   PropertyBag prop_bag = LoadProperties(_device, 1);
   prop_bag.Insert("name", dev_name);
   DeviceInit device(dev_name, dev_class, prop_bag);
   devices.insert(device.Name(), device);
  }
 }
 // create list of modules' init records
 std::cout << "Loading modules descriptions...\n";
 QDomElement modules = root.firstChildElement("modules");
 modulesetup = LoadModules(modules);
 QDomElement weapons = root.firstChildElement("weapons");
 weaponsetup = LoadWeapons(weapons);
 QDomElement Const = root.firstChildElement("constants"); // constants section
 std::cout << "General purpose numeric constants:\n";
 constants = LoadConstants(Const);
 std::cout << '\n';
}

const ModuleInit& Setup::GetModuleInit(const QString& name) const {
 const auto found = modulesetup.find(name);
 if (found != modulesetup.end()) {
  return found.value(); // found
 } else {
  // not found
  throw
   TException(__PRETTY_FUNCTION__, ": Passed invalid name for module description: ", name);
 }
}

const DeviceInit& Setup::GetDevice(const QString& devname) const {
 const auto found = devices.find(devname);
 if (found != devices.end()) { // found
  return found.value();
 } else { // not found
  const static DeviceInit EmptyDevice = DeviceInit();
  return EmptyDevice;
 }
}

const WeaponInit& Setup::GetWeapon(const QString& w) const {
 const auto found = weaponsetup.find(w);
 if (found != weaponsetup.end()) { // found
  return found.value();
 } else { // not found
  throw TException(__PRETTY_FUNCTION__, ": Passed invalid name for weapon description: ", w);
 }
}

ModuleInitMap Setup::LoadModules(const QDomElement& parent) {
 ModuleInitMap mod;
 if (parent.hasChildNodes()) {
  QDomNodeList list = parent.elementsByTagName("module"); // search for module descriptions
  for (int i = 0; i < list.count(); ++i) { // browse found items
   QDomElement node = list.item(i).toElement(); // current item
   const QString modname = node.attribute("name"); // get name
   const double frequency = std::max(LoadNumericalAttribute(node, "frequency"), 0.000001);
   std::cout <<
    "Module: " << modname <<  "\tfrequency: " << frequency << " Hz";
   const double CounterShift = LoadNumericalAttribute(node, "counter-shift");
   if (CounterShift != 0.0) std::cout << "\tcounter shift: " << CounterShift << " s";
   const unsigned int ReceptionFaultMax =
    LoadNumericalAttribute(node, "reception-fault-max", 12); // 12 - default
   std::cout << "\treception faults max: " << ReceptionFaultMax;
   const bool debug = node.attribute("debug").toInt();
   if (debug) std::cout << "\tdebug";
   std::cout << std::endl;
   QDomElement threadsnode = node.firstChildElement("threads");
   ThreadInitTable ThreadsInit;
   if (!threadsnode.isNull()) { // "threads" section present
    std::cout << "\tThreads:\n";
    QDomNodeList threads = threadsnode.elementsByTagName("thread"); // find all "thread" records
    for (int j = 0; j < threads.count(); ++j) { // browse "thread" records
     QDomElement node = threads.item(j).toElement();
     const QString threadname = node.attribute("name");
     const int priority = node.attribute("priority").toInt(); // get priority
     const bool debug = node.attribute("debug").toInt();
     const bool disable = node.attribute("disable").toInt();
     std::cout <<
      "\t\tname: " << threadname << "\tpriority: " << priority << "\tdebug: " << debug << "\t"
      "disable: " << disable << std::endl;
     ThreadsInit.insert(threadname, ThreadInit(threadname.toLocal8Bit(), priority, debug, disable));
    }
   }
   // setup device (if any)
   QDomElement device = node.firstChildElement("device");
   QString devname = device.attribute("name");
   std::cout << "\tDevice:\t" << devname << std::endl;
   const DeviceInit dev = GetDevice(devname);
   QDomElement protocol = node.firstChildElement("protocol");
   QString prottype("");
   PropertyBag protbag;
   if (!protocol.isNull()) {
    prottype = protocol.attribute("class");
    std::cout << "\tProtocol: " << prottype << std::endl;
    protbag = LoadProperties(protocol, 1);
   }
   ProtocolInit prot(prottype, protbag);
   ConstTable Constants;
   QDomElement consts = node.firstChildElement("constants");
   if (!consts.isNull()) {
    std::cout << "Constants:\n";
    Constants = LoadConstants(consts);
   }
   mod.insert(modname,  // name will be used as a hash value for search in the table
    ModuleInit(modname.toLocal8Bit(), // create anonymous ModuleInit object with given parameters
     frequency, debug, ReceptionFaultMax, CounterShift,
     Constants, ThreadsInit, dev, prot, LoadModules(node.firstChildElement("modules"))));
   std::cout << std::endl;
  }
 }
 return mod;
}

WeaponInitMap Setup::LoadWeapons(const QDomElement& parent) {
 WeaponInitMap w;
 if (parent.hasChildNodes()) {
  QDomNodeList list = parent.elementsByTagName("weapon"); // search for module descriptions
  for (int i = 0; i < list.count(); ++i) { // browse found items
   QDomElement node = list.item(i).toElement(); // current item
   const QString wname = node.attribute("name"); // get name
   const QString wtype = node.attribute("type"); // get type
   std::cout <<
    "Weapon " << wname <<  " is of type " << wtype << "\n";
   ConstTable Constants;
   QDomElement consts = node.firstChildElement("constants");
   if (!consts.isNull()) {
    std::cout << "Constants:\n";
    Constants = LoadConstants(consts);
   }
   w.insert(wname,  // name will be used as a hash value for search in the table
    WeaponInit(wname, // create anonymous ModuleInit object with given parameters
     wtype, Constants));
   std::cout << std::endl;
  }
 }
 return w;
}

Spherical2DCoords Setup::LoadSpherical2DCoords(const QDomElement& node) {
 return {
  LoadNumericalAttribute(node, "HA"), LoadNumericalAttribute(node, "elev")
 };
}

Function Setup::LoadFunction(const QDomElement& node, const QString& _units) {
 auto type = node.attribute("type");
 auto units = node.attribute("units", _units);
 PrimitiveFunction prim; // get primary function
 QString prestr;
 QString poststr;
 double factor = 1.0;
 if (units == "°") {
  factor = MATH_CONST::DEG2RAD;
 }
 auto addNumber = [](QString& s, double n) {
  if (n > 0.0) {
   s += "+" + QString::number(n);
  } else if (n < 0.0) {
   s += QString::number(n);
  }
 };
 // select by type (predefined set of functions)
 if (type == "sine") {
  Sinusoid s = {
   LoadNumericalAttribute(node, "main") * factor,
   LoadNumericalAttribute(node, "amplitude", 1.0) * factor,
   LoadNumericalAttribute(node, "period", 1.0),
   LoadNumericalAttribute(node, "time-shift")
  };
  if (s.Main > 0.0) {
   prestr += QString::number(s.Main) + "+";
  }
  if (s.Amplitude != 1.0) {
   prestr += QString::number(s.Amplitude) + "∙";
  }
  prestr += "sin(2π";
  if (s.Period != 1.0) {
   prestr += "/" + QString::number(s.Period);
  }
  prestr += "∙";
  if (s.TimeShift != 0.0) {
   prestr += "(";
   poststr = QString("+") + QString::number(s.TimeShift) + "))";
  } else {
   poststr = ")";
  }
  prim = s;
 } else if (type == "line") {
  Linear _ = {
   LoadNumericalAttribute(node, "slope", 1.0) * factor,
   LoadNumericalAttribute(node, "init") * factor
  };
  if (_.Slope != 1.0) {
   prestr = QString::number(_.Slope);
  }
  prestr += "(";
  poststr = ")";
  addNumber(poststr, _.Init);
  prim = _;
 } else if (type == "power") {
  Power _ = {
   LoadNumericalAttribute(node, "exp"),
   LoadNumericalAttribute(node, "factor", 1.0) * factor,
   LoadNumericalAttribute(node, "init") * factor,
   LoadNumericalAttribute(node, "time-shift")
  };
  if (_.Factor != 1.0) {
   prestr = QString::number(_.Factor) + "∙";
  }
  if (_.TimeShift != 0) {
   prestr += "(";
   addNumber(poststr, -_.TimeShift);
   poststr += ")";
  }
  if (_.Exponent != 1.0) {
   poststr += "**" + QString::number(_.Exponent);
  }
  addNumber(poststr, _.Init);
  prim = _;
 } else if (type == "exponent") {
  Exponent _ = {
   LoadNumericalAttribute(node, "base"),
   LoadNumericalAttribute(node, "factor", 1.0) * factor,
   LoadNumericalAttribute(node, "shift") * factor,
   LoadNumericalAttribute(node, "time-shift")
  };
  if (_.Factor != 1.0) {
   prestr = QString::number(_.Factor) + "∙";
  }
  prestr += QString::number(_.Base) + "**";
  if (_.TimeShift != 0) {
   prestr += "(";
   addNumber(poststr, -_.TimeShift);
   poststr += ")";
  }
  addNumber(poststr, _.Shift);
  prim = _;
 } else if (type == "gauss") {
  Gauss _ =  {
   LoadNumericalAttribute(node, "factor", 1.0) * factor,
   LoadNumericalAttribute(node, "time-shift"),
   LoadNumericalAttribute(node, "half-width", 1.0),
   LoadNumericalAttribute(node, "shift") * factor
  };
  if (_.Factor != 1.0) {
   prestr = QString::number(_.Factor) + "∙";
  }
  prestr += "gauss(" + QString::number(_.HalfWidth) + ",";
  addNumber(poststr, -_.TimeShift);
  poststr += ")";
  addNumber(poststr, _.Shift);
  prim = _;
 } else if (type == "logarithm") {
  Logarithm _ = {
   LoadNumericalAttribute(node, "factor", 1.0) * factor,
   LoadNumericalAttribute(node, "shift") * factor,
   LoadNumericalAttribute(node, "time-shift")
  };
  if (_.Factor != 1.0) {
   prestr = QString::number(_.Factor) + "∙";
  }
  prestr += "ln(";
  addNumber(poststr, -_.TimeShift);
  poststr += ")";
  addNumber(poststr, _.Shift);
  prim = _;
 } else if (type == "tanh") {
  TanH _ = {
   LoadNumericalAttribute(node, "factor", 1.0) * factor,
   LoadNumericalAttribute(node, "shrink"),
   LoadNumericalAttribute(node, "shift") * factor,
   LoadNumericalAttribute(node, "time-shift")
  };
  if (_.Factor != 1.0) {
   prestr = QString::number(_.Factor) + "∙";
  }
  prestr += "tanh(";
  if (_.Shrink != 1.0) {
   prestr += "(";
  }
  addNumber(poststr, -_.TimeShift);
  poststr += ")";
  if (_.Shrink != 1.0) {
   poststr += "∙" + QString::number(_.Shrink) + ")";
  }
  addNumber(poststr, _.Shift);
  prim = _;
 } else if (type == "range") {
  Range r {
   LoadNumericalAttribute(node, "low") * factor,
   LoadNumericalAttribute(node, "high") * factor
  };
  prestr = "range[" + QString::number(r.Low) + "," + QString::number(r.High) + "](";
  poststr = ")";
  prim = r;
 } else if (type == "modulo") {
  Modulo m {
   LoadNumericalAttribute(node, "low") * factor,
   LoadNumericalAttribute(node, "high") * factor
  };
  prestr = "modulo[" + QString::number(m.Low) + "," + QString::number(m.High) + "](";
  poststr = ")";
  prim = m;
 } else if (type == "noise") {
  const Noise _ {
   LoadNumericalAttribute(node, "mean") * factor,
   LoadNumericalAttribute(node, "dev") * factor
  };
  std::cout << "noise[" << _.Mean << "," << _.Dev << "]";
  prim = _;
  return prim; // break recursion
 } else if (type == "integral") {
  Integral _ {
   LoadNumericalAttribute(node, "init") * factor, CTime()
  };
  _._t.Start();
  prestr = "∫";
  poststr = "dt";
  if (_.Value > 0.0) {
   poststr += "+" + QString::number(_.Value);
  } else if (_.Value < 0.0) {
   poststr += QString::number(_.Value);
  }
  prim = _;
 } else {
  // if type is not defined then use a function that does nothing but repeating it's argument
  prim = [](double _){ return _; };
 }
 // search for secondary functions
 auto compose = ComposeType::sum;
 if (node.attribute("compose") == "*" || node.attribute("compose") == "prod") {
  compose = ComposeType::prod;
 }
 std::cout << prestr;
 std::vector<Function> second;
 size_t inners = 0;
 QString tag = "function";
 for (auto item = node.firstChildElement(tag); !item.isNull();
  item = item.nextSiblingElement(tag)) {
  ++inners;
  std::cout << "(";
  Function f = LoadFunction(item, units);
  std::cout << ")";
  second.push_back(f);
  if (!item.nextSiblingElement(tag).isNull()) {
   std::cout << enum2str(compose);
  }
 }
 tag = "constant";
 for (auto item = node.firstChildElement(tag); !item.isNull();
  item = item.nextSiblingElement(tag)) {
  ++inners;
  double v = LoadNumericalAttribute(item, "value");
  if (v >= 0.0) {
   std::cout << "+" << v;
  } else {
   std::cout << v;
  }
  Function f(v);
  second.push_back(f);
 }
 if (inners == 0) {
  std::cout << "t";
 }
 std::cout << poststr;
 Function f = Function(prim, compose, second);
 return f;
}

Cartesian2DCoords Setup::LoadCartesian2DCoords(const QDomElement& parent, const QString& Name) {
 QDomElement node = parent.firstChildElement(Name);
 return {
  LoadNumericalAttribute(node, "X"), LoadNumericalAttribute(node, "Y")
 };
}

Spherical2DShipCoords Setup::LoadSpherical2DShipCoords(const QDomElement& node) {
 return {
  LoadNumericalAttribute(node, "heading"), LoadNumericalAttribute(node, "elevation")
 };
}

Cartesian3DCoords Setup::LoadCartesian3DCoords(const QDomElement& node) {
 return {
  LoadNumericalAttribute(node, "X"), LoadNumericalAttribute(node, "Y"),
  LoadNumericalAttribute(node, "Z")
 };
}

Setup::Diagram Setup::LoadDiagram(const QDomElement& node) {
 Diagram d;
 auto sector_list = node.elementsByTagName("sector");
 for (int i = 0; i < sector_list.count(); ++i) {
  auto item = sector_list.item(i);
  auto _HA = item.firstChildElement("HA");
  auto _Elev = item.firstChildElement("elev");
  Sector s = {
   {LoadNumericalAttribute(_HA, "min"), LoadNumericalAttribute(_HA, "max")},     // HA;
   {LoadNumericalAttribute(_Elev, "min"), LoadNumericalAttribute(_Elev, "max")}, // Elev
  };
  d.push_back(s);
 }
 return d;
}

ConstTable Setup::LoadConstants(const QDomElement& parent) {
 QDomNodeList l0 = parent.elementsByTagName("category"); // search for constants category
 ConstTable table;
 for (int i = 0; i < l0.count(); ++i) {
  QDomElement category = l0.item(i).toElement();
  const QString catname = category.attribute("name");
  const QString description = category.attribute("description");
  const QString _generic_units = category.attribute("units");
  std::cout <<
   "\tCategory: " << catname;
  if (!description.isEmpty()) {
   std::cout << " (" << description << ")";
  }
  std::cout << "\n";
  ConstCategoryTable Category;
  QDomNodeList l1 = category.elementsByTagName("constant"); // search for items
  for (int j = 0; j < l1.count(); ++j) {
   QDomElement constant = l1.item(j).toElement();
   const QString constname = constant.attribute("name");
   const QString units = constant.attribute("units", _generic_units);
   const QString type = constant.attribute("type", "double");
   if (type == "cartesian 3D coords") {
    const Cartesian3DCoords value = LoadCartesian3DCoords(constant);
    std::cout << "\t\t<" << type;
    std::cout <<
     "> " << constname << "\t=\tX: " << value.X << " " << units <<
     "\tY: " << value.Y << " " << units << "\tZ: " << value.Z << " " << units;
    Category.Put(constname, value); // insert value
   } else if (type == "spherical 2D ship coords") {
    const Spherical2DShipCoords value = LoadSpherical2DShipCoords(constant);
    std::cout << "\t\t<" << type;
    std::cout <<
     "> " << constname <<
     "\t=\tHeading: " << value.Heading << "° Elevation: " << value.Elevation << "°";
    Category.Put(constname, value * MATH_CONST::DEG2RAD); // insert value
   } else if (type == "spherical 2D coords") {
    const Spherical2DCoords value = LoadSpherical2DCoords(constant);
    std::cout << "\t\t<" << type;
    std::cout << "> " << constname <<
     "\t=\tHA: " << value.HA << "° Elev: " << value.Elev << "°";
    Category.Put(constname, value * MATH_CONST::DEG2RAD); // insert value
   } else if (type == "diagram") {
    auto value = LoadDiagram(constant);
    std::cout << "\t\t<" << type << "> " << constname;
    const auto disabled = LoadNumericalAttribute(constant, "disabled");
    Category.Put(constname + "-disabled", disabled);
    if (disabled != 0) {
     std::cout << "\t - disabled!";
    }
    std::cout << "\n";
    auto s_no = 0; // sector number
    for (auto& s : value) {
     std::cout << "\t\t\tsector #" << s_no++ << ":\t"
      "HA: min: " << s.HA.min << units << " max: " << s.HA.max << units << "\t"
      "Elev: min: " << s.Elev.min << units << " max: " << s.Elev.max << units << "\n";
     if (units == "°") {
      s.HA.min *= MATH_CONST::DEG2RAD;
      s.HA.max *= MATH_CONST::DEG2RAD;
      s.Elev.min *= MATH_CONST::DEG2RAD;
      s.Elev.max *= MATH_CONST::DEG2RAD;
     }
    }
    Category.Put(constname, value);
   } else { // default: type == "double"
    const double value = LoadNumericalAttribute(constant, "value");
    std::cout <<
     "\t\t<" << type << "> " << constname << "\t= " << value << " " << units;
    if (units == "°") {
     Category.Put(constname, value * MATH_CONST::DEG2RAD); // insert value
    } else {
     Category.Put(constname, value); // insert value
    }
   }
   std::cout << std::endl;
  }
  l1 = category.elementsByTagName("function");
  // search for functions
  for (auto item = category.firstChildElement("function"); !item.isNull();
   item = item.nextSiblingElement("function")) {
   const QString name = item.attribute("name");
   std::cout << "\t\t<function> " << name << " = ";
   Function f = LoadFunction(item, "");
   Category.Put(name, f);
   std::cout << std::endl;
  }
  table.insertMulti(catname, Category); // insert whole category
 }
 return table;
}

PropertyBag Setup::LoadProperties(const QDomElement& node, int level) {
 const std::string filler("\t", level);
 QDomElement properties = node.firstChildElement("properties");
 PropertyBag bag;
 if (properties.hasChildNodes()) {
  std::cout << filler << "properties:\n";
  QDomNodeList l0 = properties.elementsByTagName("property");
  for (int j = 0; j < l0.count(); ++j) {
   QDomElement property = l0.item(j).toElement(); // current property
   const QString prop_name = property.attribute("name");
   const QString prop_value = property.attribute("value");
   bag.Insert(prop_name, prop_value);
   std::cout <<
    '\t' << filler << prop_name << "\t= " << prop_value << std::endl;
  }
 }
 return bag;
}

double Setup::LoadAndDumpNumericalAttribute(
 const QDomElement& parent, const QString& Name, const QString& Units) {
 if (parent.isNull() || Name.isEmpty())
  return 0.0;
 const QString _ = parent.attribute(Name);
 bool ok;
 double retval = _.toDouble(&ok);
 if (ok) {
  std::cout <<
   " " << Name << "=" << retval << Units;
  return retval;
 } else return 0.0;
}

double Setup::LoadNumericalAttribute(const QDomElement& parent, const QString& Name, double Default) {
 if (parent.isNull() || Name.isEmpty()) return Default;
 const QString value = parent.attribute(Name);
 bool ok;
 double retval = value.toDouble(&ok);
 if (ok) {
  return retval;
 } else {
  return Default;
 }
}

const Compound Setup::GetConst(const QString& Category, const QString& Name) const {
 try {
  return constants.Get(Category, Name);
 } catch (TException& e) {
  throw TException(__PRETTY_FUNCTION__, e.what());
 }
}

std::unique_ptr<Setup> setup;
