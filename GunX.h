/*
 * CombatModuleMechanisms.hh
 *
 *  Created on: 21.08.2015
 *      Author: Drandar
 */

#ifndef COMBATMODULEMECHANISMS_HH_
#define COMBATMODULEMECHANISMS_HH_

// Processing data of "GUN-Extended" channel
// (C) I.Drandar, 2015, 2017-2019, all rights reserved

#include "data_types.h"
#include "common/ExchangeModule.h"

struct _GunX { // types

 static constexpr size_t ChannelCount = 2;
 template <typename T>
 using Channels = std::array<T, ChannelCount>;

 struct InputPacket {
  Channels<Packed::SignedAngle11> Ship_HA;
  Channels<Bits7<Upscale<int, 512, 5>, 11>> ADC;
  std::byte Gap;
  struct Faults {
   bool CS : 1;
   bool PV : 1;
   bool ADC : 1;
  } Faults;
 };

 struct InputData {
  Channels<double> Ship_HA;
  Channels<double> ADC;
  struct Faults {
   bool CS;
   bool PV;
   bool ADC;
  } Faults;
 };

 struct OutputPacket {
  bool Test : 1;
  std::byte Gap;
 };

 struct OutputData {
  bool Test;
 };
 struct StatusVector {};
 struct CommandVector {};
};

class ModuleGunX : public ExchangeModuleImpl<_GunX> {
 void DecodePacket(bool) override;
 void EncodePacket(bool) override;
public:
 explicit ModuleGunX(const ModuleInit&);
 void Act();
};

#endif /* COMBATMODULEMECHANISMS_HH_ */
