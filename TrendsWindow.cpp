#include "TrendsWindow.h"
#include <iostream>
#include "Interface.h"
#include "StatusVector.h"
#include "widgets.h"

TrendsWindow::TrendsWindow(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::TrendsWindow), tmr(this),
  itf(nullptr) {
 ui->setupUi(this);
 itf = std::make_unique<TW>(*ui);
 connect(&tmr, SIGNAL(timeout()), SLOT(Update()));
 tmr.setTimerType(Qt::PreciseTimer);
 tmr.start(20);
}

TrendsWindow::~TrendsWindow() {
 delete ui;
}

void TrendsWindow::Update() {
 Interface::Command cmd;
 StatusVector::GetCommand(cmd);

 itf->Assign(cmd.Main);
}

void TrendsWindow::on_btnSuspendResume_toggled(bool chk) {
 auto btn = ui->btnSuspendResume;
 if (chk) {
  itf->Suspend();
  btn->setText("Відновити");
 } else {
  itf->Resume();
  btn->setText("Призупинити");
 }
}
