/*
 * ExtrapolationPolynomials.cc
 *
 *  Created on: 22.09.2015
 *      Author: Drandar
 */

// All types of extrapolation polynomials
// Three files merged
// (C) I.Drandar, 2015-2016, all rights reserved
// (C) I.Drandar, for C++1x, 2018-2019, all rights reserved

//#include "MathConst.hh"
#include "ExtrapolationPolynomials.h"
#include "common/Polynomial.h"
#include "common/CoordinatesConversion.h"
#include "DebugOut.h"

ExtrapolationPolynomials::NS::NS() :
 Time(-1), OK() {
}

Cartesian3DCoords ExtrapolationPolynomials::NS::GetVCart(double) const {
 if (!OK)
  return Cartesian3DCoords();
 return CV.SpeedPart;
}

Cartesian3DCoords ExtrapolationPolynomials::NS::GetShipCart(double dt) const {
 if (!OK)
  return Cartesian3DCoords();
 if (dt > CV.dt_Crit)
  dt = CV.dt_Crit;
 return CV.ShipCart0 + dt * CV.ShipCart1;
}

Rotations ExtrapolationPolynomials::NS::GetRotations(double dt) const {
 if (!OK)
  return Rotations();
 if (dt > CV.dt_Crit)
  dt = CV.dt_Crit;
 Rotations _ = CV.Rotations0 + dt * CV.Rotations1;
 NormalizeRadians(_.Heading);
 return _;
}

const ExtrapolationPolynomials::NS::CurrentValues& ExtrapolationPolynomials::NS::GetCV() const {
 return CV;
}

void ExtrapolationPolynomials::NS::Prepare(const NS_Data& Navig, SystemMode ModeC, bool &HaveNavigationDataInCurrentMode) {
 if (!Navig.NewInfo)
  return;

 OK = true;
 const double tp = Time;
 Time = Navig.Time;

 CV.SpeedPart = Navig.SpeedPart;
 CV.ShipCart1 = CV.SpeedPart;

 enum NS_State {
  nsNO_NAVIG_DATA,
  nsHAVE_NAVIG_DATA,
  nsUNDEFINED
 } State = nsUNDEFINED;

 if (ModeC == SYSMODE_Initial || ModeC == SYSMODE_Testing ||
     ModeC == SYSMODE_Shutdown || ModeC == SYSMODE_SS) {
  State = nsNO_NAVIG_DATA;
 }

 if (ModeC == SYSMODE_MP) {
  if (!HaveNavigationDataInCurrentMode)
   State = nsNO_NAVIG_DATA;
  else
   State = nsHAVE_NAVIG_DATA;
 }
 if (ModeC == SYSMODE_AT) {
  if (!HaveNavigationDataInCurrentMode)
   State = nsNO_NAVIG_DATA;
  else
   State = nsHAVE_NAVIG_DATA;
 }

 if (ModeC == SYSMODE_IT)
  State = nsHAVE_NAVIG_DATA;

 Cartesian3DCoords ShipCart;
 if (State == nsNO_NAVIG_DATA) {
  ShipCart = {};
 }

 if (State == nsHAVE_NAVIG_DATA) {
  const double dt = Time - tp;
  ShipCart = CV.ShipCart0;
  ShipCart += dt * CV.SpeedPart;
 }

 if (State == nsUNDEFINED) {
  ShipCart = {};
 }

 HaveNavigationDataInCurrentMode = true;
 CV.ShipCart0 = ShipCart;

 CV.Rotations0 = Navig.Rot;
 CV.Rotations1 = Navig.RotChange;
}

//////////////////////////////////////////////////////////////////////////
//                CTgAn_PkPnEx                                           //
//   - класс, содержащий данные и методы, позволяющие                    //
//      1. Вычислять (пересчитывать) при приходе каждого пакета          //
//   данных PkTgAnState совокупности полиномов экстраполяции             //
//   значений угловых координат цели в корабельной системе координат Q,F //
//   значений угловых координат цели в земной системе координат A,E      //
//      2. С использованием данных полиномов выполнять локальную         //
//   экстраполяцию углов Q,F,A,E  на произвольный                        //
//   (в допустимых пределах) момент времени.                             //
//                                                                       //
///////////////////////////////////////////////////////////////////////////

constexpr double ExtrapolationPolynomials::Angles::dt_Crit;

ExtrapolationPolynomials::Angles::Angles() :
 Time(-1), Ready(), Count(), DP_Cur(3),
 Vc_T(Size), Vc_T_Rsl(Size), Vc_RslShip(Size), Vc_RslGround(Size), VcShip(Size), VcGround(Size),
 PnShip(), PnGround() {
}

Spherical2DShipCoords ExtrapolationPolynomials::Angles::ShipVal(double dt) const {
 dt = std::min(dt, dt_Crit);
 Spherical2DShipCoords _ {
  EvaluatePolynomial(PnShip, &Spherical2DShipCoords::Heading, dt, DP_Cur),
  EvaluatePolynomial(PnShip, &Spherical2DShipCoords::Elevation, dt, DP_Cur)
 };
 Normalize(_); // приведение угла в ин-л 0-2Pi // приведение угла в ин-л [-Pi,Pi]
 return _;
}

Spherical2DGroundCoords ExtrapolationPolynomials::Angles::GroundVal(double dt) const {
 dt = std::min(dt, dt_Crit);
 Spherical2DGroundCoords _ {
  EvaluatePolynomial(PnGround, &Spherical2DGroundCoords::Bearing, dt, DP_Cur),
  EvaluatePolynomial(PnGround, &Spherical2DGroundCoords::Elevation, dt, DP_Cur)
 };
 Normalize(_); // приведение угла в ин-л 0-2Pi // приведение угла в ин-л [-Pi,Pi]
 return _;
}

const Spherical2DShipCoords& ExtrapolationPolynomials::Angles::GetPnShip(size_t n) const {
 return PnShip[std::min<decltype(n)>(n, 4)];
}

const Spherical2DGroundCoords& ExtrapolationPolynomials::Angles::GetPnGround(size_t n) const {
 return PnGround[std::min<decltype(n)>(n, 4)];
}

void ExtrapolationPolynomials::Angles::Reset() {
 Count = 0;
 Ready = {};
 Time = NAN;
}

void ExtrapolationPolynomials::Angles::Evaluate(
 const Angles_Data &Angles, double Ds,
 const ExtrapolationPolynomials::NS& NS_ExtrapolationPolynomials,
 bool UseShipCoords) {

// std::cout << __func__ << "\t" << Time << "\t" << Angles.Time << std::endl;
 const double t0 = Angles.Time;
 if (fabs(t0 - Time) < 0.00001) return;
// if (Time > 0 && t0 - Time > 0.5) return;
 DP_Cur = DP_Nom;

 ++Count;

 // Сдвиг данных
 //rotate( CompAnCn_SC.begin(), CompAnCn_SC.begin()+1, CompAnCn_SC.end());
 Vc_T.erase(Vc_T.begin());
 VcShip.erase(VcShip.begin());
 VcGround.erase(VcGround.begin());

 // вычисление полных углов q,eps
 double yTv = 0, zTv = 0;
 const double dq1 = Angles.AutotrackingMismatch.HA + zTv / Ds; // приведение угла mu в систему ОЭП
 const double deps1 = Angles.AutotrackingMismatch.Elev + yTv / Ds; // приведение угла nu в систему ОЭП

 // std::cout << "dq1 " << dq1 << "\tdeps1 " << deps1 << "\n";
 const double a = tan(Angles.Ship.Elevation);
 const double deps2 = -0.5 * dq1 * dq1 * a; // поправка в угол места цели
 const double h = dq1 / (cos(Angles.Ship.Elevation) * (1.0 - deps1 * a));
 const double dq2 = h * (1.0 - h * h / 3.0); // поправка в курсовой угол цели
 // std::cout << "dq2 " << dq2 << "\tdeps2 " << deps2 << std::endl;

 double Q = Angles.Ship.Heading + dq2; // расчетный полный курсовой угол цели
 NormalizeRadians(Q); // приведение курсового угла в ин-л 0-2Pi

 TargetCoordinates EvaluatedCoords;
 EvaluatedCoords.Time = t0;
 EvaluatedCoords.Ship.Heading = Q;
 EvaluatedCoords.Ship.Elevation = Angles.Ship.Elevation + deps1 + deps2; // расчетный полный угол места цели
 if (dump)
  std::cout << Angles.Ship << "\t" << EvaluatedCoords.Ship << std::endl;

 const double dt = t0 - NS_ExtrapolationPolynomials.Time;
 Rotations Rot = NS_ExtrapolationPolynomials.GetRotations(dt);
 if (UseShipCoords)
  EvaluatedCoords.Ground = ConvertShipToGroundSpherical(EvaluatedCoords.Ship, Rot);
 else
  EvaluatedCoords.Ground = Angles.Ground;
// std::cout << __func__ <<
//  Angles.AutotrackingMismatch << "\t" <<
//  Angles.Ground << "\t" << EvaluatedCoords.Ground << std::endl;

 // Запись последних данных.
 Vc_T.push_back(EvaluatedCoords.Time);
 VcShip.push_back(EvaluatedCoords.Ship);
 VcGround.push_back(EvaluatedCoords.Ground);

 size_t TempSize = Size;
 TempSize = std::min<CountType>(Count, TempSize);
 if (TempSize <= DP_Nom)
  Ready = Angles::ReadyState::Unsufficient;
 else {
  if (TempSize < Size)
   Ready = Angles::ReadyState::Unsufficient;
  else
   Ready = Angles::ReadyState::Sufficient;
 }

 Time = t0;
 for (size_t i = 0; i < TempSize; i++) {
  Vc_T_Rsl[i] = Vc_T[i] - t0;
//  std::cout << Vc_T_Rsl[i] << "\t";
 }
// std::cout << std::endl;

 //приведение выборки Vc_Q к единому интервалу - Resulting1
 double min = VcShip[0].Heading;
 double max = VcShip[0].Heading;
 for (size_t i = 0; i < TempSize; i++) {
  Vc_RslShip[i].Heading = VcShip[i].Heading;
  min = std::min(Vc_RslShip[i].Heading, min);
  max = std::max(Vc_RslShip[i].Heading, max);
 }
 if ((max - min) > M_PI)
  for (size_t i = 0; i < TempSize; i++)
   if (Vc_RslShip[i].Heading > M_PI)
    Vc_RslShip[i].Heading -= MATH_CONST::M_2PI;

 // приведение выборки Vc_A к единому интервалу - Resulting1
 min = VcGround[0].Bearing;
 max = VcGround[0].Bearing;
 for (size_t i = 0; i < TempSize; i++) {
  Vc_RslGround[i].Bearing = VcGround[i].Bearing;
  min = std::min(Vc_RslGround[i].Bearing, min);
  max = std::max(Vc_RslGround[i].Bearing, max);
 }
 if ((max - min) > M_PI)
  for (size_t i = 0; i < TempSize; i++)
   if (Vc_RslGround[i].Bearing > M_PI)
    Vc_RslGround[i].Bearing -= MATH_CONST::M_2PI;

 // Вычисление коэффициентов полиномa по последним NmSmSC_r значениям.
 if (Ready == Angles::ReadyState::Unsufficient) {
  PnShip[0] = EvaluatedCoords.Ship;
  PnGround[0] = EvaluatedCoords.Ground;
  DP_Cur = 0;
  return;
 }

 InverseMatrix IM(DP_Nom + 1, &Vc_T_Rsl[0], TempSize);
 EvaluateOrdinalLeastSquaresPolynomial(&Vc_T_Rsl[0], &Spherical2DShipCoords::Heading, &Vc_RslShip[0], TempSize, IM, PnShip);
 EvaluateOrdinalLeastSquaresPolynomial(&Vc_T_Rsl[0], &Spherical2DShipCoords::Elevation, &VcShip[0], TempSize, IM, PnShip);
 EvaluateOrdinalLeastSquaresPolynomial(&Vc_T_Rsl[0], &Spherical2DGroundCoords::Bearing, &Vc_RslGround[0], TempSize, IM, PnGround);
 EvaluateOrdinalLeastSquaresPolynomial(&Vc_T_Rsl[0], &Spherical2DGroundCoords::Elevation, &VcGround[0], TempSize, IM, PnGround);
}

/////////////////////////////////////////////////////////////////////////////////
//                                                                             //
//  Класс, обеспечивающий экстраполяцию дальности цели, по результатам         //
//  обработки данных ЛД, поступивших от КНС                                    //
//                                                                             //
/////////////////////////////////////////////////////////////////////////////////

ExtrapolationPolynomials::Distance::Distance() : Time(-1.0), OK(), Pn() {}

double ExtrapolationPolynomials::Distance::Evaluate(double dt) const { // count value Ds for time t+dt
 return EvaluatePolynomial(Pn, dt, 2);
}

void ExtrapolationPolynomials::Distance::Assign(const DistanceData& PkDt) {
 OK = true;
 Time = PkDt.Time;
 Pn[0] = PkDt.Distance;
 Pn[1] = PkDt.Speed;
 Pn[2] = 0.5 * PkDt.Acceleration;
}
