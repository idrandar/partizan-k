/*
 * FRAM.hh
 *
 *  Created on: 16.05.2017
 *      Author: Drandar
 */

#ifndef FRAM_HH_
#define FRAM_HH_

// FRAM control class
// (C) I.Drandar, 2017, all rights reserved
// (C) I.Drandar, for C++1x, all rights reserved

#include "common/ExchangeModule.h"
#include <math.h>

struct FRAM {

 struct Memory; // forward

 union LowLevelBus { // packed FRAM record
  struct {
   char Gap : 6;
   bool Write : 1; // 0 - read, 1 - Write
  } _;
  Bits7Shared<unsigned int, 23> Bus; // Address & Data
  void Assign(const Memory& _);
 };

 struct Layout { // layout of FRAM struct in memory
  unsigned char Data;
  unsigned char Address_Low;
  unsigned char Address_High;
  void SetAddress(unsigned short _) {
   Address_Low = LoByte(_);
   Address_High = HiByte(_);
  }
  unsigned int Address() const {
   return MakeInt(Address_Low, Address_High);
  }
 };

 struct Memory { // internal FRAM record
  bool Write; // 0 - read, 1 - Write
  Layout Bus;
  void Assign(const LowLevelBus& _);
 };


 struct InputData {
  Memory _;
 };
 struct InputPacket {
  LowLevelBus _;
  char Gap[2];
 };
 struct OutputData {
  Memory _;
 };
 struct OutputPacket {
  LowLevelBus _;
  char Gap[2];
 };
 struct StatusVector {};
 struct CommandVector {
  Spherical2DShipCoords Location;
 };

};

class ModuleFRAM : public ExchangeModuleImpl<FRAM> {
 static constexpr unsigned int FRAM_BASE = 0x7800;
 static constexpr unsigned int Heading_ADR = 0x60;
 static constexpr unsigned int Elevation_ADR = 0x61;
 static constexpr unsigned int Base(unsigned int Adr) { return FRAM_BASE + 4 * Adr; }
 static constexpr double Coeff = 0x20000000 / M_PI_2; // 341782637.7882
 Spherical2DShipCoordsType<int> LocationToWrite;
 Spherical2DShipCoordsType<int> LocationVerified;
 // read/write stuff
 const char* WriteCache;
 char* ReadCache;
 size_t Current; // pointer to current byte
 static const unsigned int LocationBase;
 static const size_t CACHE_SIZE;
 enum ACCESS_STAGE { READ, WRITE } stage;
 bool UpdateStarted;
 virtual void DecodePacket(bool);
 virtual void EncodePacket(bool);
 virtual void ProcessCommandVector();
 virtual void PrepareStatusVector(bool);
public:
 explicit ModuleFRAM(const ModuleInit&);
 void Act() override;
};

#endif /* FRAM_HH_ */
