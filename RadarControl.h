#ifndef RADARCONTROL_H
#define RADARCONTROL_H

// radar hardware control data
// (C) I.Drandar, 2019, all rights reserved
// Guidance and tracking controller - Radar

#include "common/ExchangeModule.h"

struct RadarControl {

 enum class PassiveInterferenceCompensating {
  Disabled, Auto, Manual
 };

 struct OutputData {
  bool TAGC; // temporary auto gain control
  bool RangeCalibrationInput;
  PassiveInterferenceCompensating PIC;
  bool CFAR;
  bool ManualIF;
  bool ManualUHF;
  double DetectionThreshold; // dB
  double ANI_Threshold; // dB
  double UHF_ControlThreshold; // dB
  double IF_ControlThreshold; // dB
  unsigned int SignalCode;
  double CalibrationRangeCorrection; // m
  double PassiveInterferenceCoherenceCoefficient; // γ
  double MinimumPassiveInterferenceThreshold; // β dB
 };


 struct OutputPacket {
  bool TAGC : 1; // temporary auto gain control
  bool RangeCalibrationInput : 1;
  unsigned char PIC : 2;
  bool CFAR : 1;
  bool ManualIF : 1;
  bool ManualUHF : 1;
  template <unsigned int s>
  using Threshold = Bits7<Upscale<unsigned int, s>, 7>;
  Threshold<4> DetectionThreshold; // dB
  Threshold<4> ANI_Threshold; // dB
  Threshold<2> UHF_ControlThreshold; // dB
  Threshold<2> IF_ControlThreshold; // dB
  union {
   struct {
    unsigned char : 4;
    unsigned char SignalCode : 3;
   };
   Bits7Shared<Upscale<int, 4>, 9> CalibrationRangeCorrection; // m
  };
  Threshold<128> PassiveInterferenceCoherenceCoefficient; // γ
  Threshold<4> MinimumPassiveInterferenceThreshold; // β dB
 };

 using InputData = OutputData;
 using InputPacket = OutputPacket;

 struct CommandVector {
  bool Go;
 };
 struct StatusVector {};
};

bool operator==(const RadarControl::OutputData&, const RadarControl::InputData&);
std::ostream& operator<<(std::ostream&, const RadarControl::OutputData&);

class ModuleRadarControl : public ExchangeModuleImpl<RadarControl> {
 const struct FakeData {
  const bool use;
  FakeData(const ModuleInit&, const QString&);
 } Fake;
 void DecodePacket(bool) override;
 void EncodePacket(bool) override;
 void PrepareStatusVector(bool) override;
public:
 ModuleRadarControl(const ModuleInit&);
 void Act() override;
};

#endif // RADARCONTROL_H
