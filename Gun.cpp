/*
 * CombatModule.cc
 *
 *  Created on: 12.03.2015
 *      Author: Drandar
 */
// Processing data of "GUN" channel
// (C) I.Drandar, 2015-2016, all rights reserved
// (C) I.Drandar, for Qt + C++1x, 2018-2020, all rights reserved

#include "Gun.h"
#include "DebugOut.h"
#include "common/CoordinatesConversion.h"
#include <iomanip>

std::ostream& operator<<(std::ostream& _, const struct _Gun::InputData::Faults& F) {
 return
   _ <<
    "exchange with gun: " << F.ExchangeGun << "\texchange with computer: " << F.ExchangeComp << "\t"
    "power supply: " << F.PowerSupply << "\treference voltage: " << F.URef;
}

ModuleGun::Constructive::Constructive(const ModuleInit& init, const QString& _) :
 Offset(init.GetConst(_, "offset")),
 HW_Offset(init.GetConst(_, "hardware offset")),
 AngleSpeed(init.GetConst(_, "angle speed")),
 NeutralPart(init.GetConst(_, "neutral-part")),
 Area(init.GetConst(_, "area")), AreaDisabled(init.GetConst(_, "area-disabled")),
 Leftmost(Area.size() > 0 ? Area.begin()->HA.min : 0),
 Rightmost(Area.size() > 0 ? Area.rbegin()->HA.max : 0),
 Bisector((Leftmost + Rightmost) / 2 - (Leftmost < Rightmost ? M_PI : 0)),
 Diff(std::abs(Leftmost - Bisector)),
 NeutralLeft(Bisector - Diff * NeutralPart), NeutralRight(Bisector + Diff * NeutralPart) {
 NormalizeRadians(NeutralLeft);
 NormalizeRadians(NeutralRight);
}

bool ModuleGun::Constructive::InArea(const Spherical2DShipCoords& _) const {
 if (AreaDisabled) {
  return true;
 } else {
  auto heading0_2pi = _.Heading;
  NormalizeRadians(heading0_2pi); // area sectors are in 0..2π range
  for (auto& sector : Area) {
   if (sector.HA.min <= heading0_2pi && sector.HA.max >= heading0_2pi &&
       sector.Elev.min <= _.Elevation && sector.Elev.max >= _.Elevation) {
    return true;
   }
  }
 }
 // appropriate sector is not found
 return false;
}

void ModuleGun::Constructive::EvaluateLimitsAndSetBounds(
 Spherical2DShipCoords& _, Spherical2DShipCoords& Prev,
 struct Types::StatusVector::Limits& l) const {
 std::cout << __func__ << "\t" << _ << std::endl;
 l = {}; // reset all limits
 if (AreaDisabled) {
  return; // don't use limits
 } else if (Area.empty()) {
  l = { true, true, true, true };
  return; // on empty area all limits are set
 } else {
  double Highest = M_PI_2;
  double Lowest = -M_PI_2;
  auto heading0_2pi = _.Heading;
  NormalizeRadians(heading0_2pi); // area sectors are in 0..2π range
  for (auto& sector : Area) { // scan by heading
   if (sector.HA.min <= heading0_2pi && sector.HA.max >= heading0_2pi) {
    // sector found
    Prev = _; // save the last location
    if (_.Elevation > sector.Elev.max) {
     l.HighElev = true;
     _.Elevation = sector.Elev.max; // move to the current upper limit
    }
    if (_.Elevation < sector.Elev.min) {
     l.LowElev = true;
     _.Elevation = sector.Elev.min; // move to the current lower limit
    }
    return;
   } else {
    // continue scan
    Highest = std::min(Highest, sector.Elev.max);
    Lowest = std::max(Lowest, sector.Elev.min);
   }
  }
  // no sector found
  auto toleft = Leftmost - _.Heading;
  auto toright = Rightmost - _.Heading;
  NormalizeRadiansSigned(toleft);
  NormalizeRadiansSigned(toright);
  auto left = std::abs(toleft) <= std::abs(toright);
  if (_.Elevation > Highest) {
   l.HighElev = true;
   _.Elevation = Highest;
   // move to the upper limit
  }
  if (_.Elevation < Lowest) {
   l.LowElev = true;
   _.Elevation = Lowest;
   // move to the lower limit
  }
  if (NeutralLeft <= heading0_2pi && heading0_2pi <= NeutralRight) {
   // don't move inside the neutral area
   _.Heading = Prev.Heading;
  } else if (left) {
   // move to the left limit
   _.Heading = Leftmost;
  } else {
   // move to the right limit
   _.Heading = Rightmost;
  }
 }
 // check if the gun stays on any limit
 auto ldiff = _.Heading - Leftmost;  // difference between left limit and current position
 auto rdiff = _.Heading - Rightmost; // difference between right limit and current position
 NormalizeRadiansSigned(ldiff);
 NormalizeRadiansSigned(rdiff);
 auto constexpr eps = 0.001;
 l.LeftHA = std::abs(ldiff) < eps;
 l.RightHA = std::abs(rdiff) < eps;
 Normalize(_);
 Prev = _; // save the last location
}

ModuleGun::Algorithm::Algorithm(const ModuleInit& init, const QString& s) :
 UseDiff(init.GetConst(s, "difference-use")),
 DiffLimit(init.GetConst(s, "difference-limit")) {
}

ModuleGun::FakeData::FakeData(const ModuleInit& init, const QString& s) :
 use(init.GetConst(s, "use")) {
}

ModuleGun::TestDump::TestDump(const ModuleInit& init, const QString& s) :
 Position(init.GetConst(s, "position")) {
}

ModuleGun::Control::Control(const ModuleInit& init, const QString& s) :
 UseShipCoords(init.GetConst(s, "use-ship-coords")), DesiredFinal(init.GetConst(s, "desired-final")) {
}

ModuleGun::ModuleGun(const ModuleInit& init) :
 BASE(init),
 TimeStep(1.0 / Frequency()), Algorithm(init, "algorithm"),
 Fake(init, "fake"), Constr(init, "constructive"),
 TestDump(init, "test-dump"), Control(init, "control") {
 Status.UseFinal = Control.DesiredFinal;
}

void ModuleGun::DecodePacket(bool dump) {
 Input.Faults.ExchangeGun = input.ExchangeGunFault;
 Input.Faults.ExchangeComp = input.ExchangeCompFault;
 Input.Faults.PowerSupply = input.PowerSupplyFault;
 Input.Faults.URef = input.URefFault;
 // testing stuff
 if (dump) {
  std::cout << "Dump of received " << Name << "::Input packet\n"
   "faults:\n" << Input.Faults << "\n";
 }
}

void ModuleGun::EncodePacket(bool dump) {
 if (Output.Mode == Types::gmWork) {
  output.Mode = 0;
  output.ModeNext = false;
 } else {
  output.Mode = 1;
  output.ModeNext = true;
 }
 output.Defence = true;
 output.SpeedNegative = true;
 output.AutoTimer = false;
 output.IsElevChannel = Output.IsElevChannel;
 output.Position = Output.Position;
 output.Offset = Constr.HW_Offset;
 output.AngleSpeed = Constr.AngleSpeed;
 // testing stuff
 if (dump)
  std::cout << "Dump of transmitted " << GetName() << "::Output packet\n"
   "elevation channel: " << Output.IsElevChannel << "\tmode: " << Output.Mode << "\t"
   "position (°):\t" << Output.Position << "\n"
   "constructive:\toffset: " << MATH_CONST::Rad2Deg(Constr.HW_Offset) << "°\t"
   "angle speed correction: " << Constr.AngleSpeed << "\n";
}

void ModuleGun::Act() {
 CommitReception();
 Transmit();
} // end ccRun

double ModuleGun::NextTime(double t) const {
 if ((fabs(Command.TimeOffset()) < TimeOffsetMax) &&
  ((t - Command.EvaluatedTime < time_step / 2.0) || Command.data.Mode == Types::gmWork)) {
  return Command.EvaluatedTime + time_step;
 } else {
  return t;
 }
}

void ModuleGun::EvaluateUsingGroundCoords() {
 bool _HaveSolution = Command.HaveSolution;

 double TimeOffset = Command.TimeOffset();
// dump_if(TestDump.Position) <<
//  /TimeOffset << "\t=\t" << Command.EvaluatedTime << "\t-\t" << Command.Extrapolation.ShotTime << std::endl;
 if (TimeOffset > TimeOffsetMax) {
  _HaveSolution = false;
  TimeOffset = TimeOffsetMax;
 }

 const Spherical2DGroundCoords CollisionCoords = Command.Extrapolation.GroundVal(TimeOffset);

 //checking
 Spherical2DGroundCoords FilteredCoords = Spherical2DGroundCoords();
 if (fabs(CollisionCoords.Bearing) > _LIMIT) {
  FilteredCoords.Bearing = 0.0;
  _HaveSolution = false;
 } else {
  FilteredCoords.Bearing = CollisionCoords.Bearing;
  NormalizeRadians(FilteredCoords.Bearing);
 }

 if (fabs(CollisionCoords.Elevation) > _LIMIT) {
  FilteredCoords.Elevation = 0.0;
  _HaveSolution = false;
 } else
  FilteredCoords.Elevation = CollisionCoords.Elevation;

 //if ( VCP.HaveSolution )  VCP.A_Corr1 = VCP.A_Corr1 - BComp.CComp.OutputData.GnSummCorr_Az[nCM];      // azimut    with popravra
 if (_HaveSolution) {
  FilteredCoords.Bearing -= Command.Miss.HA; // azimut    with popravra
  FilteredCoords.Elevation -= Command.Miss.Elev; // elevation with popravka
 }
 NormalizeRadians(FilteredCoords.Bearing);

 //---------------------------------------------------------------------------------------------------------

 GroundPreliminary = FilteredCoords;
 ShipPreliminary = ConvertGroundToShipSpherical(GroundPreliminary, Command.Rot);
 ShipFinal.Heading = ShipPreliminary.Heading + Command.PointingAngleMismatch.HA;
 ShipFinal.Elevation = ShipPreliminary.Elevation + Command.PointingAngleMismatch.Elev;
 Normalize(ShipFinal);
 GroundFinal = ConvertShipToGroundSpherical(ShipFinal, Command.Rot);
 if (Algorithm.UseDiff) {
  Spherical2DShipCoords Diff {
   ShipFinal.Heading + Constr.Offset.Heading - Command.Ship.Heading,
   fabs(ShipFinal.Elevation + Constr.Offset.Elevation - Command.Ship.Elevation)
  };
  NormalizeRadiansSigned(Diff.Heading);
  Diff.Heading = fabs(Diff.Heading);
  if (Diff.Heading > Algorithm.DiffLimit.HA || Diff.Elevation > Algorithm.DiffLimit.Elev)
   _HaveSolution = false;
 }
 Status.HaveSolution =
  _HaveSolution && Constr.InArea(ShipFinal) && (Command.data.Mode != Types::gmTesting);
}

void ModuleGun::EvaluateUsingShipCoords() {
 bool _HaveSolution = Command.HaveSolution;

 double TimeOffset = Command.TimeOffset();
 if (TimeOffset > TimeOffsetMax) {
  _HaveSolution = false;
  TimeOffset = TimeOffsetMax;
 }
// dump_if(TestDump.Position && Command.CollisionTaskPerformed > 0) <<
//  " time offset: " << TimeOffset << " = " << Command.EvaluatedTime << "-" <<
//  Command.Extrapolation.ShotTime << " s" << std::endl;

 const Spherical2DShipCoords ShipCollisionCoords = Command.Extrapolation.ShipVal(TimeOffset);

 //checking
 Spherical2DShipCoords ShipFilteredCoords;
 if (fabs(ShipCollisionCoords.Heading) > _LIMIT) {
  ShipFilteredCoords.Heading = 0.0;
  _HaveSolution = false;
 } else {
  ShipFilteredCoords.Heading = ShipCollisionCoords.Heading;
  NormalizeRadians(ShipFilteredCoords.Heading);
 }

 if (fabs(ShipCollisionCoords.Elevation) > _LIMIT) {
  ShipFilteredCoords.Elevation = 0.0;
  _HaveSolution = false;
 } else
  ShipFilteredCoords.Elevation = ShipCollisionCoords.Elevation;

 if (_HaveSolution) {
  ShipFilteredCoords.Heading -= Command.Miss.HA; // azimut  with popravra
  ShipFilteredCoords.Elevation -= Command.Miss.Elev; // elevation with popravka
 }
 Normalize(ShipFilteredCoords);

 Spherical2DShipCoords Corr2 = ShipFilteredCoords;
 NormalizeRadians(Corr2.Heading);

 //--------------------------------------- ultimite verification of GS ----------------------------------------------------------

 ShipPreliminary = Corr2;
 Spherical2DShipCoords Ship_WithCorrections {
  ShipPreliminary.Heading + Command.PointingAngleMismatch.HA,
  ShipPreliminary.Elevation + Command.PointingAngleMismatch.Elev
 };
 Normalize(Ship_WithCorrections);
 GroundPreliminary = ConvertShipToGroundSpherical(ShipPreliminary, Command.Rot);
 const Spherical2DGroundCoords Ground_WithCorrections =
  ConvertShipToGroundSpherical(Ship_WithCorrections, Command.Rot);

 GroundFinal = Ground_WithCorrections;
 ShipFinal = Ship_WithCorrections;

 Status.HaveSolution =
  _HaveSolution && Constr.InArea(ShipFinal) && (Command.data.Mode != Types::gmTesting);
}

void ModuleGun::ClearStatus() {
 ShipPreliminary = Constr.Offset;
 GroundPreliminary = ConvertShipToGroundSpherical(ShipPreliminary, Command.Rot);
 Status.HaveSolution = false;

 Status.EvaluatedTime = Command.EvaluatedTime;

 ShipFinal = ShipPreliminary;
 GroundFinal = GroundPreliminary;
}

void ModuleGun::PrepareStatusVector(bool) {
 if (Fake.use) {
  Status.NewInfo = true;
  Status.ExchangeFault = false;
  Status.data.Faults.ExchangeComp = false;
 }
 if (Control.UseShipCoords) {
  Status.Preliminary.HA = ShipPreliminary.Heading;
  Status.Preliminary.Elev = ShipPreliminary.Elevation;
  Status.Final.HA = ShipFinal.Heading;
  Status.Final.Elev = ShipFinal.Elevation;
 } else {
  Status.Preliminary.HA = GroundPreliminary.Bearing;
  Status.Preliminary.Elev = GroundPreliminary.Elevation;
  Status.Final.HA = GroundFinal.Bearing;
  Status.Final.Elev = GroundFinal.Elevation;
 }
}

void ModuleGun::ProcessCommandVector() {
 dump_if(TestDump.Position && Command.CollisionTaskPerformed > 0) <<
  "\tPosition:\t" << std::fixed << std::setprecision(5) <<
  MATH_CONST::Rad2Deg(Command.data.Position) << std::endl;
}
