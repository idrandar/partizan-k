#ifndef TRENDSWINDOW_H
#define TRENDSWINDOW_H

#include <QDialog>
#include <QTimer>
#include "ui_trendswindow.h"
#include <memory>

namespace Ui {
class TrendsWindow;
}

class TW;

class TrendsWindow : public QDialog
{
 Q_OBJECT

private slots:
 void Update();
 void on_btnSuspendResume_toggled(bool);

public:
 explicit TrendsWindow(QWidget *parent = 0);
 ~TrendsWindow();

private:
 Ui::TrendsWindow *ui;
 QTimer tmr;
 std::unique_ptr<TW> itf;
};

#endif // TRENDSWINDOW_H
