// radar SS mode
// (C) I.Drandar, 2019, all rights reserved
// Guidance and tracking controller - Radar

#include "RadarSS.h"
#include "DebugOut.h"

bool operator==(const RadarSS::OutputData& l, const RadarSS::InputData& r) {
 return
  l.EmissionPermitted == r.EmissionPermitted && l.Antenna == r.Antenna &&
  l.DistanceBegin == r.DistanceBegin && l.DistanceEnd == r.DistanceEnd &&
  l.LFM_SlopeNegative == r.LFM_SlopeNegative &&
  l.DS == r.DS && l.SignalCode == r.SignalCode && l.OFT == r.OFT &&
  l.FrequencyCode == r.FrequencyCode && l.RPA == r.RPA &&
  l.RepetitionPeriodCode == r.RepetitionPeriodCode &&
  l.MTS1 == r.MTS1 && l.MTS2 == r.MTS2 && l.FFT == r.FFT &&
  l.ActiveNoiseAnalysis == r.ActiveNoiseAnalysis &&
  l.OwnMovementCompensation == r.OwnMovementCompensation &&
  l.RangeCalibration == r.RangeCalibration && l.PulseCount == r.PulseCount;
}

std::ostream& operator<<(std::ostream& _, const RadarSS::InputData& d) {
 _ <<
  "emission permission: " << d.EmissionPermitted << "\t"
  "emission permission phisical: " << d.EmissionPermittedPhysical << "\t"
  "antenna id on: " << d.Antenna << "\n"
  "distance begin: " << d.DistanceBegin << " m\tend: " << d.DistanceEnd << " m\t"
  "LFM slope is negative: " << d.LFM_SlopeNegative << "\n"
  "distance scale: " << enum2str(d.DS) << "\tsignal code #" << d.SignalCode << "\n"
  "operating frequency tuning: " << enum2str(d.OFT) << "\t"
  "frequency code #" << d.FrequencyCode << "\n"
  "repetition period adjustment: " << enum2str(d.RPA) << "\t"
  "repetition period code #" << d.RepetitionPeriodCode << "\n"
  "moving target selection 1: " << enum2str(d.MTS1) << "\t"
  "moving target selection 2: " << enum2str(d.MTS2) << "\n"
  "fast Fourier transform: " << enum2str(d.FFT) << "\t"
  "active noise analysis enabled: " << d.ActiveNoiseAnalysis << "\n"
  "own movement compensation enabled: " << d.OwnMovementCompensation << "\t"
  "range calibration enabled: " << d.RangeCalibration << "\tpulse count: " << d.PulseCount;
 return _;
}

std::ostream& operator<<(std::ostream& _, const RadarSS::OutputData& d) {
 _ <<
  "emission permission: " << d.EmissionPermitted << "\t"
  "antenna id on: " << d.Antenna << "\n"
  "distance begin: " << d.DistanceBegin << " m\tend: " << d.DistanceEnd << " m\t"
  "LFM slope is negative: " << d.LFM_SlopeNegative << "\n"
  "distance scale: " << enum2str(d.DS) << "\tsignal code #" << d.SignalCode << "\n"
  "operating frequency tuning: " << enum2str(d.OFT) << "\t"
  "frequency code #" << d.FrequencyCode << "\n"
  "repetition period adjustment: " << enum2str(d.RPA) << "\t"
  "repetition period code #" << d.RepetitionPeriodCode << "\n"
  "moving target selection 1: " << enum2str(d.MTS1) << "\t"
  "moving target selection 2: " << enum2str(d.MTS2) << "\n"
  "fast Fourier transform: " << enum2str(d.FFT) << "\t"
  "active noise analysis enabled: " << d.ActiveNoiseAnalysis << "\n"
  "own movement compensation enabled: " << d.OwnMovementCompensation << "\t"
  "range calibration enabled: " << d.RangeCalibration << "\tpulse count: " << d.PulseCount;
 return _;
}

ModuleRadarSS::FakeData::FakeData(const ModuleInit& init, const QString& s) :
 use(init.GetConst(s, "use")) {
}

ModuleRadarSS::ModuleRadarSS(const ModuleInit& init) :
 BASE(init), Fake(init, "fake") {
}

void ModuleRadarSS::Act() {
 if (Command.Go) {
  Transmit();
 }
 CommitReception();
}

void ModuleRadarSS::EncodePacket(bool dump) {
 output.EmissionPermitted = Output.EmissionPermitted;
 output.Antenna = Output.Antenna; // 0 - equivalent, 1 - antenna
 output.DistanceBegin = Output.DistanceBegin;
 output.DistanceEnd = Output.DistanceEnd;
 output.LFM_SlopeNegative = Output.LFM_SlopeNegative;
 output.DS = static_cast<unsigned char>(Output.DS);
 output.SignalCode = Output.SignalCode;
 output._.OFT = static_cast<unsigned char>(Output.OFT);
 output._.FrequencyCode = Output.FrequencyCode;
 output.__.RPA = static_cast<unsigned char>(Output.RPA);
 output.__.RepetitionPeriodCode = Output.RepetitionPeriodCode;
 output.MTS1 = static_cast<unsigned char>(Output.MTS1);
 output.___.MTS2 = static_cast<unsigned char>(Output.MTS2);
 output.FFT = static_cast<unsigned char>(Output.FFT);
 output.ActiveNoiseAnalysis = Output.ActiveNoiseAnalysis;
 output.OwnMovementCompensation = Output.OwnMovementCompensation;
 output.___.RangeCalibration = Output.RangeCalibration; // 0 - disabled, 1 - enabled
 output.___.PulseCount = Output.PulseCount;
 if (dump) {
  std::cout <<
   "Dump of transmitted " << GetName() << "::Output packet\n" << Output << "\n";
 }
}

void ModuleRadarSS::DecodePacket(bool dump) {
 Input.EmissionPermitted = input.EmissionPermitted;
 Input.EmissionPermittedPhysical = input.EmissionPermittedPhysical;
 Input.Antenna = input.Antenna; // 0 - equivalent, 1 - antenna
 Input.DistanceBegin = input.DistanceBegin;
 Input.DistanceEnd = input.DistanceEnd;
 Input.LFM_SlopeNegative = input.LFM_SlopeNegative;
 Input.DS = static_cast<Radar::DistanceScale>(input.DS);
 Input.SignalCode = input.SignalCode;
 Input.OFT = static_cast<Radar::OperatingFrequencyTuning>(input._.OFT);
 Input.FrequencyCode = input._.FrequencyCode;
 Input.RPA = static_cast<Radar::RepetitionPeriodAdjustment>(input.__.RPA);
 Input.RepetitionPeriodCode = input.__.RepetitionPeriodCode;
 Input.MTS1 = static_cast<Radar::MovingTargetSelection>(input.MTS1);
 Input.MTS2 = static_cast<Radar::MovingTargetSelection>(input.___.MTS2);
 Input.FFT = static_cast<Radar::FastFourierTransform>(input.FFT);
 Input.ActiveNoiseAnalysis = input.ActiveNoiseAnalysis;
 Input.OwnMovementCompensation = input.OwnMovementCompensation;
 Input.RangeCalibration = input.___.RangeCalibration; // 0 - disabled, 1 - enabled
 Input.PulseCount = input.___.PulseCount;
 if (dump) {
  std::cout <<
   "Dump of received " << GetName() << "::Input packet\n" << Input << "\n";
 }
}

void ModuleRadarSS::PrepareStatusVector(bool) {
 if (Fake.use) {
  Status.NewInfo = true;
  Status.ExchangeFault = false;
  auto& s = Status.data;
  const auto& c = Command.data;
  s.EmissionPermitted = c.EmissionPermitted;
  s.EmissionPermittedPhysical = c.EmissionPermitted;
  s.Antenna = c.Antenna; // 0 - equivalent, 1 - antenna
  s.DistanceBegin = c.DistanceBegin;
  s.DistanceEnd = c.DistanceEnd;
  s.LFM_SlopeNegative = c.LFM_SlopeNegative;
  s.DS = c.DS;
  s.SignalCode = c.SignalCode;
  s.OFT = c.OFT;
  s.FrequencyCode = c.FrequencyCode;
  s.RPA = c.RPA;
  s.RepetitionPeriodCode = c.RepetitionPeriodCode;
  s.MTS1 = c.MTS1;
  s.MTS2 = c.MTS2;
  s.FFT = c.FFT;
  s.ActiveNoiseAnalysis = c.ActiveNoiseAnalysis;
  s.OwnMovementCompensation = c.OwnMovementCompensation;
  s.RangeCalibration = c.RangeCalibration; // 0 - disabled, 1 - enabled
  s.PulseCount = c.PulseCount;
 }
}
